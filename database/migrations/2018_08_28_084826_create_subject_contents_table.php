<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subject_contents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_en');
            $table->string('name_ar'); 
            $table->string('bref_en');
            $table->string('bref_ar');
            $table->text('content_en');
            $table->text('content_ar');
            $table->integer('course_id');
            $table->boolean('trash')->default(0);
            $table->integer('update_by');
            $table->integer('create_by'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subject_contents');
    }
}
