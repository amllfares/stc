<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subject_id');
            $table->string('price');
            $table->string('offer');
            $table->string('name_ar');
            $table->string('name_en');
            $table->string('exam_link');
            $table->integer('branch_id');
            $table->integer('exam_id');
            $table->integer('number_hours');
            $table->integer('min_candidates')->default(3);
            $table->integer('max_candidates')->default(10);
            $table->boolean('trash')->default(0);
            $table->boolean('have_exam')->default(0);
            $table->integer('update_by');
            $table->integer('create_by');
            $table->timestamps(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
