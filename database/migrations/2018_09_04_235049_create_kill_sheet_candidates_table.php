<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKillSheetCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kill_sheet_candidates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kill_sheet_id');
            $table->integer('user_id');
            // $table->integer('user_id');
            $table->string('value');
            $table->boolean('trash')->default(0);
            $table->integer('update_by');
            $table->integer('create_by'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kill_sheet_candidates');
    }
}
