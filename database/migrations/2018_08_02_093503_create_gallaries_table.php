<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGallariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gallaries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('related_id');
            $table->string('name_en');
            $table->string('name_ar');
            $table->string('bref_ar');
            $table->string('bref_en');
            $table->string('table');
            $table->string('file');
            $table->string('banner');
            $table->boolean('trash')->default(0);
            $table->integer('update_by');
            $table->integer('create_by'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gallaries');
    }
}
