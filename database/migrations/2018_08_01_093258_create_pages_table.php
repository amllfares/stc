<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_en')->nullable();
            $table->string('name_ar')->nullable();
            $table->string('slug_en')->nullable();
            $table->string('slug_ar')->nullable();
            $table->string('bref_en')->nullable();
            $table->string('bref_ar')->nullable();
            $table->text('description_en')->nullable();
            $table->text('description_ar')->nullable();
            $table->integer('order')->nullable();
            $table->integer('sub_of')->default(0);
            $table->boolean('is_main_page')->default(0);
            $table->boolean('nav')->default(0);
            $table->boolean('category')->default(0);
            $table->boolean('delete')->default(0);
            $table->boolean('can_not_delete' )->default(0);
            $table->boolean('accordion')->default(0);
            $table->boolean('gallery')->default(0);
            $table->boolean('selector')->default(0);
            $table->string('photo');
            $table->string('banner');
            $table->boolean('trash')->default(0);
            $table->integer('update_by');
            $table->integer('create_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
