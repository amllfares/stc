<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidateAttendancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidate_attendances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('candidate_id');
            $table->integer('course_date_id');
            $table->enum('status', ['0', '1']);
            $table->boolean('trash')->default(0);
            $table->integer('update_by');
            $table->integer('create_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidate_attendances');
    }
}
