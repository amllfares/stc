<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedback', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('feedback');
            $table->string('email');
            $table->string('mobile');
            $table->integer('user_id');
            $table->string('type');
            $table->integer('course_id')->default(0);
            $table->integer('instructor_rate')->default(0);
            $table->integer('schedule_id')->default(0);
            $table->integer('rate')->default(0);
            $table->boolean('seen')->default(0);
            $table->boolean('trash')->default(0);
            $table->integer('update_by');
            $table->integer('create_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedback');
    }
}
