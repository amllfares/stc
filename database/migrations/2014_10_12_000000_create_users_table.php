<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->boolean('trash')->default(0);
            $table->boolean('instructor')->default(0);
            $table->boolean('admin')->default(0);
            $table->boolean('employe')->default(0);
            $table->boolean('candidate')->default(0);
            $table->integer('company_id')->default(0);
            $table->string('lang')->default('en');
            $table->string('phone')->default('NULL');
            $table->string('address')->default('NULL');
            $table->string('country')->default('NULL');
            $table->string('latitude')->default('NULL');
            $table->string('longitude')->default('NULL');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
