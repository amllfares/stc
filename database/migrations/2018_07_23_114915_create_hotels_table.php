<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_en');
            $table->string('name_ar');
            $table->string('email');
            $table->string('contact_num');
            $table->string('contact_title');
            $table->string('address_en');
            $table->string('address_ar');
            $table->string('currency');
            $table->text('extra');
            $table->string('media');
            $table->string('price_per_day');
            $table->boolean('trash')->default(0);
            $table->integer('update_by');
            $table->integer('create_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotels');
    }
}
