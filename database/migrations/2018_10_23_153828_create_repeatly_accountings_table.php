<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRepeatlyAccountingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repeatly_accountings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('amount');
            $table->integer('category_id');
            $table->integer('foreign_id');
            $table->text('extra');
            $table->boolean('confirm')->default(0);
            $table->integer('user_id');
            $table->integer('month');
            $table->text('labels');
            $table->string('date');
            $table->boolean('trash')->default(0);
            $table->integer('update_by');
            $table->integer('create_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('repeatly_accountings');
    }
}
