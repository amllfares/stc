<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatirialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matirials', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_id');
            $table->string('title_ar');
            $table->string('title_en');
            $table->string('matirial');
            $table->boolean('download')->default(0);
            $table->boolean('trash')->default(0);
            $table->integer('update_by');
            $table->integer('create_by'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matirials');
    }
}
