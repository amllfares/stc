<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_en');
            $table->string('name_ar');
            $table->string('image');
            $table->text('sub_title_en');
            $table->text('sub_title_ar');
            $table->text('descrption_en1');
            $table->text('descrption_ar1');
            $table->text('descrption_en2');
            $table->text('descrption_ar2');
            $table->text('extra');
            $table->string('media');
            $table->boolean('trash')->default(0);
            $table->integer('category_id');
            $table->integer('update_by');
            $table->integer('create_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subjects');
    }
}
