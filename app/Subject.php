<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    function subCategory()
    {
        return $this->hasOne('App\SubjectCategory','id','category_id')->where('trash', '0');
    }

    function course()
    {
        return $this->hasOne('App\Course')->where('trash', '0');
    }
}
