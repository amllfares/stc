<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidate_attendance extends Model
{
    function user()
    {
        return $this->hasOne('App\User','id','candidate_id')->where('trash', '0');
    }

    function course_date()
    {
        return $this->hasOne('App\CourseDates','id','course_date_id')->where('trash', '0');
    }
}
