<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company_reservation extends Model
{
     function user()
    {
        return $this->hasOne('App\User','id','user_id')->where('trash', '0');
    }

    function course_schedule()
    {
        return $this->hasOne('App\Course_schedule','id','course_id')->where('trash', '0');
    }


    function course()
    {
        return $this->hasOne('App\Course','id','course_id')->where('trash', '0');
    }

    function company()
    {
        return $this->hasOne('App\Company_account','id','company_id')->where('trash', '0');
    }
}
