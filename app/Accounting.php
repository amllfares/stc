<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accounting extends Model
{
    function user()
    {
        return $this->hasOne('App\User','id','user_id')->where('trash', '0');
    }

    function users()
    {
        return $this->hasOne('App\User','id','foreign_id')->where('trash', '0');
    }
}
