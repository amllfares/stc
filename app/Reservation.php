<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    function user()
    {
        return $this->hasOne('App\User','id','user_id')->where('trash', '0');
    }

    function course_schedule()
    {
        return $this->hasOne('App\Course_schedule','id','course_id')->where('trash', '0');
    }

    function hotel()
    {
        return $this->hasOne('App\Hotel','id','hotel_id')->where('trash', '0');
    }

    function resturant()
    {
        return $this->hasOne('App\Restaurant','id','resturant_id');
    }

    function course()
    {
        return $this->hasOne('App\Course','id','course_id')->where('trash', '0');
    }
}
