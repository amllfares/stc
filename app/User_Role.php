<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_Role extends Model
{
   function role()
    {
        return $this->hasOne('App\Role','id','role_id');
    }
}
