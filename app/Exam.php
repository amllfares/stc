<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
     function course()
    {
        return $this->hasOne('App\Course','id','course_id')->where('trash', '0');
    }

    function subject()
    {
        return $this->hasOne('App\Subject','id','subject_id')->where('trash', '0');
    }
}
