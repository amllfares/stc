<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role_Permission extends Model
{
    function role()
    {
        return $this->hasOne('App\Role','id','role_id');
    }

    function section()
    {
        return $this->hasOne('App\Section','id','section_id');
    }

}
