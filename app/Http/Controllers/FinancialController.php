<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Financial;

class FinancialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['financials'] = Financial::orderBy('id', 'desc')->paginate(10);
        return view('back.financial.view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // var_dump($id);die();
       $Financial = Financial::find($id);
       $Financial->delete();
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }


    public function trash(Request $request)
    {
      $id=$request->segment(4);
      $trash =$request->segment(5);
      if ($trash==1) {
         Financial::where('id',$id)->update(array( 'trash' => 0, ));
         Session::flash('untrash', ' UNTrashed Successfully!' );
         return redirect()->back();
        
      }
      else
      {
         Financial::where('id',$id)->update(array( 'trash' => 1, ));
         Session::flash('warning', ' Trashed Successfully!' );
         return redirect()->back();
      }

    }


    public function delete()
    {
       return view('back.confirm_delete');
    }


    public function status(Request $request)
    {
      $id=$request->segment(4);
      $status =$request->segment(5);
      if ($status==0) {
         Financial::where('id',$id)->update(array( 'status' => 0, ));
         Session::flash('warning', ' Pending Successfully!' );
         return redirect()->back();
        
      }
      elseif($status==1)
      {
         Financial::where('id',$id)->update(array( 'status' => 1, ));
         Session::flash('success', ' Done Successfully!' );
         return redirect()->back();
      }
      else
      {
         Financial::where('id',$id)->update(array( 'status' => 2, ));
         Session::flash('danger', ' Canceld Successfully!' );
         return redirect()->back();
      }

    }
}
