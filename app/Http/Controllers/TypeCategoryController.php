<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TypeCategory;
use Session;
use App\Http\Controllers\ActivitesController;
class TypeCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['types'] = TypeCategory::orderBy('ID', 'desc')->paginate(10);
        return view('back.types.view',$data);
    }

    /** 
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back.types.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ActivitesController $ActivitesController)
    {
        $type                           = new TypeCategory;
        $type->name_ar                  = $request->name_ar;
        $type->name_en                  = $request->name_en;
        $type->create_by                = auth()->user()->id;
        $type->update_by                = auth()->user()->id;

        if ($request->hasFile('image'))
        {
            $image = $request->file('image');
            $name ='/assets/back/upload/'.time(). $image->getClientOriginalName();
            $destinationPath = public_path('/assets/back/upload/');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $type->image = $name;
        }

        $type->save();
        $ActivitesController->store(auth()->user()->id ,'Created','TypeCategory',$type->id);
        return redirect()->route('types.create')->with('success',  $request->name ." Course Created Successfully!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['type']= TypeCategory::find($id);
        return view('back.types.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ActivitesController $ActivitesController)
    {
        $type                           = TypeCategory::find($id);
        $type->name_en                  = $request->name_en;
        $type->name_ar                  = $request->name_ar;
        $type->create_by                = auth()->user()->id;
        $type->update_by                = auth()->user()->id;

        if ($request->hasFile('image'))
        {
            $image = $request->file('image');
            $name ='/assets/back/upload/'.time(). $image->getClientOriginalName();
            $destinationPath = public_path('/assets/back/upload/');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $type->image = $name;
        }

        $type->save();
        $ActivitesController->store(auth()->user()->id ,'Updated','TypeCategory',$type->id);
        $edited_area = $type;
        Session::flash('info',   $type->name_en .' Updated Successfully!' );
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,ActivitesController $ActivitesController)
    {
       $TypeCategory = TypeCategory::find($id);
       $TypeCategory->delete();
       $ActivitesController->store(auth()->user()->id ,'Destroy','TypeCategory',$id);
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }


    public function trash(Request $request,ActivitesController $ActivitesController)
    {
      $id=$request->segment(4);
      $trash =$request->segment(5);
      if ($trash==1) {
         TypeCategory::where('id',$id)->update(array( 'trash' => 0, ));
         $ActivitesController->store(auth()->user()->id ,'UNTrashed','TypeCategory',$id);
         Session::flash('untrash', ' UNTrashed Successfully!' );
         return redirect()->back();
        
      }
      else
      {
         TypeCategory::where('id',$id)->update(array( 'trash' => 1, ));
         $ActivitesController->store(auth()->user()->id ,'Trashed','TypeCategory',$id);
         Session::flash('warning', ' Trashed Successfully!' );
         return redirect()->back();
      }

    }


    public function delete()
    {
       return view('back.confirm_delete');
    }
}
