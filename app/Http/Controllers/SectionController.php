<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Section;
use Session;
use App\Http\Controllers\ActivitesController;
class SectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['sections'] = Section::orderBy('id', 'desc')->paginate(10);
        return view('back.sections.view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['sectionss'] = Section::where('trash', '0')->where('sub_of','0')->get();
        return view('back.sections.add',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ActivitesController $ActivitesController)
    {
        $Section                           = new Section;
        $Section->name                     = $request->name;
        $Section->sub_of                   = $request->sub_of;
        $Section->icon                     = $request->icon;
        $Section->link                     = $request->link;
        $Section->create_by                = auth()->user()->id;
        $Section->update_by                = auth()->user()->id;
        $Section->save();
        $ActivitesController->store(auth()->user()->id ,'Created','Section',$Section->id);
       
        return redirect()->route('sections.create')->with('success',  $request->name ." Created Successfully!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['section']= Section::find($id);
        $data['sectionss'] = Section::where('trash', '0')->where('sub_of','0')->get();
        return view('back.sections.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ActivitesController $ActivitesController)
    {
        $Section                           = Section::find($id);
        $Section->name                     = $request->name;
        $Section->sub_of                   = $request->sub_of;
        $Section->icon                     = $request->icon;
        $Section->link                     = $request->link;
        $Section->create_by                = auth()->user()->id;
        $Section->update_by                = auth()->user()->id;
        $Section->save();
        $ActivitesController->store(auth()->user()->id ,'Updated','Section',$id);

        $edited_area = $Section;
        Session::flash('info',  $Section->name .' Updated Successfully!' );
        return redirect('/admin/sections/'.$edited_area->id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id,ActivitesController $ActivitesController)
    {
       $Section = Section::find($id);
       $Section->delete();
       $ActivitesController->store(auth()->user()->id ,'Destroy','Section',$id);
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }



    public function trash(Request $request,ActivitesController $ActivitesController)
    {
      $id=$request->segment(4);
      $trash =$request->segment(5);
      if ($trash==1) {
         Section::where('id',$id)->update(array( 'trash' => 0, ));
         $ActivitesController->store(auth()->user()->id ,'UNTrashed','Section',$id);
         Session::flash('untrash', ' UNTrashed Successfully!' );
         return redirect()->back();
        
      }
      else
      {
         Section::where('id',$id)->update(array( 'trash' => 1, ));
         $ActivitesController->store(auth()->user()->id ,'Trashed','Section',$id);
         Session::flash('warning', ' Trashed Successfully!' );
         return redirect()->back();
      }

    }


    public function delete()
    {
       return view('back.confirm_delete');
    }
}
