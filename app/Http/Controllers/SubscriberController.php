<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subscribe;
use App\Mail\SubscribeCourse;
use App\Mail\SubscribeCourseAll;
use Illuminate\Support\Facades\Mail;
use App\User;
use Session;

class SubscriberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
         $data['subscribes']=Subscribe::where('type','1' )->paginate(10);
         return view('back.subscriber.subscribe',$data); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function subscribe_all_web()
    {
         $data['subscribes']=Subscribe::where('type', '1')->get();
         return view('back.subscriber.subscribe_all',$data);
    }

    public function subscrib_all_save(Request $request)
    {
       
        $data['subscribes']=Subscribe::where('type', '1')->get();
        foreach ($data['subscribes'] as $subscribe)
        {
             $subscribe->subject=$request->subject;
             
             Mail::to($subscribe->email)->queue(new SubscribeCourseAll($subscribe));
        }

       
        return redirect()->back();
    }

}
