<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hotel;
use App\Restaurant;
use App\User;
use App\Http\controllers\SubjectController;
use App\Subject;
use App\SubjectCategory;
use App\Instructor;
use App\Course;
use App\Exam;
use App\Page;
use App\Gallary;
use App\Accordion;
use App\CourseDates;
use App\CompanyAccount;
use App\Reservation;
use App\Employee_salary;
use App\Subject_content;
use Session;
use App\Question;
use App\Accounting;
use App\RepeatlyAccounting;

class ViewpopupController extends Controller
{
    public function Viewpop()
    {
    	$id= $_GET['id'];
    	$table=$_GET['table'];
    	if ($table=="hotels") {
    		$view='hotel';
    		$data['hotel']= Hotel::find($id);
    	}

        if ($table=="restaurants") {
            $view='Restaurant';
            $data['restaurant']= Restaurant::find($id);
        }

        if ($table=="users") {
            $view='users';
            $data['users']= User::find($id);
        }
        if ($table=="subjects") {
            $view='subject';
            $data['subject']=Subject::find($id)->with('subCategory')->first();
            // dd($data['subject']);
        }
        if ($table=="instructors") {
            $view='instructor';
            $data['instructor']= Instructor::find($id);
        }
        if ($table=="courses") {
            $view='course';
            $data['course']= Course::find($id)->with('subject')->first();
            $data['dates'] = CourseDates::where('course_id',$id)->get();
            $data['reservations']=Reservation::where('course_id',$id)->count();
        }
        if ($table=="exams") {
            $view='exam';
            $data['exam']= Exam::find($id)->with('course')->with('subject')->first();
        }
         if ($table=="pages") {
            $view='pages';
            $data['page']= Page::find($id);
        }
        if ($table=="gallaries") {
            $view='gallary';
            $data['gallary']= Gallary::find($id);
        }
        if ($table=="accordions") {
            $view='accordion';
            $data['accordion']= Accordion::find($id);
        }
        if ($table=="company_accounts") {
            $view='company_account';
            $data['company_account']= CompanyAccount::find($id);
        }

        if ($table=="reservations") {
            $view='reservations';
            $data['reservation']= Reservation::find($id)->with('user')->with('course_schedule')->with('hotel')->with('resturant')->first();
        }

        if ($table=="employee_salaries") {
            $view='employee_salary';
            $data['employee']= Employee_salary::find($id)->with('user')->first();
           
        }

        if ($table=="subject_contents") {
            $view='subject_content';
            $data['subject_content']= Subject_content::find($id);
           
        }

        if ($table=="xam_questions") {
            $view='questions';
            $data['question']= Question::find($id);
           
        }
        if ($table=="accountings") {
            $view='accounting';
            $data['account']= Accounting::find($id);
           
        }
         if ($table=="repeatly_accountings") {
            $view='repeatly_accounting';
            $data['account']= RepeatlyAccounting::find($id);
           
        }

 
    	return view('back/'.$view.'/view_popup',$data);
    }

   
}
