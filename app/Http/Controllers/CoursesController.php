<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\Instructor;
use App\Subject;
use App\CourseDates;
use Session;
use App\Course_schedule;
use App\Subscribe;
use App\Mail\SubscribeCourse;
use App\Mail\SubscribeCourseAll;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Branch;
use App\Exam;
use App\Http\Controllers\ActivitesController;
use App\Reservation;
use App\Candidate_attendance;

class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['courses'] = Course::orderBy('id', 'desc')->with('subject')->paginate(10);
        return view('back.course.view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['instructor'] = Instructor::where('trash', '0')->get();
        $data['subjetes'] = Subject::where('trash', '0')->get();
      
        return view('back.course.add',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ActivitesController $ActivitesController)
    {
        // var_dump($request->dates);die();
        $course                           = new Course;
        $course->name_ar                  = $request->name_ar;
        $course->name_en                  = $request->name_en;
        $course->price                    = $request->price;
        $course->offer                    = $request->offer;
        $course->number_hours             = $request->number_hours;
        $course->subject_id               = $request->subject_id;
        $course->min_candidates           = $request->min_candidates;
        $course->max_candidates           = $request->max_candidates;
        $course->have_exam                = $request->have_exam;
        $course->create_by                = auth()->user()->id;
        $course->update_by                = auth()->user()->id;
        if (isset($request->exam_link)&& !empty($request->exam_link))
        {
           $course->exam_link             = $request->exam_link;
        }
        else
        {
           $course->exam_link             = "";
        }

        $course->save();
        $ActivitesController->store(auth()->user()->id ,'Created','Course',$course->id);

        // foreach ($request->dates as $date)
        // {
        // $dat                           = new CourseDates;
        // $dat->date                     = $date;
        // $dat->course_id                = $course->id;
        // $dat->save();
        // }

        return redirect()->route('course.create')->with('success',  $request->name ." Course Created Successfully!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['course']= Course::find($id);
        $data['instructor'] = Instructor::where('trash', '0')->get();
        $data['subjetes'] = Subject::where('trash', '0')->get();
        $data['dates']=CourseDates::where('trash', '0')->where('course_id',$id)->get();
        $data['branches'] = Branch::all();
        return view('back.course.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ActivitesController $ActivitesController)
    {
        $course                           = Course::find($id);
        $course->name_ar                  = $request->name_ar;
        $course->name_en                  = $request->name_en;
        $course->price                    = $request->price;
        $course->offer                    = $request->offer;
        $course->number_hours             = $request->number_hours;
        $course->subject_id               = $request->subject_id;
        $course->min_candidates           = $request->min_candidates;
        $course->max_candidates           = $request->max_candidates;
        $course->have_exam                = $request->have_exam;
        $course->create_by                = auth()->user()->id;
        $course->update_by                = auth()->user()->id;
        if (isset($request->exam_link)&& !empty($request->exam_link))
        {
           $course->exam_link             = $request->exam_link;
        }
        else
        {
           $course->exam_link             = "";
        }
        $course->save();
        $ActivitesController->store(auth()->user()->id ,'Updated','Course',$course->id);
        $course_date = CourseDates::where('course_id',$id);
        $course_date->delete();

        // if (isset($request->dates) && !empty($request->dates)) {
        //     // var_dump($request->dates);die();
        //    foreach ($request->dates as $date)
        //     {
        //         $dat                           = new CourseDates;
        //         $dat->date                     = $date;
        //         $dat->course_id                = $course->id;
        //         $dat->save();
        //     }
        // }

        $edited_area = $course;


        Session::flash('info', ' Course Updated Successfully!' );
        // return view('back.content.categories.edit');
        return redirect('/admin/course/'.$edited_area->id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,ActivitesController $ActivitesController)
    {
        // var_dump($id);die();
       $course = Course::find($id);
       $course->delete();
       $ActivitesController->store(auth()->user()->id ,'Destroy','Course',$id);
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }


    public function trash(Request $request,ActivitesController $ActivitesController)
    {
      $id=$request->segment(4);
      $trash =$request->segment(5);
      if ($trash==1) {
         Course::where('id',$id)->update(array( 'trash' => 0, ));
         $ActivitesController->store(auth()->user()->id ,'UNTrashed','Course',$id);
         Session::flash('untrash', ' UNTrashed Successfully!' );
         return redirect()->back();
        
      }
      else
      {
         Course::where('id',$id)->update(array( 'trash' => 1, ));
          $ActivitesController->store(auth()->user()->id ,'Trashed','Course',$id);
         Session::flash('warning', ' Trashed Successfully!' );
         return redirect()->back();
      }

    }


    public function delete()
    {
       return view('back.confirm_delete');
    }

    public function schedule_add(Request $request)
    {
      $data['instructor'] = Instructor::where('trash', '0')->get();
      $data['branches'] = Branch::all();
      $data['exams']=Exam::all();
      return view('back.course.schedule_add',$data);
    }

    public function schedule_save(Request $request)
    {

        $id=$request->segment(4);
        $course                           = new Course_schedule;
        $course->from                     = $request->from;
        $course->to                       = $request->to   ;
        $course->course_id                = $id;
        $course->instructor_id            = $request->instruct_id;
        $course->private                  = $request->private;
        $course->schedule_ar              = $request->schedule_ar;
        $course->schedule_en              = $request->schedule_en;
        $course->branch_id                = $request->branch_id;
        $course->exam_id                  = $request->exam_id;
        $course->create_by                = auth()->user()->id;
        $course->update_by                = auth()->user()->id;
        $course->save();

        foreach ($request->dates as $date)
        {
            $dat                           = new CourseDates;
            $dat->date                     = $date;
            $dat->course_id                = $id;
            $dat->course_schedule_id       =$course->id;
            $dat->save();
        }

          return redirect('/admin/course/schedule_add/'.$id)->with('success'," Course Schedule Created Successfully!");
        

    }

    public function schedule_view(Request $request)
    {
        $course_id=$request->segment(4);
        $data['schedules'] = Course_schedule::orderBy('id', 'desc')->where('course_id',$course_id)->paginate(10);
        $dates=array();
        foreach ($data['schedules'] as $key)
        {
          $course_schedule_id=$key->id;
          $key->dates= CourseDates::where('course_id',$course_id)->where('course_schedule_id',$course_schedule_id)->get();
            // var_dump($dates);die();
        }
       //  return $data['schedules'];
       //  $data['dates']=$dates;
       // var_dump($data['dates']);die();
        return view('back.course.schedule_view',$data);
    }


    public function schedule_edit($id)
    {
        $data['schedule']= Course_schedule::find($id);
        $data['instructor'] = Instructor::where('trash', '0')->get();
        $data['dates']=CourseDates::where('trash', '0')->where('course_schedule_id',$id)->get();
        $data['branches'] = Branch::all();
        $data['exams']=Exam::all();
        return view('back.course.schedule_edit',$data); 
    }


     public function schedule_update(Request $request, $id)
    {
        $course                           = Course_schedule::find($id);
        $course->from                     = $request->from;
        $course->to                       = $request->to   ;
        // $course->course_id                = $id;
        $course->instructor_id            = $request->instruct_id;
        $course->private                  = $request->private      ;
        $course->schedule_ar              = $request->schedule_ar;
        $course->schedule_en              = $request->schedule_en;
        $course->branch_id                = $request->branch_id;
        $course->exam_id                  = $request->exam_id;     ;
        $course->create_by                = auth()->user()->id;
        $course->update_by                = auth()->user()->id;
        $course->save();

        $course_date = CourseDates::where('course_schedule_id',$id);
        $course_date->delete();

        foreach ($request->dates as $date)
        {
            $dat                           = new CourseDates;
            $dat->date                     = $date;
            $dat->course_id                = $id;
            $dat->course_schedule_id       =$course->id;
            $dat->save();
        }

        $edited_area = $course;


        Session::flash('info', ' Course Schedule Updated Successfully!' );
        // return view('back.content.categories.edit');
        return redirect('/admin/course/schedule_edit/'.$edited_area->id);
    }

    public function subscribe(Request $request)
    {
         $id=$request->segment(4);
         $data['subscribes']=Subscribe::where('course_id', $id)->paginate(10);
         return view('back.course.subscribe',$data); 
    }

    public function subscribe_course()
    {
        $data['email']=$_GET['email'];
        return view('back.course.subscribe_user',$data);
    }

    public function subscrib_course_save(Request $request)
    {
    
        Mail::send(new SubscribeCourse($request));
        return redirect()->back();
    }

     public function subscribe_all()
    {
        $data['id']=$_GET['id'];
        $id=$_GET['id'];
        $data['subscribes']=Subscribe::where('course_id', $id)->get();
        return view('back.course.subscribe_all',$data);
    }

    public function subscrib_all_save(Request $request)
    {
        // var_dump($request->subject);die();
        $id=$request->id;
        $data['subscribes']=Subscribe::where('course_id', $id)->get();
        foreach ($data['subscribes'] as $subscribe)
        {
             $subscribe->subject=$request->subject;
             
             Mail::to($subscribe->email)->queue(new SubscribeCourseAll($subscribe));
        }

       
        return redirect()->back();
    }

    public function date_attendance(Request $request)
    {
        $schedule_id=$request->segment(4);
        $date_id=$request->segment(5);
        $data['schedules']=Reservation::where('trash','0')->where('schedule_id',$schedule_id)->with('user')->paginate(10);
        $data['candidates']=Candidate_attendance::where('trash','0')->where('course_date_id',$date_id)->with('user')->get();
        $candidate = array();
        foreach ($data['candidates'] as $key) {
            
            $candidate[]+=$key->candidate_id;
        }
        $data['cand']=$candidate;

        foreach ($data['schedules'] as $k) {
            $data['course_id']=$k->course_id;
        }
       
        return view('back.course.view_attendance',$data);
       
    }


}
