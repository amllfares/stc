<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\SubjectCategory;

use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // die();
        $data['under_banner'] = Page::where('sub_of','22')->orderBy('order', 'asc')->get();
        // return $data['under_banner'];
        return view('home',$data);
    }

    public function welcome_admin()
    {
       
        return view('welcom_home');
    }

    public function admin()
    {
       
        return view('back.admin');
    }

    public function welcome_error()
    {
       
        return view('welcome_error');
    }

    
}
