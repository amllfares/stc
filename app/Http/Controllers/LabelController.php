<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use App\Label;
use App\Http\Controllers\ActivitesController;
use App\Section;
use App\Permiation;
use App\Role_Permission;

class LabelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['lables'] = Label::orderBy('id', 'desc')->paginate(10);
        return view('back.lables.view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('back.lables.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ActivitesController $ActivitesController)
    {
        $request->create_by=auth()->user()->id;
        $request->update_by=auth()->user()->id;
        $role=Label::create(array_except($request->all(),['_token']));
        $ActivitesController->store(auth()->user()->id ,'Created','Label',$role->id);
       
        Session::flash('success',  $role->name.' Label Created Successfully!' );
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['lable']= Label::find($id);
        $data['sections']=Section::where('sub_of','0')->get();
        $data['permiations'] = Permiation::orderBy('id', 'desc')->get();
       
        return view('back.lables.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ActivitesController $ActivitesController)
    {
        $Label                           = Label::find($id);
        $Label->name                     = $request->name;
        $Label->create_by                = auth()->user()->id;
        $Label->update_by                = auth()->user()->id;
        $Label->save();
        $ActivitesController->store(auth()->user()->id ,'Updated','Label',$Label->id);

        $edited_area = $Label;
        Session::flash('info',  $Label->name.' Updated Successfully!' );
        // return view('back.content.categories.edit');
        return redirect('/admin/lables/'.$edited_area->id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,ActivitesController $ActivitesController)
    {
       $Label = Label::find($id);
       $Label->delete();
       $ActivitesController->store(auth()->user()->id ,'Destroy','Label',$id);
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }

    public function delete()
    {
       return view('back.confirm_delete');
    }
}
