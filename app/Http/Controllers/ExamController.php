<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exam;
use App\Course;
use App\Subject;
use Session;
use App\Http\Controllers\ActivitesController;
class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['exams'] = Exam::orderBy('id', 'desc')->paginate(10);
        return view('back.exam.view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['courses'] = Course::where('trash', '0')->get();
        $data['subjetes'] = Subject::where('trash', '0')->get();
        return view('back.exam.add',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ActivitesController $ActivitesController)
    {
        $exam                           = new Exam;
        $exam->name_en                  = $request->name_en;
        $exam->name_ar                  = $request->name_ar;
        $exam->num_hours                = $request->num_hours;
        $exam->type                     = $request->type;
        $exam->course_id                = $request->course_id; 
        $exam->create_by                = auth()->user()->id;
        $exam->update_by                = auth()->user()->id;

        $exam->save();
        $ActivitesController->store(auth()->user()->id ,'Created','Exam',$exam->id);

        return redirect()->route('exam.create')->with('success',  $request->name ." Created Successfully!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['exam']= Exam::find($id);
        $data['courses'] = Course::where('trash', '0')->get();
        $data['subjetes'] = Subject::where('trash', '0')->get();
        return view('back.exam.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ActivitesController $ActivitesController)
    {
        $exam                           = Exam::find($id);
        $exam->name_en                  = $request->name_en;
        $exam->name_ar                  = $request->name_ar;
        $exam->num_hours                = $request->num_hours;
        $exam->type                     = $request->type;
        $exam->course_id                = $request->course_id;
        $exam->create_by                = auth()->user()->id;
        $exam->update_by                = auth()->user()->id;
        $exam->save();
        $ActivitesController->store(auth()->user()->id ,'Updated','Exam',$exam->id);
        $edited_area = $exam;
        Session::flash('info',  $exam->name .' Updated Successfully!' );
        // return view('back.content.categories.edit');
        return redirect('/admin/exam/'.$edited_area->id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,ActivitesController $ActivitesController)
    {
       
       $exam = Exam::find($id);
       $exam->delete();
       $ActivitesController->store(auth()->user()->id ,'Destroy','Exam',$id);
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }


    public function trash(Request $request,ActivitesController $ActivitesController)
    {
      $id=$request->segment(4);
      $trash =$request->segment(5);
      if ($trash==1) {
         Exam::where('id',$id)->update(array( 'trash' => 0, ));
         $ActivitesController->store(auth()->user()->id ,'UNTrashed','Exam',$id);
         Session::flash('untrash', ' UNTrashed Successfully!' );
         return redirect()->back();
        
      }
      else
      {
         Exam::where('id',$id)->update(array( 'trash' => 1, ));
         $ActivitesController->store(auth()->user()->id ,'Trashed','Exam',$id);
         Session::flash('warning', ' Trashed Successfully!' );
         return redirect()->back();
      }

    }


    public function delete()
    {
       return view('back.confirm_delete');
    }
}
