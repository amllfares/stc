<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subject_content;
use Session;
use App\Http\Controllers\ActivitesController;
class Subject_contentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sub_id=$request->segment('3');
        $data['subject_contents'] = Subject_content::orderBy('id', 'desc')->where('course_id',$sub_id)->paginate(10);
        return view('back.subject_content.view',$data);
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('back.subject_content.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ActivitesController $ActivitesController)
    {
        $sub_id=$request->segment('4');
       
        $sub_content                           = new Subject_content;
        $sub_content->name_en                  = $request->name_en;
        $sub_content->name_ar                  = $request->name_ar;
        $sub_content->bref_en                  = $request->bref_en;
        $sub_content->bref_ar                  = $request->bref_ar;
        $sub_content->content_en               = $request->content_en;
        $sub_content->content_ar               = $request->content_ar;
        $sub_content->course_id                =  $sub_id;
        $sub_content->create_by                = auth()->user()->id;
        $sub_content->update_by                = auth()->user()->id;
        $sub_content->save();
        $ActivitesController->store(auth()->user()->id ,'Created','Subject_content',$sub_content->id);

        Session::flash('success', ' Subject_Content Successfully!' );
        return redirect('admin/subject_content/add/'.$sub_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['subject_content']= Subject_content::find($id);
        return view('back.subject_content.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ActivitesController $ActivitesController)
    {
        $sub_content                           = Subject_content::find($id);
        $sub_content->name_en                  = $request->name_en;
        $sub_content->name_ar                  = $request->name_ar;
        $sub_content->bref_en                  = $request->bref_en;
        $sub_content->bref_ar                  = $request->bref_ar;
        $sub_content->content_en               = $request->content_en;
        $sub_content->content_ar               = $request->content_ar;
        $sub_content->course_id                = $request->sub_id;
        $sub_content->create_by                = auth()->user()->id;
        $sub_content->update_by                = auth()->user()->id;
        $sub_content->save();
        $ActivitesController->store(auth()->user()->id ,'Updated','Subject_content',$sub_content->id);
        $edited_area = $sub_content;
        Session::flash('info',  $sub_content->name_en .' Updated Successfully!' );
        // return view('back.content.categories.edit');
        return redirect('/admin/subject_content/'.$edited_area->id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,ActivitesController $ActivitesController)
    {
        // var_dump($id);die();
       $Subject_content = Subject_content::find($id);
       $Subject_content->delete();
       $ActivitesController->store(auth()->user()->id ,'Destroy','Subject_content',$id);
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }


    public function trash(Request $request,ActivitesController $ActivitesController)
    {
      $id=$request->segment(4);
      $trash =$request->segment(5);
      if ($trash==1) {
         Subject_content::where('id',$id)->update(array( 'trash' => 0, ));
         $ActivitesController->store(auth()->user()->id ,'UNTrashed','Subject_content',$id);
         Session::flash('untrash', ' UNTrashed Successfully!' );
         return redirect()->back();
        
      }
      else
      {
         Subject_content::where('id',$id)->update(array( 'trash' => 1, ));
         $ActivitesController->store(auth()->user()->id ,'Trashed','Subject_content',$id);
         Session::flash('warning', ' Trashed Successfully!' );
         return redirect()->back();
      }

    }


    public function delete()
    {
       return view('back.confirm_delete');
    }
}
