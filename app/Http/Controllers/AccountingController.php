<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Accounting;
use Session;
use App\Http\Controllers\ActivitesController;
use App\CategoryAccounting; 
use App\User; 

class AccountingController extends Controller
{ 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {
        $data['accounting'] = Accounting::orderBy('id', 'desc')->paginate(10);
        return view('back.accounting.view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // var_dump(date("d"));die();
        $data['users']=User::where('trash','0')->get();
        $data['category']=CategoryAccounting::orderBy('id', 'desc')->get();
        return view('back.accounting.add',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ActivitesController $ActivitesController)
    {
        $Accounting                           = new Accounting;
        $Accounting->amount                   = $request->amount;
        $Accounting->category_id              = $request->category_id;
        $Accounting->foreign_id               = '0';
        $Accounting->extra                    = $request->extra;
        $Accounting->confirm                  = $request->confirm;
        $Accounting->user_id                  = $request->user_id;
        $Accounting->month                    = $request->month;
        $Accounting->labels                   = $request->labels;
        $Accounting->create_by                = auth()->user()->id;
        $Accounting->update_by                = auth()->user()->id;
        $Accounting->save();
        $ActivitesController->store(auth()->user()->id ,'Created','Accounting',$Accounting->id);
        return redirect()->route('accounting.create')->with('success',  $request->name_en ." Created Successfully!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['accounting']= Accounting::find($id);
        $data['users']=User::where('trash','0')->get();
        $data['category']=CategoryAccounting::orderBy('ID', 'desc')->get();
        return view('back.accounting.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ActivitesController $ActivitesController)
    {
        $Accounting                           = Accounting::find($id);
        $Accounting->amount                   = $request->amount;
        $Accounting->category_id              = $request->category_id;
        $Accounting->foreign_id               = '0';
        $Accounting->extra                    = $request->extra;
        $Accounting->confirm                  = $request->confirm;
        $Accounting->user_id                  = $request->user_id;
        $Accounting->month                    = $request->month;
        $Accounting->labels                   = $request->labels;
        $Accounting->create_by                = auth()->user()->id;
        $Accounting->update_by                = auth()->user()->id;
        $Accounting->save();
       
        $ActivitesController->store(auth()->user()->id ,'Updated','Accounting',$Accounting->id);
 
        Session::flash('info', ' Accounting Updated Successfully!' );
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,ActivitesController $ActivitesController)
    {
       $Accounting = Accounting::find($id);
       $Accounting->delete();
       $ActivitesController->store(auth()->user()->id ,'Destroy','Accounting',$id);
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }

    public function delete()
    {
       return view('back.confirm_delete');
    }

    public function user_category()
    {
        // var_dump($_GET['category_id']);die();
        $data['category_id']=$_GET['category_id'];
        if ($data['category_id']=='7')
        {
            $data['users']=User::where('trash','0')->where('employe','1')->orWhere('admin','1')->get();
            
            $HTML="
                <label>
                User:
                </label>
                <div class=\"m-input-icon m-input-icon--right\">
               <select name=\"user_id\" id=\"parent_category\" class=\"form-control m-input  m-input--square\">";
            $HTML.="<option  disabled selected>Choose User</option>";
                
                
            foreach($data['users'] as $user){
                $HTML.="<option value='".$user->id."'>".$user->name."</option>";
            }

            $HTML.="</div>
                    <span class=\"m-form__help\">
                        Please choose user
                    </span>";
        
            echo $HTML;
        }
        elseif ($data['category_id']=='3')
        {
            $data['users']=User::where('trash','0')->where('employe','1')->where('admin','1')->get();
             $HTML="
               <input type=\"hidden\"   name=\"user_id\" value=\"0\">";

            echo $HTML;
        }
        else
        {
            $data['users']=User::where('trash','0')->get();
             $HTML="
                <label>
                User:
                </label>
                <div class=\"m-input-icon m-input-icon--right\">
               <select name=\"user_id\" id=\"parent_category\" class=\"form-control m-input  m-input--square\">";
            $HTML.="<option  disabled selected>Choose User</option>";
                
                
            foreach($data['users'] as $user){
                $HTML.="<option value='".$user->id."'>".$user->name."</option>";
            }

            $HTML.="</div>
                    <span class=\"m-form__help\">
                        Please choose category
                    </span>";
                
                


            echo $HTML;
        }
       
        // return view('back.accounting.user_category',$data);
       
    }
}
