<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question_Type;
use Session;
use App\Http\Controllers\ActivitesController;
class QuestionTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['question_types'] = Question_Type::orderBy('ID', 'desc')->paginate(10);
        return view('back.question_type.view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back.question_type.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ActivitesController $ActivitesController)
    {
        $Question_Type                           = new Question_Type;
        $Question_Type->Type                     = $request->Type;
        $Question_Type->save();
        $ActivitesController->store(auth()->user()->id ,'Created','Question_Type',$Question_Type->id);
        return redirect()->route('question_type.create')->with('success',  $request->Type ." Created Successfully!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['question_Type']= Question_Type::find($id);
        return view('back.question_type.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ActivitesController $ActivitesController)
    {
       
        Question_Type::where('ID',$id)->update(array( 'Type' =>$request->Type, ));
        $ActivitesController->store(auth()->user()->id ,'Created','Question_Type',$id);
        Session::flash('info', $request->Type .' Updated Successfully!' );
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,ActivitesController $ActivitesController)
    {
       Question_Type::where('ID',$id)->delete();
       $ActivitesController->store(auth()->user()->id ,'Destroy','Question_Type',$id);
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }

    public function delete()
    {
       return view('back.confirm_delete');
    }
}
