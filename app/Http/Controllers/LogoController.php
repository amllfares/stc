<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Logo;
use Session;
use App\Http\Controllers\ActivitesController;

class LogoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['logos'] = Logo::orderBy('id', 'desc')->paginate(10);
        return view('back.logo.view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('back.logo.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ActivitesController $ActivitesController)
    {
        $logo                           = new Logo;
        if ($request->hasFile('image'))
        {
            $image = $request->file('image');
            $name ='/assets/back/upload/'.time().$image->getClientOriginalName();
            $destinationPath = public_path('/assets/back/upload/');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $logo->image = $name;
        }
       
        $logo->save();
        $ActivitesController->store(auth()->user()->id ,'Created','Logo',$logo->id);
        return redirect()->route('logo.create')->with('success',  " Logo Created Successfully!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['logo']= Logo::find($id);
        return view('back.logo.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ActivitesController $ActivitesController)
    {
        $logo                           = Logo::find($id);
            if ($request->hasFile('image'))
            {
                $image = $request->file('image');
                $name ='/assets/back/upload/'.time().$image->getClientOriginalName();
                $destinationPath = public_path('/assets/back/upload/');
                $imagePath = $destinationPath. "/".  $name;
                $image->move($destinationPath, $name);
                $logo->image = $name;
            }
       
        $logo->save();
       
        $ActivitesController->store(auth()->user()->id ,'Updated','Logo',$logo->id);
 
        Session::flash('info', ' Logo Updated Successfully!' );
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,ActivitesController $ActivitesController)
    {
       $Logo = Logo::find($id);
       $Logo->delete();
       $ActivitesController->store(auth()->user()->id ,'Destroy','Logo',$id);
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }

    public function delete()
    {
       return view('back.confirm_delete');
    }
}
