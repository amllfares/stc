<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee_salary;
use App\User;
use Session;
use App\Http\Controllers\ActivitesController;
use Illuminate\Support\Facades\Hash;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        $data['employees'] = Employee_salary::orderBy('id', 'desc')->with('user')->paginate(10);
        return view('back.employee_salary.view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['employees'] = User::where('trash', '0')->where('employe','1')->with('user')->get();
        return view('back.employee_salary.add',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ActivitesController $ActivitesController)
    {
        $request->validate([
        'name' => 'required|required|string|email|max:255|unique:users',
         ]);
       
        $user= User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'employe'=>'1',

        ]);

        $employee                           = new Employee_salary;
        $employee->employee_id              = $user->id;
        $employee->salary                   = $request->salary;
        $employee->position                 = $request->position;
        $employee->create_by                = auth()->user()->id;
        $employee->update_by                = auth()->user()->id;
        $employee->save();
        $ActivitesController->store(auth()->user()->id ,'Created','Employee_salary',$employee->id);

        return redirect()->route('employee_salary.create')->with('success',  "Salary" ." Created Successfully!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['employee_salary']= Employee_salary::where('id',$id)->with('user')->first();
        // var_dump($data['employee_salary']->user->name);die();
        // $data['employee'] = User::where('trash', '0')->where('id',$data['employee_salary']->id)->first();
        return view('back.employee_salary.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ActivitesController $ActivitesController)
    {
        $employee                           = Employee_salary::find($id);
        $employee->position                 = $request->position;
        $employee->salary                   = $request->salary;
        $employee->create_by                = auth()->user()->id;
        $employee->update_by                = auth()->user()->id;
        $employee->save();
        $ActivitesController->store(auth()->user()->id ,'Updated','Employee_salary',$employee->id);
        $edited_area = $employee;
        Session::flash('info',  'Salary' .' Updated Successfully!' );
        // return view('back.content.categories.edit');
        return redirect('/admin/employee_salary/'.$edited_area->id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,ActivitesController $ActivitesController)
    {
       $employee = Employee_salary::find($id);
       $employee->delete();
       $ActivitesController->store(auth()->user()->id ,'Destroy','Employee_salary',$id);
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }


    public function trash(Request $request,ActivitesController $ActivitesController)
    {
      $id=$request->segment(4);
      $trash =$request->segment(5);
      if ($trash==1) {
         Employee_salary::where('id',$id)->update(array( 'trash' => 0, ));
         $ActivitesController->store(auth()->user()->id ,'UNTrashed','Employee_salary',$id);
         Session::flash('untrash', ' UNTrashed Successfully!' );
         return redirect()->back();
        
      }
      else
      {
         Employee_salary::where('id',$id)->update(array( 'trash' => 1, ));
         $ActivitesController->store(auth()->user()->id ,'Trashed','Employee_salary',$id);
         Session::flash('warning', ' Trashed Successfully!' );
         return redirect()->back();
      }

    }


    public function delete()
    {
       return view('back.confirm_delete');
    }
}
