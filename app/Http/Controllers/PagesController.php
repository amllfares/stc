<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use Session;
use App\Gallary;
use App\Accordion;
use App\Http\Controllers\ActivitesController;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::where('sub_of','0')->orderBy('id', 'asc')->paginate(10);
// return $pages;
        foreach ($pages as $row) {
            // var_dump($row['data']);
            $row->sec_level = Page::where('sub_of',$row->id)->orderBy('id', 'asc')->get();

            foreach ($row->sec_level as $value) {
                $value->thrd_level=Page::where('sub_of',$value->id)->orderBy('id', 'asc')->get();
            }
        }
        // return $row->sec_level;
        // return $pages;
        $data['pages']=$pages;
        return view('back.pages.view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $data['sub_page'] = Page::where('selector','1')->get();
       return view('back.pages.add',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ActivitesController $ActivitesController)
    {
        
        $page=Page::create(array_except($request->all(),['_token','photo','banner']));
        $page->create_by=auth()->user()->id;
        $page->update_by=auth()->user()->id;
        // $page                           = new Page;
        // $page->name_en                  = $request->name_en;
        // $page->name_ar                  = $request->name_ar;
        // $page->slug_en                  = str_slug($request->name_en);
        // $page->slug_ar                  = str_slug($request->name_ar);
        // $page->bref_en                  = $request->bref_en;
        // $page->bref_ar                  = $request->bref_ar;
        // $page->description_en           = $request->description_en;
        // $page->description_ar           = $request->description_ar;
        // $page->order                    = $request->order;
        // $page->sub_of                   = $request->sub_of;
        // $page->is_main_page             = $request->is_main_page;
        // $page->nav                      = $request->nav;
        // $page->delete                   = $request->delete;
        // $page->can_not_delete           = $request->can_not_delete;
        // $page->accordion                = $request->accordion;
        // $page->gallery                  = $request->gallery;
        // $page->selector                 = $request->selector;
        // $page->create_by                = auth()->user()->id;
        // $page->update_by                = auth()->user()->id;

        if ($request->hasFile('photo'))
        {
            $image = $request->file('photo');
            $name ='/assets/back/upload/'.time().$image->getClientOriginalName();
            $destinationPath = public_path('/assets/back/upload/');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $page->photo = $name;
        }
        if ($request->hasFile('banner'))
        {
            $image = $request->file('banner');
            $name ='/assets/back/upload/'.time(). $image->getClientOriginalName();
            $destinationPath = public_path('/assets/back/upload/');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $page->banner = $name;
        }

        $page->save();
        $ActivitesController->store(auth()->user()->id ,'Created','Page',$page->id);
        return redirect()->route('pages.create')->with('success',  $request->name_en ." Created Successfully!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $data['page']= Page::find($id);
        $page_id=$request->segment('3');
        $data['gallery'] = Gallary::orderBy('id', 'desc')->where('related_id',$page_id)->where('table','pages')->paginate(10);
        $data['accordion'] = Accordion::orderBy('id', 'desc')->where('related_id',$page_id)->where('table','pages')->paginate(10);
        $data['sub_page'] = Page::where('selector','1')->get();
        return view('back.pages.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ActivitesController $ActivitesController)
    {
        $page                           = Page::find($id);
        $page->name_en                  = $request->name_en;
        $page->name_ar                  = $request->name_ar;
        $page->slug_en                  = str_slug($request->name_en);
        $page->slug_ar                  = str_slug($request->name_ar);
        $page->bref_en                  = $request->bref_en;
        $page->bref_ar                  = $request->bref_ar;
        $page->description_en           = $request->description_en;
        $page->description_ar           = $request->description_ar;
        $page->order                    = $request->order;
        $page->sub_of                   = $request->sub_of;
        $page->is_main_page             = $request->is_main_page;
        $page->nav                      = $request->nav;
        $page->delete                   = $request->delete;
        $page->can_not_delete           = $request->can_not_delete;
        $page->accordion                = $request->accordion;
        $page->gallery                  = $request->gallery;
        $page->selector                 = $request->selector;
        $page->create_by                = auth()->user()->id;
        $page->update_by                = auth()->user()->id;

        if ($request->hasFile('photo'))
        {
            $image = $request->file('photo');
            $name ='/assets/back/upload/'.time(). $image->getClientOriginalName();
            $destinationPath = public_path('/assets/back/upload/');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $page->photo = $name;
        }
        if ($request->hasFile('banner'))
        {
            $image = $request->file('banner');
            $name ='/assets/back/upload/'.time(). $image->getClientOriginalName();
            $destinationPath = public_path('/assets/back/upload/');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $page->banner = $name;
        }

        $page->save();
        $ActivitesController->store(auth()->user()->id ,'Updated','Page',$page->id);
        $edited_area = $page;
        Session::flash('info',  $page->name_en .' Updated Successfully!' );
        // return view('back.content.categories.edit');
        return redirect('/admin/pages/'.$edited_area->id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,ActivitesController $ActivitesController)
    {
        // var_dump($id);die();
       $page = Page::find($id);
       $page->delete();
       $ActivitesController->store(auth()->user()->id ,'Destroy','Page',$id);
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }


    public function trash(Request $request,ActivitesController $ActivitesController)
    {
      $id=$request->segment(4);
      $trash =$request->segment(5);
      if ($trash==1) {
         Page::where('id',$id)->update(array( 'trash' => 0, ));
         $ActivitesController->store(auth()->user()->id ,'UNTrashed','Page',$id);
         Session::flash('untrash', ' UNTrashed Successfully!' );
         return redirect()->back();
        
      }
      else
      {
         Page::where('id',$id)->update(array( 'trash' => 1, ));
         $ActivitesController->store(auth()->user()->id ,'Trashed','Page',$id);
         Session::flash('warning', ' Trashed Successfully!' );
         return redirect()->back();
      }

    }


    public function delete()
    {
       return view('back.confirm_delete');
    }
}
