<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Matirial;
use Session;
use App\Http\Controllers\ActivitesController;

class MatirialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $course_id=$request->segment('3');
        $data['matirial'] = Matirial::orderBy('id', 'desc')->where('course_id',$course_id)->get();
        return view('back.matirial.view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('back.matirial.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ActivitesController $ActivitesController)
    {
        $course_id=$request->segment('4');
        $matirial                           = new Matirial;
        $matirial->course_id                = $course_id;
        $matirial->title_en                 = $request->title_en;
        $matirial->title_ar                 = $request->title_ar;
        $matirial->create_by                = auth()->user()->id;
        $matirial->update_by                = auth()->user()->id;
        if ($request->hasFile('matirial'))
        {
        $image = $request->file('matirial');
        $name = time().$image->getClientOriginalName();
        $destinationPath = public_path('/assets/back/upload/');
        $imagePath = $destinationPath. "/".  $name;
        $image->move($destinationPath, $name);
        $matirial->matirial = $name;
        }

        $matirial->save();
        $ActivitesController->store(auth()->user()->id ,'Created','Matirial',$matirial->id);
        Session::flash('success', ' Matirial Added Successfully!' );
        return redirect('admin/matirial/add/'.$course_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $data['matirial']= Matirial::find($id);
         return view('back.matirial.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ActivitesController $ActivitesController)
    {
        $course_id=$request->segment('3');
        $matirial                           =  Matirial::find($id);
        $matirial->course_id                = $course_id;
        $matirial->title_en                  = $request->title_en;
        $matirial->title_ar                  = $request->title_ar;
        $matirial->create_by                = auth()->user()->id;
        $matirial->update_by                = auth()->user()->id;
        if ($request->hasFile('matirial'))
        {
        $image = $request->file('matirial');
        $name ='/assets/back/upload/'.time(). $image->getClientOriginalName();
        $destinationPath = public_path('/assets/back/upload/');
        $imagePath = $destinationPath. "/".  $name;
        $image->move($destinationPath, $name);
        $matirial->matirial = $name;
        }

        $matirial->save();
        $ActivitesController->store(auth()->user()->id ,'Updated','Matirial',$matirial->id);
        $edited_area = $matirial;
        Session::flash('info', ' Matirial updates Successfully!' );
        return redirect('/admin/matirial/'.$edited_area->id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // var_dump($id);die();
       $Matirial = Matirial::find($id);
       $Matirial->delete();
       $ActivitesController->store(auth()->user()->id ,'Destroy','Matirial',$id);
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }


    public function trash(Request $request,ActivitesController $ActivitesController)
    {
      $id=$request->segment(4);
      $trash =$request->segment(5);
      if ($trash==1) {
         Matirial::where('id',$id)->update(array( 'trash' => 0, ));
         $ActivitesController->store(auth()->user()->id ,'UNTrashed','Matirial',$id);
         Session::flash('untrash', ' UNTrashed Successfully!' );
         return redirect()->back();
        
      }
      else
      {
         Matirial::where('id',$id)->update(array( 'trash' => 1, ));
         $ActivitesController->store(auth()->user()->id ,'Trashed','Matirial',$id);
         Session::flash('warning', ' Trashed Successfully!' );
         return redirect()->back();
      }

    }


    public function delete()
    {
       return view('back.confirm_delete');
    }
}
