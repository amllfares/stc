<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Career;
use Session;
use App\Apply_Career; 
use Response;
use App\Mail\ApproveJob;
use App\Mail\RejectJob;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\ActivitesController;

class CareerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['careers'] = Career::orderBy('id', 'desc')->paginate(10);
        return view('back.careers.view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back.careers.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ActivitesController $ActivitesController)
    {

        $career                           = new Career;
        $career->tilte_en                 = $request->tilte_en;
        $career->tilte_ar                 = $request->tilte_ar;
        $career->description_en           = $request->description_en;
        $career->description_ar           = $request->description_ar;
        $career->create_by                = auth()->user()->id;
        $career->update_by                = auth()->user()->id;

        if ($request->hasFile('image'))
        {
            $image = $request->file('image');
            $name ='/assets/back/upload/'.time(). $image->getClientOriginalName();
            $destinationPath = public_path('/assets/back/upload/');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $career->image = $name;
        }
       
        $career->save();
        $ActivitesController->store(auth()->user()->id ,'Created','Career',$career->id);

        return redirect()->route('careers.create')->with('success',  $request->name_en ." Created Successfully!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['career']= Career::find($id);
        return view('back.careers.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ActivitesController $ActivitesController)
    {
        $career                           = Career::find($id);
        $career->tilte_en                 = $request->tilte_en;
        $career->tilte_ar                 = $request->tilte_ar;
        $career->description_en           = $request->description_en;
        $career->description_ar           = $request->description_ar;
        $career->create_by                = auth()->user()->id;
        $career->update_by                = auth()->user()->id;

        if ($request->hasFile('image'))
        {
            $image = $request->file('image');
            $name ='/assets/back/upload/'.time(). $image->getClientOriginalName();
            $destinationPath = public_path('/assets/back/upload/');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $career->image = $name;
        }
       
        $career->save();
        $ActivitesController->store(auth()->user()->id ,'Updated','Career',$career->id);
        $edited_area = $career;

        Session::flash('info',  $career->tilte_en .' Updated Successfully!' );
        return redirect('/admin/careers/'.$edited_area->id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,ActivitesController $ActivitesController)
    {
        // var_dump($id);die();
       $Career = Career::find($id);
       $Career->delete();
       $ActivitesController->store(auth()->user()->id ,'Destroy','Career',$id);
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }


    public function trash(Request $request,ActivitesController $ActivitesController)
    {
      $id=$request->segment(4);
      $trash =$request->segment(5);
      if ($trash==1) {
         Career::where('id',$id)->update(array( 'trash' => 0, ));
         $ActivitesController->store(auth()->user()->id ,'UNTrashed','Career',$id);
         Session::flash('untrash', ' UNTrashed Successfully!' );
         return redirect()->back();
        
      }
      else
      {
         Career::where('id',$id)->update(array( 'trash' => 1, ));
         $ActivitesController->store(auth()->user()->id ,'Trashed','Career',$id);
         Session::flash('warning', ' Trashed Successfully!' );
         return redirect()->back();
      }

    }


    public function delete()
    {
       return view('back.confirm_delete');
    }

    public function apply(Request $request)
    {
        $id=$request->segment('4');
        $data['applies']=Apply_Career::orderBy('id', 'desc')->where('career_id',$id)->paginate(10);
        return view('back.careers.apply_job',$data);
    }

    public function getDownload(Request $request) 
    {
        $id=$request->segment(4);
        //PDF file is stored under project/public/download/info.pdf
        $apply_career= Apply_Career::where('id',$id)->first();
        Apply_Career::where('id',$id)->update(array( 'seen' => 1, ));
        $file= public_path().'/assets/back/upload/'.$apply_career->file;

        $headers = array(
                  'Content-Type: application/pdf',
                );

        return Response::download($file, $apply_career->file, $headers);
    }

    public function reject(Request $request)
    {
        $id=$request->segment(4);
        $apply_career= Apply_Career::where('id',$id)->first();
        Apply_Career::where('id',$id)->update(array( 'status' => 0, ));
        Mail::to($apply_career->user->email)->send(new RejectJob());
        Session::flash('danger', ' Rejected Successfully!' );
        return redirect()->back();

    }

    public function approve(Request $request)
    {
        $id=$request->segment(4);
        $apply_career= Apply_Career::where('id',$id)->first();
        Apply_Career::where('id',$id)->update(array( 'status' => 1, ));
        Mail::to($apply_career->user->email)->send(new ApproveJob());
        Session::flash('success', ' Approved Successfully!' );
        return redirect()->back();
    }
}
