<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Company_account;
use Session;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\Reservation;
use App\Instructor;
use App\CourseDates;
use App\Matirial;
use Response;
use App\Killsheet;
use App\Kill_sheet_candidates;
use App\Career;
use App\Apply_Career;
use App\Mail\Register;
use App\Notifications\ReservationNotification;
use Illuminate\Support\Facades\Mail;
use App\User_Role;
use App\Role;
use App\UserExam;
use App\Http\Controllers\ActivitesController;
use App\Notifications\ApplyJob;
use App\Feedback;
use App\Mail\Contact_Us;
use App\Candidate_attendance;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response

     */
    public function __construct()
    {
       
         $this->middleware('SharedVariables');
    }
    public function index()
    {
        $data['users'] = User::orderBy('id', 'desc')->paginate(20);
        return view('back.users.view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['roles']=Role::orderBy('id', 'desc')->get();
        $data['companies']= Company_account::where('trash', '0')->get();
        return view('back.users.add',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ActivitesController $ActivitesController)
    {
     $request->validate([
        'name' => 'required|required|string|email|max:255|unique:users',
         ]);
       $user= User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),

        ]);
        if (isset($request->admin)&& !empty($request->admin))
        {
            $user->admin                    = $request->admin;
        }
        else
        {
            $user->admin                    = 0;
        }
        if (isset($request->instructor)&& !empty($request->instructor))
        {
            $user->instructor                    = $request->instructor;
        }
        else
        {
            $user->instructor                    = 0;
        }

        if (isset($request->employe)&& !empty($request->employe))
        {
            $user->employe                    = $request->employe;
        }
        else
        {
            $user->employe                    = 0;
        }

        if (isset($request->candidate)&& !empty($request->candidate))
        {
            $user->candidate                    = $request->candidate;
        }
        else
        {
            $user->candidate                    = 0;
        }

        if (isset($request->company_id)&& !empty($request->company_id))
        {
           $user->company_id                    = $request->company_id;
        }
        else
        {
            $user->company_id                    = 0;
        }

        $user->save();
        if ($request->role_id != 0) {
            $role                           = new User_Role;
            $role->user_id                  = $user->id;
            $role->role_id                  = $request->role_id;
            $role->save();
        }

        $ActivitesController->store(auth()->user()->id ,'Created','User',$user->id);

        auth()->user()->notify(new ReservationNotification);

        $user->pass=$request->password;
        Mail::to($user->email)->send(new Register($user));
        return redirect()->route('users.create')->with('success',  $request->name_en ." Created Successfully!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['user']= User::find($id);
        $data['user_role']=User_Role::where('user_id',$id)->first();
        $data['role_count']=User_Role::where('user_id',$id)->count();
        $data['companies']= Company_account::where('trash', '0')->get();
        $data['roles']=Role::orderBy('id', 'desc')->get();
        // var_dump($data['user_role']);die();
        return view('back.users.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ActivitesController $ActivitesController)
    {
            $user                           = User::find($id);
            $user->name                     = $request->name;
            $user->email                    = $request->email;
            $user->password                 = Hash::make($request->password);
        if (isset($request->admin)&& !empty($request->admin))
        {
            $user->admin                    = $request->admin;
        }
        else
        {
            $user->admin                    = 0;
        }
        if (isset($request->instructor)&& !empty($request->instructor))
        {
            $user->instructor                    = $request->instructor;
        }
        else
        {
            $user->instructor                    = 0;
        }

        if (isset($request->employe)&& !empty($request->employe))
        {
            $user->employe                    = $request->employe;
        }
        else
        {
            $user->employe                    = 0;
        }

        if (isset($request->candidate)&& !empty($request->candidate))
        {
            $user->candidate                    = $request->candidate;
        }
        else
        {
            $user->candidate                    = 0;
        }

         if (isset($request->company_id)&& !empty($request->company_id))
        {
            $user->company_id                    = $request->company_id;
        }
        else
        {
            $user->company_id                    = 0;
        }
          
            $user->save();
            $ActivitesController->store(auth()->user()->id ,'Updated','User',$user->id);
            if ($request->role_id != 0)
            {
                $check= User_Role::where('user_id',$id)->first();
                if ($check)
                {
                    User_Role::where('user_id',$id)->update(array('user_id'=>$user->id,'role_id'=>$request->role_id));
                }
                else
                {
                    $role                           = new User_Role;
                    $role->user_id                  = $user->id;
                    $role->role_id                  = $request->role_id;
                    $role->save();
                }
               
            }
            
          
            $edited_area = $user;
            Session::flash('info',  $user->name_en .' Updated Successfully!' );
            // return view('back.content.categories.edit');
            return redirect('/admin/users/'.$edited_area->id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,ActivitesController $ActivitesController)
    {
       $user = User::find($id);
       $user->delete();
       $ActivitesController->store(auth()->user()->id ,'Destroy','User',$id);
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }


    public function delete()
    {
       return view('back.confirm_delete');
    }


    public function trash(Request $request,ActivitesController $ActivitesController)
    {
      $id=$request->segment(4);
      $trash =$request->segment(5);
      if ($trash==1) {
         User::where('id',$id)->update(array( 'trash' => 0, ));
         $ActivitesController->store(auth()->user()->id ,'UNTrashed','User',$id);
         Session::flash('success', ' UNTrashed Successfully!' );
         return redirect()->back();
        
      }
      else
      {
         User::where('id',$id)->update(array( 'trash' => 1, ));
         $ActivitesController->store(auth()->user()->id ,'Trashed','User',$id);
         Session::flash('warning', ' Trashed Successfully!' );
         return redirect()->back();
      }

    }

    public function view_profile()
    {
         return view('front.pages.view_profile');
    }

    public function edit_profile(Request $request)
    {
            // dd('aa');
            $id=Auth::user()->id;
            $user                             = User::find($id);
            $user->name                       = $request->name;
            $user->email                      = $request->email;
            $user->phone                      =$request->phone;
            $user->lang                       =$request->lang;
            $user->address                    =$request->address;
            $user->country                    =$request->country;
            $user->latitude                   =$request->latitude;
            $user->longitude                  =$request->longitude;
       
        if (isset($request->instructor)&& !empty($request->instructor))
        {
            $user->instructor                    = $request->instructor;
        }
        else
        {
            $user->instructor                    = 0;
        }

       
        if (isset($request->candidate)&& !empty($request->candidate))
        {
            $user->candidate                    = $request->candidate;
        }
        else
        {
            $user->candidate                    = 0;
        }

  
    
            $user->save();
            $edited_area = $user;
            Session::flash('success',  $user->name_en .' Personal Profile Info Updated Successfully!' );
            return redirect()->back();
    }


    public function update_password()
    {
         return view('front.pages.update_password');
    }
    public function edit_password(Request $request)
    {
         $id=Auth::user()->id;
         $user                             = User::find($id);
         $user->password                   = Hash::make($request->password);
         $user->save();
        $edited_area = $user;
        Session::flash('success', ' Password Updated Successfully!' );
        return redirect()->back();
    }
 

    public function view_user_course()
    {
        $id=Auth::user()->id;
      
        if (Auth::user()->candidate==1)
        {
           $data['user_courses'] = Reservation::orderBy('id', 'desc')->where('user_id',$id)->with('course_schedule')->get();
           foreach ($data['user_courses'] as $key)
           {
                $key->instructor = Instructor::orderBy('id', 'desc')->where('id',$key->course_schedule->instructor_id)->first();
           }
          
           return view('front.pages.user_candidate',$data); 
        }
        if (Auth::user()->instructor==1)
        {
            $data['user_courses'] = Reservation::orderBy('id', 'desc')->where('user_id',$id)->with('course_schedule')->get();
            foreach ($data['user_courses'] as $key)
           {
                $key->instructor = Instructor::orderBy('id', 'desc')->where('id',$key->course_schedule->instructor_id)->first();
           }
           return view('front.pages.user_instructor',$data); 
        }
      
        
    }

    public function course_dates()
    {
       $course_id=$_GET['course_id'];
       $schedule_id=$_GET['schedule_id'];
       $data['lang']=$_GET['lang'];
       $data['past_dates'] = CourseDates::orderBy('id', 'desc')->where('course_id',$course_id)->where('course_schedule_id',$schedule_id)->where('date','<',date("Y-m-d"))->get();
       $data['past_count']=CourseDates::orderBy('id', 'desc')->where('course_id',$course_id)->where('course_schedule_id',$schedule_id)->count();
     
       $data['coming_dates'] = CourseDates::orderBy('id', 'desc')->where('course_id',$course_id)->where('course_schedule_id',$schedule_id)->where('date','>',date("Y-m-d"))->get();
       $data['coming_count']=CourseDates::orderBy('id', 'desc')->where('course_id',$course_id)->where('course_schedule_id',$schedule_id)->count();
       // var_dump($data['coming_count']);die();
       return view('front.pages.course_dates',$data); 
    }
 

    public function course_matirial()
    {
       $course_id=$_GET['course_id']; 
       $data['lang']=$_GET['lang'];
       $data['matirials'] = Matirial::orderBy('id', 'desc')->where('course_id',$course_id)->get();
       return view('front.pages.course_matirial',$data); 
    }


    public function getDownload(Request $request) 
    {
        $id=$request->segment(3);
        // var_dump($id);die();
        //PDF file is stored under project/public/download/info.pdf
        $matirial= Matirial::where('id',$id)->first();
        Matirial::where('id',$id)->update(array( 'download' => 1, ));
        // var_dump($matirial);die();
        $file= public_path().'/assets/back/upload/'.$matirial->matirial;

        $headers = array(
                  'Content-Type: application/pdf',
                );

        return Response::download($file, $matirial->matirial, $headers);
    }

    public function view_kill_sheet()
    {
       $course_id=$_GET['course_id'];
       $data['course_id']=$course_id;
       $instructor=Auth::user()->id;
       $data['lang']=$_GET['lang'];
       $data['kill_sheets'] = Killsheet::orderBy('id', 'desc')->where('course_id',$course_id)->where('instructor_id',$instructor)->get();
       return view('front.pages.view_kill_sheet',$data);
    }
    public function add_kill_sheet()
    {
        $course_id=$_GET['course_id'];
        $data['course_id']=$course_id;
       return view('front.pages.add_kill_sheet',$data);
    }

    public function store_kill_sheet(Request $request)
    {

        $kill                           = new Killsheet;
        $kill->name_en                  = $request->name_en;
        $kill->name_ar                  = $request->name_ar;
        $kill->course_id                = $request->course_id;
        $kill->instructor_id            = auth()->user()->id;
        $kill->create_by                = auth()->user()->id;
        $kill->update_by                = auth()->user()->id;
        $kill->save();
        Session::flash('success', 'kill Sheet Added Successfully!' );
        return redirect()->back();
    }

    public function del(Request $request)
    {
       $id=$request->segment('3');
       $kill = Killsheet::find($id);
       $kill->delete();
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }

    public function edit_kill_sheet(Request $request)
    {
        $id=$_GET['id'];
        $data['kill'] = Killsheet::find($id);
         return view('front.pages.edit_kill_sheet',$data);
    }

    public function date(Request $request)
    {
       $course_id=$request->segment(3);
       $schedule_id=$request->segment(4);
       $data['past_dates'] = CourseDates::orderBy('id', 'desc')->where('course_id',$course_id)->where('course_schedule_id',$schedule_id)->where('date','<',date("Y-m-d"))->get();
     
       $data['coming_dates'] = CourseDates::orderBy('id', 'desc')->where('course_id',$course_id)->where('course_schedule_id',$schedule_id)->where('date','>',date("Y-m-d"))->get();
       return view('front.pages.instructor_dates',$data);
    }


    public function kill_can()
    {
        $course_id=$_GET['course_id'];
        $data['lang']=str_replace(" ", "", $_GET['lang']);
        // var_dump($data['lang']);
        $schedule_id=$_GET['schedule_id'];
        $instructor_id=Auth::user()->id;
        // var_dump($course_id);die();
        $data['user_schedule'] = Reservation::orderBy('id', 'desc')->where('user_id',$instructor_id)->where('schedule_id',$schedule_id)->get();
        foreach ($data['user_schedule'] as $user_course) {
          $user_course->user= User::orderBy('id', 'desc')->where('id',$user_course->user_id)->first();
        }
        $data['kill_sheets']=Killsheet::orderBy('id', 'desc')->where('instructor_id',$instructor_id)->where('course_id',$course_id)->get();
        // var_dump($data['lang']);die();
        return view('front.pages.kill_sheet_candidates',$data);
    }
    public function kill_can_store(Request $request)
    {
        $kill                           = new Kill_sheet_candidates;
        $kill->value                    = $request->value;
        $kill->kill_sheet_id            = $request->kill_sheet_id;
        $kill->user_id                  = $request->user_id;
       
        $kill->create_by                = auth()->user()->id;
        $kill->update_by                = auth()->user()->id;
        $kill->save();
        Session::flash('success', 'kill Sheet candidates Added Successfully!' );
        return redirect()->back();
    }

    public function view_kill_can()
    {
        $course_id=$_GET['course_id'];
        $schedule_id=$_GET['schedule_id'];
        $instructor_id=Auth::user()->id;
        $data['kill_sheets']=Killsheet::orderBy('id', 'desc')->where('instructor_id',$instructor_id)->where('course_id',$course_id)->get();

        $data['user_schedule'] = Reservation::orderBy('id', 'desc')->where('user_id',$instructor_id)->where('schedule_id',$schedule_id)->get();
        foreach ($data['user_schedule'] as $user_course) {
          $user_course->user= User::orderBy('id', 'desc')->where('id',$user_course->user_id)->first();
        }
        
        return view('front.pages.instructor_kill_sheet',$data);
    }

    public function user_rate()
    {
        $course_id=$_GET['course_id'];
        $schedule_id=$_GET['schedule_id'];
        $instructor_id=Auth::user()->id;
        $data['kill_sheets']=Killsheet::orderBy('id', 'desc')->where('instructor_id',$instructor_id)->where('course_id',$course_id)->get();

        return view('front.pages.user_rate',$data); 
    }


    public function applay()
    {
        $id=$_GET['id'];
        // var_dump($id);die();
        $data['career']=Career::where('id',$id)->where('trash','0')->first();
        return view('front.pages.applay_for_job',$data);
    }

    public function applay_store(Request $request)
    {
             $request->validate([
            'file' => 'required',
             ]);
            $Apply_Career                            = new Apply_Career;
            $Apply_Career->user_id                   = auth()->user()->id;;
            $Apply_Career->career_id                 = $request->career_id;

            if ($request->hasFile('file'))
            {
                $image = $request->file('file');
                $name =time().$image->getClientOriginalName();
                $destinationPath = public_path('/assets/back/upload/');
                $imagePath = $destinationPath. "/".  $name;
                $image->move($destinationPath, $name);
                $Apply_Career->file = $name;
            }
 

            $Apply_Career ->save();
            $users=User::where('admin','1')->get();
            foreach ($users as $user) {
               $user->notify(new ApplyJob);
            }
            
            Session::flash('Reservation', ' Applay Done Successfully!' );
            Session::flash('Reserv', 'تم  التقدم بنجاح ' );
            return redirect()->back();
    }

    public function user_exams(Request $request)
    {
        $id=$request->segment('4'); 
        $data['users'] = UserExam::orderBy('id', 'desc')->where('User_ID',$id)->paginate(20);
        return view('back.users.user_exam',$data);
    }

   public function switchLang($lang)
   {
      // so we can redir back to where the user was
      $url   = url()->previous();
      $url_explode = explode("/",$url);
      $url_explode[3] = $lang;
      $redir = implode('/',$url_explode);

      return redirect($redir);
   }

   public function course_feedback()
   {
       $course_id=$_GET['course_id']; 
       $schdule_id=$_GET['schdule_id'];
       $data['course_id']=$course_id;
       $data['schdule_id']=$schdule_id;
       $data['lang']=$_GET['lang'];
       return view('front.pages.course_feedback',$data); 
    
   }

   public function feedback_course_store(Request $request)
   {
        $Feedback                      = new Feedback;
        $Feedback->email               =$request->email;
        $Feedback->feedback            =$request->feedback;
        $Feedback->mobile              =$request->mobile;
        $Feedback->type                ="course";
        $Feedback->rate                =$request->rate;
        $Feedback->instructor_rate     =$request->instructor_rate;
        $Feedback->schedule_id         =$request->schdule_id;
        $Feedback->course_id           =$request->course_id;
        $Feedback->user_id             =auth()->user()->id;
        $Feedback->save();
        
        Mail::to($Feedback->email)->send(new Contact_Us($Feedback));
        Session::flash('Reservation', ' Feedback Send Successfully!' );
        return redirect()->back();
   }

   public function instructor_candidate_attendances()
   {
       $data['date_id']=$_GET['date_id'];
       $data['course_id']=$_GET['course_id'];  
       $data['lang']=$_GET['lang'];
       $data['schedule_id']=$_GET['schedule_id'];
       $schedule_id=$data['schedule_id'];
       $course_id=$data['course_id'];
       $data['users'] = Reservation::orderBy('id', 'desc')->where('course_id',$course_id)->where('schedule_id',$schedule_id)->with('user')->get();
       return view('front.pages.instructor_candidate_attendances',$data); 
   }

   public function store_instructor_candidate_attendances(Request $request)
   {
        $candidate_attendances                           = new Candidate_attendance;
        $candidate_attendances->candidate_id             = $request->candidate_id;
        $candidate_attendances->course_date_id           = $request->date_id;
        $candidate_attendances->status                   = $request->status ;
        $candidate_attendances->create_by                = auth()->user()->id;
        $candidate_attendances->update_by                = auth()->user()->id;
        $candidate_attendances->save();
        Session::flash('success', ' Candidate Attendance Created Successfully!' );
        return redirect()->back();
   }

   public function check_right()
   {

        $lang=$_GET['lang'];
        $user_id=$_GET['user_id'];
        $date=$_GET['date_id'];

       $Candidate_attendance= Candidate_attendance::where('candidate_id',$user_id)->where('course_date_id',$date)->first();
       // return $Candidate_attendance;
       if ($Candidate_attendance)
       {
                $Candidate_attendance->candidate_id             = $user_id;
                $Candidate_attendance->course_date_id           = $date;
                $Candidate_attendance->status                   = '1' ;
                $Candidate_attendance->create_by                = auth()->user()->id;
                $Candidate_attendance->update_by                = auth()->user()->id;
                $Candidate_attendance->save();
       }
       else
       {
          
            $candidate_attendances                           = new Candidate_attendance;
            $candidate_attendances->candidate_id             = $user_id;
            $candidate_attendances->course_date_id           = $date;
            $candidate_attendances->status                   = '1' ;
            $candidate_attendances->create_by                = auth()->user()->id;
            $candidate_attendances->update_by                = auth()->user()->id;
            $candidate_attendances->save();
       }
   
   }

   public function check_false()
   {

        $lang=$_GET['lang'];
        $user_id=$_GET['user_id'];
        $date=$_GET['date_id'];

       $Candidate_attendance= Candidate_attendance::where('candidate_id',$user_id)->where('course_date_id',$date)->first();
       // return $Candidate_attendance;
       if ($Candidate_attendance)
       {
                $Candidate_attendance->candidate_id             = $user_id;
                $Candidate_attendance->course_date_id           = $date;
                $Candidate_attendance->status                   = '0' ;
                $Candidate_attendance->create_by                = auth()->user()->id;
                $Candidate_attendance->update_by                = auth()->user()->id;
                $Candidate_attendance->save();
       }
       else
       {
          
            $candidate_attendances                           = new Candidate_attendance;
            $candidate_attendances->candidate_id             = $user_id;
            $candidate_attendances->course_date_id           = $date;
            $candidate_attendances->status                   = '0' ;
            $candidate_attendances->create_by                = auth()->user()->id;
            $candidate_attendances->update_by                = auth()->user()->id;
            $candidate_attendances->save();
       }
   
   }


}



