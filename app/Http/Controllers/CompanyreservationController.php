<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company_reservation;
use App\User;
use App\Course;
use App\Hotel;
use App\Restaurant;
use App\Course_schedule;
use Illuminate\Support\Facades\Hash;
use Session;
use App\Company_account;
use App\Mail\Reserv;
use Illuminate\Support\Facades\Mail;
use App\Notifications\ReservationNotification;
use App\Http\Controllers\ActivitesController;
use App\Reservation;
use App\Mail\Register;
use App\Instructor;
use App\Branch;
use App\Exam;
use App\CourseDates;
use App\Accounting;
use App\CategoryAccounting; 

class CompanyreservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['reservations'] = Company_reservation::orderBy('id', 'desc')->with('user')->with('course_schedule')->with('company')->paginate(10);
        return view('back.company_reservations.view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
        $data['courses']=Course::orderBy('id', 'desc')->with('subject')->where('trash','0')->get();
        return view('back.company_reservations.add',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ActivitesController $ActivitesController)
    {
        $user_email=User::where('email',$request->email)->first();
        $schedule =Course_schedule::where('id',$request->from)->first();
        $course=Course::where('id',$request->course_id)->first();
        if ($user_email)
        {
            $company=Company_account::where('user_id',$user_email->id)->first();
            $Reservation                            = new Company_reservation;
            $Reservation->company_id                = $company->id;
            $Reservation->course_id                 = $request->course_id;
            $Reservation->status                    = "0";
            $Reservation->candidate_number          = $request->candidate_number;
            $Reservation->fees                      = $course->price;
            $Reservation->discount                  = '';
            $Reservation->create_by                 = auth()->user()->id;
            $Reservation->update_by                 = auth()->user()->id;
            $Reservation ->save();

            $user=User::where('id',$user_email->id)->first();
            $course= Course::where('id',$request->course_id)->first();
            $user->subject=$course->subject->{'name_'.$user_email->lang};
            $user->schedule=$schedule->{'schedule_'.$user_email->lang};
            $user->from=$schedule->from;
            $user->to=$schedule->to;
            // var_dump($user);die();
            Mail::to($user->email)->send(new Reserv($user));
            $ActivitesController->store(auth()->user()->id ,'Created','Company_reservation',$Reservation->id);
            auth()->user()->notify(new ReservationNotification);
        }
        else
        {
             $user= User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),

            ]);
            if (isset($request->admin)&& !empty($request->admin))
            {
                $user->admin                    = $request->admin;
            }
            else
            {
                $user->admin                    = 0;
            }
            if (isset($request->instructor)&& !empty($request->instructor))
            {
                $user->instructor                    = $request->instructor;
            }
            else
            {
                $user->instructor                    = 0;
            }

            if (isset($request->employe)&& !empty($request->employe))
            {
                $user->employe                    = $request->employe;
            }
            else
            {
                $user->employe                    = 0;
            }

            if (isset($request->candidate)&& !empty($request->candidate))
            {
                $user->candidate                    = $request->candidate;
            }
            else
            {
                $user->candidate                    = 0;
            }

            if (isset($request->company_id)&& !empty($request->company_id))
            {
                $user->company_id                    = $request->company_id;
            }
            else
            {
                $user->company_id                    = 0;
            }

            $user->save();

            $company_account                           = new CompanyAccount;
            $company_account->user_id                  = $user->id;
            $company_account->name_en                  = $request->name_en;
            $company_account->name_ar                  = $request->name_ar;
            $company_account->address                  = $request->address;
            $company_account->contact_name             = $request->contact_name;
            $company_account->create_by                = auth()->user()->id;
            $company_account->update_by                = auth()->user()->id;

            $company_account->save();


            $Reservation                            = new Company_reservation;
            $Reservation->company_id                = $company_account->id;
            $Reservation->course_id                 = $request->course_id;
            $Reservation->status                    = "0";
            $Reservation->candidate_number          = $request->candidate_number;
            $Reservation->fees                      = $course->price;
            $Reservation->discount                  = '';
            $Reservation->create_by                 = auth()->user()->id;
            $Reservation->update_by                 = auth()->user()->id;
            $Reservation->save();

            $user=User::where('id',$user->id)->first();
            $course= Course::where('id',$request->course_id)->first();
            $user->subject=$course->subject->{'name_'.$user->lang};
            $user->schedule=$schedule->{'schedule_'.$user->lang};
            $user->from=$schedule->from;
            $user->to=$schedule->to;
            Mail::to($user->email)->send(new Reserv($user));
            $ActivitesController->store(auth()->user()->id ,'Updated','Company_reservation',$Reservation->id);
            auth()->user()->notify(new ReservationNotification);
        }
       
        Session::flash('success', ' Reservation Done Successfully!' );
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['reserve']= Company_reservation::find($id);
        $data['courses']=Course::orderBy('id', 'desc')->with('subject')->where('trash','0')->get();
        return view('back.company_reservations.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ActivitesController $ActivitesController)
    {
        $course=Course::where('id',$request->course_id)->first();
        $Reservation                            = Company_reservation::find($id);
        $Reservation->course_id                 = $request->course_id;
        $Reservation->candidate_number          = $request->candidate_number;
        $Reservation->fees                      = $course->price;
        $Reservation->discount                  = $request->discount;
        $Reservation->create_by                 = auth()->user()->id;
        $Reservation->update_by                 = auth()->user()->id;
        $Reservation->save();
        $ActivitesController->store(auth()->user()->id ,'Updated','Company_reservation',$Reservation->id);
        $edited_area = $Reservation;
        Session::flash('info',  $Reservation->name_en .' Updated Successfully!' );
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,ActivitesController $ActivitesController)
    {
        // var_dump($id);die();
       $company_account = Company_reservation::find($id);
       $company_account->delete();
       $ActivitesController->store(auth()->user()->id ,'Destroy','Company_reservation',$id);
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }


    public function trash(Request $request,ActivitesController $ActivitesController)
    {
      $id=$request->segment(4);
      $trash =$request->segment(5);
      if ($trash==1) {
         Company_reservation::where('id',$id)->update(array( 'trash' => 0, ));
          $ActivitesController->store(auth()->user()->id ,'UNTrashed','Company_reservation',$id);
         Session::flash('untrash', ' UNTrashed Successfully!' );
         return redirect()->back();
        
      }
      else
      {
         Company_reservation::where('id',$id)->update(array( 'trash' => 1, ));
          $ActivitesController->store(auth()->user()->id ,'Trashed','CompanyAccount',$id);
         Session::flash('warning', ' Trashed Successfully!' );
         return redirect()->back();
      }

    }


    public function delete()
    {
       return view('back.confirm_delete');
    }

    public function company_search()
    {
         $email=$_GET['email'];
         $data['user']= User::where('email',$email)->first();
        
         if ($data['user'])
         {
             $data['company']=Company_account::where('user_id',$data['user']->id)->first();
             return view('back.company_reservations.company_user',$data);
         }
         else
         {
            return view('back.company_reservations.new_reservation');
         }
       
    }

    public function change_status()
    {
        $id= $_GET['id'];
        $data['company_reservation']=Company_reservation::where('id',$id)->first();
        return view('back.company_reservations.status',$data);
    }

    public function change_status_save(Request $request)
    {
        // var_dump($request->status);die();
         Company_reservation::where('id',$request->id)->update(array( 'status' =>$request->status, ));
         Session::flash('untrash', ' Status Changed Successfully!' );
         return redirect()->back();
    }

    public function status(Request $request,ActivitesController $ActivitesController)
    {
        $id=$request->segment(4);
        $status =$request->segment(5);
        $reseve_company=Company_reservation::where('id',$id)->first();
        if ($status==1) {
         Company_reservation::where('id',$id)->update(array( 'status' => 1, ));
         $ActivitesController->store(auth()->user()->id ,'Company_reservation Status Change To Lead','Company_reservation',$id);
         Session::flash('success', ' Company_reservation Lead Successfully!' );
         return redirect()->back();

        }
        elseif ($status==2) {
         Company_reservation::where('id',$id)->update(array( 'status' => 2, ));
         $ActivitesController->store(auth()->user()->id ,'Company_reservation Status Change To Trasnfer To Accounting','Company_reservation',$id);
         Session::flash('success', ' Company_reservation Trasnfer To Accounting Successfully!' );
         return redirect()->back();
        }
        else
        {
         $users = User::where('company_id',$id)->get();
         if ($users)
         {
            foreach ($users as $user) {
                    $Reservation                            = new Reservation;
                    $Reservation->user_id                   = $user->id;
                    $Reservation->course_id                 = $reseve_company->course_id;
                    $Reservation->status                    = "3";
                    $Reservation->private                   = '1';
                    $Reservation->schedule_id               = '0';
                    $Reservation->resturant_id              = '1';
                    $Reservation->hotel_id                  = '1';
                    $Reservation->fees                      = '0';
                    $Reservation->company_id                = $id;
                    $Reservation->create_by                 = auth()->user()->id;
                    $Reservation->update_by                 = auth()->user()->id;
                    $Reservation ->save();
                    $ActivitesController->store(auth()->user()->id ,'Created','Reservation',$Reservation->id);
            }
           
         }
         $reserve=Company_reservation::where('id',$id)->first();
         Company_reservation::where('id',$id)->update(array( 'status' => 3, ));
            $Accounting                           = new Accounting;
            $Accounting->amount                   = '+'.$reserve->fees;
            $Accounting->category_id              = '6';
            $Accounting->foreign_id               = $reserve->user_id;
            $Accounting->confirm                  = '1';
            $Accounting->user_id                  = $reserve->company_id;
            $Accounting->create_by                = auth()->user()->id;
            $Accounting->update_by                = auth()->user()->id;
            $Accounting->save();
         $ActivitesController->store(auth()->user()->id ,'Company_reservation Status Change To Done','Company_reservation',$id);
         Session::flash('success', ' Company_reservation Done Successfully!' );
         return redirect()->back();
        }
    }

    public function users(Request $request)
    {
        $company_id=$request->segment('4');
        $data['users']=User::where('company_id',$company_id)->where('trash','0')->paginate(10);
        return view('back.company_reservations.users_company',$data);

    }

    public function user_add(Request $request)
    {
        return view('back.company_reservations.add_user');
    }

    public function user_save(Request $request,ActivitesController $ActivitesController)
    {
        $company_id=$request->segment('4');
        $user= User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'company_id'=>$company_id,
            'candidate'=>'1',
            


        ]);
        $user->save();
        $ActivitesController->store(auth()->user()->id ,'Created','User',$user->id);

        auth()->user()->notify(new ReservationNotification);

        $user->pass=$request->password;
        Mail::to($user->email)->send(new Register($user));
        Session::flash('success', 'User Created Successfully!' );
        return redirect()->back();
    }
    public function schedule_add(Request $request)
    {
      $data['instructor'] = Instructor::where('trash', '0')->get();
      $data['branches'] = Branch::all();
      $data['exams']=Exam::all();
      return view('back.company_reservations.schedule_add',$data);
    }

    public function schedule_save(Request $request)
    {
        $id=$request->segment(4);
        $course_id=$request->segment(5);
        $course                           = new Course_schedule;
        $course->from                     = $request->from;
        $course->to                       = $request->to   ;
        $course->course_id                = $course_id;
        $course->instructor_id            = $request->instruct_id;
        $course->private                  = $request->private;
        $course->schedule_ar              = $request->schedule_ar;
        $course->schedule_en              = $request->schedule_en;
        $course->branch_id                = $request->branch_id;
        $course->exam_id                  = $request->exam_id;
        $course->create_by                = auth()->user()->id;
        $course->update_by                = auth()->user()->id;
        $course->save();

        foreach ($request->dates as $date)
        {
            $dat                           = new CourseDates;
            $dat->date                     = $date;
            $dat->course_id                = $id;
            $dat->course_schedule_id       =$course->id;
            $dat->save();
        }
        $users = User::where('company_id',$id)->get();
        foreach ($users as $user)
        {
           Reservation::where('user_id',$user->id)->update(array( 'schedule_id' => $course->id, ));
        }

        Session::flash('success', 'Schedule Added  Successfully To Company Users!' );
        return redirect()->back();
    }
}
