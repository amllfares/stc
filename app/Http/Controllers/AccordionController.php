<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Accordion;
use Session;
use App\Http\Controllers\ActivitesController;

class AccordionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('back.accordion.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ActivitesController $ActivitesController)
    {
        $related_id=$request->segment('4');
        $accordion                           = new Accordion;
        $accordion->name_en                  = $request->name_en;
        $accordion->name_ar                  = $request->name_ar;
        $accordion->content_en               = $request->content_en;
        $accordion->content_ar               = $request->content_ar;
        $accordion->related_id               = $related_id;
        $accordion->table                    = $request->table;
        $accordion->order                    = $request->order;
        $accordion->create_by                = auth()->user()->id;
        $accordion->update_by                = auth()->user()->id;

        if ($request->hasFile('image'))
        {
            $image = $request->file('image');
            $name ='/assets/back/upload/'.time(). $image->getClientOriginalName();
            $destinationPath = public_path('/assets/back/upload/');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $accordion->image = $name;
        }
        

        $accordion->save();
        $ActivitesController->store(auth()->user()->id ,'Created','Accordion',$accordion->id);

        Session::flash('success',  $accordion->name_en .' Added Successfully!' );
        return redirect('/admin/accordion/create/'.$request->segment('4'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['accordion']= Accordion::find($id);
        return view('back.accordion.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ActivitesController $ActivitesController)
    {
        $related_id=$request->segment('3');
        $accordion                           = Accordion::find($id);
        $accordion->name_en                  = $request->name_en;
        $accordion->name_ar                  = $request->name_ar;
        $accordion->content_en               = $request->content_en;
        $accordion->content_ar               = $request->content_ar;
        $accordion->related_id               = $related_id;
        $accordion->table                    = $request->table;
        $accordion->order                    = $request->order;
        $accordion->create_by                = auth()->user()->id;
        $accordion->update_by                = auth()->user()->id;

        if ($request->hasFile('image'))
        {
            $image = $request->file('image');
            $name ='/assets/back/upload/'.time(). $image->getClientOriginalName();
            $destinationPath = public_path('/assets/back/upload/');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $accordion->image = $name;
        }
        

        $accordion->save();
        $ActivitesController->store(auth()->user()->id ,'Updated','Accordion',$accordion->id);
        $edited_area = $accordion;
        Session::flash('info',  $accordion->name_en .' Updated Successfully!' );
        // return view('back.content.categories.edit');
        return redirect('/admin/accordion/'.$edited_area->id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,ActivitesController $ActivitesController)
    {
        // var_dump($id);die();
       $page = Accordion::find($id);
       $page->delete();
       $ActivitesController->store(auth()->user()->id ,'Destroy','Accordion',$id);
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }


    public function trash(Request $request,ActivitesController $ActivitesController)
    {
      $id=$request->segment(4);
      $trash =$request->segment(5);
      if ($trash==1) {
         Accordion::where('id',$id)->update(array( 'trash' => 0, ));
         $ActivitesController->store(auth()->user()->id ,'UNTrashed','Accordion',$id);
         Session::flash('untrash', ' UNTrashed Successfully!' );
         return redirect()->back();
        
      }
      else
      {
         Accordion::where('id',$id)->update(array( 'trash' => 1, ));
         $ActivitesController->store(auth()->user()->id ,'Trashed','Accordion',$id);
         Session::flash('warning', ' Trashed Successfully!' );
         return redirect()->back();
      }

    }


    public function delete()
    {
       return view('back.confirm_delete');
    }
}
