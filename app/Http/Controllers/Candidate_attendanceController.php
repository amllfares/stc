<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Candidate_attendance;
use Session;
use App\User;
use App\CourseDates;
use App\Http\Controllers\ActivitesController;

class Candidate_attendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['candidate_attendances'] = Candidate_attendance::orderBy('id', 'desc')->with('user')->with('course_date')->paginate(10);

        return view('back.candidate_attendance.view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['users']=User::where('trash','0')->where('candidate','1')->get();
        $data['course_dates']=CourseDates::where('trash','0')->get();
        return view('back.candidate_attendance.add',$data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ActivitesController $ActivitesController)
    {
        $candidate_attendances                           = new Candidate_attendance;
        $candidate_attendances->candidate_id             = $request->candidate_id;
        $candidate_attendances->course_date_id           = $request->course_date_id;
        $candidate_attendances->status                   = $request->status ;
        $candidate_attendances->create_by                = auth()->user()->id;
        $candidate_attendances->update_by                = auth()->user()->id;

        $candidate_attendances->save();
        $ActivitesController->store(auth()->user()->id ,'Created','Candidate_attendance',$candidate_attendances->id);
        return redirect()->route('candidate_attendances.create')->with('success',"candidate_attendance Created Successfully!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['users']=User::where('trash','0')->where('candidate','1')->get();
        $data['course_dates']=CourseDates::where('trash','0')->get();
        $data['candidate_attendance']= Candidate_attendance::where('id',$id)->with('user')->first();
        return view('back.candidate_attendance.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ActivitesController $ActivitesController)
    {
        $candidate_attendances                           = Candidate_attendance::find($id);
        $candidate_attendances->candidate_id             = $request->candidate_id;
        $candidate_attendances->course_date_id           = $request->course_date_id;
        $candidate_attendances->status                   = $request->status ;
        $candidate_attendances->create_by                = auth()->user()->id;
        $candidate_attendances->update_by                = auth()->user()->id;

        $candidate_attendances->save();
        $ActivitesController->store(auth()->user()->id ,'Updated','Candidate_attendance',$candidate_attendances->id);
        $edited_area = $candidate_attendances;
        Session::flash('info', 'candidate_attendance Updated Successfully!' );
        // return view('back.content.categories.edit');
        return redirect('/admin/candidate_attendances/'.$edited_area->id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,ActivitesController $ActivitesController)
    {
        // var_dump($id);die();
       $Candidate_attendance = Candidate_attendance::find($id);
       $Candidate_attendance->delete();
       $ActivitesController->store(auth()->user()->id ,'Destroy','Candidate_attendance',$branche->id);
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }


    public function trash(Request $request,ActivitesController $ActivitesController)
    {
      $id=$request->segment(4);
      $trash =$request->segment(5);
      if ($trash==1) {
         Candidate_attendance::where('id',$id)->update(array( 'trash' => 0, ));
         $ActivitesController->store(auth()->user()->id ,'UNTrashed','Candidate_attendance',$branche->id);
         Session::flash('untrash', ' UNTrashed Successfully!' );
         return redirect()->back();
        
      }
      else
      {
         Candidate_attendance::where('id',$id)->update(array( 'trash' => 1, ));
         $ActivitesController->store(auth()->user()->id ,'Trashed','Candidate_attendance',$branche->id);
         Session::flash('warning', ' Trashed Successfully!' );
         return redirect()->back();
      }

    }


    public function delete()
    {
       return view('back.confirm_delete');
    }
}
