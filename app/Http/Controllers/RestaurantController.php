<?php

namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Restaurant;
use Session;
use App\Activity;
use App\Http\Controllers\ActivitesController;
class RestaurantController extends Controller 
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['restaurants'] = Restaurant::orderBy('id', 'desc')->paginate(10);
        return view('back.restaurant.view',$data); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back.restaurant.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ActivitesController $ActivitesController)
    {

        $Restaurant                           = new Restaurant;
        $Restaurant->name_en                  = $request->name_en;
        $Restaurant->name_ar                  = $request->name_ar;
        $Restaurant->contact_num              = $request->contact_num;
        $Restaurant->contact_title            = $request->contact_title;
        $Restaurant->address_en               = $request->address_en;
        $Restaurant->address_ar               = $request->address_ar;
        $Restaurant->price_per_day            = $request->price_per_day;
        $Restaurant->create_by                = auth()->user()->id;
        $Restaurant->update_by                = auth()->user()->id;

        if ($request->hasFile('logo'))
        {
        $image = $request->file('logo');
        $name ='/assets/back/upload/'.time(). $image->getClientOriginalName();
        $destinationPath = public_path('/assets/back/upload/');
        $imagePath = $destinationPath. "/".  $name;
        $image->move($destinationPath, $name);
        $Restaurant->logo = $name;
        }

        $Restaurant->save();
        $ActivitesController->store(auth()->user()->id ,'Created','Restaurant',$Restaurant->id);
        return redirect()->route('restaurants.create')->with('success',  $request->name_en ." Created Successfully!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['restaurant']= Restaurant::find($id);
        return view('back.restaurant.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ActivitesController $ActivitesController)
    {
        $Restaurant                           = Restaurant::find($id);
        $Restaurant->name_en                  = $request->name_en;
        $Restaurant->name_ar                  = $request->name_ar;
        $Restaurant->contact_num              = $request->contact_num;
        $Restaurant->contact_title            = $request->contact_title;
        $Restaurant->address_en               = $request->address_en;
        $Restaurant->address_ar               = $request->address_ar;
        $Restaurant->price_per_day            = $request->price_per_day;
        $Restaurant->create_by                = auth()->user()->id;
        $Restaurant->update_by                = auth()->user()->id;

        if ($request->hasFile('logo'))
        {
        $image = $request->file('logo');
        $name ='/assets/back/upload/'.time(). $image->getClientOriginalName();
        $destinationPath = public_path('/assets/back/upload/');
        $imagePath = $destinationPath. "/".  $name;
        $image->move($destinationPath, $name);
        $Restaurant->logo = $name;
        }

        $Restaurant->save();
        $ActivitesController->store(auth()->user()->id ,'Updated','Restaurant',$Restaurant->id);
        $edited_area = $Restaurant;
        Session::flash('info',  $Restaurant->name_en .' Updated Successfully!' );
        // return view('back.content.categories.edit');
        return redirect('/admin/restaurants/'.$edited_area->id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,ActivitesController $ActivitesController)
    {
       $Restaurant = Restaurant::find($id);
       $Restaurant->delete();
       $ActivitesController->store(auth()->user()->id ,'Destroy','Restaurant',$id);
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }



    public function trash(Request $request,ActivitesController $ActivitesController)
    {
      $id=$request->segment(4);
      $trash =$request->segment(5);
      if ($trash==1) {
         Restaurant::where('id',$id)->update(array( 'trash' => 0, ));
         $ActivitesController->store(auth()->user()->id ,'UNTrashed','Restaurant',$id);
         Session::flash('untrash', ' UNTrashed Successfully!' );
         return redirect()->back();
        
      }
      else
      {
         Restaurant::where('id',$id)->update(array( 'trash' => 1, ));
         $ActivitesController->store(auth()->user()->id ,'Trashed','Restaurant',$id);
         Session::flash('warning', ' Trashed Successfully!' );
         return redirect()->back();
      }

    }


    public function delete()
    {
       
       return view('back.confirm_delete');
    }



}
