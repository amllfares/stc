<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use App\Role;
use App\Http\Controllers\ActivitesController;
use App\Section;
use App\Permiation;
use App\Role_Permission;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {
        $data['roles'] = Role::orderBy('id', 'desc')->paginate(10);
        return view('back.roles.view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back.roles.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ActivitesController $ActivitesController)
    {
       
        $request->create_by=auth()->user()->id;
        $request->update_by=auth()->user()->id;
        $role=Role::create(array_except($request->all(),['_token']));
        $ActivitesController->store(auth()->user()->id ,'Created','Role',$role->id);
       
        Session::flash('success',  $role->name.' Role Created Successfully!' );
        return redirect('/admin/roles/'.$role->id.'/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $section=array();
        $permiation=array();
        $data['role']= Role::find($id);
        $data['sections']=Section::where('sub_of','0')->get();
        $data['permiations'] = Permiation::orderBy('id', 'desc')->get();
       
        return view('back.roles.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ActivitesController $ActivitesController)
    {
        $role                           = Role::find($id);
        $role->name                     = $request->name;
        $role->create_by                = auth()->user()->id;
        $role->update_by                = auth()->user()->id;
        $role->save();
        $ActivitesController->store(auth()->user()->id ,'Updated','Role',$role->id);

        $edited_area = $role;
        Session::flash('info',  $role->name.' Updated Successfully!' );
        // return view('back.content.categories.edit');
        return redirect('/admin/roles/'.$edited_area->id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,ActivitesController $ActivitesController)
    {
       $role = Role::find($id);
       $role->delete();
       $ActivitesController->store(auth()->user()->id ,'Destroy','Role',$id);
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }


    public function trash(Request $request,ActivitesController $ActivitesController)
    {
      $id=$request->segment(4);
      $trash =$request->segment(5);
      if ($trash==1) {
         Role::where('id',$id)->update(array( 'trash' => 0, ));
         $ActivitesController->store(auth()->user()->id ,'UNTrashed','Role',$id);
         Session::flash('untrash', ' UNTrashed Successfully!' );
         return redirect()->back();
        
      }
      else
      {
         Role::where('id',$id)->update(array( 'trash' => 1, ));
         $ActivitesController->store(auth()->user()->id ,'Trashed','Role',$id);
         Session::flash('warning', ' Trashed Successfully!' );
         return redirect()->back();
      }

    }


    public function delete()
    {
       return view('back.confirm_delete');
    }
}
