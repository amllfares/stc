<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role_Permission;
use App\Role;
use App\Section;
use Session;
use App\Http\Controllers\ActivitesController;
use App\Permiation;

class PermissionRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['permissions'] = Role_Permission::orderBy('id', 'desc')->with('role')->with('section')->paginate(10);
        return view('back.permissions_role.view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() 
    { 
        $data['roles']=Role::orderBy('id', 'desc')->get();
        $data['sections']=Section::orderBy('id', 'desc')->where('sub_of','0')->paginate(10);
        $data['permiations'] = Permiation::orderBy('id', 'desc')->get();
        // return $data;
        return view('back.permissions_role.add',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ActivitesController $ActivitesController)
    {
        $Permission                           = new Role_Permission;
        $Permission->role_id                  = $request->role_id;
        $Permission->section_id               = $request->section_id;
        $Permission->permission_type          = $request->permission_type;
        $Permission->create_by                = auth()->user()->id;
        $Permission->update_by                = auth()->user()->id;
        $Permission->save();
        $ActivitesController->store(auth()->user()->id ,'Created','Role_Permission',$Permission->id);
       
        return redirect()->route('permissions_role.create')->with('success', "Role Permission Created Successfully!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          $data['permission']= Role_Permission::find($id);
          $data['roles']=Role::orderBy('id', 'desc')->get();
          $data['sections']=Section::orderBy('id', 'desc')->where('sub_of','0')->get();
          return view('back.permissions_role.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ActivitesController $ActivitesController)
    {
        $Permission                           = Role_Permission::find($id);
        $Permission->role_id                  = $request->role_id;
        $Permission->section_id               = $request->section_id;
        $Permission->permission_type          = $request->permission_type;
        $Permission->create_by                = auth()->user()->id;
        $Permission->update_by                = auth()->user()->id;
        $Permission->save();
        $ActivitesController->store(auth()->user()->id ,'Updated','Role_Permission',$Permission->id);
        $edited_area = $Permission;
        Session::flash('info',  'Role Permission Updated Successfully!' );
        return redirect('/admin/permissions_role/'.$edited_area->id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,ActivitesController $ActivitesController)
    {
        // var_dump($id);die();
       $page = Role_Permission::find($id);
       $page->delete();
        $ActivitesController->store(auth()->user()->id ,'Destroy','Role_Permission',$id);
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }


    public function trash(Request $request,ActivitesController $ActivitesController)
    {
      $id=$request->segment(4);
      $trash =$request->segment(5);
      if ($trash==1) {
         Role_Permission::where('id',$id)->update(array( 'trash' => 0, ));
          $ActivitesController->store(auth()->user()->id ,'UNTrashed','Role_Permission',$id);
         Session::flash('untrash', ' UNTrashed Successfully!' );
         return redirect()->back();
        
      }
      else
      {
         Role_Permission::where('id',$id)->update(array( 'trash' => 1, ));
          $ActivitesController->store(auth()->user()->id ,'Trashed','Role_Permission',$id);
         Session::flash('warning', ' Trashed Successfully!' );
         return redirect()->back();
      }

    }


    public function delete()
    {
       return view('back.confirm_delete');
    }

    public function confirm(ActivitesController $ActivitesController)
    {
      
        $role_id=$_GET['role_id'];
        $section_id=$_GET['section_id'];
        $permiation_id=$_GET['permiation_id'];
        if ($permiation_id != 4)
        {
            $check=Role_Permission::where('section_id',$section_id)->where('role_id',$role_id)->first();
        if ($check)
        {
            $check->delete();
            $Permission                           = new Role_Permission;
            $Permission->role_id                  = $role_id;
            $Permission->section_id               = $section_id;
            $Permission->permission_id            = $permiation_id;
            $Permission->create_by                = auth()->user()->id;
            $Permission->update_by                = auth()->user()->id;
            $Permission->save();
            $ActivitesController->store(auth()->user()->id ,'Created','Role_Permission',$Permission->id);
        }
        else
        {
            $Permission                           = new Role_Permission;
            $Permission->role_id                  = $role_id;
            $Permission->section_id               = $section_id;
            $Permission->permission_id            = $permiation_id;
            $Permission->create_by                = auth()->user()->id;
            $Permission->update_by                = auth()->user()->id;
            $Permission->save();
            $ActivitesController->store(auth()->user()->id ,'Created','Role_Permission',$Permission->id);
           
        }
 
        }
        else
        {
            $no=Role_Permission::where('section_id',$section_id)->where('role_id',$role_id)->first();
            $no->delete();
        }
      
       

    }
}
