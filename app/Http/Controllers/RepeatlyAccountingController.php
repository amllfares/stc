<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RepeatlyAccounting;
use Session;
use App\Http\Controllers\ActivitesController;
use App\CategoryAccounting; 
use App\User;

class RepeatlyAccountingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['accounting'] = RepeatlyAccounting::orderBy('id', 'desc')->paginate(10);
        return view('back.repeatly_accounting.view',$data); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['users']=User::where('trash','0')->get();
        $data['category']=CategoryAccounting::orderBy('id', 'desc')->get();
        return view('back.repeatly_accounting.add',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ActivitesController $ActivitesController)
    {
        $Accounting                           = new RepeatlyAccounting;
        $Accounting->amount                   = $request->amount;
        $Accounting->category_id              = $request->category_id;
        $Accounting->foreign_id               = '0';
        $Accounting->extra                    = $request->extra;
        $Accounting->confirm                  = $request->confirm;
        $Accounting->user_id                  = $request->user_id;
        $Accounting->month                    = '0';
        $Accounting->labels                   = $request->labels;
        $Accounting->date                     = $request->date;
        $Accounting->create_by                = auth()->user()->id;
        $Accounting->update_by                = auth()->user()->id;
        $Accounting->save();
        $ActivitesController->store(auth()->user()->id ,'Created','RepeatlyAccounting',$Accounting->id);
        return redirect()->back()->with('success',  $request->name_en ." Created Successfully!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['accounting']= RepeatlyAccounting::find($id);
        $data['users']=User::where('trash','0')->get();
        $data['category']=CategoryAccounting::orderBy('ID', 'desc')->get();
        return view('back.repeatly_accounting.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id ,ActivitesController $ActivitesController)
    {
        $Accounting                           = RepeatlyAccounting::find($id);
        $Accounting->amount                   = $request->amount;
        $Accounting->category_id              = $request->category_id;
        $Accounting->foreign_id               = '0';
        $Accounting->extra                    = $request->extra;
        $Accounting->confirm                  = $request->confirm;
        $Accounting->user_id                  = $request->user_id;
        $Accounting->month                    = '0';
        $Accounting->labels                   = $request->labels;
        $Accounting->date                     = $request->date;
        $Accounting->create_by                = auth()->user()->id;
        $Accounting->update_by                = auth()->user()->id;
        $Accounting->save();
       
        $ActivitesController->store(auth()->user()->id ,'Updated','RepeatlyAccounting',$Accounting->id);
 
        Session::flash('info', ' Repeatly Accounting Updated Successfully!' );
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,ActivitesController $ActivitesController)
    {
       $RepeatlyAccounting = RepeatlyAccounting::find($id);
       $RepeatlyAccounting->delete();
       $ActivitesController->store(auth()->user()->id ,'Destroy','RepeatlyAccounting',$id);
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }

    public function delete()
    {
       return view('back.confirm_delete');
    }
}
