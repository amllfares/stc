<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hotel;
use Session;
use App\Http\Controllers\ActivitesController;

class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['hotels'] = Hotel::orderBy('id', 'desc')->paginate(10);
        return view('back.hotel.view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('back.hotel.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ActivitesController $ActivitesController)
    {
     
        $hotel                           = new Hotel;
        $hotel->name_en                  = $request->name_en;
        $hotel->name_ar                  = $request->name_ar;
        $hotel->email                    = $request->email;
        $hotel->contact_num              = $request->contact_num;
        $hotel->contact_title            = $request->contact_title;
        $hotel->address_en               = $request->address_en;
        $hotel->address_ar               = $request->address_ar;
        $hotel->price_per_day            = $request->price_per_day;
        $hotel->currency                 = $request->currency;
        $hotel->extra                    = "test";
        $hotel->media                    = $request->media;
        $hotel->create_by                = auth()->user()->id;
        $hotel->update_by                = auth()->user()->id;

        $hotel->save();
        $ActivitesController->store(auth()->user()->id ,'Created','Hotel',$hotel->id);

        return redirect()->route('hotels.create')->with('success',  $request->name_en ." Created Successfully!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['hotel']= Hotel::find($id);
        // dd($data);
        return view('back.hotel.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ActivitesController $ActivitesController)
    {
        $hotel                           = Hotel::find($id);
        $hotel->name_en                  = $request->name_en;
        $hotel->name_ar                  = $request->name_ar;
        $hotel->email                    = $request->email;
        $hotel->contact_num              = $request->contact_num;
        $hotel->contact_title            = $request->contact_title;
        $hotel->address_en               = $request->address_en;
        $hotel->address_ar               = $request->address_ar;
        $hotel->price_per_day            = $request->price_per_day;
        $hotel->currency                 = $request->currency;
        $hotel->extra                    = "test";
        $hotel->media                    = $request->media;
        $hotel->create_by                = auth()->user()->id;
        $hotel->update_by                = auth()->user()->id;
        $hotel->save();

        $ActivitesController->store(auth()->user()->id ,'Updated','Hotel',$hotel->id);
        $edited_area = $hotel;
        Session::flash('info',  $hotel->name_en .' Updated Successfully!' );
        // return view('back.content.categories.edit');
        return redirect('/admin/hotels/'.$edited_area->id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,ActivitesController $ActivitesController)
    {
        // var_dump($id);die();
       $hotel = Hotel::find($id);
       $hotel->delete();
       $ActivitesController->store(auth()->user()->id ,'Destroy','Hotel',$id);
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }


    public function trash(Request $request,ActivitesController $ActivitesController)
    {
      $id=$request->segment(4);
      $trash =$request->segment(5);
      if ($trash==1) {
         Hotel::where('id',$id)->update(array( 'trash' => 0, ));
         $ActivitesController->store(auth()->user()->id ,'UNTrashed','Hotel',$id);
         Session::flash('untrash', ' UNTrashed Successfully!' );
         return redirect()->back();
        
      }
      else
      {
         Hotel::where('id',$id)->update(array( 'trash' => 1, ));
         $ActivitesController->store(auth()->user()->id ,'Trashed','Hotel',$id);
         Session::flash('warning', ' Trashed Successfully!' );
         return redirect()->back();
      }

    }


    public function delete()
    {
       return view('back.confirm_delete');
    }

}
