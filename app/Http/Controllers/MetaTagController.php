<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MetaTag;
use Session;
use App\Http\Controllers\ActivitesController;

class MetaTagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['meta_tags'] = MetaTag::orderBy('id', 'desc')->paginate(10);
        return view('back.metatag.view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back.metatag.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ActivitesController $ActivitesController)
    {
        $MetaTag                           = new MetaTag;
        $MetaTag->title                    = $request->title;
        $MetaTag->describtion              = $request->describtion;
        $MetaTag->keyword                  = $request->keyword;

        if ($request->hasFile('image'))
        {
        $image = $request->file('image');
        $name ='/assets/back/upload/'.time(). $image->getClientOriginalName();
        $destinationPath = public_path('/assets/back/upload/');
        $imagePath = $destinationPath. "/".  $name;
        $image->move($destinationPath, $name);
        $MetaTag->image = $name;
        }

        $MetaTag->save();
        $ActivitesController->store(auth()->user()->id ,'Created','MetaTag',$instructor->id);
        return redirect()->route('meta_tags.create')->with('success',  $request->title ." Created Successfully!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['meta_tag']= MetaTag::find($id);
        return view('back.metatag.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ActivitesController $ActivitesController)
    {
        $MetaTag                              = MetaTag::find($id);
        $MetaTag->title                       = $request->title;
        $MetaTag->describtion                 = $request->describtion;
        $MetaTag->keyword                     = $request->keyword    ;

        if ($request->hasFile('image'))
        {
        $image = $request->file('image');
        $name ='/assets/back/upload/'.time(). $image->getClientOriginalName();
        $destinationPath = public_path('/assets/back/upload/');
        $imagePath = $destinationPath. "/".  $name;
        $image->move($destinationPath, $name);
        $MetaTag->image = $name;
        }

        $MetaTag->save();
        $ActivitesController->store(auth()->user()->id ,'Updated','MetaTag',$instructor->id);
        $edited_area = $MetaTag;
        Session::flash('info',  $MetaTag->title .' Updated Successfully!' );
        return redirect('/admin/meta_tags/'.$edited_area->id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,ActivitesController $ActivitesController)
    {
       $MetaTag = MetaTag::find($id);
       $MetaTag->delete();
       $ActivitesController->store(auth()->user()->id ,'Destroy','MetaTag',$instructor->id);
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }



    public function trash(Request $request,ActivitesController $ActivitesController)
    {
      $id=$request->segment(4);
      $trash =$request->segment(5);
      if ($trash==1) {
         MetaTag::where('id',$id)->update(array( 'trash' => 0, ));
         $ActivitesController->store(auth()->user()->id ,'UNTrashed','MetaTag',$id);
         Session::flash('untrash', ' UNTrashed Successfully!' );
         return redirect()->back();
        
      }
      else
      {
         MetaTag::where('id',$id)->update(array( 'trash' => 1, ));
         $ActivitesController->store(auth()->user()->id ,'Trashed','MetaTag',$id);
         Session::flash('warning', ' Trashed Successfully!' );
         return redirect()->back();
      }

    }


    public function delete()
    {
        // var_dump("gdgg");die();
       return view('back.confirm_delete');
    }
}
