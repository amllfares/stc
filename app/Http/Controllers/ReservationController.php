<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reservation;
use App\User;
use App\Course;
use App\Hotel;
use App\Restaurant;
use App\Course_schedule;
use Illuminate\Support\Facades\Hash;
use Session;
use App\Mail\Reserv;
use App\Notifications\ReservationNotification;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\ActivitesController;
use App\Accounting;
use App\CategoryAccounting; 

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['reservations'] = Reservation::orderBy('id', 'desc')->where('private','0')->with('user')->with('course_schedule')->with('hotel')->with('resturant')->paginate(10);

        return view('back.reservations.view',$data);
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['hotels'] = Hotel::orderBy('id', 'desc')->where('trash','0')->get();
        $data['restaurants'] = Restaurant::orderBy('id', 'desc')->where('trash','0')->get();
        $data['courses']=Course::orderBy('id', 'desc')->with('subject')->where('trash','0')->get();
        return view('back.reservations.add',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ActivitesController $ActivitesController)
    {
        $user_email=User::where('email',$request->email)->first();
        $schedule =Course_schedule::where('id',$request->from)->first();
        $course=Course::where('id',$request->course_id)->first();
        if ($user_email)
        {
            $Reservation                            = new Reservation;
            $Reservation->user_id                   = $user_email->id;
            $Reservation->course_id                 = $request->course_id;
            $Reservation->status                    = "0";
            $Reservation->schedule_id               = $schedule->id;
            $Reservation->resturant_id              = $request->rest_id;
            $Reservation->hotel_id                  = $request->hotel_id;
            $Reservation->fees                      = $course->price;
            $Reservation->create_by                 = auth()->user()->id;
            $Reservation->update_by                 = auth()->user()->id;
            $Reservation ->save();
            $ActivitesController->store(auth()->user()->id ,'Created','Reservation',$Reservation->id);

            $user=User::where('id',$user_email->id)->first();
            $course= Course::where('id',$request->course_id)->first();
            $user->subject=$course->subject->{'name_'.$user_email->lang};
            $user->schedule=$schedule->{'schedule_'.$user_email->lang};
            $user->from=$schedule->from;
            $user->to=$schedule->to;
            // var_dump($user);die();
            Mail::to($user->email)->send(new Reserv($user));
            auth()->user()->notify(new ReservationNotification);

        }
        else
        {
             $user= User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),

            ]);
            if (isset($request->admin)&& !empty($request->admin))
            {
                $user->admin                    = $request->admin;
            }
            else
            {
                $user->admin                    = 0;
            }
            if (isset($request->instructor)&& !empty($request->instructor))
            {
                $user->instructor                    = $request->instructor;
            }
            else
            {
                $user->instructor                    = 0;
            }

            if (isset($request->employe)&& !empty($request->employe))
            {
                $user->employe                    = $request->employe;
            }
            else
            {
                $user->employe                    = 0;
            }

            if (isset($request->candidate)&& !empty($request->candidate))
            {
                $user->candidate                    = $request->candidate;
            }
            else
            {
                $user->candidate                    = 0;
            }

            if (isset($request->company_id)&& !empty($request->company_id))
            {
                $user->company_id                    = $request->company_id;
            }
            else
            {
                $user->company_id                    = 0;
            }

            $user->save();
            $Reservation                            = new Reservation;
            $Reservation->user_id                   = $user->id;
            $Reservation->course_id                 = $request->course_id;
            $Reservation->status                    = "0";
            $Reservation->private                   = '0';
            $Reservation->company_id                = '0';
            $Reservation->schedule_id               = $schedule->id;
            $Reservation->resturant_id              = $request->rest_id;
            $Reservation->hotel_id                  = $request->hotel_id;
            $Reservation->create_by                 = auth()->user()->id;
            $Reservation->update_by                 = auth()->user()->id;
            $Reservation ->save();
            $ActivitesController->store(auth()->user()->id ,'Created','Reservation',$Reservation->id);
            $user=User::where('id',$user->id)->first();
            $course= Course::where('id',$request->course_id)->first();
            $user->subject=$course->subject->{'name_'.$user->lang};
            $user->schedule=$schedule->{'schedule_'.$user->lang};
            $user->from=$schedule->from;
            $user->to=$schedule->to;
            Mail::to($user->email)->send(new Reserv($user));
            auth()->user()->notify(new ReservationNotification);

        }
       
        Session::flash('success', ' Reservation Done Successfully!' );
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['reserve']= Reservation::find($id);
        $data['hotels'] = Hotel::orderBy('id', 'desc')->where('trash','0')->get();
        $data['restaurants'] = Restaurant::orderBy('id', 'desc')->where('trash','0')->get();
        $data['courses']=Course::orderBy('id', 'desc')->with('subject')->where('trash','0')->get();
        $data['course_schedules']=Course_schedule::where('course_id',$data['reserve']->course_id)->where('trash','0')->get();
        return view('back.reservations.edit',$data); 
    }

    /**
     * Update the specified resource in storage.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ActivitesController $ActivitesController)
    {
        $schedule =Course_schedule::where('id',$request->from)->first();
        $Reservation                            = Reservation::find($id);
        $Reservation->course_id                 = $request->course_id;
        $Reservation->schedule_id               = $schedule->id;
        $Reservation->resturant_id              = $request->rest_id;
        $Reservation->private                   = '0';
        $Reservation->company_id                = '0';
        $Reservation->hotel_id                  = $request->hotel_id;
        $Reservation->create_by                 = auth()->user()->id;
        $Reservation->update_by                 = auth()->user()->id;
        $Reservation ->save();
        $ActivitesController->store(auth()->user()->id ,'Updated','Reservation',$Reservation->id);
        $edited_area = $Reservation;
        Session::flash('info',  $Reservation->name_en .' Updated Successfully!' );
        return redirect()->back();
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,ActivitesController $ActivitesController)
    {
       $reservation = Reservation::find($id);
       $reservation->delete();
       $ActivitesController->store(auth()->user()->id ,'Destroy','Reservation',$id);
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }

    public function trash(Request $request,ActivitesController $ActivitesController)
    {
      $id=$request->segment(4);
      $trash =$request->segment(5);
      if ($trash==1) {
         Reservation::where('id',$id)->update(array( 'trash' => 0, ));
         $ActivitesController->store(auth()->user()->id ,'UNTrashed','Reservation',$id);
         Session::flash('untrash', ' UNTrashed Successfully!' );
         return redirect()->back();
        
      }
      else
      {
         Reservation::where('id',$id)->update(array( 'trash' => 1, ));
         $ActivitesController->store(auth()->user()->id ,'Trashed','Reservation',$id);
         Session::flash('warning', ' Trashed Successfully!' );
         return redirect()->back();
      }

    }


    public function delete()
    {
       return view('back.confirm_delete');
    }


    public function status(Request $request,ActivitesController $ActivitesController)
    {
        $id=$request->segment(4);
        $status =$request->segment(5);
        if ($status==1) {
         Reservation::where('id',$id)->update(array( 'status' => 1, ));
         $ActivitesController->store(auth()->user()->id ,'Reservation Status Change To Lead','Reservation',$id);
         Session::flash('success', ' Reservation Lead Successfully!' );
         return redirect()->back();

        }
        elseif ($status==2) {
         $reserve=Reservation::where('id',$id)->update(array( 'status' => 2, ));
         $ActivitesController->store(auth()->user()->id ,'Reservation Status Change To Trasnfer To Accounting','Reservation',$id);
         Session::flash('success', ' Reservation Trasnfer To Accounting Successfully!' );
         return redirect()->back();
        }
        else
        {
            $reserve=Reservation::where('id',$id)->first();
            Reservation::where('id',$id)->update(array( 'status' => 3, ));
            $Accounting                           = new Accounting;
            $Accounting->amount                   = '+'.$reserve->fees;
            $Accounting->category_id              = '5';
            $Accounting->foreign_id               = $reserve->course_id;
            $Accounting->confirm                  = '1';
            $Accounting->user_id                  = $reserve->user_id;
            $Accounting->create_by                = auth()->user()->id;
            $Accounting->update_by                = auth()->user()->id;
            $Accounting->save();

         $ActivitesController->store(auth()->user()->id ,'Reservation Status Change To Done','Reservation',$id);
         Session::flash('success', ' Reservation Done Successfully!' );
         return redirect()->back();
        }
    }




}
