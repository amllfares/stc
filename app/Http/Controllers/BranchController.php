<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Branch;
use Session;
use App\Http\Controllers\ActivitesController;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['branches'] = Branch::orderBy('id', 'desc')->paginate(10);
        return view('back.branchs.view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back.branchs.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ActivitesController $ActivitesController)
    {
        $branche                           = new Branch;
        $branche->name_en                  = $request->name_en;
        $branche->name_ar                  = $request->name_ar;
        $branche->address_ar               = $request->address_ar;
        $branche->address_en               = $request->address_en;
        $branche->latitude                 = $request->latitude;
        $branche->longitude                = $request->longitude;
        $branche->contact_email            = $request->contact_email;
        $branche->contact_phone            = $request->contact_phone;
        $branche->contact_mobile           = $request->contact_mobile; 
      
        $branche->save();
        $ActivitesController->store(auth()->user()->id ,'Created','Branch',$branche->id);
        return redirect()->route('branches.create')->with('success',  $request->name_en ." Created Successfully!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['branch']= Branch::find($id);
        return view('back.branchs.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ActivitesController $ActivitesController)
    {
        $branche                           = Branch::find($id);
        $branche->name_en                  = $request->name_en;
        $branche->name_ar                  = $request->name_ar;
        $branche->address_ar               = $request->address_ar;
        $branche->address_en               = $request->address_en;
        $branche->latitude                 = $request->latitude;
        $branche->longitude                = $request->longitude;
        $branche->contact_email            = $request->contact_email;
        $branche->contact_phone            = $request->contact_phone;
        $branche->contact_mobile           = $request->contact_mobile; 
      
        $branche->save();
        $ActivitesController->store(auth()->user()->id ,'Updated','Branch',$id);

        $edited_area = $branche;
        Session::flash('info',  $branche->name .' Updated Successfully!' );
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response 
     */
    public function destroy($id,ActivitesController $ActivitesController)
    {
        // var_dump($id);die();
       $Branch = Branch::find($id);
       $Branch->delete();
       $ActivitesController->store(auth()->user()->id ,'Destroy','Branch',$id);
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }


    public function trash(Request $request,ActivitesController $ActivitesController)
    {
      $id=$request->segment(4);
      $trash =$request->segment(5);
      if ($trash==1) {
         Branch::where('id',$id)->update(array( 'trash' => 0, ));
         $ActivitesController->store(auth()->user()->id ,'UNTrashed','Branch',$id);
         Session::flash('untrash', ' UNTrashed Successfully!' );
         return redirect()->back();
        
      }
      else
      {
         Branch::where('id',$id)->update(array( 'trash' => 1, ));
         $ActivitesController->store(auth()->user()->id ,'Trashed','Branch',$id);
         Session::flash('warning', ' Trashed Successfully!' );
         return redirect()->back();
      }

    }


    public function delete()
    {
       return view('back.confirm_delete');
    }
}
