<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Feedback; 

class FeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\Response
     */
    public function index($type)
    {
        if ($type == "all") {
           $data['feedbacks'] = Feedback::orderBy('id', 'desc')->paginate(10);
        }
        elseif ($type == "contact_us") {
            $data['feedbacks'] = Feedback::orderBy('id', 'desc')->where('type',$type)->paginate(10);
        }
        elseif ($type == "course") {
            $data['feedbacks'] = Feedback::orderBy('id', 'desc')->where('type',$type)->paginate(10);
        }
        elseif ($type == "admin") {
            $data['feedbacks'] = Feedback::orderBy('id', 'desc')->where('type',$type)->paginate(10);
        }
        elseif ($type == "instructor") {
            $data['feedbacks'] = Feedback::orderBy('id', 'desc')->where('type',$type)->paginate(10);
        }        
        elseif ($type == "employe") {
            $data['feedbacks'] = Feedback::orderBy('id', 'desc')->where('type',$type)->paginate(10);
        }  
        elseif ($type == "candidate") {
            $data['feedbacks'] = Feedback::orderBy('id', 'desc')->where('type',$type)->paginate(10);
        }                      
        return view('back.feedback.view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
