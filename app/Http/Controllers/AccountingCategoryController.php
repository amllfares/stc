<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Accounting;
use Session;
use App\Http\Controllers\ActivitesController;
use App\CategoryAccounting;

class AccountingCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['category'] = CategoryAccounting::orderBy('id', 'desc')->paginate(10);
        return view('back.category_accouting.view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $data['subs']=CategoryAccounting::where('sub_of','0')->get();
         return view('back.category_accouting.add',$data);
    } 

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ActivitesController $ActivitesController)
    {
        $Accounting                           = new CategoryAccounting;
        $Accounting->name                     = $request->name  ;
        $Accounting->sub_of                   = $request->sub_of;
        $Accounting->system                   = $request->system;
        $Accounting->create_by                = auth()->user()->id;
        $Accounting->update_by                = auth()->user()->id;
        $Accounting->save();
        $ActivitesController->store(auth()->user()->id ,'Created','CategoryAccounting',$Accounting->id);
        return redirect()->route('category_accounting.create')->with('success',  $request->name ." Created Successfully!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['category']= CategoryAccounting::find($id);
        $data['subs']=CategoryAccounting::where('sub_of','0')->get();
        return view('back.category_accouting.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ActivitesController $ActivitesController)
    {
        $Accounting                           = CategoryAccounting::find($id);
        $Accounting->name                     = $request->name  ;
        $Accounting->sub_of                   = $request->sub_of;
        $Accounting->system                   = $request->system;
        $Accounting->create_by                = auth()->user()->id;
        $Accounting->update_by                = auth()->user()->id;
        $Accounting->save();
       
        $ActivitesController->store(auth()->user()->id ,'Updated','CategoryAccounting',$Accounting->id);
 
        Session::flash('info', ' CategoryAccounting Updated Successfully!' );
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,ActivitesController $ActivitesController)
    {
       $Accounting = CategoryAccounting::find($id);
       $Accounting->delete();
       $ActivitesController->store(auth()->user()->id ,'Destroy','CategoryAccounting',$id);
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }

    public function delete()
    {
       return view('back.confirm_delete');
    }
}
