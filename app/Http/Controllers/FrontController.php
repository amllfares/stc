<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\SubjectCategory;
use App\Subject;
use App\Course;
use App\Gallary;
use App\Subject_content;
use App\Course_schedule;
use App\CourseDates;
use App\Hotel;
use App\Reservation ;
use App\Restaurant;
use App\Private_reserve;
use App\Company_account;
use App\Company_reservation;
use App\Career;
use Session;
use Auth;
use App\Branch;
use App\Mail\Reserv;
use App\Mail\Contact_Us;
use App\Mail\WeclomeSubscribe; 
use App\Mail\courseSubscribe;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Feedback;
use App\Subscribe;
use App\TypeCategory;
use Illuminate\Support\Facades\Validator;
use App\Apply_Career;

class FrontController extends Controller
{
   public function page(Request $request,$slug)
    {
    	$slug=$request->segment('2');
        $data['page'] = Page::where('slug_en',$slug)->first();
        $data['categories']=SubjectCategory::where('trash','0')->get();

  
        if(Page::where('sub_of',$data['page']->id)->get() ){
            $data['children']=Page::where('sub_of',$data['page']->id)->where('trash','0')->get();
            $data['count']=Page::where('sub_of',$data['page']->id)->where('trash','0')->count();
            // return $data['count'];
            foreach ($data['children'] as $value) {
                if ($value->gallery=='1') {
                    if (Gallary::where(['related_id'=>$value->id, 'table'=>'pages'])->get()) {
                        $value->gall=Gallary::where(['related_id'=>$value->id, 'table'=>'pages'])->get(); 
                    }else{
                        $value->gall=''; 
                    }
                    
                }
            }
                
        }

        $data['careers']=Career::where('trash','0')->get();

        return view('front.pages.view_nav',$data);
    }

    public function course(Request $request)
    {
    	$id=$request->segment('3');
    	$data['subject']=Subject::where('trash','0')->where('category_id',$id)->get();
        return view('front.pages.view_course',$data);
    }

    public function course_all(Request $request)
    {
    	$id=$request->segment('3');
        $data['subject_title']=Subject::where('trash','0')->where('id',$id)->first();
        if ($data['subject_title']->image==null) {
           $data['subject_title']->image='/assets/back/upload/stc.png';
        }
    	$data['course_all']=Subject::where('trash','0')->where('category_id',$id)->get();
        $data['subject']=Course::where('trash','0')->where('subject_id',$id)->get();
        $data['subject_count']=Course::where('trash','0')->where('subject_id',$id)->count();
        $data['branches']=Branch::all();
        // var_dump($data['subject_count']);die();
        foreach ($data['subject'] as $sub) {
           $data['schedules']= Course_schedule::orderBy('id', 'desc')->where('course_id',$sub->id)->where('private','0')->with('instructor')->get();
        }
        // var_dump($data['schedules']);die();
        return view('front.pages.view_course_all',$data);
    }

     public function course_schedule(Request $request)
    {
        $id=$request->segment('3');
        $data['subject']=Subject::where('trash','0')->where('id',$id)->first();
        // var_dump( $data['subject']);die();
        $data['contents']=Subject_content::where('trash','0')->where('course_id',$id)->get();
        $data['schedules']= Course_schedule::orderBy('id', 'desc')->where('course_id',$id)->with('instructor')->get();
        foreach ($data['schedules'] as $schedule) {

           $schedule->dates=CourseDates::orderBy('id', 'desc')->where('course_id',$id)->where('course_schedule_id',$schedule->id)->get();
        }
      
        return view('front.pages.view_course_schedule',$data);
    }

    public function course_reservation(Request $request)
    {
        $data['hotels'] = Hotel::orderBy('id', 'desc')->where('trash','0')->get();
        $data['restaurants'] = Restaurant::orderBy('id', 'desc')->where('trash','0')->get();
        return view('front.pages.reservation',$data);
    }

    public function confirm_reservation(Request $request)
    {
        // var_dump($request->rest_id);die();
       
        $course_id=$request->segment(3);
        $course=Course::where('id',$course_id)->first();
        $schedule_id =$request->segment(4);
        $Reservation                            = new Reservation;
        $Reservation->user_id                   = auth()->user()->id;
        $Reservation->course_id                 = $course_id;
        $Reservation->status                    = "0";
        $Reservation->private                   = "0";
        $Reservation->fees                      = $course->price;
        $Reservation->schedule_id               = $schedule_id;
        $Reservation->resturant_id              = $request->rest_id;
        $Reservation->hotel_id                  = $request->hotel_id;
        $Reservation->user_id                   = auth()->user()->id;
        $Reservation->create_by                 = auth()->user()->id;
        $Reservation->update_by                 = auth()->user()->id;

        $Reservation ->save();
// var_dump($Reservation);die();

        $id=auth()->user()->id;
        $user=User::where('id',$id)->first();
        $course= Course::where('id',$course_id)->first();
        $user->subject=$course->subject->{'name_'.Auth::user()->lang};
        $schedule=Course_schedule::where('id',$schedule_id)->first();
        $user->schedule=$schedule->{'schedule_'.Auth::user()->lang};
        $user->from=$schedule->from;
        $user->to=$schedule->to;
        Mail::to($user->email)->send(new Reserv($user));
        Session::flash('Reservation', ' Reservation Done Successfully!' );
        Session::flash('Reserv', 'تم الحجز بنجاح ' );
        return redirect()->back();
    }


    public function confirm__private_reservation(Request $request)
    {
        $course_id=$request->segment(3);
        $schedule_id =$request->segment(4);
        $schedule     =Course_schedule::where('course_id',$course_id)->where('id',$schedule_id)->first();
        $course=Course::where('id',$schedule->course_id)->first();
        // var_dump($schedule);die();
        $Reservation                            = new Reservation;
        $Reservation->user_id                   = auth()->user()->id;
        $Reservation->course_id                 = $course_id;
        $Reservation->status                    = "0";
        $Reservation->private                   = "1";
        $Reservation->fees                      = $course->price;
        $Reservation->schedule_id               = '0';
        $Reservation->resturant_id              = $request->rest_id;
        $Reservation->hotel_id                  = $request->hotel_id;
        $Reservation->user_id                   = auth()->user()->id;
        $Reservation->create_by                 = auth()->user()->id;
        $Reservation->update_by                 = auth()->user()->id;

        $Reservation ->save();

        $id=auth()->user()->id;
        $user=User::where('id',$id)->first();
        $course= Course::where('id',$course_id)->first();
        $user->subject=$course->subject->{'name_'.Auth::user()->lang};
        // $schedule=Course_schedule::where('id',$schedule_id)->first();
        $user->schedule=$schedule->{'schedule_'.Auth::user()->lang};
        $user->from=$schedule->from;
        $user->to=$schedule->to;
        Mail::to($user->email)->send(new Reserv($user));

        Session::flash('Reservation', ' Reservation Done Successfully!' );
        Session::flash('Reserv', 'تم الحجز بنجاح ' );
        return redirect()->back();

    }

    public function confirm__company_reservation(Request $request)
    {
        $course_id=$request->segment(3);
        $schedule_id =$request->segment(4);
        $user_id=Auth::user()->id;
        $user_company=Company_account::where('user_id',$user_id)->first();
        $course=Course::where('id',$course_id)->first();

        if ($user_company)
        {
            $Reservation                            = new Company_reservation;
            $Reservation->company_id                = $user_company->id;
            $Reservation->course_id                 = $course_id;
            $Reservation->status                    = "0";
            $Reservation->candidate_number          = $request->candidate_number;
            $Reservation->fees                      = $course->price;
            $Reservation->discount                  = "";
            $Reservation->create_by                 = auth()->user()->id;
            $Reservation->update_by                 = auth()->user()->id;
            $Reservation ->save();

            $id=auth()->user()->id;
            $user=User::where('id',$id)->first();
            $course= Course::where('id',$course_id)->first();
            $user->subject=$course->subject->{'name_'.Auth::user()->lang};
            $schedule=Course_schedule::where('id',$schedule_id)->first();
            $user->schedule=$schedule->{'schedule_'.Auth::user()->lang};
            $user->from=$schedule->from;
            $user->to=$schedule->to;
            Mail::to($user->email)->send(new Reserv($user));

            Session::flash('Reservation', ' Reservation Done Successfully!' );
            Session::flash('Reserv', 'تم الحجز بنجاح ' );
            return redirect()->back();
        }
        else
        {
            $Company_account                            = new Company_account;
            $Company_account->name_en                   = $request->name_en;
            $Company_account->name_ar                   = $request->name_ar;
            $Company_account->address                   = $request->address;
            $Company_account->user_id                   = $user_id;
            $Company_account->contact_name              = $request->contact_name;
            $Company_account->create_by                 = auth()->user()->id;
            $Company_account->update_by                 = auth()->user()->id;
            $Company_account ->save();

            $Reservation                            = new Company_reservation;
            $Reservation->company_id                = $Company_account->id;
            $Reservation->course_id                 = $course_id;
            $Reservation->status                    = "0";
            $Reservation->candidate_number          = $request->candidate_number;
            $Reservation->fees                      = $course->price;
            $Reservation->discount                  = "";
            $Reservation->create_by                 = auth()->user()->id;
            $Reservation->update_by                 = auth()->user()->id;
            $Reservation ->save();

            $id=auth()->user()->id;
            $user=User::where('id',$id)->first();
            $course= Course::where('id',$course_id)->first();
            $user->subject=$course->subject->{'name_'.Auth::user()->lang};
            $schedule=Course_schedule::where('id',$schedule_id)->first();
            $user->schedule=$schedule->{'schedule_'.Auth::user()->lang};
            $user->from=$schedule->from;
            $user->to=$schedule->to;
            Mail::to($user->email)->send(new Reserv($user));

            Session::flash('Reservation', ' Reservation Done Successfully!' );
            Session::flash('Reserv', 'تم الحجز بنجاح ' );
            return redirect()->back();
        }
       

    }

    public function branch(Request $request)
    {
        $id=$request->segment(3);
        $data['branch']=Branch::where('id',$id)->first();
        return view('front.pages.branch',$data);

    }

    public function news(Request $request)
    {
        $id=$request->segment(3);
        $data['page']=Page::where('id',$id)->first();
        $data['latest_news']=Page::orderBy('order', 'desc')->where('sub_of','6')->where('id','<>',$data['page']->id)->take(6)->get();
        return view('front.pages.view_news',$data);
    }

    public function events(Request $request)
    {
        $id=$request->segment(3);
        
        $data['page']=Page::where('id',$id)->first();
        $data['latest_events']=Page::orderBy('order', 'desc')->where('sub_of','7')->where('id','<>',$data['page']->id)->take(6)->get();
        return view('front.pages.view_events',$data);
    }

    public function contact(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email|max:255|',
            'feedback' => 'required|string',
            'mobile' => 'required',
            'name'=>'required',
        ]);
        $Feedback                      = new Feedback; 
        $Feedback->name                =$request->name;
        $Feedback->email               =$request->email;
        $Feedback->feedback            =$request->feedback;
        $Feedback->mobile              =$request->mobile;
        $Feedback->type                ="contact_us";
        $Feedback->user_id             ='0';
        $Feedback->save();
        
         Mail::to($Feedback->email)->send(new Contact_Us($Feedback));
         Session::flash('Reservation', ' Feedback Send Successfully!' );
         return redirect()->back();

    }

    public function content()
    {
        $course_id=$_GET['course_id'];
        $data['contents']=Subject_content::where('trash','0')->where('course_id',$course_id)->get();
        return view('front.pages.content',$data);
    }

    public function more()
    { 
        $course_id=$_GET['course_id'];
        $schedule_id=$_GET['schedule_id'];
        $data['lang']=$_GET['lang'];
        // var_dump($schedule_id);die();
        $data['days_number']=CourseDates::where('course_id',$course_id)->where('course_schedule_id',$schedule_id)->count();
        $data['days_reservation']=Reservation::where('course_id',$course_id)->where('schedule_id',$schedule_id)->count();
        $data['course']=Course::where('trash','0')->where('id',$course_id)->first();
        return view('front.pages.more',$data);

    }

    public function subscribe(Request $request)
    {
         $request->validate([
            'email' => 'required|string|email|max:255|',
        ]);
        $email=Subscribe::where('email',$request->email)->where('course_id','0')->first();
        if ($email)
        {
            Session::flash('Reservation', ' You are actually a subscriber!' );
            return redirect()->back();
        }
        else
        {
            $Subscribe= new Subscribe;
            $Subscribe->email               =$request->email;
            $Subscribe->course_id           ='0';  
            $Subscribe->type                ="1";
            if(Auth::check())
            {
            $Subscribe->user_id             =auth()->user()->id;
            }
            else
            {
            $Subscribe->user_id             ='0'; 
            }

            $Subscribe->save();

            Mail::to($Subscribe->email)->send(new WeclomeSubscribe($Subscribe));
            Session::flash('Reservation', ' Subscribe Send Successfully!' );
            return redirect()->back();
        }
       

    }

    public function subscribe_course()
    {
        $data['course_id']=$_GET['course_id'];
        $data['lang']=$_GET['lang'];
        return view('front.pages.subscribe_course',$data);

    }

    public function subscribe_store(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email|max:255|',
            
        ]);
        $email=Subscribe::where('email',$request->email)->where('course_id',$request->course_id)->first();
        if ($email)
        {
            Session::flash('Reservation', ' You are actually a subscriber!' );
            return redirect()->back();
        }
        else
        {
            $Subscribe= new Subscribe;
            $Subscribe->email               =$request->email;
            $Subscribe->course_id           =$request->course_id;  
            $Subscribe->type                ="2";
            if(Auth::check())
            {
            $Subscribe->user_id             =auth()->user()->id;
            }
            else
            {
            $Subscribe->user_id             ='0';
            }
 
            $Subscribe->save();

            Mail::to($Subscribe->email)->send(new courseSubscribe());
            Session::flash('Reservation', ' Subscribe Send Successfully!' );
            return redirect()->back();
        }
    }

    public function get_branch(Request $request)
    {
        $branch_id=$_GET['id'];
        $id=$_GET['course_id']; 
        $data['branch_id']=$branch_id;
        $data['lang']=$_GET['lang'];
        $data['subject_title']=Subject::where('trash','0')->where('id',$id)->first();
        $data['course_all']=Subject::where('trash','0')->where('category_id',$id)->get();
        $data['subject']=Course::where('trash','0')->where('subject_id',$id)->get();
        $data['branches']=Branch::all();
        // var_dump($data['subject']);die();
        foreach ($data['subject'] as $sub) {
           $data['schedules']= Course_schedule::orderBy('id', 'desc')->where('course_id',$sub->id)->where('branch_id',$branch_id)->with('instructor')->get();
        }
        // var_dump($data['schedules']);die();
        return view('front.pages.view_course_all_branch',$data);

    }

    public function type(Request $request)
    {
        $id=$request->segment('3');
        $data['courses']=SubjectCategory::where('trash','0')->where('type_id',$id)->get();
        return view('front.pages.view_type',$data);
    }

    public function not_apply()
    {
        return view('front.pages.not_apply');
    }

   
}
