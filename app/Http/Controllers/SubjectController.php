<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subject;
use App\SubjectCategory;
use Session;
use App\Http\Controllers\ActivitesController;

class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['subject'] = Subject::orderBy('id', 'desc')->paginate(10);
        return view('back.subject.view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $data['sub_cate'] = SubjectCategory::where('trash', '0')->get();
       return view('back.subject.add',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ActivitesController $ActivitesController)
    {
       
        $subject                           = new Subject;
        $subject->name_en                  = $request->name_en;
        $subject->name_ar                  = $request->name_ar;
        $subject->sub_title_en             = $request->sub_title_en;
        $subject->sub_title_ar             = $request->sub_title_ar;
        $subject->descrption_en1           = $request->description_en1;
        $subject->descrption_ar1           = $request->description_ar1;
        $subject->descrption_en2           = $request->description_en2;
        $subject->descrption_ar2           = $request->description_ar2;
        $subject->extra                    = "test";
        $subject->media                    = $request->media;
        $subject->category_id              = $request->cate_id;
        $subject->create_by                = auth()->user()->id;
        $subject->update_by                = auth()->user()->id;

        if ($request->hasFile('image'))
        {
        $image = $request->file('image');
        $name ='/assets/back/upload/'.time(). $image->getClientOriginalName();
        $destinationPath = public_path('/assets/back/upload/');
        $imagePath = $destinationPath. "/".  $name;
        $image->move($destinationPath, $name);
        $subject->image = $name;
        }

        $subject->save();
        $ActivitesController->store(auth()->user()->id ,'Created','Subject',$subject->id);
        return redirect()->route('subjects.create')->with('success',  $request->name_en ." Created Successfully!");
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['subject']= Subject::find($id);
        $data['sub_cate'] = SubjectCategory::where('trash', '0')->get();
        return view('back.subject.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ActivitesController $ActivitesController)
    {
        $subject                           = Subject::find($id);
        $subject->name_en                  = $request->name_en;
        $subject->name_ar                  = $request->name_ar;
        $subject->sub_title_en             = $request->sub_title_en;
        $subject->sub_title_ar             = $request->sub_title_ar;
        $subject->descrption_en1           = $request->description_en1;
        $subject->descrption_ar1           = $request->description_ar1;
        $subject->descrption_en2           = $request->description_en2;
        $subject->descrption_ar2           = $request->description_ar2;
        $subject->extra                    = "test";
        $subject->media                    = $request->media;
        $subject->category_id              = $request->cate_id;
        $subject->create_by                = auth()->user()->id;
        $subject->update_by                = auth()->user()->id;

        if ($request->hasFile('image'))
        {
        $image = $request->file('image');
        $name ='/assets/back/upload/'.time(). $image->getClientOriginalName();
        $destinationPath = public_path('/assets/back/upload/');
        $imagePath = $destinationPath. "/".  $name;
        $image->move($destinationPath, $name);
        $subject->image = $name;
        }

        $subject->save();
        $ActivitesController->store(auth()->user()->id ,'Updated','Subject',$subject->id);
        $edited_area = $subject;
        Session::flash('info',   $subject->name_en .' Updated Successfully!' );
        // return view('back.content.categories.edit');
        return redirect('/admin/subjects/'.$edited_area->id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,ActivitesController $ActivitesController)
    {
       $subject = Subject::find($id);
       $subject->delete();
       $ActivitesController->store(auth()->user()->id ,'Destroy','Subject',$id);
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }

    public function trash(Request $request,ActivitesController $ActivitesController)
    {
      $id=$request->segment(4);
      $trash =$request->segment(5);
      if ($trash==1) {
         Subject::where('id',$id)->update(array( 'trash' => 0, ));
         $ActivitesController->store(auth()->user()->id ,'UNTrashed','Subject',$id);
         Session::flash('untrash', ' UNTrashed Successfully!' );
         return redirect()->back();
        
      }
      else
      {
         Subject::where('id',$id)->update(array( 'trash' => 1, ));
         $ActivitesController->store(auth()->user()->id ,'Trashed','Subject',$id);
         Session::flash('warning', ' Trashed Successfully!' );
         return redirect()->back();
      }

    }


    public function delete()
    {
        // var_dump("gdgg");die();
       return view('back.confirm_delete');
    }

    function viewss(){
        $data['subjects']=Subject::where(['id' => '2'])->get();
        return $data['subjects'];
    }
}
