<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SubjectCategory;
use Session;
use App\TypeCategory;
use App\Http\Controllers\ActivitesController;

class SubjectCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data['sub_cate'] = SubjectCategory::orderBy('id', 'desc')->paginate(10);
        return view('back.sub_cate.view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['types'] = TypeCategory::orderBy('ID', 'desc')->get();
        return view('back.sub_cate.add',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ActivitesController $ActivitesController)
    {
        $sub_cat                           = new SubjectCategory;
        $sub_cat->name_en                  = $request->name_en;
        $sub_cat->name_ar                  = $request->name_ar;
        $sub_cat->type_id                  = $request->type_id;
        $sub_cat->create_by                = auth()->user()->id;
        $sub_cat->update_by                = auth()->user()->id;

       if ($request->hasFile('image'))
        {
            $image = $request->file('image');
            $name ='/assets/back/upload/'.time(). $image->getClientOriginalName();
            $destinationPath = public_path('/assets/back/upload/');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $sub_cat->image = $name;
        }

        $sub_cat->save();
        $ActivitesController->store(auth()->user()->id ,'Created','SubjectCategory',$sub_cat->id);

        return redirect()->route('subject_categories.create')->with('success',  $request->name ." Created Successfully!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $data['types'] = TypeCategory::orderBy('ID', 'desc')->get();
       $data['sub_cat']= SubjectCategory::find($id);
       return view('back.sub_cate.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ActivitesController $ActivitesController)
    {
        // var_dump($request->type_id);die();
        $sub_catl                           = SubjectCategory::find($id);
        $sub_catl->type_id                  = $request->type_id;
        $sub_catl->name_en                  = $request->name_en;
        $sub_catl->name_ar                  = $request->name_ar;
        $sub_catl->create_by                = auth()->user()->id;
        $sub_catl->update_by                = auth()->user()->id;
       

        if ($request->hasFile('image'))
        {
            $image = $request->file('image');
            $name ='/assets/back/upload/'.time(). $image->getClientOriginalName();
            $destinationPath = public_path('/assets/back/upload/');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $sub_catl->image = $name;
        }
        $sub_catl->save();
        $ActivitesController->store(auth()->user()->id ,'Updated','SubjectCategory',$sub_catl->id);
        $edited_area = $sub_catl;
        Session::flash('info',  $sub_catl->name .' Updated Successfully!' );
        // return view('back.content.categories.edit');
        return redirect('/admin/subject_categories/'.$edited_area->id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,ActivitesController $ActivitesController)
    {
       $sub_cat = SubjectCategory::find($id);
       $sub_cat->delete();
       $ActivitesController->store(auth()->user()->id ,'Destroy','SubjectCategory',$id);
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }

    public function trash(Request $request,ActivitesController $ActivitesController)
    {
      $id=$request->segment(4);
      $trash =$request->segment(5);
      if ($trash==1) {
         SubjectCategory::where('id',$id)->update(array( 'trash' => 0, ));
         $ActivitesController->store(auth()->user()->id ,'UNTrashed','SubjectCategory',$id);
         Session::flash('untrash', ' UNTrashed Successfully!' );
         return redirect()->back();
        
      }
      else
      {
         SubjectCategory::where('id',$id)->update(array( 'trash' => 1, ));
         $ActivitesController->store(auth()->user()->id ,'Trashed','SubjectCategory',$id);
         Session::flash('warning', ' Trashed Successfully!' );
         return redirect()->back();
      }

    }

    public function delete()
    {
       return view('back.confirm_delete');
    }
}
