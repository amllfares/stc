<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Session;
use App\Mail\Register;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->middleware('SharedVariables');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function register()
    {
        return view('auth.register');
    }

    public function create_new(Request $request)
    {
            $user                             = new User;
            $user->name                       = $request->name;
            $user->email                      = $request->email;
            $user->password                   = Hash::make($request->password);
            $user->phone                      =$request->phone;
            $user->lang                       =$request->lang;
            $user->address                    =$request->address;
            $user->country                    =$request->country;
            $user->latitude                   =$request->latitude;
            $user->longitude                  =$request->longitude;

           
            if (isset($request->instructor)&& !empty($request->instructor))
            {
                $user->instructor                    = $request->instructor;
            }
            else
            {
                $user->instructor                    = 0;
            }

            if (isset($request->other)&& !empty($request->other))
            {
                $user->candidate                  = $request->other;
            }
            else
            {
                $user->candidate                  = 0;
            }

            if (isset($request->candidate)&& !empty($request->candidate))
            {
                $user->candidate                    = $request->candidate;
            }
            else
            {
                $user->candidate                    = 0;
            }

            $user->save();
            $user->pass=$request->password;
     

         Mail::to($user->email)->send(new Register($user));
         Session::flash('Register', ' Register Done Successfully You Can Login New!' );

        return redirect($request->segment('1').'/login_page');
    }
}
