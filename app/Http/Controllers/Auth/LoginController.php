<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use Illuminate\Http\Request;
use Session;
use Auth;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/welcome';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
         $this->middleware('SharedVariables');
    }

   public function login_page()
   {    
       return view('front.login_page');
   }

   public function check_login(Request $request){
      
        if (Auth::attempt([
           'email'=>$request->email,
           'password'=>$request->password
           ]))
       {
        // return "allam";
          $user=User::where('email', $request->email)->first();  
          // return Auth::user()->name; 
          return redirect('/');

       }
       else{
        // return "aa";
            Session::flash('error_log','Sorry email or password is incorrect');
           return redirect($request->segment('1').'/login_page');
       }

   }
}
