<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CompanyAccount;
use Session;
use App\Http\Controllers\ActivitesController;

class CompanyAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['companies'] = CompanyAccount::orderBy('id', 'desc')->paginate(10);
        return view('back.company_account.view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('back.company_account.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ActivitesController $ActivitesController)
    {
        $company_account                           = new CompanyAccount;
        $company_account->name_en                  = $request->name_en;
        $company_account->name_ar                  = $request->name_ar;
        $company_account->address                  = $request->address;
        $company_account->contact_name             = $request->contact_name;
        $company_account->create_by                = auth()->user()->id;
        $company_account->update_by                = auth()->user()->id;

        $company_account->save();
        $ActivitesController->store(auth()->user()->id ,'Created','CompanyAccount',$company_account->id);
        return redirect()->route('company_account.create')->with('success',  $request->name_en ." Created Successfully!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['company_account']= CompanyAccount::find($id);
        return view('back.company_account.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ActivitesController $ActivitesController)
    {
        $company_account                           = CompanyAccount::find($id);
        $company_account->name_en                  = $request->name_en;
        $company_account->name_ar                  = $request->name_ar;
        $company_account->address                  = $request->address;
        $company_account->contact_name             = $request->contact_name;
        $company_account->create_by                = auth()->user()->id;
        $company_account->update_by                = auth()->user()->id;

        $company_account ->save();
        $ActivitesController->store(auth()->user()->id ,'Updated','CompanyAccount',$company_account->id);
        $edited_area = $company_account;
        Session::flash('info',  $company_account->name_en .' Updated Successfully!' );
        // return view('back.content.categories.edit');
        return redirect('/admin/company_account/'.$edited_area->id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,ActivitesController $ActivitesController)
    {
        // var_dump($id);die();
       $company_account = CompanyAccount::find($id);
       $company_account->delete();
       $ActivitesController->store(auth()->user()->id ,'Destroy','CompanyAccount',$company_account->id);
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }


    public function trash(Request $request,ActivitesController $ActivitesController)
    {
      $id=$request->segment(4);
      $trash =$request->segment(5);
      if ($trash==1) {
         CompanyAccount::where('id',$id)->update(array( 'trash' => 0, ));
         $ActivitesController->store(auth()->user()->id ,'UNTrashed','CompanyAccount',$id);
         Session::flash('untrash', ' UNTrashed Successfully!' );
         return redirect()->back();
        
      }
      else
      {
         CompanyAccount::where('id',$id)->update(array( 'trash' => 1, ));
         $ActivitesController->store(auth()->user()->id ,'Trashed','CompanyAccount',$id);
         Session::flash('warning', ' Trashed Successfully!' );
         return redirect()->back();
      }

    }


    public function delete()
    {
       return view('back.confirm_delete');
    }
}
