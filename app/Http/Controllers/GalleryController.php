<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use Session;
use App\Gallary;
use App\Accordion;
use App\Http\Controllers\ActivitesController;
class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $data['pages'] = Page::orderBy('id', 'desc')->where('sub_of','8')->paginate(10);
        return view('back.gallary.view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back.gallary.page_gallary');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ActivitesController $ActivitesController)
    {
        $page=Page::create(array_except($request->all(),['_token','photo','banner']));
        $page->sub_of="8";
        $page->create_by=auth()->user()->id;
        $page->update_by=auth()->user()->id;

        // $page                           = new Page;
        // $page->name_en                  = $request->name_en;
        // $page->name_ar                  = $request->name_ar;
        // $page->slug_en                  = str_slug($request->name_en);
        // $page->slug_ar                  = str_slug($request->name_ar);
        // $page->bref_en                  = $request->bref_en;
        // $page->bref_ar                  = $request->bref_ar;
        // $page->description_en           = $request->description_en;
        // $page->description_ar           = $request->description_ar;
        // $page->order                    = $request->order;
        // $page->sub_of                   = '8';
        // $page->is_main_page             = $request->is_main_page;
        // $page->nav                      = $request->nav;
        // $page->delete                   = $request->delete;
        // $page->can_not_delete           = $request->can_not_delete;
        // $page->accordion                = $request->accordion;
        // $page->gallery                  = $request->gallery;
        // $page->accordion_selection      = $request->accordion_selection;
        // $page->create_by                = auth()->user()->id;
        // $page->update_by                = auth()->user()->id;

        if ($request->hasFile('photo'))
        {
            $image = $request->file('photo');
            $name ='/assets/back/upload/'.time().$image->getClientOriginalName();
            $destinationPath = public_path('/assets/back/upload/');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $page->photo = $name;
        }
        if ($request->hasFile('banner'))
        {
            $image = $request->file('banner');
            $name ='/assets/back/upload/'.time().$image->getClientOriginalName();
            $destinationPath = public_path('/assets/back/upload/');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $page->banner = $name;
        }

        $page->save();
        $ActivitesController->store(auth()->user()->id ,'Created','Gallary',$page->id);
        return redirect()->route('gallery.create')->with('success',  $request->name_en ." Created Successfully!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['gallery']= Gallary::find($id);
        return view('back.gallary.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ActivitesController $ActivitesController)
    {
        $gallery                           = Gallary::find($id);
        $gallery->name_en                  = $request->name_en;
        $gallery->name_ar                  = $request->name_ar;
        $gallery->bref_en                  = $request->bref_en;
        $gallery->table                    = $request->table;
        $gallery->related_id               = $request->related_id;
        $gallery->create_by                = auth()->user()->id;
        $gallery->update_by                = auth()->user()->id;

        if ($request->hasFile('file'))
        {
            $image = $request->file('file');
            $name ='/assets/back/upload/'.time().$image->getClientOriginalName();
            $destinationPath = public_path('/assets/back/upload/');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $gallery->file = $name;
        }
        if ($request->hasFile('banner'))
        {
            $image = $request->file('banner');
            $name ='/assets/back/upload/'.time(). $image->getClientOriginalName();
            $destinationPath = public_path('/assets/back/upload/');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $gallery->banner = $name;
        }

        $gallery->save();
        $ActivitesController->store(auth()->user()->id ,'Updated','Gallary',$gallery->id);
        $edited_area = $gallery;
        Session::flash('info',  $gallery->name_en .' Updated Successfully!' );
        // return view('back.content.categories.edit');
        return redirect('/admin/gallery/'.$edited_area->id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,ActivitesController $ActivitesController)
    {
        // var_dump($id);die();
       $gallery = Gallary::find($id);
       $gallery->delete();
       $ActivitesController->store(auth()->user()->id ,'Created','Gallary',$id);
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }


    public function trash(Request $request,ActivitesController $ActivitesController)
    {
      $id=$request->segment(4);
      $trash =$request->segment(5);
      if ($trash==1) {
         Gallary::where('id',$id)->update(array( 'trash' => 0, ));
         $ActivitesController->store(auth()->user()->id ,'UNTrashed','Gallary',$id);
         Session::flash('untrash', ' UNTrashed Successfully!' );
         return redirect()->back();
        
      }
      else
      {
         Gallary::where('id',$id)->update(array( 'trash' => 1, ));
         $ActivitesController->store(auth()->user()->id ,'Trashed','Gallary',$id);
         Session::flash('warning', ' Trashed Successfully!' );
         return redirect()->back();
      }

    }


    public function delete()
    {
       return view('back.confirm_delete');
    }

     public function add()
    {
        return view('back.gallary.add');
    }

    public function add_store(Request $request,ActivitesController $ActivitesController)
    {
        $related_id=$request->segment('4');
        $table=$request->segment('5');
        $gallery                           = new Gallary;
        $gallery->name_en                  = $request->name_en;
        $gallery->name_ar                  = $request->name_ar;
        $gallery->bref_en                  = $request->bref_en;
        $gallery->bref_ar                  = $request->bref_ar;
        $gallery->table                    = $table;
        $gallery->related_id               = $related_id;
        $gallery->create_by                = auth()->user()->id;
        $gallery->update_by                = auth()->user()->id;

        if ($request->hasFile('file'))
        {
            $image = $request->file('file');
            $name ='/assets/back/upload/'.time(). $image->getClientOriginalName();
            $destinationPath = public_path('/assets/back/upload/');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $gallery->file = $name;
        }
        if ($request->hasFile('banner'))
        {
            $image = $request->file('banner');
            $name ='/assets/back/upload/'.time(). $image->getClientOriginalName();
            $destinationPath = public_path('/assets/back/upload/');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $gallery->banner = $name;
        }

        $gallery->save();
        $ActivitesController->store(auth()->user()->id ,'Created','Gallary',$gallery->id);
        Session::flash('success',  $gallery->name_en .' created Successfully!' );
        return redirect('/admin/gallery/add/'.$request->segment('4').'/pages');
    }

}
