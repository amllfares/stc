<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Permiation;
use App\Http\Controllers\Controller;
use Session;
use App\Http\Controllers\ActivitesController;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {
        $data['permiations'] = Permiation::orderBy('id', 'desc')->paginate(10);
        return view('back.permiation.view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back.permiation.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request ,ActivitesController $ActivitesController)
    {
        $request->create_by=auth()->user()->id;
        $request->update_by=auth()->user()->id;
        $Permiation=Permiation::create(array_except($request->all(),['_token']));
        $ActivitesController->store(auth()->user()->id ,'Created','Permiation',$Permiation->id);
       
        return redirect()->route('permissions.create')->with('success', "Permiation Created Successfully!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['permiation']= Permiation::find($id);
        return view('back.permiation.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ActivitesController $ActivitesController)
    {
        $Permiation                           = Permiation::find($id);
        $Permiation->name                     = $request->name;
        $Permiation->create_by                = auth()->user()->id;
        $Permiation->update_by                = auth()->user()->id;
        $Permiation->save();
        $ActivitesController->store(auth()->user()->id ,'Updated','Permiation',$Permiation->id);

        $edited_area = $Permiation;
        Session::flash('info',  $Permiation->name.' Updated Successfully!' );
        // return view('back.content.categories.edit');
        return redirect('/admin/permissions/'.$edited_area->id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,ActivitesController $ActivitesController)
    {
       $role = Permiation::find($id);
       $role->delete();
       $ActivitesController->store(auth()->user()->id ,'Destroy','Permiation',$id);
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }


    public function trash(Request $request,ActivitesController $ActivitesController)
    {
      $id=$request->segment(4);
      $trash =$request->segment(5);
      if ($trash==1) {
         Permiation::where('id',$id)->update(array( 'trash' => 0, ));
         $ActivitesController->store(auth()->user()->id ,'UNTrashed','Permiation',$id);
         Session::flash('untrash', ' UNTrashed Successfully!' );
         return redirect()->back();
        
      }
      else
      {
         Permiation::where('id',$id)->update(array( 'trash' => 1, ));
         $ActivitesController->store(auth()->user()->id ,'Trashed','Permiation',$id);
         Session::flash('warning', ' Trashed Successfully!' );
         return redirect()->back();
      }

    }


    public function delete()
    {
       return view('back.confirm_delete');
    }
}
