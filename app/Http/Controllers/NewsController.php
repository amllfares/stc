<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use Session;
use App\Gallary;
use App\Accordion;
use App\Http\Controllers\ActivitesController;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['pages'] = Page::orderBy('id', 'desc')->where('sub_of','6')->paginate(10);
        return view('back.news.view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       // $data['sub_page'] = Page::where('sub_of','0')->where('is_main_page','1')->get();
       return view('back.news.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ActivitesController $ActivitesController)
    {

         $page=Page::create(array_except($request->all(),['_token','photo','banner']));
         $page->sub_of="6";
         $page->create_by=auth()->user()->id;
         $page->update_by=auth()->user()->id;
        // $page                           = new Page;
        // $page->name_en                  = $request->name_en;
        // $page->name_ar                  = $request->name_ar;
        // $page->slug_en                  = str_slug($request->name_en);
        // $page->slug_ar                  = str_slug($request->name_ar);
        // $page->bref_en                  = $request->bref_en;
        // $page->bref_ar                  = $request->bref_ar;
        // $page->description_en           = $request->description_en;
        // $page->description_ar           = $request->description_ar;
        // $page->order                    = $request->order;
        // $page->sub_of                   = '6';
        // $page->is_main_page             = $request->is_main_page;
        // $page->nav                      = $request->nav;
        // $page->delete                   = $request->delete;
        // $page->can_not_delete           = $request->can_not_delete;
        // $page->accordion                = $request->accordion;
        // $page->gallery                  = $request->gallery;
        // $page->selector                 = $request->selector;
        // $page->create_by                = auth()->user()->id;
        // $page->update_by                = auth()->user()->id;

        if ($request->hasFile('photo'))
        {
            $image = $request->file('photo');
            $name ='/assets/back/upload/'.time(). $image->getClientOriginalName();
            $destinationPath = public_path('/assets/back/upload/');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $page->photo = $name;
        }
        if ($request->hasFile('banner'))
        {
            $image = $request->file('banner');
            $name ='/assets/back/upload/'.time(). $image->getClientOriginalName();
            $destinationPath = public_path('/assets/back/upload/');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $page->banner = $name;
        }

        $page->save();
        $ActivitesController->store(auth()->user()->id ,'Created','Page',$page->id);
        return redirect()->route('news.create')->with('success',  $request->name_en ." Created Successfully!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
