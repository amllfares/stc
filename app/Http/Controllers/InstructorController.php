<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Instructor;
use Session;
use App\Http\Controllers\ActivitesController;
use App\Course_schedule;
use App\CourseDates;
use App\Course;
class InstructorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['instructor'] = Instructor::orderBy('id', 'desc')->paginate(10);
        return view('back.instructor.view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back.instructor.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ActivitesController $ActivitesController)
    {
        $instructor                           = new Instructor;
        $instructor->name_en                  = $request->name_en;
        $instructor->name_ar                  = $request->name_ar;
        $instructor->email                    = $request->email;
        $instructor->contact_num              = $request->contact_num;
        $instructor->address_en               = $request->address_en;
        $instructor->address_ar               = $request->address_ar;
        $instructor->price_per_hour           = $request->price_per_hour;
        $instructor->extra                    = "test";
        $instructor->create_by                = auth()->user()->id;
        $instructor->update_by                = auth()->user()->id;

        $instructor->save();
        $ActivitesController->store(auth()->user()->id ,'Created','Instructor',$instructor->id);
        return redirect()->route('instructors.create')->with('success',  $request->name_en ." Created Successfully!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['instructor']= Instructor::find($id);
        return view('back.instructor.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ActivitesController $ActivitesController)
    {
        $instructor                           = Instructor::find($id);
        $instructor->name_en                  = $request->name_en;
        $instructor->name_ar                  = $request->name_ar;
        $instructor->email                    = $request->email;
        $instructor->contact_num              = $request->contact_num;
        $instructor->address_en               = $request->address_en;
        $instructor->address_ar               = $request->address_ar;
        $instructor->price_per_hour           = $request->price_per_hour;
        $instructor->extra                    = "test";
        $instructor->create_by                = auth()->user()->id;
        $instructor->update_by                = auth()->user()->id;
        $instructor->save();
        $ActivitesController->store(auth()->user()->id ,'Updated','Instructor',$instructor->id);
        $edited_area = $instructor;
        Session::flash('info',  $instructor->name_en .' Updated Successfully!' );
        // return view('back.content.categories.edit');
        return redirect('/admin/instructors/'.$edited_area->id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,ActivitesController $ActivitesController)
    {
       $instructor = Instructor::find($id);
       $instructor->delete();
       $ActivitesController->store(auth()->user()->id ,'Destroy','Instructor',$id);
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }

    public function trash(Request $request,ActivitesController $ActivitesController)
    {
      $id=$request->segment(4);
      $trash =$request->segment(5);
      if ($trash==1) {
         Instructor::where('id',$id)->update(array( 'trash' => 0, ));
         $ActivitesController->store(auth()->user()->id ,'UNTrashed','Instructor',$id);
         Session::flash('untrash', ' UNTrashed Successfully!' );
         return redirect()->back();
        
      }
      else
      {
         Instructor::where('id',$id)->update(array( 'trash' => 1, ));
         $ActivitesController->store(auth()->user()->id ,'Trashed','Instructor',$id);
         Session::flash('warning', ' Trashed Successfully!' );
         return redirect()->back();
      }

    }


    public function delete()
    {
       return view('back.confirm_delete');
    }

    public function instructor_course(Request $request)
    {
        $id=$request->segment('5');
        $instructor_id=$request->segment('4');
        if ($id=="future")
        {
            $data['schedules'] = Course_schedule::orderBy('id', 'desc')->where('instructor_id',$instructor_id)->where('from','>',date("Y.m.d"))->where('to','>',date("Y.m.d"))->paginate(10);
            
        }
        elseif ($id=="past")
        {
            $data['schedules'] = Course_schedule::orderBy('id', 'desc')->where('instructor_id',$instructor_id)->where('from','<',date("Y.m.d"))->where('to','<',date("Y.m.d"))->paginate(10);
        }
        elseif ($id=="current")
        {
            $data['schedules'] = Course_schedule::orderBy('id', 'desc')->where('instructor_id',$instructor_id)->where('from','<=',date("Y.m.d"))->where('to','>=',date("Y.m.d"))->paginate(10);
        }
        else
        {
            $data['schedules'] = Course_schedule::orderBy('id', 'desc')->where('instructor_id',$instructor_id)->paginate(10);
        }

        foreach ($data['schedules'] as $key) {
           $data['course']=Course::orderBy('id', 'desc')->where('id',$key->course_id)->first();
        }
       

        return view('back.instructor.courses',$data);
    }

}
