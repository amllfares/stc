<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use Session;
use App\Question_Type;
use App\Http\Controllers\ActivitesController;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['questions'] = Question::orderBy('ID', 'desc')->paginate(10);
        return view('back.questions.view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['types'] = Question_Type::orderBy('ID', 'desc')->get();
        return view('back.questions.add',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ActivitesController $ActivitesController)
    {
        $Question                                = new Question;
        $Question->Question                      = $request->Question;
        if (isset($request->Text_Answer))
        {
            $Question->Text_Answer                   = $request->Text_Answer;
        }
        else
        {
            $Question->Text_Answer                   = "";
        }
      
        $Question->Type_ID                       = $request->Type_ID;
        $Question->Degree                        = $request->Degree;
        $Question->Type                          = $request->Type;
        $Question->save();
        $ActivitesController->store(auth()->user()->id ,'Created','Question',$Question->id);
        return redirect()->route('questions.create')->with('success', "New Question Created Successfully!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['types'] = Question_Type::orderBy('ID', 'desc')->get();
        $data['question']= Question::find($id);
        return view('back.questions.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ActivitesController $ActivitesController)
    {
        if (isset($request->Text_Answer))
        {
            Question::where('ID',$id)->update(array( 'Question' =>$request->Question, 'Text_Answer'=>$request->Text_Answer ,'Type_ID'=>$request->Type_ID,'Degree'=>$request->Degree,'Type'=>$request->Type));
             $ActivitesController->store(auth()->user()->id ,'Updated','Question',$id);
        }
        else
        {
            Question::where('ID',$id)->update(array( 'Question' =>$request->Question, 'Text_Answer'=>'' ,'Type_ID'=>$request->Type_ID,'Degree'=>$request->Degree,'Type'=>$request->Type));
             $ActivitesController->store(auth()->user()->id ,'Updated','Question',$id);
        }
       
        Session::flash('info', $request->Type .' Updated Successfully!' );
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,ActivitesController $ActivitesController)
    {
       Question::where('ID',$id)->delete();
        $ActivitesController->store(auth()->user()->id ,'Destroy','Question',$id);
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }

    public function delete()
    {
       return view('back.confirm_delete');
    }
}
