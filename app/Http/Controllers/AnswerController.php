<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Answer;
use App\Http\Controllers\ActivitesController;

class AnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $ques_id =$request->segment('4');
        $data['answers'] = Answer::orderBy('ID', 'desc')->where('Question_ID',$ques_id)->paginate(10);
        return view('back.answers.view',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back.answers.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ActivitesController $ActivitesController)
    {
        $ques_id =$request->segment('4');
        $Answer                           = new Answer;
        $Answer->Question_ID              = $ques_id;
        $Answer->Answer                   = $request->Answer;
        $Answer->Is_Right                 = $request->Is_Right;
        $Answer->save();
        $ActivitesController->store(auth()->user()->id ,'Created','Answer',$Answer->id);
        Session::flash('success', ' Answer Created Successfully!' );
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $data['answer']= Answer::find($id);
       return view('back.answers.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,ActivitesController $ActivitesController)
    {

        Answer::where('ID',$id)->update(array('Answer'=>$request->Answer ,'Is_Right'=>$request->Is_Right));
        $ActivitesController->store(auth()->user()->id ,'Updated','Answer',$id);
        Session::flash('info', ' Updated Successfully!' );
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,ActivitesController $ActivitesController)
    {
       Answer::where('ID',$id)->delete();
       $ActivitesController->store(auth()->user()->id ,'Destroy','Answer',$id);
       Session::flash('danger', ' Deleted Successfully!' );
       return redirect()->back();
    }

    public function delete()
    {
       return view('back.confirm_delete');
    }
}
