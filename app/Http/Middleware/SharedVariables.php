<?php

namespace App\Http\Middleware;

use Closure;
use View;
use Auth;
use App\Page;
use App\Gallary;
use App\SubjectCategory;
use App\Subject;
use App;
use Request;
use App\Branch;
use App\MetaTag;
use App\TypeCategory;
 

class SharedVariables
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // var_dump(Request::segment('3'));die();

        if (Request::segment('1') == "ar") {
            $data['lang']='ar';
        }elseif (Request::segment('1') == "en") {
            $data['lang']="en"; 
        }else{
            $data['lang']="en"; 
        }

        if (Request::segment('2')!==null)  
        {
           $title=Request::segment('2');
           $data['MetaTag']=MetaTag::where('title',$title)->first();
        }


        $data['under_banner'] = Page::where('sub_of','22')->orderBy('order', 'asc')->get();
        $data['logos'] = Gallary::orderBy('id', 'desc')->where('related_id','15')->where('table','pages')->get();
        
        $data['pages']=Page::orderBy('order', 'ASC')->where('is_main_page','1')->where('nav','1')->where('sub_of','0')->get();
        $data['courses']=SubjectCategory::where('trash','0')->get();
        $data['branches']=Branch::all();
        $data['types']= TypeCategory::all();

        foreach ($data['types'] as $key => $value) {
            $value->categories = SubjectCategory::where('trash','0')->where('type_id',$value->id)->get();

            foreach ($value->categories as $key1 => $cat) {
                $cat->subjects = Subject::where('trash','0')->where('category_id',$cat->id)->get();
            }
        }

        
         
        // return TypeCategory::where('trash','0')->get();

         $a=array("gears","leaf","fire","flash","flask","cubes","gears","leaf","fire","flash","flask","cubes");
         // $random_keys=array_rand($a,'10');
         foreach ($data['courses'] as $key => $sub_course) {

            
            $sub_course->icon=$a[$key];
            $sub_course->sub_sub_course=Subject::where('trash','0')->where('category_id',$sub_course->id)->get();
        
         }
         foreach ($data['pages'] as $page) {
            
            $data['count']=Page::orderBy('order', 'ASC')->where('sub_of',$page->id)->count();
           
         }

         $data['latest_events']=Page::orderBy('order', 'desc')->where('sub_of','7')->take(6)->get();
         $data['latest_news']=Page::orderBy('order', 'desc')->where('sub_of','6')->take(6)->get();
         $data['last_courses']=SubjectCategory::orderBy('created_at','desc')->where('trash','0')->take(5)->get();
         // var_dump($data['sub_page']);die();
         // return $data['courses'];
         view::share($data);

        return $next($request);
    }
}
