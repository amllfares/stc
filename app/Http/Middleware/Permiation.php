<?php

namespace App\Http\Middleware;

use Closure;
use App\User_Role;
use App\Role_Permission;
use View;
use Auth;
use App\Section;


class Permiation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if ($request->segment('2')!='')
      {
         $id=$request->segment('2');
          $sec= str_replace('_', ' ', $id) ;
          $secs=$sec.'s';
          
          $sec_name=Section::where('name',$sec)->orwhere('name',$secs)->first();
          $data['user_role']=User_Role::where('user_id',auth()->user()->id)->with('role')->first();
          $data['user_sections']=Role_Permission::where('role_id',$data['user_role']->role_id)->get();
          foreach ($data['user_sections'] as $key )
          {
             $data['check_perimation']= Role_Permission::orderBy('id', 'desc')->where('role_id',$data['user_role']->role_id)->where('section_id',$sec_name->id)->first();
          }
       
      }
      else
      {
         
          $data['user_role']=User_Role::where('user_id',auth()->user()->id)->with('role')->first();
          $data['user_sections']=Role_Permission::where('role_id',$data['user_role']->role_id)->get();
         
      }

          
        view::share($data);
        return $next($request);
    }
}
