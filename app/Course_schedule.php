<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course_schedule extends Model
{
    function instructor()
    {
       return $this->hasOne('App\Instructor','id','instructor_id')->where('trash', '0');
    }

    function branch()
    {
       return $this->hasOne('App\Branch','id','branch_id');
    }

    
}
