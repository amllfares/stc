<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instructor extends Model
{
    function course()
    {
        return $this->hasOne('App\Course')->where('trash', '0');
    }

     function course_schedule()
    {
        return $this->hasOne('App\Course_schedule')->where('trash', '0');
    }
}
