<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Label extends Model
{
    protected $fillable=['name','trash','update_by','create_by','created_at','updated_at'];
}
