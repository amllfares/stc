<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
   protected $table = "xam_questions";

    function question_type()
    {
        return $this->hasOne('App\Question_Type','ID','Type_ID');
    }
}
