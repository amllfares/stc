<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    protected $fillable = [
        'name', 'email', 'contact num','contact title','address','price per day','update_by','create_by'
    ];
}
