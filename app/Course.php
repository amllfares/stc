<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    function instructor()
    {
        return $this->hasOne('App\Instructor','id','instructor_id')->where('trash', '0');
    }

    function subject()
    {
        return $this->hasOne('App\Subject','id','subject_id')->where('trash', '0');
    }
}
