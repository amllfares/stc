<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use DB;
use App\RepeatlyAccounting;
use App\Accounting;
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
         $schedule->call(function () {
         $reapet=RepeatlyAccounting::where('date',date("d"))->first();
         if (isset($reapet)&& !empty($reapet))
         {
            $Accounting                           = new Accounting;
            $Accounting->amount                   = $reapet->amount;
            $Accounting->category_id              = $reapet->category_id;
            $Accounting->foreign_id               = '0';
            $Accounting->extra                    = $reapet->extra;
            $Accounting->confirm                  = '0';
            $Accounting->user_id                  = $reapet->user_id;
            $Accounting->month                    = date("m");
            $Accounting->labels                   = $reapet->labels;
            $Accounting->create_by                = $reapet->create_by;
            $Accounting->update_by                = $reapet->update_by;
            $Accounting->save();
         }
        

        })->daily();
   
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
