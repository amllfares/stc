<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable=['name_en','name_ar','slug_en','slug_ar','bref_en','bref_ar','description_en','description_ar','order','sub_of','is_main_page','nav','category','delete','can_not_delete','accordion','gallery','selector','photo','banner','trash','update_by','create_by','created_at','updated_at'];
}
