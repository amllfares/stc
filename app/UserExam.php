<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserExam extends Model
{
    protected $table = "xam_user_exam";

    function user()
    {
        return $this->hasOne('App\User','id','User_ID');
    }
}
