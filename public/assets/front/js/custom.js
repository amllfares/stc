 $(function(){
  $('.show_date').on("click", function(){

      var course_id = $(this).attr("course_id");
      var lang =$(this).attr("lang");
      var schedule_id = $(this).attr("schedule_id");
      var base_url = $(this).attr("base_url");
      var srcimage = "{{ asset('upload/ajax-loader-4.gif') }}";
    // alert(lang);
       var loader = '<p class="text-center"><img src= "'+base_url+'/upload/ajax-loader-4.gif"/></p>';
      $('#exampleModal .modal-body').html(loader);
      $('#exampleModal').modal({show:true});
      $.ajax({            
                  type: "GET",
                  url:base_url+'/'+lang+'/course_dates',
                  
                  data: {'course_id':course_id, 'schedule_id':schedule_id,'lang':lang},
                  cache: false,
                  success: function(result)
                  {
                      $('#exampleModal .modal-body').html(result);
                        
                  }
      });
    })
  }); 


  $(function(){
  $('.show').on("click", function(){

      var course_id = $(this).attr("course_id");
      var base_url = $(this).attr("base_url");
      var srcimage = "{{ asset('upload/ajax-loader-4.gif') }}";
      var lang =$(this).attr("lang");
    // alert(lang);
       var loader = '<p class="text-center"><img src= "'+base_url+'/upload/ajax-loader-4.gif"/></p>';
      $('#exampleModal .modal-body').html(loader);
      $('#exampleModal').modal({show:true});
      $.ajax({            
                  type: "GET",
                  url:base_url+'/'+lang+'/course_matirial',
                  
                  data: {'course_id':course_id,'lang':lang},
                  cache: false,
                  success: function(result)
                  {
                      $('#exampleModal .modal-body').html(result);
                        
                  }
      });
    })
  }); 


  $(function(){
  $('.show_kill').on("click", function(){

      var course_id = $(this).attr("course_id");
      var base_url = $(this).attr("base_url");
      var srcimage = "{{ asset('upload/ajax-loader-4.gif') }}";
       var lang =$(this).attr("lang");
    // alert(lang);
       var loader = '<p class="text-center"><img src= "'+base_url+'/upload/ajax-loader-4.gif"/></p>';
      $('#exampleModal .modal-body').html(loader);
      $('#exampleModal').modal({show:true});
      $.ajax({            
                  type: "GET",
                  url:base_url+'/'+lang+'/view_kill_sheet',
                  
                  data: {'course_id':course_id,'lang':lang},
                  cache: false,
                  success: function(result)
                  {
                      $('#exampleModal .modal-body').html(result);
                        
                  }
      });
    })
  }); 


  $(function(){
  $('.add_kill').on("click", function(){

      var course_id = $(this).attr("course_id");
      var base_url = $(this).attr("base_url");
      var srcimage = "{{ asset('upload/ajax-loader-4.gif') }}";
       var lang =$(this).attr("lang");
    // alert(course_id);
       var loader = '<p class="text-center"><img src= "'+base_url+'/upload/ajax-loader-4.gif"/></p>';
      $('#exampleModal .modal-body').html(loader);
      $('#exampleModal').modal({show:true});
      $.ajax({            
                  type: "GET",
                  url:base_url+'/'+lang+'/add_kill_sheet',
                  
                  data: {'course_id':course_id},
                  cache: false,
                  success: function(result)
                  {
                      $('#exampleModal .modal-body').html(result);
                        
                  }
      });
    })
  }); 

   $(function(){
  $('.edit_kill').on("click", function(){

      var id = $(this).attr("id");
      var base_url = $(this).attr("base_url");
      var srcimage = "{{ asset('upload/ajax-loader-4.gif') }}";
       var lang =$(this).attr("lang");
    // alert(course_id);
       var loader = '<p class="text-center"><img src= "'+base_url+'/upload/ajax-loader-4.gif"/></p>';
      $('#exampleModal2 .modal-body').html(loader);
      $('#exampleModal2').modal({show:true});
      $.ajax({            
                  type: "GET",
                  url:base_url+'/'+lang+'/edit_kill_sheet',
                  
                  data: {'course_id':course_id},
                  cache: false,
                  success: function(result)
                  {
                      $('#exampleModal2 .modal-body').html(result);
                        
                  }
      });
    })
  }); 


  $(function(){
  $('.kill_can').on("click", function(){

      var course_id = $(this).attr("course_id");
      var schedule_id = $(this).attr("schedule_id");
      var base_url = $(this).attr("base_url");
      var srcimage = "{{ asset('upload/ajax-loader-4.gif') }}";
       var lang =$(this).attr("lang");
    // alert(course_id);
       var loader = '<p class="text-center"><img src= "'+base_url+'/upload/ajax-loader-4.gif"/></p>';
      $('#exampleModal .modal-body').html(loader);
      $('#exampleModal').modal({show:true});
      $.ajax({            
                  type: "GET",
                  url:base_url+'/'+lang+'/kill_can',
                  data: {'course_id':course_id,'schedule_id':schedule_id,'lang':lang},
                 
                  cache: false,
                  success: function(result)
                  {
                      $('#exampleModal .modal-body').html(result);
                        
                  }
      });
    })
  }); 



  $(function(){
  $('.kill_can_view').on("click", function(){

      var course_id = $(this).attr("course_id");
      var schedule_id = $(this).attr("schedule_id");
      var base_url = $(this).attr("base_url");
      var srcimage = "{{ asset('upload/ajax-loader-4.gif') }}";
       var lang =$(this).attr("lang");
    // alert(course_id);
       var loader = '<p class="text-center"><img src= "'+base_url+'/upload/ajax-loader-4.gif"/></p>';
      $('#exampleModal .modal-body').html(loader);
      $('#exampleModal').modal({show:true});
      $.ajax({            
                  type: "GET",
                  url:base_url+'/'+lang+'/view_kill_can',
                  data: {'course_id':course_id,'schedule_id':schedule_id},
                 
                  cache: false,
                  success: function(result)
                  {
                      $('#exampleModal .modal-body').html(result);
                        
                  }
      });
    })
  }); 



  $(function(){
  $('.rate').on("click", function(){

      var course_id = $(this).attr("course_id");
      var schedule_id = $(this).attr("schedule_id");
      var base_url = $(this).attr("base_url");
      var srcimage = "{{ asset('upload/ajax-loader-4.gif') }}";
       var lang =$(this).attr("lang");
    // alert(course_id);
       var loader = '<p class="text-center"><img src= "'+base_url+'/upload/ajax-loader-4.gif"/></p>';
      $('#exampleModal .modal-body').html(loader);
      $('#exampleModal').modal({show:true});
      $.ajax({            
                  type: "GET",
                  url:base_url+'/'+lang+'/user_rate',
                  data: {'course_id':course_id,'schedule_id':schedule_id},
                 
                  cache: false,
                  success: function(result)
                  {
                      $('#exampleModal .modal-body').html(result);
                        
                  }
      });
    })
  }); 

  $(function(){
  $('.applay').on("click", function(){

      var id = $(this).attr("id");
      var base_url = $(this).attr("base_url");
      var srcimage = "{{ asset('upload/ajax-loader-4.gif') }}";
      var lang =$(this).attr("lang");
    // alert(lang);
       var loader = '<p class="text-center"><img src= "'+base_url+'/upload/ajax-loader-4.gif"/></p>';
      $('#exampleModal .modal-body').html(loader);
      $('#exampleModal').modal({show:true});
      $.ajax({            
                  type: "GET",
                  url:base_url+'/'+lang+'/applay',
                  
                  data: {'id':id},
                  cache: false,
                  success: function(result)
                  {
                      $('#exampleModal .modal-body').html(result);
                        
                  }
      });
    })
  }); 


  $(function(){
  $('.content').on("click", function(){

      var course_id = $(this).attr("course_id");
      var base_url = $(this).attr("base_url");
      var srcimage = "{{ asset('upload/ajax-loader-4.gif') }}";
       var lang =$(this).attr("lang");
    // alert(course_id);
       var loader = '<p class="text-center"><img src= "'+base_url+'/upload/ajax-loader-4.gif"/></p>';
      $('#exampleModal .modal-body').html(loader);
      $('#exampleModal').modal({show:true});
      $.ajax({            
                  type: "GET",
                  url:base_url+'/'+lang+'/content',
                  
                  data: {'course_id':course_id},
                  cache: false,
                  success: function(result)
                  {
                      $('#exampleModal .modal-body').html(result);
                        
                  }
      });
    })
  }); 

  function show_date(base_url,course_id,schedule_id,lang){
    
       var srcimage = "{{ asset('upload/ajax-loader-4.gif') }}";
  
       var loader = '<p class="text-center"><img src= "'+base_url+'/upload/ajax-loader-4.gif"/></p>';
      $('#exampleModal .modal-body').html(loader);
      $('#exampleModal').modal({show:true});
      $.ajax({            
                  type: "GET",
                  url:base_url+'/'+lang+'/course_dates',
                  
                  data: {'course_id':course_id,'schedule_id':schedule_id,'lang':lang},
                  cache: false,
                  success: function(result)
                  {
                      $('#exampleModal .modal-body').html(result);
                        
                  }
      });
  }

function content(base_url,course_id,schedule_id,lang){
    
       var srcimage = "{{ asset('upload/ajax-loader-4.gif') }}";
  
       var loader = '<p class="text-center"><img src= "'+base_url+'/upload/ajax-loader-4.gif"/></p>';
      $('#exampleModal .modal-body').html(loader);
      $('#exampleModal').modal({show:true});
      $.ajax({            
                  type: "GET",
                  url:base_url+'/'+lang+'/content',
                  
                  data: {'course_id':course_id,'schedule_id':schedule_id,'lang':lang},
                  cache: false,
                  success: function(result)
                  {
                      $('#exampleModal .modal-body').html(result);
                        
                  }
      });
  }
  function more(base_url,course_id,schedule_id,lang){
    
       var srcimage = "{{ asset('upload/ajax-loader-4.gif') }}";
  
       var loader = '<p class="text-center"><img src= "'+base_url+'/upload/ajax-loader-4.gif"/></p>';
      $('#exampleModal .modal-body').html(loader);
      $('#exampleModal').modal({show:true});
      $.ajax({            
                  type: "GET",
                  url:base_url+'/'+lang+'/more',
                  
                  data: {'course_id':course_id,'schedule_id':schedule_id,'lang':lang},
                  cache: false,
                  success: function(result)
                  {
                      $('#exampleModal .modal-body').html(result);
                        
                  }
      });
  }

  $(function(){
  $('.more').on("click", function(){

      var course_id = $(this).attr("course_id");
      var schedule_id=$(this).attr("schedule_id");
      var base_url = $(this).attr("base_url");
      var srcimage = "{{ asset('upload/ajax-loader-4.gif') }}";
       var lang =$(this).attr("lang");
    // alert(course_id);
       var loader = '<p class="text-center"><img src= "'+base_url+'/upload/ajax-loader-4.gif"/></p>';
      $('#exampleModal .modal-body').html(loader);
      $('#exampleModal').modal({show:true});
      $.ajax({            
                  type: "GET",
                  url:base_url+'/'+lang+'/more',
                  
                  data: {'course_id':course_id,'schedule_id':schedule_id,'lang':lang},
                  cache: false,
                  success: function(result)
                  {
                      $('#exampleModal .modal-body').html(result);
                        
                  }
      });
    })
  }); 

 $(function(){
  $('.Subscribe').on("click", function(){

      var course_id = $(this).attr("course_id");
      var base_url = $(this).attr("base_url");
      var srcimage = "{{ asset('upload/ajax-loader-4.gif') }}";
      var lang =$(this).attr("lang");
    // alert(course_id);
       var loader = '<p class="text-center"><img src= "'+base_url+'/upload/ajax-loader-4.gif"/></p>';
      $('#exampleModal .modal-body').html(loader);
      $('#exampleModal').modal({show:true});
      $.ajax({            
                  type: "GET",
                  url:base_url+'/'+lang+'/subscribe_course',
                  data: {'course_id':course_id,'lang':lang},
                  cache: false,
                  success: function(result)
                  {
                      $('#exampleModal .modal-body').html(result);
                        
                  }
      });
    })
  }); 


 function branch(base_url,id,course_id,lang){
    // var dataString = 'id='+ id;
  // alert(id);
    $.ajax({
     type: "GET",
      url:base_url+'/en/course_branch',
      data: {'id':id ,'course_id':course_id,'lang':lang},
      cache: false,
      success: function(result){
      
        $("#branch").html(result);  
      },
      error: function() {
        alert("error");
      }
    });
  }


 $(function(){
  $('.feedback').on("click", function(){

      var course_id = $(this).attr("course_id");
      var schdule_id = $(this).attr("schdule_id");
      var base_url = $(this).attr("base_url");
      var srcimage = "{{ asset('upload/ajax-loader-4.gif') }}";
      var lang =$(this).attr("lang");
    // alert(course_id);
       var loader = '<p class="text-center"><img src= "'+base_url+'/upload/ajax-loader-4.gif"/></p>';
      $('#exampleModal .modal-body').html(loader);
      $('#exampleModal').modal({show:true});
      $.ajax({            
                  type: "GET",
                  url:base_url+'/'+lang+'/course_feedback',
                  
                  data: {'course_id':course_id,'schdule_id':schdule_id,'lang':lang},
                  cache: false,
                  success: function(result)
                  {
                      $('#exampleModal .modal-body').html(result);
                        
                  }
      });
    })
  }); 


$(function(){
  $('.candidate_attendances').on("click", function(){

      var date_id = $(this).attr("date_id");
      var course_id = $(this).attr("course_id");
      var schedule_id = $(this).attr("schedule_id");
      var base_url = $(this).attr("base_url");
      var srcimage = "{{ asset('upload/ajax-loader-4.gif') }}";
      var lang =$(this).attr("lang");
    // alert(course_id);
       var loader = '<p class="text-center"><img src= "'+base_url+'/upload/ajax-loader-4.gif"/></p>';
      $('#exampleModal .modal-body').html(loader);
      $('#exampleModal').modal({show:true});
      $.ajax({            
                  type: "GET",
                  url:base_url+'/'+lang+'/instructor_candidate_attendances',
                  data: {'date_id':date_id,'schedule_id':schedule_id,'lang':lang,'course_id':course_id},
                 
                  cache: false,
                  success: function(result)
                  {
                      $('#exampleModal .modal-body').html(result);
                        
                  }
      });
    })
  }); 

 function right(base_url,user_id,date_id,lang){
   
    $.ajax({
      type: "GET",
      url:base_url+'/'+lang+'/check_right',
      data: {'user_id':user_id ,'date_id':date_id,'lang':lang},
      cache: false,
      success: function(result){
         
      },
      error: function() {
        alert("error");
      }
    });
  };

  function fals(base_url,user_id,date_id,lang){
   
    $.ajax({
      type: "GET",
      url:base_url+'/'+lang+'/check_false',
      data: {'user_id':user_id ,'date_id':date_id,'lang':lang},
      cache: false,
      success: function(result){
         
      },
      error: function() {
        alert("error");
      }
    });
  };

 function apply_again(base_url,lang,id){
   
    $.ajax({
     type: "GET",
      url:base_url+'/'+lang+'/not_apply',
      data: {'lang':lang},
      cache: false,
      success: function(result){
      
        $("#not_"+id).html(result);  
      },
      error: function() {
        alert("error");
      }
    });
  }











