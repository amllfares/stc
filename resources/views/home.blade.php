@extends('layout.frontlayout')
@section('content')
    
    <!-- BANNER -->
    <div id="slides" class="section banner">
        <ul class="slides-container">
            <?php $gallary=App\Gallary::where('related_id','1')->where('table','pages')->get(); ?>
            @foreach($gallary as $gall)
            <li>
                <img src="{{asset($gall->banner)}}" alt="">
                <div class="overlay-bg"></div>
                <div class="container">
                    <div class="wrap-caption">
                        <h2 class="caption-heading">
                            
                            <?php echo $gall->{'name_'.$lang } ?>
                        </h2>
                        <p class="excerpt"><?php echo $gall->{'bref_'.$lang } ?></p> 
                        <!-- <a href="#" class="btn btn-primary" title="LEARN MORE">@lang('lang.LEARN MORE')</a> <a href="#" class="btn btn-secondary" title="CONTACT US">@lang('lang.Contact us')</a> -->
                    </div>
                </div>
            </li>
            @endforeach
           
            
        </ul>

        <nav class="slides-navigation">
            <div class="container">
                <a href="#" class="next">
                    <i class="fa fa-chevron-right"></i>
                </a>
                <a href="#" class="prev">
                    <i class="fa fa-chevron-left"></i>
                </a>
            </div>
        </nav>
        
    </div>

    <!-- ABOUT FEATURE -->
    <div class="section feature overlap">
        <div class="container">

            <div class="row">
                <?php
                    // var_dump(Auth::user()->lang);
                 ?>
                @foreach($under_banner as $key => $under_banners)
                    @if($key=='0')
                        <?php $ico="star-o";  ?>
                    @elseif($key=='1')
                        <?php $ico="fire";  ?>                    
                    @else
                        <?php $ico="gears";  ?>
                    @endif
                <div class="col-sm-4 col-md-4">
                    <!-- BOX 1 -->
                    <div class="box-icon-2">
                        <div class="icon">
                            <div class="fa fa-{{ $ico }}"></div>
                        </div>
                        <div class="body-content">
                            <div class="heading"> <?php echo $under_banners->{'name_'.$lang} ?></div>
                            <?php echo $under_banners->{'description_'.$lang} ?>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
       <?php  $about_us=App\Page::where('slug_en','about-us')->first(); 
       ?>
            <div class="row">
                <div class="col-sm-12 col-md-12" style="margin-bottom:-50px;">
                    <h2 class="section-heading">
                        <?php echo $about_us->{'name_'.$lang}  ?>
                    </h2>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-5 col-md-5">
                    <div class="jumbo-heading">
                        <img src="{{url($about_us->photo)}}" style="width: 455px;height: 342px;margin-top: 20px;">
                    </div>
                </div>
                <div class="col-sm-7 col-md-7">
                    <h2><?php echo $about_us->{'bref_'.$lang}  ?></h2>
                    <p class="lead"><?php echo str_limit($about_us->{'description_'.$lang}, $limit = 600, $end = '...')   ?></p> 
                </div>

            </div>

        </div>
    </div>


    <div class="section services">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <h2 class="section-heading">
                        @lang('lang.Courses')
                    </h2>
                </div>
            </div>

            <div class="row ">
                @foreach($courses as $course)
                    <div class="col-sm-6 col-md-4" style="max-height: 450px;
                    margin-top: 40px;">
                        <!-- BOX 1 -->
                        <a href="{{url($lang.'/course/'.$course->id)}}">
                        <div class="feature-box-8">
                          <div class="media" style="    height: 241px;">
                            <img src="{{asset($course->image)}}" alt="rud" class="img-responsive">
                          </div>
                          <div class="body">
                            <div class="icon-holder">
                              <span class="fa fa-{{$course->icon}}"></span>
                            </div>
                            <a href="{{ url($lang.'/course/'.$course->id)}}" class="title"><?php echo $course->{'name_'.$lang} ?></a>
                            <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed</p> -->
                            <a href="{{ url($lang.'/course/'.$course->id)}}" class="readmore">@lang('lang.LEARN MORE')</a>
                          </div>
                        </div>
                        </a>
                    </div>
                @endforeach       
                
            </div>
        </div>
    </div>


    <!-- Service -->
    <div class="section why overlap">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <h2 class="section-heading">
                        @lang('lang.comming_events')
                    </h2>
                </div>
            </div>
            <div class="row">

                @foreach($latest_events as $events)
                
                <div class="col-sm-6 col-md-4">
                    <!-- BOX 1 -->
                    <div class="box-news-1">
                        <div class="media gbr">
                            <a href="{{ url($lang.'/events/'.$events->id)}}">
                            <img src="{{asset($events->photo)}}" alt="<?php echo $events->{'name_'.$lang} ?>" class="img-responsive"></a>
                        </div>
                        <div class="body">
                            <div class="title"><a href="{{ url($lang.'/events/'.$events->id)}}" title=""><?php echo $events->{'name_'.$lang} ?></a></div>
                            <div class="meta">
                                <span class="date"><i class="fa fa-clock-o"></i> {{$events->created_at}}</span>
                                <!-- <span class="comments"><i class="fa fa-comment-o"></i> 0 Comments</span> -->
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                         
                
            </div>
        </div>
    </div>

     <div class="section why overlap">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <h2 class="section-heading">
                        @lang('lang.comming_news')
                    </h2>
                </div>
            </div>
            <div class="row">

                @foreach($latest_news as $news)
                
                <div class="col-sm-6 col-md-4">
                    <!-- BOX 1 -->
                    <div class="box-news-1">
                        <div class="media gbr">
                            <a href="{{ url($lang.'/news/'.$news->id)}}">
                            <img src="{{asset($news->photo)}}" alt="<?php echo $news->{'name_'.$lang} ?>" class="img-responsive"></a>
                        </div>
                        <div class="body">
                            <div class="title"><a href="{{ url($lang.'/news/'.$news->id)}}" title=""><?php echo $news->{'name_'.$lang} ?></a></div>
                            <div class="meta">
                                <span class="date"><i class="fa fa-clock-o"></i> {{$news->created_at}}</span>
                                <!-- <span class="comments"><i class="fa fa-comment-o"></i> 0 Comments</span> -->
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                         
                
            </div>
        </div>
    </div>

    <!-- CLIENT -->
    <div class="section stat-client">
        <div class="container">
            
            <div class="row">
                @foreach($logos as $logo)
                <div class="col-sm-4 col-md-4">
                    <div class="client-img">
                        <a href="#">
                            <img src="{{asset($logo->file)}}" alt="" class="img-responsive">
                        </a>
                    </div>
                </div>
                @endforeach
                
            </div>
        </div>
    </div>


  <!--  -->

  @endsection