@extends('layout.frontlayout')
@section('content')
<div class="section banner-page" style="background-image:url('{{asset('/assets/back/upload/stc.png')}}')">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="title-page"> Send Mail </div>
                    <ol class="breadcrumb">
                        <li><a href="{{url('/')}}">STC</a></li>
                        <li class="active">Send Mail  </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>


<div class="section contact overlap">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-md-4 col-md-push-8">
                    <div class="widget download">
                        
                    </div>
                    

                </div>
                <div class="col-sm-8 col-md-8 col-md-pull-2">
                    <div class="content">
                        
                        <div class="margin-bottom-30"></div>
                        <h3 class="section-heading-2">
                            Send Mail to Rest Password
                        </h3>
                        <br><br><br>
                         @if(Session::has('status'))
                            <div class="alert alert-danger" style="background-color:#041e42;">
                                <div class="m-alert__icon">
                                    <i class="flaticon-exclamation-1"></i>
                                </div>
                                <div class="m-alert__text" style="color: white;">
                                    {{ Session::get('status') }}
                                </div>
                            </div>
                          @endif
                         

                         <div class="col-md-8">
                            <div class="card">
                               

                                <div class="card-body">
                                  
                                    <form method="POST" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}">
                                        @csrf

                                        <div class="form-group row">
                                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                                @if ($errors->has('email'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                           
                                        </div>

                                        <div class="form-group row mb-0">
                                            <div class="col-md-6 offset-md-4">
                                                <button type="submit" class="btn btn-primary">
                                                    {{ __('Send Password Reset Link') }}
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="margin-bottom-50"></div>
                        
                     </div>
                </div>

            </div>
            
        </div>
    </div>
@endsection
