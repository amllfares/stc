@extends('layout.frontlayout')
@section('content')
<div class="section banner-page" style="background-image:url('{{asset('/assets/back/upload/stc.png')}}')">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="title-page"> Reset Password</div>
                    <ol class="breadcrumb">
                        <li><a href="{{url('/')}}">STC</a></li>
                        <li class="active">Reset Password </li>
                    </ol>
                </div>
            </div>
        </div>
 </div>

<div class="section contact overlap">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-md-4 col-md-push-8">
                    <div class="widget download">
                        
                    </div>
                    

                </div>
                <div class="col-sm-8 col-md-8 col-md-pull-2">
                    <div class="content">
                        
                        <div class="margin-bottom-30"></div>
                        <h3 class="section-heading-2">
                            Login Details
                        </h3>
                        <br><br><br>
                         @if(Session::has('Register'))
                            <div class="alert alert-danger" style="background-color:#041e42;">
                                <div class="m-alert__icon">
                                    <i class="flaticon-exclamation-1"></i>
                                </div>
                                <div class="m-alert__text" style="color: white;">
                                    {{ Session::get('Register') }}
                                </div>
                            </div>
                          @endif
                         

                        <form action="{{ route('password.request') }}"  class=" form-contact"  data-toggle="validator" novalidate="true" method="POST" enctype="multipart/form-data" >
                             {{ csrf_field() }}
             

                           <input type="hidden" name="token" value="{{ $token }}">

                         <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                           
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                           
                                
                                <div class="help-block with-errors"></div>
                         </div>

                         <div class="form-group row">
                               <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                           
                                
                                <div class="help-block with-errors"></div>
                         </div>
                          <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                           
                        </div>

                         
                         
                            
                            <div class="form-group">
                               <button type="submit" class="btn btn-secondary " style="pointer-events: all; cursor: pointer;">
                                    {{ __('Reset Password') }}
                                </button>

                            </div>

                        </form>

                        <div class="margin-bottom-50"></div>
                        
                     </div>
                </div>

            </div>
            
        </div>
    </div>
@endsection
