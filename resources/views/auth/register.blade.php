@extends('layout.frontlayout')
@section('content')
<div class="section banner-page" style="background-image:url('{{asset('/assets/back/upload/stc.png')}}')">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="title-page">@lang('lang.Register')</div>
                    <ol class="breadcrumb">
                        <li><a href="{{url('/')}}">STC</a></li>
                        <li class="active">@lang('lang.Register') </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

<div class="section contact overlap">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-md-4 col-md-push-8">
                    <div class="widget download">
                        
                    </div>
                    

                </div>
                <div class="col-sm-8 col-md-8 col-md-pull-2">
                    <div class="content">
                        
                        <div class="margin-bottom-30"></div>
                        <h3 class="section-heading-2">
                             @lang('lang.Register Details')
                        </h3>
                        <br><br><br>
                         @if(Session::has('Register'))
                            <div class="alert alert-danger" style="background-color:#041e42;">
                                <div class="m-alert__icon">
                                    <i class="flaticon-exclamation-1"></i>
                                </div>
                                <div class="m-alert__text" style="color: white;">
                                    {{ Session::get('Register') }}
                                </div>
                            </div>
                          @endif

                        <form action="{{url(Request::segment('1').'/register_success')}}"  class=" form-contact"  data-toggle="validator" novalidate="true" method="POST" enctype="multipart/form-data" >
                             {{ csrf_field() }}
                           

                            <div class="form-group row">
                                 <label for="name" class="col-md-4 col-form-label text-md-right">@lang('lang.Name')</label>

                           
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value ="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                           
                                <div class="help-block with-errors"></div>
                           </div>

                          <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">@lang('lang.E-Mail Address')</label>

                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                
                                <div class="help-block with-errors"></div>
                         </div>

                         <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">@lang('lang.Password')</label>

                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                           
                                
                                <div class="help-block with-errors"></div>
                         </div>

                          <div class="form-group row">
                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right"> @lang('lang.Confirm Password')</label>

                               
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                             
                               
                                
                                <div class="help-block with-errors"></div>
                         </div>

                          <div class="form-group row">
                                <label for="Phone" class="col-md-4 col-form-label text-md-right"> @lang('lang.Phone') </label>

                                <input id="phone" type="text" class="form-control{{ $errors->has('Phone') ? ' is-invalid' : '' }}" name="phone" required>

                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                           
                                
                                <div class="help-block with-errors"></div>
                         </div>


                          <div class="form-group row">
                                <label for="parent_category" class="col-md-4 col-form-label text-md-right">@lang('lang.Language')</label>

                                <select name="lang" id="parent_category" class="form-control"  required="">
                                    <option value="">@lang('lang.Language')</option>
                                   
                                        <option value="en">@lang('lang.EN')</option>
                                        <option value="ar">@lang('lang.Ar')</option>
                                   
                                </select>
                                
                                <div class="help-block with-errors"></div>
                         </div>

                           <div class="form-group row">
                                                
                           
                                <label for="address" class="col-md-12 col-form-label text-md-right">@lang('lang.User Type')</label>
                               
                                            <div class="col-6">
                                                <div class="m-checkbox-inline">

                                                     <label class="col-md-4 col-form-label text-md-right">
                                                        <input type="checkbox" name="candidate" value="1"  >
                                                        Candidate
                                                        <span></span>
                                                    </label>
                                                    
                                                    <label class="col-md-4 col-form-label text-md-right">
                                                        <input type="checkbox" name="instructor" value="1">
                                                        Instructor
                                                        <span></span>
                                                    </label>
                                                    <label class="col-md-4 col-form-label text-md-right">
                                                        <input type="checkbox" name="other" value="1"  >
                                                        other
                                                        <span></span>
                                                    </label>

                                                   
                                                </div>
                                                
                                            </div>
                                       
                                
                                                  
                                               
 
                                </div>


                        <div class="form-group row">
                            <label for="address" class="col-md-4 col-form-label text-md-right">@lang('lang.Address')</label>
                            <input id="searchTextField" type="text" class="form-control" name="address" >
                            <br>
                            <input name="latitude" class="MapLat"  type="hidden" placeholder="Latitude" style="width: 161px;"  >
                            <input name="longitude" class="MapLon"  type="hidden" placeholder="Longitude" style="width: 161px;" > 

                            <div id="map_canvas" style="height: 350px;width: 500px;margin: 0.6em;"></div>
                            <div class="help-block with-errors"></div>
                         </div>

                          

                            
                            <div class="form-group">
                                <button type="submit" class="btn btn-secondary " style="pointer-events: all; cursor: pointer;">@lang('lang.Register Now')</button>

                            </div>

                        </form>
                        <div class="margin-bottom-50"></div>
                        
                     </div>
                </div>

            </div>
            
        </div>
    </div>
@endsection
