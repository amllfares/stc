<!doctype html>
<!--[if lte IE 8]>
  <html class="ie8 no-js" lang="en">
<![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html class="not-ie no-js" lang="en">
<!--<![endif]-->

<head>
	<title>STC</title>
	<!-- Start meta -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- End meta -->

	<!-- Start favicon -->
	<link rel="shortcut icon" href="{{ url('assets/back/intro/images/favicon/favicon.png') }}">
	<link rel="apple-touch-icon" href="{{ url('assets/back/intro/images/favicon/apple-touch-icon.png') }}">
	<link rel="apple-touch-icon" sizes="72x72" href="{{ url('assets/back/intro/images/favicon/apple-touch-icon-72x72.png') }}">
	<link rel="apple-touch-icon" sizes="114x114" href="{{ url('assets/back/images/favicon/apple-touch-icon-114x114.png') }}">
	<!-- End favicon -->

	<!-- Start CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	    crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9"
	    crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="{{ url('assets/back/intro/css/styles.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('assets/back/intro/css/mediaqueryes.css') }}">
	<!-- End CSS -->

	<!-- Start html5 shiv -->
	<script src="{{ url('assets/back/intro/css/mediaqueryes.css') }}"></script>
	<!-- End html5 shiv -->

</head>

<body>
	<!-- Start header -->
	<header>
		<div class="container-fluid">
			<div class="d-flex justify-content-between align-items-center">
				<a href="#" class="logo">
					<img src="{{ url('assets/logo.png') }}" alt="test" >
				</a>
				<!-- Start dropdown -->
				<div class="dropdown">
					<button class="btn dropdown-toggle scale-icons-hover d-flex align-items-center" type="button" id="notificationDropdown" data-toggle="dropdown"
					    aria-haspopup="true" aria-expanded="false">
						<div class="first-navbar-item d-flex align-items-center">
							<i class="fas fa-user"></i>
							<span>{{ auth()->user()->name }} </span>
						</div>
					</button>
					<!-- Start dropdown-menu -->
				

					  <div class="dropdown-menu custom-dropdown-small changeLang-dropdown-menu" aria-labelledby="notificationDropdown">
					         	
                                    <a class="notificationBlock d-flex align-items-center" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                       <i class="fas fa-sign-out-alt" style="padding-left: 10px;"></i> {{ __('Logout') }}
                                    </a> 
                                 

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                       {{ csrf_field() }}
                                    </form>
                                </div>
					<!-- End dropdown-menu -->
				</div>
				<!-- End dropdown -->
			</div>
		</div>
	</header>
	<!-- End header -->
	<div class="contaienr-fluid">
		<div class="row">
		<div style="margin: auto; width: 700px;">
		<!-- 	<div class="col-md-3">
				<a href="{{ url('/admin') }}" class="kashat-link">
					<i class="fas fa-cogs"></i>
					<span>administrator</span>
				</a>
			</div>
			<div class="col-md-3">
				<a href="#" class="kashat-link">
					<i class="fas fa-chart-line"></i>
					<span>sales</span>
				</a>
			</div> -->
	      <div class="col-md-12" style="width: 330px; float: left;">
				<a href="{{ url('/admin') }}" class="kashat-link">
					<i class="fas fa-file-alt"></i>
					<span>content</span>
				</a>
			</div>
			<div class="col-md-12" style="float: left;width: 330px; margin-left: 40px;">
				<a href="{{ url('/') }}" class="kashat-link">
					<i class="fas fa-link"></i>
					<span>website</span>
				</a>
			</div>
			</div>
<!-- 			<div class="col-md-3">
				<a href="#" class="kashat-link">
					<i class="fas fa-file"></i>
					<span>Kashat contaract</span>
				</a>
			</div>
			<div class="col-md-3">
				<a href="{{ url('campaign') }}" class="kashat-link">
					<i class="fas fa-star"></i>
					<span>Kashat campaign</span>
				</a>
			</div>
			<div class="col-md-3">
				<a href="#" class="kashat-link">
					<i class="fas fa-dollar-sign"></i>
					<span>Kashat financial</span>
				</a>
			</div>
			<div class="col-md-3">
				<a href="#" class="kashat-link">
					<i class="fas fa-life-ring"></i>
					<span>Kashat help desk</span>
				</a>
			</div>
			<div class="col-md-3">
				<a href="#" class="kashat-link">
					<i class="fas fa-globe"></i>
					<span>Kashat marketing</span>
				</a>
			</div>
			<div class="col-md-3">
				<a href="#" class="kashat-link">
					<i class="fas fa-chart-line"></i>
					<span>Kashat kpi's</span>
				</a>
			</div>
			<div class="col-md-3">
				<a href="#" class="kashat-link">
					<i class="fas fa-users"></i>
					<span>Kashat loyalty</span>
				</a>
			</div>
			<div class="col-md-3">
				<a href="#" class="kashat-link">
					<i class="fas fa-home"></i>
					<span>Kashat stores</span>
				</a>
			</div>
 -->
		</div>
	</div>

	<!--Start JS-->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	    crossorigin="anonymous"></script>
	<!--[if lt IE 9]>
		<script src="js/respond.min.js"></script>
		<script src="js/jquery.selectivizr.min.js"></script>
	<![endif]-->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	    crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	    crossorigin="anonymous"></script>
	<script src="js/custom.js"></script>
	<!--End JS-->
</body>

</html>
