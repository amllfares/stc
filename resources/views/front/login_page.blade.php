@extends('layout.frontlayout')
@section('content')
<div class="section banner-page" style="background-image:url('{{asset('/assets/back/upload/stc.png')}}')">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="title-page"> @lang('lang.Login') </div>
                    <ol class="breadcrumb">
                        <li><a href="{{url('/')}}">STC</a></li>
                        <li class="active">@lang('lang.Login') </li>
                    </ol>
                </div>
            </div>
        </div>
    </div> 

<div class="section contact overlap">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-md-4 col-md-push-8">
                    <div class="widget download">
                        
                    </div>
                    

                </div>
                <div class="col-sm-8 col-md-8 col-md-pull-2">
                    <div class="content">
                        
                        <div class="margin-bottom-30"></div>
                        <h3 class="section-heading-2">
                            @lang('lang.Login Details')
                        </h3>
                        <br><br><br>
                         @if(Session::has('Register'))
                            <div class="alert alert-danger" style="background-color:#041e42;">
                                <div class="m-alert__icon">
                                    <i class="flaticon-exclamation-1"></i>
                                </div>
                                <div class="m-alert__text" style="color: white;">
                                    {{ Session::get('Register') }}
                                </div>
                            </div>
                          @endif
                         

                        <form action="{{url(Request::segment('1').'/login_success')}}"  class=" form-contact"  data-toggle="validator" novalidate="true" method="POST" enctype="multipart/form-data" >
                             {{ csrf_field() }}
             

                          <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">@lang('lang.E-Mail Address')</label>

                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                
                                <div class="help-block with-errors"></div>
                         </div>

                         <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">@lang('lang.Password')</label>

                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                           
                                
                                <div class="help-block with-errors"></div>
                         </div>

                         
                         
                            
                            <div class="form-group">
                                <button type="submit" class="btn btn-secondary " style="pointer-events: all; cursor: pointer;">@lang('lang.Login')</button>
                                <a type="button" href="{{route('password.request')}}" class="btn btn-cta" style="pointer-events: all; cursor: pointer;">@lang('lang.Forget Password?')</a>
                                @if(Request::segment('1') == 'en')
                                    <br>
                                    <br>
                                    <b> If You Don't Have An Account  </b>                                
                                    <a type="button"  href="{{url(Request::segment('1').'/register_page')}}" class="btn btn-secondary " style="pointer-events: all; cursor: pointer;">@lang('lang.Register Now')</a>
                                @else
                                    <br>
                                    <br>
                                    <b> اذا كنت لا تملك حسابا   </b>
                                    <a type="button"  href="{{url(Request::segment('1').'/register_page')}}" class="btn btn-secondary " style="pointer-events: all; cursor: pointer;">  @lang('lang.Register Now')</a>
                                @endif


                            </div>

                        </form>

                        <div class="margin-bottom-50"></div>
                        
                     </div>
                </div>

            </div>
            
        </div>
    </div>

@endsection

