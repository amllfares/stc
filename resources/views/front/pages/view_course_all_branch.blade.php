@foreach($subject as $sub)
						<?php if ($branch_id== '0') {
							$schedules= App\Course_schedule::orderBy('id', 'desc')->where('course_id',$sub->id)->with('instructor')->get(); 
						}
						else{

						  $schedules= App\Course_schedule::orderBy('id', 'desc')->where('course_id',$sub->id)->where('branch_id',$branch_id)->with('instructor')->get(); 
						}?>
						<table class="">

						    <thead>
						    <tr style="background-color: #041e42;color: white;">
						      
						   
						        <th title="Field #5"  class="text-center "> @lang('lang.Courses') </th>
					 	        <th title="Field #5"  class="text-center "> @lang('lang.Schedule') </th>
					 	        <th title="Field #5"  class="text-center ">@lang('lang.Course content') </th>
					 	        <th title="Field #5"  class="text-center ">@lang('lang.Dates') </th>
	 				 	        <th title="Field #5"  class="text-center ">@lang('lang.Course Details') </th>
					 	        <th title="Field #5"  class="text-center ">@lang('lang.Reservation') </th>
					 	        <th title="Field #5"  class="text-center ">@lang('lang.Branch') </th>


						      
						    </tr>
						    </thead>
						    <tbody>
					        		@foreach($schedules as $schedule)
					                <tr>
					                    <td><?php echo $sub->{'name_'.$lang} ?></td>
					                    <td><?php echo $schedule->{'schedule_'.$lang} ?> </td>
					                     <td><?php echo $schedule->branch->{'name_'.$lang} ?> </td>
					                    <td>
					                    	<a class="btn btn-secondary"  onclick="content('{{url('/')}}','{{$sub->id}}','{{$schedule->id}}','{{$lang}}')" style="padding: 10px;" > 
					                    	 <i class="m-nav__link-icon fa fa-list" style="color: white;"></i>@lang('lang.Course content')</a>
					                    </td>
					                    <td>
					                    	<a class="btn btn-primary show_date" onclick="show_date('{{url('/')}}','{{$sub->id}}','{{$schedule->id}}','{{$lang}}')" style="padding: 10px;">
                                            <i class="m-nav__link-icon fa fa-calendar" style="color: white;"></i> @lang('lang.Dates')
                                        </a>
					                    	
					                    </td>
					                     <td>
					                    	<a class="btn btn-primary"  style="padding: 10px;" onclick="more('{{url('/')}}','{{$sub->id}}','{{$schedule->id}}','{{$lang}}')">
					                    	 <i class="m-nav__link-icon fa fa-eye" style="color: white;"></i> @lang('lang.Course Details')</a>
					                    	
					                    </td>

					                    <td class="text-center">
					                    	@guest
					                    	<a href="javascript:void(0);"  class="btn btn-secondary text-center resve"  style="padding: 10px;"  data-target=".bd-example-modal2-sm" data-toggle="modal" lang="{{$lang}}">
					                    	 <i class="m-nav__link-icon fa fa-check-circle" style="color: white;"></i>@lang('lang.Reservation')  </a>
					                    	@else
					                    	<a href="{{ url(Request::segment('1').'/course_reservation/'.$sub->id.'/'.$schedule->id)}}" class="btn btn-secondary text-center" style="padding: 10px;" lang="{{$lang}}">
					                    	 <i class="m-nav__link-icon fa fa-check-circle" style="color: white;"></i>@lang('lang.Reservation')  </a>
					                    	 @endguest
					                    	
					                    </td>
					             
					                </tr>
					                @endforeach
					               

						    </tbody>
						</table>




@endforeach




