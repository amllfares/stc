@extends('layout.frontlayout')
@section('content')

	<!-- BANNER -->
	<div class="section banner-page" style="background-image:url('{{asset($page->banner)}}')">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="title-page"><?php echo $page->{'name_'.Request::segment('1')} ?></div>
					<ol class="breadcrumb">
						<li><a href="{{url('/')}}">STC</a></li>
						<li class="active"><?php echo $page->{'name_'.Request::segment('1')} ?></li>
					</ol>
				</div>
			</div>
		</div>
	</div>

	

		<!-- Team -->
	<div class="section why overlap">
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-md-4 col-md-push-8">
					
					<div class="widget widget-text">
						<div class="widget-title">
							 @lang('lang.comming_news')
						</div>
					
					</div> 
					@foreach($latest_news as $last_new)

					<div class="box-partner">
						<!-- item 1 -->
						<div class="itee">
							<div class="box-image">
								<div class="client-img">
									<a href="{{ url($lang.'/news/'.$last_new->id)}}"><img src="{{asset($last_new->photo)}}" alt="" class="img-responsive imgg"></a>
								</div>
							</div>
							<div class="meta">
								<div class="meta-date"><i class="fa fa-clock-o"></i> <?php echo $page->created_at ?></div>
						    </div>
							<div class="box-info">
								<div class="heading"><?php echo $last_new->{'name_'.$lang} ?></div>
								<p><?php echo substr($page->{'description_'.$lang},0,150) ?>.......</p>
							</div>
						</div>
					
				    </div>
				    @endforeach

					

				</div>
				<div class="col-sm-8 col-md-8 col-md-pull-4">
					<div class="single-news">
						<div class="image">
							<img src="{{asset($page->photo)}}" alt="" class="img-responsive">  
						</div>
						<h2 class="blok-title">
							<?php echo $page->{'name_'.$lang} ?>
						</h2>
						<div class="meta">
							<div class="meta-date"><i class="fa fa-clock-o"></i> <?php echo $page->created_at ?></div>
		
						</div>
						<p><?php echo $page->{'description_'.$lang} ?>.</p>
						
				 	</div>

				
				</div>

			</div>
			
		</div>
	</div>

@endsection	