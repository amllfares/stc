                   <div>
                    <div class="single-page">
                        
                    <div class="content">
                        
                        <div class="margin-bottom-30"></div>
                     
                        <form action="{{url(Auth::user()->lang.'/applay_store/')}}"  class="form-contact"  data-toggle="validator" novalidate="true" method="POST" enctype="multipart/form-data" >
                             {{ csrf_field() }}
                             <input type="hidden" name="career_id" value="{{$career->id}}">
                            
                             <div class="form-group ">
                                <label for="file" class="col-md-4 col-form-label text-md-right">@lang('lang.file')</label>

                                <input id="file" type="file" class="form-control{{ $errors->has('file') ? ' is-invalid' : '' }}" name="file" required>

                                @if ($errors->has('file'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('file') }}</strong>
                                    </span>
                                @endif
                           
                                
                                <div class="help-block with-errors"></div>
                            </div>
 

                            <div class="form-group">
                                <button type="submit" class="btn btn-secondary " style="pointer-events: all; cursor: pointer;">@lang('lang.Confirm')</button>

                            </div>

                        </form>
                        <div class="margin-bottom-50"></div>
                        
                     </div>
                        
                     </div>
                </div>