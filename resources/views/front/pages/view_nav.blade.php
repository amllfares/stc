@extends('layout.frontlayout')
@section('content')

	<!-- BANNER -->
	<div class="section banner-page" style="background-image:url('{{asset($page->banner)}}')">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="title-page"><?php echo $page->{'name_'.Request::segment('1')} ?></div>
					<ol class="breadcrumb">
						<li><a href="{{url('/')}}">STC</a></li>
						<li class="active"><?php echo $page->{'name_'.Request::segment('1')} ?></li>
					</ol>
				</div>
			</div>
		</div> 
	</div>

	<!-- Team -->
	

	@if($page->id=="3")

	<div class="section contact overlap">
		<div class="container">
			<div class="row">
				<div>
					<h3 class="text-center"><?php echo $page->{'name_'.Request::segment('1')} ?></h3>
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3453.1893314661347!2d31.327839000000015!3d30.060107000000002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14583e5cdb650f4f%3A0x9da55607d18a6930!2sSwcS+Oil+%26+Gas+Training!5e0!3m2!1sar!2seg!4v1420639407103" width="100%" height="450" frameborder="0" style="border:0"></iframe>
				</div>
				<div class="col-sm-4 col-md-4 col-md-push-8">

					<div class="widget download">
						
					</div>
					<div class="widget contact-info-sidebar">
						<div class="widget-title">
							
							@lang('lang.Contact Data')</div>
						<ul class="list-info">
							<li>
								<div class="info-icon">
									<span class="fa fa-map-marker"></span> 
		 						</div>
								<div class="info-text"> {{setting('address_'.Request::segment('1'))}}</div> </li>
							<li>
								<div class="info-icon">
									<span class="fa fa-phone"></span>
								</div>
								  @if(Request::segment('1') == "ar")
                                  <?php $western_arabic = array('0','1','2','3','4','5','6','7','8','9');
                                  $eastern_arabic = array('٠','١','٢','٣','٤','٥','٦','٧','٨','٩');
                                  $str=setting('phone_'.Request::segment('1'));
                                  $str=str_replace($western_arabic, $eastern_arabic, $str);
                                  $s=str_replace(' (+2) ','', setting('phone_en'));
                                  $s2=str_replace('(+2) ','', setting('phone2_en'));
                                  $str2=setting('phone2_'.Request::segment('1'));
                                  $str2=str_replace($western_arabic, $eastern_arabic, $str2);?>
                                  <div class="info-text"><a href="https://api.whatsapp.com/send?phone=002{{$s}}" target="_blank">{{$str}}</a></div>
                                  @else
                                    <?php 
                                   		$s=str_replace(' (+2) ','', setting('phone_en'));
                                        $s2=str_replace('(+2) ','', setting('phone2_en')); 
                                    ?>
                                  <div class="info-text">
                                  	<a href="https://api.whatsapp.com/send?phone=002{{$s}}" target="_blank">{{setting('phone_en')}}</a>
                                  </div>
                                  @endif
								
							</li>
							<li>
								<div class="info-icon">
									<span class="fa fa-phone"></span>
								</div>
								@if(Request::segment('1') == "ar")
								<?php 
									$s=str_replace(' (+2) ','', setting('phone_en'));
                                    $s2=str_replace('(+2) ','', setting('phone2_en')); 
                                ?>
								<div class="info-text"><a href="https://api.whatsapp.com/send?phone=002{{$s2}}" target="_blank">{{$str2}}</a></div>
								@else
								<?php
									$s=str_replace(' (+2) ','', setting('phone_en'));
									$s2=str_replace('(+2) ','', setting('phone2_en')); 

								?>
								<div class="info-text">
									<a href="https://api.whatsapp.com/send?phone=002{{$s2}}" target="_blank">

										{{setting('phone2_en')}}
									</a>
								</div>
								@endif
							</li>
							<li>
								<div class="info-icon">
									<span class="fa fa-envelope"></span>
								</div>
								<div class="info-text">{{setting('email')}}</div>
							</li>
							
						</ul>
					</div> 

				</div>
				<div class="col-sm-8 col-md-8 col-md-pull-4">
					<div class="content">
						<p class="section-heading-3"></p>
						<div class="margin-bottom-30"></div>
						<h3 class="section-heading-2">
							@lang('lang.contact details')
						</h3>
						@if ($errors->any())
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
						@endif
						<form action="{{url(Request::segment('1').'/contact_us')}}" class="form-contact"  data-toggle="validator" novalidate="true" method="post">
							 {{ csrf_field() }} 
							<div class="form-group">
								<input type="text" class="form-control" id="p_email" placeholder="@lang('lang.Name')....." required="" name="name">
								<div class="help-block with-errors"></div>
							</div>

							<div class="form-group">
								<input type="email" class="form-control" id="p_email" placeholder="@lang('lang.enter email')....." required="" name="email">
								<div class="help-block with-errors"></div>
							</div>
							<div class="form-group">
								<input type="text" class="form-control" id="p_subject" placeholder="@lang('lang.mobile')......" name="mobile">
								<div class="help-block with-errors"></div>
							</div>
							<div class="form-group">
								 <textarea id="p_message" class="form-control" rows="6" placeholder="@lang('lang.Write Message')...." name="feedback"></textarea>
								<div class="help-block with-errors"></div>
							</div>
							<div class="form-group">
								
								<button type="submit" class="btn btn-secondary disabled" style="pointer-events: all; cursor: pointer;"> @lang('lang.Send')  <i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
							</div>
						</form>
						<div class="margin-bottom-50"></div>
						<p><em></em></p>
					 </div>
				</div>

			</div>
			
		</div>
	</div>	
	@elseif($page->id=="2")
	<div class="section why overlap">
		<div class="container">
			<div class="row">
				<h3 style="margin-left: 18px;margin-right: 18px;"><?php echo $page->{'name_'.Request::segment('1')} ?></h3>
				<div class="col-sm-5 col-md-5">
					<div class="director-image">
						<div class="director-image-title"><?php echo $page->{'bref_'.Request::segment('1')} ?></div>
						<img src="{{ URL($page->photo)}}" alt="bos-photo">
					</div>
					<div class="margin-bottom-30"></div>
				</div>
				<div class="col-sm-7 col-md-7">
					<h3 class="text-center"></h3>
					<h3 class="director-title"><?php echo $page->{'bref_'.Request::segment('1')} ?></h3>
					
					<div class="margin-bottom-30"></div>
					<p><?php echo $page->{'description_'.Request::segment('1')} ?></p>

						
					<blockquote>
						<p></p>
					</blockquote>
				</div>
				
			</div>
		</div>
	</div>
	@elseif($page->id=="5")
	<div class="section why overlap">
		<div class="container">
			<h3 class="text-center"><?php echo $page->{'name_'.Request::segment('1')} ?></h3>
			<div class="row ">
				@foreach($types as $type)
				<div class="col-sm-6 col-md-4 thum">
					<!-- BOX 5 -->
					<div class="feature-box-8">
		              <div class="media" style="    height: 241px;">
		                <img src="{{asset($type->image)}}" alt="rud" class="img-responsive">
		              </div>
		              <div class="body">
		                <div class="icon-holder">
		                  <span class="fa fa-flask"></span>
		                </div>
		                <a href="{{ url($lang.'/type/'.$type->id)}}" class="title"><?php echo $type->{'name_'.Request::segment('1')} ?></a>
	 	                <p><?php echo $page->{'name_'.Request::segment('1')} ?></p>
		                <a href="{{ url($lang.'/type/'.$type->id)}}" class="readmore">@lang('lang.LEARN MORE')</a>
		              </div>
		            </div>
				</div>
				@endforeach
				 <a href="javascript:void(0);" class="btn btn-secondary text-center share" base_url="{{ url('/') }}"  data-target=".bd-example-modal-sm" data-toggle="modal">
					<i class="m-nav__link-icon fa fa-share-alt" style="color: white;"></i>  @lang('lang.Shar')  </a>
			</div>
		</div>
	</div>
	@elseif($page->id=="12")
	<div class="section why overlap">
		<div class="container">
			<h3 class="text-center"><?php echo $page->{'name_'.Request::segment('1')} ?></h3>
			<div class="row ">
				@foreach($branches as $branch)
				<div class="box-icon-4" style="margin: 32px;">
                        <div class="icon"><i class="fa fa-map-marker"></i></div>
                        <div class="body-content">
                        	<a href="{{ url($lang.'/branch/'.$branch->id)}}"  class="heading"><?php echo $branch->{'name_'.Request::segment('1')} ?></a><br>
                            <p>
                            	<?php echo $branch->{'address_'.Request::segment('1')} ?><br>
                            	<?php echo $branch->contact_email ?> <br>
                            	

                            </p>
                          

                        </div>
                 </div>
				@endforeach
			</div>
		</div>
	</div>
	@elseif($page->id=="6")
		
	<div class="section blog">
		<div class="container">
			@if($count > 0)
			<div class="row">
				
				
				@foreach($children as $child)
				<div class="col-sm-6 col-md-4">
					<!-- BOX 1 -->
					<div class="box-news-1">
						<div class="media gbr">
							<a href="{{ url($lang.'/news/'.$child->id)}}">
							<img src="{{asset($child->photo)}}" alt="" class="img-responsive">
							</a>
						</div>
						<div class="body">
							<div class="title"><a href="{{ url($lang.'/news/'.$child->id)}}" title=""><?php echo $child->{'name_'.$lang} ?></a></div>
							<div class="meta">
								<span class="date"><i class="fa fa-clock-o"></i> <?php echo $page->created_at?></span>
								<!-- <span class="comments"><i class="fa fa-comment-o"></i> 0 Comments</span> -->
							</div>
						</div>
					</div>
				</div>
				@endforeach
					
			</div>
			@else
			<div class="row">
				<div class="text-center">
				<div class="alert alert-warning" role="alert">
				  @lang('lang.There Is No News') 
				</div>
				</div>
			</div>
				
			@endif
		</div>
	</div> 
	@elseif($page->id=="7")
	<div class="section blog">
		<div class="container">
			@if($count > 0)
			<div class="row">
				
				@foreach($children as $child)
				<div class="col-sm-6 col-md-4">
					<!-- BOX 1 -->
					<div class="box-news-1">
						<div class="media gbr">
							<a href="{{ url($lang.'/events/'.$child->id)}}">
							<img src="{{asset($child->photo)}}" alt="" class="img-responsive">
							</a>
						</div>
						<div class="body">
							<div class="title"><a href="{{ url($lang.'/events/'.$child->id)}}" title=""><?php echo $child->{'name_'.$lang} ?></a></div>
							<div class="meta">
								<span class="date"><i class="fa fa-clock-o"></i> <?php echo $page->created_at?></span>
								<!-- <span class="comments"><i class="fa fa-comment-o"></i> 0 Comments</span> -->
							</div>
						</div>
					</div>
				</div>
				@endforeach
			
				
			</div>
			@else
			<div class="row">
				<div class="text-center">
				<div class="alert alert-warning" role="alert">
				  @lang('lang.There Is No Event') 
				</div>
				</div>
			</div>
				
			@endif

		</div>
	</div> 
	@elseif($page->id=="8")

	<div class="section why overlap">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<nav class="categories">
					<ul class="portfolio_filter dark">
						<li><a href="" class="active" data-filter="*">@lang('lang.all')</a></li>
						@foreach($children as $child)
						<li><a href="" data-filter=".eco<?php echo $child->id ?>"><?php echo $child->{'name_'.$lang} ?></a></li>
						@endforeach
					</ul>
				</nav>
				</div>
			</div>
			<div class="row grid-services">

				@foreach($children as $childs)
					@if(isset($childs->gall) && !empty($childs->gall))
						@foreach($childs->gall as $gall)
							<div class="col-sm-6 col-md-4 eco<?php echo $childs->id ?>">
								<div class="box-image-4">
									<a href="{{asset($gall->file)}}" title="Industrial Complex">
										<div class="media">
											<img src="{{asset($gall->file)}}" alt="" class="img-responsive">
										</div>
										<div class="body">
											<div class="content">
												<h4 class="title"><?php echo $gall->{'name_'.$lang} ?></h4>
												<span class="category"><?php echo $gall->{'bref_'.$lang} ?></span>
											</div>
										</div>
									</a>
								</div>
							</div>
						@endforeach	
					@endif		
				@endforeach
				


			</div>
		</div>
	</div> 

			
	@elseif($page->id=="11")

	<div class="section why overlap">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<h3 class="text-center"><?php echo $page->{'name_'.Request::segment('1')} ?></h3>

					<div class="margin-bottom-50"></div>
					 <div class="single-page" style="">
					 	    @if ($errors->any())
							    <div class="alert alert-danger">
							        <ul>
							            @foreach ($errors->all() as $error)
							                <li>{{ $error }}</li>
							            @endforeach
							        </ul>
							    </div>
							@endif
				
						<div class="panel-group panel-faq" id="accordion" role="tablist" aria-multiselectable="true">
					
					     @foreach($careers as $career)
					     
						  <div class="panel panel-default">

							<div class="panel-heading" role="tab" id="heading3">
							  <h4 class="panel-title" >
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#{{$career->id}}" aria-expanded="false" aria-controls="{{$career->id}}">
								 <?php echo $career->{'tilte_'.Request::segment('1')} ?>
								</a>
							  </h4>
							  <br>
							</div>
							<div id="{{$career->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3" aria-expanded="false" style="height: 0px;">
							  <div class="panel-body">
							  	
							  	<div class="col-md-7">
							  	<div id="not_{{$career->id}}">
							  		
							  	</div>

								<p></p>
								<?php echo $career->{'description_'.Request::segment('1')} ?>
								
							    </div>
							    <div class="col-md-5">
							    	<img src="{{url($career->image)}}">
							    	
							    </div>
							    @guest
							    <a href="javascript:void(0);"  class="btn btn-secondary text-center resve"  style="padding: 10px;"  data-target=".bd-example-modal2-sm" data-toggle="modal">
					                    	 <i class="m-nav__link-icon fa fa-check-circle" style="color: white;"></i>@lang('lang.Applay for job')  </a>
							    
				                @else
				                <?php  $user_count=App\Apply_Career::where('user_id',auth()->user()->id)->where('career_id',$career->id)->count(); ?>
							    @if($user_count >0)
							    <a class="btn btn-secondary"  type="button"   onclick="apply_again('{{url('/')}}','{{Request::segment('1')}}','{{$career->id}}')" > @lang('lang.Applay for job')
				                    <i class="fa fa-check" aria-hidden="true"></i>
				                </a>
				                @else
				                <a class="btn btn-secondary applay" href="javascript:void(0);" type="button"  id="<?php echo $career->id; ?>" lang="{{Request::segment('1')}}"  base_url="{{ url('/') }}" ldata-dismiss="modal"> @lang('lang.Applay for job')
				                    <i class="fa fa-check" aria-hidden="true"></i>
				                </a>
				                @endif 
				                @endguest

							  </div>
							</div>
						  </div>
					     @endforeach
						</div>
					 </div>
					

				</div>

			</div>
		</div>
	</div>

	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog dial" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	       
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
	      </div>
	    </div>
	  </div>
	</div>

    @else
    <div class="section why overlap">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<h3 class="text-center"><?php echo $page->{'name_'.Request::segment('1')} ?></h3>

					<div class="margin-bottom-50"></div>
					<div class="faq-1">
						<div class="item">
							
							<div>
							<?php echo $page->{'description_'.Request::segment('1')} ?>
							</div>
						</div>
					
					</div>
					

				</div>

			</div>
		</div>
	</div>
	@endif

	


 @endsection

 <div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">share course</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<div class="text-center">
      	<div id="social-links">
		 
		   <a href="https://www.facebook.com/sharer/sharer.php?u={{url(Request::segment('1').'/courses/')}}" class="social-button " id="" ><span class="fa fa-facebook facebookk"></span></a>
		    <a href="https://twitter.com/intent/tweet?text=my share text&amp;url={{url(Request::segment('1').'/courses/')}}" class="social-button " id=""><span class="fa fa-twitter twitterr"></span></a>
		    <a href="https://plus.google.com/share?url={{url(Request::segment('1').'/courses/')}}" class="social-button " id=""><span class="fa fa-google-plus googlepluss"></span></a>
		   <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url={{url(Request::segment('1').'/courses/')}}" class="social-button " id=""><span class="fa fa-linkedin linkedinn"></span></a>

		 
       </div>
       </div>
        
      </div>
      
      </div>

	
    </div>
  </div>
</div>


<div class="modal fade bd-example-modal2-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" style="width: 350px;">
    
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<div class="text-center">
      	 <h6>@lang('lang.you need to be logged in to apply to this job') </h6>
      	
       </div>
        
      </div>
       <div class="modal-footer">
        <a type="button" class="btn btn-secondary" style="padding: 10px 9px;font-size: 12px; margin-right: 125px;" href="{{url(Request::segment('1').'/login_page')}}">@lang('lang.Login')</a>
      </div>
      
      </div>

	
    </div>
  </div>
</div>




 