@extends('layout.frontlayout')
@section('content')

<div class="section banner-page" style="background-image:url('{{asset('/assets/back/upload/stc.png')}}')">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="title-page">@lang('lang.Courses')</div>
					<ol class="breadcrumb">
						<li><a href="{{url('/')}}">STC</a></li>
						<li class="active">@lang('lang.Courses')</li>
					</ol>
				</div>
			</div>
		</div>
</div>
<div>
	
</div>
<div class="section why overlap">
		<div class="container">
			
			<div class="row">
				<div class="col-sm-4 col-md-4 col-md-push-8">
					<div class="widget categories">
						<ul class="category-nav">
							<li ><a href="{{url(Auth::user()->lang.'/view_profile')}}"> @lang('lang.Edit Profile information')</a></li>
							<li ><a href="{{url(Auth::user()->lang.'/update_password')}}">@lang('lang.Update password')</a></li>
							<li class="active"><a href="{{url(Auth::user()->lang.'/view_course_users')}}">@lang('lang.Courses')</a></li>
							<!-- <li><a href="#">Power And Energy</a></li>
							<li><a href="#">Chemical Research</a></li>
							<li><a href="#">Material Engineering</a></li> -->
						</ul>
					</div> 
		

				</div>
			<div class="col-sm-8 col-md-8 col-md-pull-4">	
			<div class="single-page text-center" style="">
					
						<h2>
							@lang('lang.Courses')
						</h2>
						 <div class="margin-bottom-50"></div>
						  <div class="margin-bottom-50"></div>


				<table class="" width="100%">
                    <thead>
                      
                    <tr style="background-color: #041e42;color: white;">
                       <!--  <th title="Field #1">course </th> -->

                        <th title="Field #2">@lang('lang.Instructor') </th>
                        <th title="Field #2">@lang('lang.From') </th>
                        <th title="Field #3">@lang('lang.To') </th>
                        <th width="150px;">@lang('lang.Schedule')</th>
                        <th width="80px;">@lang('lang.Dates')</th>
                        <th   title="Field #5">@lang('lang.Matrial')</th>
                        <th width="80px;"  title="Field #5">@lang('lang.Rate')</th>
                        <th width="80px;"  title="Field #5">@lang('lang.Feedback')</th>
                      
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($user_courses as $user_course)
                                <tr @if($user_course->trash==1) class='kashat_alert_danger' @endif id="trashed_{{ $user_course->id }}">
                                   
                                    <td>{{ $user_course->instructor->name_en }} </td>
                                    <td>{{ $user_course->course_schedule->from }}</td>
                                    <td>{{ $user_course->course_schedule->to }}</td>
                                    <td><?php echo $user_course->course_schedule->{'schedule_'.Auth::user()->lang} ?></td>
                                   
                                    <td>
                                         <a class="btn btn-primary show_date" href="javascript:void(0);"  course_id="<?php echo $user_course->course_id; ?>" schedule_id="<?php echo $user_course->schedule_id; ?>" base_url="{{ url('/') }}" lang="{{Auth::user()->lang}} "  style="padding:5px 5px;" >
                                            <i class="m-nav__link-icon fa fa-calendar" style="color: white;"></i> @lang('lang.Dates')
                                        </a>
                                        
                                       
                                       
                                    </td>
                                    <td>
                                    	 <a class="btn btn-secondary show text-center" href="javascript:void(0);"   base_url="{{ url('/') }}" course_id="<?php echo $user_course->course_id; ?>" style="padding:5px 5px;" lang="{{Auth::user()->lang}} " >
						                    <i class="fa fa-files-o" aria-hidden="true"></i>@lang('lang.Matrial')
						                </a>
                                    </td>

                                     <td>
                                         <a class="btn btn-primary rate" href="javascript:void(0);"  course_id="<?php echo $user_course->course_id; ?>" schedule_id="<?php echo $user_course->schedule_id; ?>" base_url="{{ url('/') }}" style="padding:5px 5px;" lang="{{Auth::user()->lang}} " >
                                            <i class="m-nav__link-icon fa fa-star-half-o" style="color: white;"></i> @lang('lang.Rate')
                                        </a>
                                        
                                       
                                       
                                    </td>

                                    <td>
                                    	@if($user_course->course_schedule->from < date("Y.m.d") && $user_course->course_schedule->to < date("Y.m.d"))
                                        <a class="btn btn-secondary feedback text-center" href="javascript:void(0);"   base_url="{{ url('/') }}" course_id="<?php echo $user_course->course_id; ?>" style="padding:5px 5px;" lang="{{Auth::user()->lang}} " schdule_id="<?php echo  $user_course->course_schedule->id; ?>" >
						                    <i class="fa fa-pencil" aria-hidden="true"></i>@lang('lang.Feedback')
						                </a>
                                        @endif
                                        
                                       
                                       
                                    </td>

                                   
                                </tr>
                                
                               
                        @endforeach
                    </tbody>

                </table>

                <!--end: Datatable -->
                <br>
              
						
					 </div>
					</div>
				
			
		</div>
		</div>
</div>



	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog dial" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
	      </div>
	    </div>
	  </div>
	</div>

@endsection