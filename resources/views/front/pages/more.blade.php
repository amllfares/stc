<table class="" >
    <thead>
    <tr style="background-color: #041e42;color: white;">
        
        <th  class="text-center ">@lang('lang.Price') </th>
        <th  class="text-center ">@lang('lang.offer') </th>
        <th  class="text-center ">@lang('lang.number of hours') </th>
        <th  class="text-center ">@lang('lang.min_candidates') </th>
        <th  class="text-center ">@lang('lang.max_candidates') </th>
        <th  class="text-center ">@lang('lang.Number Of Days') </th>
        <th  class="text-center ">@lang('lang.Number Of Reservation') </th>
       
     
    </tr>
    </thead>
    <tbody>
                                       
            <tr>
     
               <td>{{ $course->price }} @lang('lang.EGP') </td> 
               <td>{{ $course->offer }} %</td> 
               <td>{{ $course->number_hours }} @lang('lang.hour')</td>
               <td>{{ $course->min_candidates }}</td> 
               <td>{{ $course->max_candidates }}</td> 
               <td>{{ $days_number }}</td>
               <td>{{ $days_reservation }}</td>
 
            </tr>
       

    </tbody>
</table>