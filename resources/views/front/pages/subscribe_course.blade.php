                   <div>
                    <div class="single-page">
                        
                    <div class="content">
                        
            
                        <form action="{{url($lang.'/subscribe_store/')}}"  class="form-contact"  data-toggle="validator" novalidate="true" method="POST" >
                             {{ csrf_field() }}
                             
                             <input type="hidden" name="course_id" value="{{$course_id}}">
                            <div class="form-group">
                                <p class="tex">@lang('lang.subscribe now').</p>
                             <div class=" col-md-8 text-center" style="padding-right: 0px;">
                                <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            
                                <div class="help-block with-errors"></div>
                            </div>
 

                            <div>
                                <button type="submit" class="btn btn-secondary subscribe " style="pointer-events: all; cursor: pointer; padding: 3px 7px;"><i class="fa fa-envelope"></i></button>

                            </div>
                            </div>

                        </form>
                       
                        
                     </div>
                        
                     </div>
                </div>