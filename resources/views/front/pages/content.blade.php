	
			<div class="single-page" style="">
					
						<h2>
							@lang('lang.Course content')
						</h2>
						<div class="panel-group panel-faq" id="accordion" role="tablist" aria-multiselectable="true">
					
					     @foreach($contents as $content)
						  <div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading3">
							  <h4 class="panel-title" >
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#{{$content->id}}" aria-expanded="false" aria-controls="{{$content->id}}">
								 <?php echo $content->name_en ?>
								</a>
							  </h4>
							  <br>
							</div>
							<div id="{{$content->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3" aria-expanded="false" style="height: 0px;">
							  <div class="panel-body">
								<p><?php echo $content->bref_en ?></p>
								<?php echo $content->content_en ?>
							  </div>
							</div>
						  </div>
					     @endforeach
						</div>
					 </div>
			