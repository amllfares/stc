                   <div>
                    <div class="single-page">
                        
                    <div class="content">
                        
                        <div class="margin-bottom-30"></div>
                        <h3 class="section-heading-2">
                           @lang('lang.add kill sheet') 
                        </h3>
                      
                        

                        <form action="{{url($lang.'/kill_can_store/')}}"  class="form-contact"  data-toggle="validator" novalidate="true" method="POST" >
                             {{ csrf_field() }}
                            <div class="form-group">
                                 <label for="kill_sheet_id" class="col-md-4 col-form-label text-md-right">@lang('lang.kill sheet')</label>
                               <select name="kill_sheet_id" id="parent_category" class="form-control" placeholder="Kill_sheet" required="">
                                
                                    @foreach ($kill_sheets as $kill_sheet )
                                        <option value="{{ $kill_sheet->id }}"> <?php echo $kill_sheet->{'name_'.$lang} ?></option>
                                    @endforeach
                                </select>
                           
                                
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                 <label for="kill_sheet_id" class="col-md-4 col-form-label text-md-right">@lang('lang.User')</label>
                               <select name="user_id" id="parent_category" class="form-control" placeholder="Kill_sheet" required="">
                                    
                                    @foreach ($user_schedule as $user_sch )
                                        <option value="{{ $user_sch->id }}">{{ $user_sch->user->name}}</option>
                                    @endforeach
                                </select>
                           
                                
                                <div class="help-block with-errors"></div>
                            </div>

                             <div class="form-group ">
                                <label for="value" class="col-md-4 col-form-label text-md-right">@lang('lang.Value')</label>

                                <input id="value" type="text" class="form-control{{ $errors->has('value') ? ' is-invalid' : '' }}" name="value" required>

                                @if ($errors->has('value'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('value') }}</strong>
                                    </span>
                                @endif
                           
                                
                                <div class="help-block with-errors"></div>
                            </div>
 

                            <div class="form-group">
                                <button type="submit" class="btn btn-secondary " style="pointer-events: all; cursor: pointer;">@lang('lang.Confirm')</button>

                            </div>

                        </form>
                        <div class="margin-bottom-50"></div>
                        
                     </div>
                        
                     </div>
                </div>