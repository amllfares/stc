@extends('layout.frontlayout')
@section('content')

<div class="section banner-page" style="background-image:url('{{asset('/assets/back/upload/stc.png')}}')">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="title-page"> {{Auth::user()->name}}</div>
					<ol class="breadcrumb">
						<li><a href="{{url('/')}}">STC</a></li>
						<li class="active">{{Auth::user()->name}}</li>
					</ol>
				</div>
			</div>
		</div>
</div>

<div class="section why overlap"  id="edit">
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-md-4 col-md-push-8">
					<div class="widget categories">
						<ul class="category-nav">
							<li ><a href="{{url(Auth::user()->lang.'/view_profile')}}"> @lang('lang.Edit Profile information')</a></li>
                            <li class="active"><a href="{{url(Auth::user()->lang.'/update_password')}}">@lang('lang.Update password')</a></li>
                            <li ><a href="{{url(Auth::user()->lang.'/view_course_users')}}">@lang('lang.Courses')</a></li>
                          <!--   <li><a href="#">Power And Energy</a></li>
                            <li><a href="#">Chemical Research</a></li>
                            <li><a href="#">Material Engineering</a></li> -->
						</ul>
					</div> 
		

				</div>
				<div class="col-sm-6 col-md-6 col-md-pull-3">
					<div class="single-page">
						
                    <div class="content">
                        
                        <div class="margin-bottom-30"></div>
                        <h3 class="section-heading-2">
                           @lang('lang. Edit user password')
                        </h3>
                        <br><br><br>
                        <!--  @if(Session::has('edit_profile'))
                            <div class="alert alert-danger" style="background-color:#041e42;">
                                <div class="m-alert__icon">
                                    <i class="flaticon-exclamation-1"></i>
                                </div>
                                <div class="m-alert__text" style="color: white;">
                                    {{ Session::get('edit_profile') }}
                                </div>
                            </div>
                          @endif -->

                        <form action="{{url(Auth::user()->lang.'/edit_password/'.Auth::user()->id)}}"  class="form-contact"  data-toggle="validator" novalidate="true" method="POST" >
                             {{ csrf_field() }}
                             {{ method_field('PATCH') }}
                           
                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">@lang('lang.Password')</label>

                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                           
                                
                                <div class="help-block with-errors"></div>
                            </div>
 
                           <div class="form-group row">
                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">@lang('lang.Confirm Password')</label>

                               
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                             
                               
                                
                                <div class="help-block with-errors"></div>
                            </div>


                            <div class="form-group">
                                <button type="submit" class="btn btn-secondary " style="pointer-events: all; cursor: pointer;">@lang('lang.Confirm')</button>

                            </div>

                        </form>
                        <div class="margin-bottom-50"></div>
                        
                     </div>
						
					 </div>
				</div>

			</div>
		</div>
	</div>
@endsection