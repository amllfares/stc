@extends('layout.frontlayout')
@section('content')

<div class="section banner-page" style="background-image:url('{{asset('/assets/back/upload/stc.png')}}')">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="title-page">@lang('lang.Courses')</div>
					<ol class="breadcrumb">
						<li><a href="{{url('/')}}">STC</a></li>
						<li class="active">@lang('lang.Courses')</li>
					</ol>
				</div>
			</div>
		</div>
</div>
<div> 
	
</div>
<div class="section why overlap">
		<div class="container">
			
			<div class="row">
				<div class="col-sm-4 col-md-4 col-md-push-8">
					<div class="widget categories">
						<ul class="category-nav">
							<li class=""><a href="{{url(Auth::user()->lang.'/view_profile')}}"> @lang('lang.Edit Profile information')</a></li>
							<li class=""><a href="{{url(Auth::user()->lang.'/update_password')}}">@lang('lang.Update password')</a></li>
							<li class="active"><a href="{{url(Auth::user()->lang.'/view_course_users')}}">@lang('lang.Courses')</a></li>
							
						</ul>
					</div> 
		

				</div>
			<div class="col-sm-8 col-md-8 col-md-pull-4">	
			<div class="single-page text-center" style="">
					
						<h2>
							@lang('lang.Courses')
						</h2>
						 <div class="margin-bottom-50"></div>
						  <div class="margin-bottom-50"></div>

                          
				
					<table class="" style="padding:10%;">
					    <thead>
					    <tr style="background-color: #041e42;color: white;">
					      
					   
					        <th title="Field #5"  class="text-center "> @lang('lang.Past Dates') </th>
					         <th title="Field #5"  class="text-center "> @lang('lang.kill sheet') </th>
					      
					    </tr>
					    </thead>
					    <tbody>
					        		@foreach($past_dates as $past_date)
					                <tr>
					                   
					                    <td>{{ $past_date->date}} </td>
					                    <td>
					                    	<a class="btn btn-secondary kill_can_view" href="javascript:void(0);" style="color: white;padding:7px 7px;" base_url="{{ url('/') }}" course_id="<?php echo Request::segment('3'); ?>" schedule_id="<?php echo Request::segment('4'); ?>" lang="{{Auth::user()->lang}} " > @lang('lang.view kill sheet')</a>
					                    	<a class="btn btn-primary kill_can" href="javascript:void(0);" style="color: white;padding:7px 7px;" base_url="{{ url('/') }}" course_id="<?php echo Request::segment('3'); ?>" schedule_id="<?php echo Request::segment('4'); ?>"  lang="{{Auth::user()->lang}} "> @lang('lang.add kill sheet')</a>

					                    	<a class="btn btn-secondary candidate_attendances" href="javascript:void(0);" style="color: white;padding:7px 7px;" base_url="{{ url('/') }}" date_id="<?php echo $past_date->id; ?>" schedule_id="<?php echo Request::segment('4'); ?>" lang="{{Auth::user()->lang}} " course_id="<?php echo Request::segment('3'); ?>"> @lang('lang.Candidate Attendances')</a>
					                    	
					                    </td>
					             
					                </tr>
					                @endforeach

					    </tbody>
					</table>

					<table class="" style="padding:10%;">
					    <thead>
					    <tr style="background-color: #041e42;color: white;">
					      
					   
					         <th title="Field #5"  class="text-center "> @lang('lang.Coming Dates') </th>
					         <th title="Field #5"  class="text-center "> @lang('lang.kill sheet') </th>
				 	      
					    </tr>
					    </thead>
					    <tbody>
					        		@foreach($coming_dates as $coming_date)
					                <tr>
					                  
					                    <td>{{ $coming_date->date}} </td>
					                    <td><a class="btn btn-secondary kill_can_view" href="javascript:void(0);" style="color: white;padding:7px 7px;" base_url="{{ url('/') }}" course_id="<?php echo Request::segment('2'); ?>" schedule_id="<?php echo Request::segment('3'); ?>" lang="{{Auth::user()->lang}} " > @lang('lang.view kill sheet')</a>
					                    	<a class="btn btn-primary kill_can" href="javascript:void(0);" style="color: white;padding:7px 7px;" course_id="<?php echo Request::segment('2'); ?>"  base_url="{{ url('/') }}" schedule_id="<?php echo Request::segment('3'); ?> " lang="{{Auth::user()->lang}} "> @lang('lang.add kill sheet')</a>
					                    <!-- 	<a class="btn btn-secondary candidate_attendances" href="javascript:void(0);" style="color: white;padding:7px 7px;" base_url="{{ url('/') }}" date_id="<?php echo $past_date->id; ?>" schedule_id="<?php echo Request::segment('4'); ?>" lang="{{Auth::user()->lang}} " course_id="<?php echo Request::segment('3'); ?>" > @lang('lang.Candidate Attendances')</a> -->
					                    	
					                    </td>
					                  
					                   
					                </tr>
					                @endforeach

					    </tbody>
					</table>


                <!--end: Datatable -->
                <br>
              
						
		 </div>
		</div>
				
			
		</div>
		</div>
</div>



	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog dial" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	       
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
	      </div>
	    </div>
	  </div>
	</div>

@endsection