<table class="" style="padding:10%;">
    <thead>
    <tr style="background-color: #041e42;color: white;">
      	
      	<th title="Field #1" class="text-center "></th>
   		@foreach($kill_sheets as $kill_sheet)
        <th title="Field #5" class="text-center "><?php echo $kill_sheet->{'name_'.Auth::user()->lang} ?> </th>
       	@endforeach
    </tr>
    </thead>
    <tbody>
                                       
            <tr>
            	<td>{{ Auth::user()->name }}</td>   

            	@foreach($kill_sheets as $kill_sheet)
            	<?php $user_id=Auth::user()->id;

            	$kill_value=App\Kill_sheet_candidates::orderBy('id', 'desc')->where('kill_sheet_id',$kill_sheet->id)->where('user_id',$user_id)->first();
            	?>
		        <td>{{ $kill_value->value }}</td>  

		       	@endforeach 
                   
            </tr>
       

    </tbody>
</table>