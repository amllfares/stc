<div>
	@if($lang=='en')
		<div class="alert alert-danger" style="background-color:#ed1c24;">
            <div class="m-alert__icon">
                <i class="flaticon-exclamation-1"></i>
            </div>
            <div class="m-alert__text" style="color: white;">
                you Already Applied For This Job!
            </div>
        </div>
	@else
		<div class="alert alert-danger" style="background-color:#ed1c24;">
            <div class="m-alert__icon">
                <i class="flaticon-exclamation-1"></i>
            </div>
            <div class="m-alert__text" style="color: white;">
                لقد قمت بالتقدم لهذة الوظيفه سابقا 
            </div>
        </div>
	@endif
</div>