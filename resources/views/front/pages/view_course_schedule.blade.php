@extends('layout.frontlayout')
@section('content')

	<!-- BANNER -->
	<div class="section banner-page" style="background-image:url('{{asset('/assets/back/upload/stc.png')}}')">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="title-page">@lang('lang.Courses')</div>
					<ol class="breadcrumb">
						<li><a href="{{url('/')}}">STC</a></li>
						<li class="active">@lang('lang.Courses')</li>
					</ol>
				</div>
			</div>
		</div>
	</div>

		
	<div class="section why overlap">
		<div class="container">
			
			<div class="row">
				
			<div class="single-page" style="">
					
						<h2>
							@lang('lang.Course content')
						</h2>
						<div class="panel-group panel-faq" id="accordion" role="tablist" aria-multiselectable="true">
					
					     @foreach($contents as $content)
						  <div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading3">
							  <h4 class="panel-title" >
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#{{$content->id}}" aria-expanded="false" aria-controls="{{$content->id}}">
								 <?php echo $content->{'name_'.Request::segment('1')} ?>
								</a>
							  </h4>
							  <br>
							</div>
							<div id="{{$content->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3" aria-expanded="false" style="height: 0px;">
							  <div class="panel-body">
								<p><?php echo $content->{'bref_'.Request::segment('1')} ?></p>
								<?php echo $content->{'content_'.Request::segment('1')} ?>
							  </div>
							</div>
						  </div>
					     @endforeach
						</div>
					 </div>
				
			</div>
		</div>
	</div>
	<br>
	<div class="section why overlap">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<h3 >@lang('lang.Course Dates') </h3> 
					
					<div class="margin-bottom-50"></div>

					<div class="career-tabs" data-example-id="togglable-tabs">
						<ul id="myTabs" class="nav nav-tabs" role="tablist">
							@foreach($schedules as $schedule)
							<li class=""><a href="#{{'data'.$schedule->id}}" aria-controls="{{'data'.$schedule->id}}" role="tab" data-toggle="tab" aria-expanded="false" ><?php echo $schedule->{'schedule_'.Request::segment('1')} ?></a></li>
							@endforeach
							
						</ul>
						
						<div id="myTabContent" class="tab-content">
							@foreach($schedules as $schedule)
							<div role="tabpanel" class="tab-pane fade" id="{{'data'.$schedule->id}}">
								<div class="row">
									<div class="col-sm-12 col-md-12">
										<div class="body-tab">
											<h4>@lang('lang.From'):{{$schedule->from}}</h4>
											<div class="margin-bottom-30"></div>
											<h4>@lang('lang.To'):{{$schedule->to}}</h4>
											<div class="margin-bottom-30"></div>
											<h4>@lang('lang.Instructor'):<?php echo $schedule->instructor->{'name_'.Request::segment('1')} ?> </h4>
										
											<div class="margin-bottom-30"></div>
											<h4>@lang('lang.Course Dates')</h4>
											@foreach($schedule->dates as $date)
											<p>{{$date->date}}</p>
											@endforeach
											
											<div class="margin-bottom-30"></div>
											<a href="{{ url(Request::segment('1').'/course_reservation/'.$schedule->course_id.'/'.$schedule->id)}}" class="btn btn-secondary text-center" title="OUR COMPANY" style="margin-left: 556px;">@lang('lang.Reservation')  </a>
										</div>
									</div>
								</div>
							</div>
							@endforeach


							
						</div>
						
					</div>
					<!-- END TAB -->

				</div>
			</div>
		</div>
	</div>
	
	


@endsection