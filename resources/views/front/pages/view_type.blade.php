@extends('layout.frontlayout')
@section('content')

	<!-- BANNER -->
	<div class="section banner-page" style="background-image:url('{{asset('/assets/back/upload/stc.png')}}')">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="title-page">@lang('lang.Courses')</div>
					<ol class="breadcrumb">
						<li><a href="{{url('/')}}">STC</a></li>
						<li class="active">@lang('lang.Courses')</li>
					</ol>
				</div>
			</div>
		</div>
	</div>


	<!-- Team -->
	<div class="section why overlap">
		<div class="container">
			<?php $id=Request::segment('3');
			 $type=App\TypeCategory::where('id',$id)->first() ?>
			<h3 class="text-center"><?php echo $type->{'name_'.Request::segment('1')} ?></h3>
			<div class="row ">
				@foreach($courses as $category)
				<div class="col-sm-6 col-md-4" >
					<!-- BOX 5 -->
					<div class="feature-box-8" style="max-height: 430px;" >
		              <div class="media" style="height: 219px;">
		                <img src="{{asset($category->image)}}" alt="rud" class="img-responsive">
		              </div>
		              <div class="body">
		                <div class="icon-holder">
		                  <span class="fa fa-flask"></span>
		                </div>
		                <a href="{{ url($lang.'/course/'.$category->id)}}" class="title" style="height:54px; "><?php echo $category->{'name_'.Request::segment('1')} ?></a>
		                
		                <a href="{{ url($lang.'/course/'.$category->id)}}" class="readmore">@lang('lang.LEARN MORE')</a>
		              </div>
		            </div>
				</div>
				@endforeach
			</div>
		</div>
	</div>

@endsection

