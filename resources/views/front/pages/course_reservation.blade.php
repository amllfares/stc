@extends('layout.frontlayout')
@section('content')

 <!-- BANNER -->
	<div class="section banner-page" style="background-image:url('{{asset('/assets/back/upload/stc.png')}}')">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="title-page">@lang('lang.Reservation')</div>
					<ol class="breadcrumb">
						<li><a href="{{url('/')}}">STC</a></li>
						<li class="active">@lang('lang.Reservation')</li>
					</ol>
				</div>
			</div>
		</div>
	</div>

	<!-- Contact -->
	<div class="section contact overlap">
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-md-4 col-md-push-8">
					<div class="widget download">
						
					</div>
					

				</div>
				<div class="col-sm-8 col-md-8 col-md-pull-2">
					<div class="content">
						
						<div class="margin-bottom-30"></div>
						<h3 class="section-heading-2">
							 @lang('lang.Reservation Details')
						</h3>
						<br><br><br>
						 @if(Session::has('Reservation'))
			                <div class="alert alert-danger" style="background-color: #ed1c24;">
			                    <div class="m-alert__icon">
			                        <i class="flaticon-exclamation-1"></i>
			                    </div>
			                    <div class="m-alert__text" style="color: white;">
			                        {{ Session::get('Reservation') }}
			                    </div>
			                </div>
			          	  @endif

						<form action="{{ url(Auth::user()->lang.'/stc/confirm_reservation/'.Request::segment(4).'/'.Request::segment(5))}}" class=" form-contact"  data-toggle="validator" novalidate="true" method="POST" enctype="multipart/form-data" >
							 {{ csrf_field() }}
				 			<div class="form-group">
								
								<select name="hotel_id" id="parent_category" class="form-control"  required="">
									<option value="">@lang('lang.choose hotel')  </option>
                                    @foreach ($hotels as $hotel )
                                        <option value="{{ $hotel->id }}"><?php echo $hotel->{'name_'.Auth::user()->lang} ?></option>
                                    @endforeach
                                </select>
                                <div class="help-block with-errors"></div>
							</div>
							<div class="form-group">
								
								<select name="rest_id" id="parent_category" class="form-control"  required="">
									<option value=""> @lang('lang.choose restaurant') </option>
                                    @foreach ($restaurants as $restaurant )
                                        <option value="{{ $restaurant->id }}"><?php echo $restaurant->{'name_'.Auth::user()->lang} ?></option>
                                    @endforeach
                                </select>
                                <div class="help-block with-errors"></div>
							</div>
							
							<div class="form-group">
								<button type="submit" class="btn btn-secondary " style="pointer-events: all; cursor: pointer;">@lang('lang.Reservation')</button>

							</div>

						</form>
						<div class="margin-bottom-50"></div>
						
					 </div>
				</div>

			</div>
			
		</div>
	</div>	





@endsection