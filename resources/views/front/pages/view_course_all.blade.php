@extends('layout.frontlayout')
@section('content')

	<!-- BANNER -->
	<div class="section banner-page" style="background-image:url('{{url($subject_title->image)}}')">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="title-page"><?php echo $subject_title->{'name_'.Request::segment('1')} ?></div>
					<ol class="breadcrumb">
						<li><a href="{{url('/')}}">STC</a></li>
						<li class="active"><?php echo $subject_title->{'name_'.Request::segment('1')} ?></li>
					</ol>
				</div>
			</div>
		</div>
	</div>

	<!-- Team -->

	<div class="section why overlap">
		<div class="container">
			<div class="row">
				<h3 class="text-center"><?php echo $subject_title->{'name_'.Request::segment('1')} ?></h3>
			
				<div class="col-sm-10 col-md-10 col-md-offset-1" style="background-color:#eae7e7;padding-bottom: 20px;">

					<div class="margin-bottom-30"></div>
					
					<h3 class="subtitle-404">@lang('lang.Course Outline')</h3>
					<p><?php echo $subject_title->{'descrption_'.Request::segment('1').'1'} ?></p>
					
					<div class="margin-bottom-50"></div>
					
				</div>
			

			</div>
		</div>
	</div>
	

<div class="section why overlap">
	<div class="container">
		
		<div class="row">
			
			<div class="col-sm-12 col-md-12 ">	
				<div class="single-page text-center" style="">
						
							<h2>
								<?php echo $subject_title->{'name_'.Request::segment('1')} ?>
							</h2>
							
							<div class="container">
   								<div class="row">    
                                  <div class="col-xs-8 col-xs-offset-2">
                                  	 @if ($errors->any())
			                            <div class="alert alert-danger">
			                                <ul>
			                                    @foreach ($errors->all() as $error)
			                                        <li>{{ $error }}</li>
			                                    @endforeach
			                                </ul>
			                            </div>
		                             @endif
			                         <div class="input-group">
	                                   <div class="input-group-btn search-panel">
		                                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" style="line-height:4px; margin-bottom: 15px;">
		                                	<span id="search_concept">@lang('lang.Branch')</span> 
		                                  </button>
	                                   
	                                   </div>
	                                          
	                                   <select name="branch_id" id="parent_category" class="form-control branch"  required=""   onchange="branch('{{url('/')}}',this.options[this.selectedIndex].value,'{{Request::segment('3')}}','{{Request::segment('1')}}')">
										<option value="" disabled="" >@lang('lang.Search for branch')...</option>
										<option value="0">@lang('lang.All Branches')</option>
	                                   	@foreach($branches as $branch)
	                                   	<option value="{{$branch->id}}"><?php echo $branch->{'name_'.Request::segment('1')} ?></option>
	                                   	@endforeach
	                                 </select>

	                                
	                                 </div>
                                   </div>
								</div>
								</div>
							 <div class="margin-bottom-50"></div>

							 <div class="margin-bottom-50"></div>

		                @if($subject_count > 0)
						@foreach($subject as $sub)
						<?php  $schedules= App\Course_schedule::orderBy('id', 'desc')->where('course_id',$sub->id)->with('instructor')->get();?>
								 
						<table class="" id="branch">
						    <thead>
						    <tr style="background-color: #041e42;color: white;">
						      
						   
						        <th title="Field #5"  class="text-center "> @lang('lang.Courses') </th>
					 	        <th title="Field #5"  class="text-center "> @lang('lang.Schedule') </th>
					 	         <th title="Field #5"  class="text-center ">@lang('lang.Branch') </th>
					 	        <th title="Field #5"  class="text-center ">@lang('lang.Course content') </th>
					 	        <th title="Field #5"  class="text-center ">@lang('lang.Dates') </th>
					 	        <th title="Field #5"  class="text-center ">@lang('lang.Course Details') </th>
					 	        <th title="Field #5"  class="text-center ">@lang('lang.Reservation') </th>
					 	       


						      
						    </tr>
						    </thead>
						    <tbody>
					        		@foreach($schedules as $schedule)
					                <tr>
					                    <td class="text-center"><?php echo $sub->{'name_'.Request::segment('1')} ?></td>
					                    <td class="text-center"><?php echo $schedule->{'schedule_'.Request::segment('1')} ?> </td>
					                     <td><?php echo $schedule->branch->{'name_'.Request::segment('1')} ?> </td>
					                    <td class="text-center">
					                    	<a class="btn btn-secondary content" href="javascript:void(0);"  base_url="{{ url('/') }}" course_id="<?php echo $sub->id; ?>" style="padding: 10px;"  lang="{{Request::segment('1')}}"> 
					                    	 <i class="m-nav__link-icon fa fa-list" style="color: white;"></i>@lang('lang.Course content')</a>
					                    </td>
					                    <td class="text-center">
					                    	<a class="btn btn-primary show_date" href="javascript:void(0);"  course_id="<?php echo $sub->id; ?>" schedule_id="<?php echo $schedule->id; ?>" base_url="{{ url('/') }}" lang="{{Request::segment('1')}}" style="padding: 10px;">
                                            <i class="m-nav__link-icon fa fa-calendar" style="color: white;"></i> @lang('lang.Dates')
                                        </a>
					                    	
					                    </td>
					                     <td class="text-center">
					                    	<a class="btn btn-primary more" href="javascript:void(0);"  base_url="{{ url('/') }}" course_id="<?php echo $sub->id; ?>" style="padding: 10px;" lang="{{Request::segment('1')}}" schedule_id="<?php echo $schedule->id; ?>" >
					                    	 <i class="m-nav__link-icon fa fa-eye" style="color: white;"></i> @lang('lang.Course Details')</a>
					                    	
					                    </td>

					                     <td class="text-center">
					                    	@guest
					                    	<a href="javascript:void(0);"  class="btn btn-secondary text-center resve"  style="padding: 10px;"  data-target=".bd-example-modal2-sm" data-toggle="modal">
					                    	 <i class="m-nav__link-icon fa fa-check-circle" style="color: white;"></i>@lang('lang.Reservation')  </a>
					                    	@else
					                    	<a href="{{ url(Request::segment('1').'/course_reservation/'.$sub->id.'/'.$schedule->id)}}" class="btn btn-secondary text-center" style="padding: 10px;" lang="{{Request::segment('1')}}">
					                    	 <i class="m-nav__link-icon fa fa-check-circle" style="color: white;"></i>@lang('lang.Reservation')  </a>
					                    	 @endguest
					                    	
					                    </td>
					             
					                </tr>
					                @endforeach
					               

						    </tbody>
						</table>
						@endforeach
						@else
							<div class="alert alert-warning">
							  <strong>@lang('lang.sorry') , </strong> @lang('lang.there is no schedules (content or dates or details ) for this course').
							</div>
						@endif

		            <!--end: Datatable -->
		            <br>

		            <a href="javascript:void(0);" class="btn btn-secondary text-center Subscribe" base_url="{{ url('/') }}" course_id="<?php echo Request::segment('4'); ?>" lang="{{Request::segment('1')}}" >
					                    	 <i class="m-nav__link-icon fa fa-envelope" style="color: white;"></i>  @lang('lang.Subscribe')  </a>
            	    <a href="javascript:void(0);" class="btn btn-primary text-center" base_url="{{ url('/') }}" course_id="<?php echo Request::segment('4'); ?>" data-target=".bd-example-modal-sm" data-toggle="modal">
					                    	 <i class="m-nav__link-icon fa fa-share-alt" style="color: white;"></i>  @lang('lang.Shar')  </a>
		          
							
			 </div>
		</div>
				
			
		</div>
	</div>
</div>



	<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	
    <div class="modal-dialog dial" role="document">
   
    <div class="modal-content">
      <div class="modal-header">
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>


<!-- Small modal -->


<div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">share course</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<div class="text-center">
      	<div id="social-links">
		 
		   <a href="https://www.facebook.com/sharer/sharer.php?u={{url(Request::segment('1').'/course_all/'.Request::segment('4'))}}" class="social-button " id="" ><span class="fa fa-facebook facebookk"></span></a>
		    <a href="https://twitter.com/intent/tweet?text=my share text&amp;url={{url(Request::segment('1').'/course_all/'.Request::segment('4'))}}" class="social-button " id=""><span class="fa fa-twitter twitterr"></span></a>
		    <a href="https://plus.google.com/share?url={{url(Request::segment('1').'/course_all/'.Request::segment('4'))}}" class="social-button " id=""><span class="fa fa-google-plus googlepluss"></span></a>
		   <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url={{url(Request::segment('1').'/course_all/'.Request::segment('4'))}}" class="social-button " id=""><span class="fa fa-linkedin linkedinn"></span></a>

		 
       </div>
       </div>
        
      </div>
      
      </div>

	
    </div>
  </div>
</div>

<div class="modal fade bd-example-modal2-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content mo" style="width: 350px;">
    
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<div class="text-center">
      	 <h6>@lang('lang.you need to be logged in to make a reservation') </h6>
      	
       </div>
        
      </div>
       <div class="modal-footer">
        <a type="button" class="btn btn-secondary" style="padding: 10px 9px;font-size: 12px; margin-right: 125px;" href="{{url(Request::segment('1').'/login_page')}}">@lang('lang.Login')</a>
      </div>
      
      </div>

	
    </div>
  </div>
</div>




@endsection