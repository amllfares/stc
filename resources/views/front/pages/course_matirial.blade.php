<table width="100%;">
    <thead>
    <tr style="background-color: #041e42;color: white;">
      
   
        <th title="Field #5"> @lang('lang.Name') </th>
       
        <th title="Field #5"> @lang('lang.Download') </th>
      
    </tr>
    </thead>
    <tbody>
                 @foreach($matirials as $matirial)
             
                <tr>
                    <td><?php echo $matirial->{'title_'.Auth::user()->lang} ?> </td>
                    <td>
                    @if($matirial->download == 0)
                     <a class="btn btn-primary" href="{{url($lang.'/download/'.$matirial->id)}}">
                        <i class="fa fa-download" aria-hidden="true"></i>@lang('lang.Download')
                    </a>
                    @endif
                </td>
                </tr>
              
                @endforeach

    </tbody>
</table>