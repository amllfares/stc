                   <div>
                    <div class="single-page">
                        
                    <div class="content">
                        
                        <div class="margin-bottom-30"></div>
                        <h3 class="section-heading-2">
                            @lang('lang.Feedback Course')
                        </h3>
                    
                    <form action="{{url($lang.'/feedback_course_store')}}" class="form-contact"  data-toggle="validator" novalidate="true" method="post">
                             {{ csrf_field() }} 
                            <input type="hidden" value="{{$course_id}}" name="course_id">
                            <input type="hidden" value="{{$schdule_id}}" name="schdule_id">
                            <div class="form-group">
                                <input type="email" class="form-control" id="p_email" placeholder="@lang('lang.enter email')....." required="" name="email">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="p_subject" placeholder="@lang('lang.mobile')......" name="mobile">
                                <div class="help-block with-errors"></div>
                            </div>
                             <div class="form-group">
                                <input type="number" class="form-control" id="p_email" placeholder="@lang('lang.rate instructor')....." required="" name="instructor_rate">
                                <div class="help-block with-errors"></div>
                            </div>
                             <div class="form-group">
                                <input type="number" class="form-control" id="p_email" placeholder="@lang('lang.rate')....." required="" name="rate">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                 <textarea id="p_message" class="form-control" rows="6" placeholder="@lang('lang.Write Message')...." name="feedback"></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                            
                            <div class="form-group">
                                
                                <button type="submit" class="btn btn-secondary disabled" style="pointer-events: all; cursor: pointer;"> @lang('lang.Send')  <i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                            </div>
                        </form>
                        <div class="margin-bottom-50"></div>
                        
                     </div>
                        
                     </div>
                </div>