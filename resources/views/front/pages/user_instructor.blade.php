 @extends('layout.frontlayout')
@section('content')

<div class="section banner-page" style="background-image:url('{{asset('/assets/back/upload/stc.png')}}')">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="title-page">@lang('lang.Courses')</div>
					<ol class="breadcrumb">
						<li><a href="{{url('/')}}">STC</a></li>
						<li class="active">@lang('lang.Courses')</li>
					</ol>
				</div>
			</div>
		</div>
</div>
<div>
	
</div>
<div class="section why overlap">
		<div class="container">
			
			<div class="row">
				<div class="col-sm-4 col-md-4 col-md-push-8">
					<div class="widget categories">
						<ul class="category-nav">
							<li ><a href="{{url(Auth::user()->lang.'/view_profile')}}"> @lang('lang.Edit Profile information')</a></li>
							<li ><a href="{{url(Auth::user()->lang.'/update_password')}}">@lang('lang.Update password')</a></li>
							<li class="active"><a href="{{url(Auth::user()->lang.'/view_course_users')}}">@lang('lang.Courses')</a></li>
							<!-- <li><a href="#">Power And Energy</a></li>
							<li><a href="#">Chemical Research</a></li>
							<li><a href="#">Material Engineering</a></li> -->
						</ul>
					</div> 
		

				</div>
			<div class="col-sm-8 col-md-8 col-md-pull-4">	
			<div class="single-page text-center" style="">
					
						<h2>
							@lang('lang.Courses')
						</h2>
						 <div class="margin-bottom-50"></div>
						 <div class="margin-bottom-50"></div>

						  @if(Session::has('add_kill'))
                            <div class="alert alert-danger" style="background-color:#041e42;">
                                <div class="m-alert__icon">
                                    <i class="flaticon-exclamation-1"></i>
                                </div>
                                <div class="m-alert__text" style="color: white;">
                                    {{ Session::get('add_kill') }}
                                </div>
                            </div>
                          @endif

                           @if(Session::has('delete'))
                            <div class="alert alert-danger" style="background-color:#ed1c24;">
                                <div class="m-alert__icon">
                                    <i class="flaticon-exclamation-1"></i>
                                </div>
                                <div class="m-alert__text" style="color: white;">
                                    {{ Session::get('delete') }}
                                </div>
                            </div>
                          @endif
				<table class="" width="100%">
                    <thead>
                      
                    <tr style="background-color: #041e42;color: white;">
                       <!--  <th title="Field #1">course </th> -->

                        <th title="Field #2">@lang('lang.Instructor') </th>
                        <th title="Field #2">@lang('lang.From') </th>
                        <th title="Field #3">@lang('lang.To') </th>
                        <th width="150px;">@lang('lang.Schedule')</th>
                        <th width="142px;">@lang('lang.Dates')</th>
                        <th  width="160px;" title="Field #5">@lang('lang.kill sheet')</th>
                      
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($user_courses as $user_course)
                                <tr @if($user_course->trash==1) class='kashat_alert_danger' @endif id="trashed_{{ $user_course->id }}">
                                   
                                    <td>{{ Auth::user()->name}} </td>
                                    <td>{{ $user_course->course_schedule->from }}</td>
                                    <td>{{ $user_course->course_schedule->to }}</td>
                                    <td><?php echo $user_course->course_schedule->{'schedule_'.Auth::user()->lang} ?></td>
                                   
                                    <td>
                                         <a class="btn btn-primary" href="{{ url(Auth::user()->lang.'/date/'.$user_course->course_id.'/'.$user_course->schedule_id)}}" >
                                            <i class="m-nav__link-icon fa fa-calendar" style="color: white;"></i> @lang('lang.Dates')
                                        </a>
                                 
                                    </td>
                                  	<td>
	                                	 <li class="dropdown btn btn-secondary" style="background-color: #041e42;">
			                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="color: white"> @lang('lang.kill sheet') <span class="caret"></span></a>
			                              <ul class="dropdown-menu" style="background-color: #041e42;">
			                                
			                                 <li><a class="show_kill" href="javascript:void(0);" style="color: white;" course_id="<?php echo $user_course->course_id; ?>" base_url="{{ url('/') }}" lang="{{Auth::user()->lang}} " > @lang('lang.view kill sheet')</a></li>
			                                 <li><a class="add_kill" href="javascript:void(0);" style="color: white;" course_id="<?php echo $user_course->course_id; ?>" base_url="{{ url('/') }}" lang="{{Auth::user()->lang}} "  > @lang('lang.add kill sheet')</a></li>
			                                
			                              </ul>
			                            </li>
                                   </td> 
                                   
                                </tr>
                                
                               
                        @endforeach
                    </tbody>

                </table>

                <!--end: Datatable -->
                <br>
              
						
					 </div>
					</div>
				
			
		</div>
		</div>
</div>



	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog dial" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	       
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
	      </div>
	    </div>
	  </div>
	</div>

@endsection