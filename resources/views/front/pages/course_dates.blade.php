<!-- @if(isset($past_dates)&&!empty($past_dates))
<table class="" style="padding:10%;">
    <thead>
    <tr style="background-color: #041e42;color: white;">
      
       @if($lang=="ar")
        <th title="Field #5" colspan="{{$past_count}}" class="text-center ">التواريخ السابقه  </th>
        @else
         <th title="Field #5" colspan="{{$past_count}}" class="text-center "> Past Dates </th>
        @endif
      
    </tr>
    </thead>
    <tbody>
        
                <tr>
                   @foreach($past_dates as $past_date)
                    <td>{{ $past_date->date}} </td>
                   @endforeach 
                   
                </tr>

    </tbody>
</table>
@else
<table>
    <div>
        no
    </div>
</table>
@endif -->
@if($coming_count > 0)

<table class="table table-bordered" style="padding:10%;">
    <thead>
        <tr style="background-color: #041e42;color: white;">
          
            @if($lang=="ar")
            <th title="Field #5" colspan="{{$coming_count}}" class="text-center "> التواريخ </th>
            @else
            <th title="Field #5" colspan="{{$coming_count}}" class="text-center "> Dates </th>
            @endif
          
        </tr>
    </thead>
    <tbody>
        <tr>
           @foreach($past_dates as $past_date)
            <td class="text-center"><b> {{ $past_date->date}} </b></td>
           @endforeach           
        </tr>
        <tr  >  
           @foreach($coming_dates as $coming_date)
            <td class="text-center"><b> {{ $coming_date->date}} </b></td>
           @endforeach
        </tr>
    </tbody>
  </table>
@else
    <br><br>
    <div class="alert alert-warning text-center">
        @if($lang=="ar")
            <strong>عفواً ,</strong> لا يوجد تواريخ قادمة
        @else
            <strong>Sorry ,</strong>  There Is No Comming Dates
        @endif
    </div>
@endif
