@extends('layout.frontlayout')
@section('content')

	<!-- BANNER -->
	<div class="section banner-page" style="background-image:url('{{asset('/assets/back/upload/stc.png')}}')">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="title-page">@lang('lang.Courses')</div>
					<ol class="breadcrumb">
						<li><a href="{{url('/')}}">STC</a></li>
						<li class="active">@lang('lang.Courses')</li>
					</ol>
				</div>
			</div>
		</div>
	</div>

	<!-- Team -->

	<div class="section why overlap">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<h3 class="text-center">@lang('lang.Reservation')</h3> 

					<div class="margin-bottom-50"></div>

					<div class="career-tabss" data-example-id="togglable-tabs">
						<ul id="myTabs" class="nav nav-tabss" role="tablist">
							<li class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab" aria-expanded="true">@lang('lang.General')</a></li>
							<li class=""><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab" aria-expanded="false">@lang('lang.Private')</a></li>
							<li class=""><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab" aria-expanded="false">@lang('lang.Company')</a></li>
							
						</ul>
						<div id="myTabContent" class="tab-content">
						    <div role="tabpanel" class="tab-pane fade active in" id="tab1">
								<div class="row">
									<div class="col-sm-12 col-md-12">
										<div class="body-tab">
											<div class="content">
												
													<div class="margin-bottom-30"></div>
													<h3 class="section-heading-2">
														 @lang('lang.Reservation Details')
													</h3>
													<br>
													<!--  @if(Session::has('Reservation'))
										                <div class="alert alert-danger" style="background-color: #ed1c24;">
										                    <div class="m-alert__icon">
										                        <i class="flaticon-exclamation-1"></i>
										                    </div>
										                    <div class="m-alert__text" style="color: white;">
										                        {{ Session::get('Reservation') }}
										                    </div>
										                </div>
										          	  @endif -->

													<form action="{{ url(Auth::user()->lang.'/confirm_reservation/'.Request::segment(3).'/'.Request::segment(4))}}" class=" form-contact"  data-toggle="validator" novalidate="true" method="POST" enctype="multipart/form-data" >
														 {{ csrf_field() }}
											 			<div class="form-group">
															
															<select name="hotel_id" id="parent_category" class="form-control"  required="">
																<option value="">@lang('lang.choose hotel')  </option>
							                                    @foreach ($hotels as $hotel )
							                                        <option value="{{ $hotel->id }}"><?php echo $hotel->{'name_'.Auth::user()->lang} ?></option>
							                                    @endforeach
							                                </select>
							                                <div class="help-block with-errors"></div>
														</div>
														<div class="form-group">
															
															<select name="rest_id" id="parent_category" class="form-control"  required="">
																<option value=""> @lang('lang.choose restaurant') </option>
							                                    @foreach ($restaurants as $restaurant )
							                                        <option value="{{ $restaurant->id }}"><?php echo $restaurant->{'name_'.Auth::user()->lang} ?></option>
							                                    @endforeach
							                                </select>
							                                <div class="help-block with-errors"></div>
														</div>
														
														<div class="form-group">
															<button type="submit" class="btn btn-secondary " style="pointer-events: all; cursor: pointer;">@lang('lang.Reservation')</button>

														</div>

													</form>
													<div class="margin-bottom-50"></div>
							
						                         </div>
											</div>
										</div>
									</div>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="tab2">
								<div class="row">
									<div class="col-sm-12 col-md-12">
										<div class="body-tab">
												<div class="content">
												
													<div class="margin-bottom-30"></div>
													<h3 class="section-heading-2">
														 @lang('lang.Reservation Details')
													</h3>
													<br>
													<!--  @if(Session::has('Reservation'))
										                <div class="alert alert-danger" style="background-color: #ed1c24;">
										                    <div class="m-alert__icon">
										                        <i class="flaticon-exclamation-1"></i>
										                    </div>
										                    <div class="m-alert__text" style="color: white;">
										                        {{ Session::get('Reservation') }}
										                    </div>
										                </div>
										          	  @endif -->

													<form action="{{ url(Auth::user()->lang.'/confirm__private_reservation/'.Request::segment(3).'/'.Request::segment(4))}}" class=" form-contact"  data-toggle="validator" novalidate="true" method="POST" enctype="multipart/form-data" >
														 {{ csrf_field() }}
											 			<div class="form-group">
															
															<select name="hotel_id" id="parent_category" class="form-control"  required="">
																<option value="">@lang('lang.choose hotel')  </option>
							                                    @foreach ($hotels as $hotel )
							                                        <option value="{{ $hotel->id }}"><?php echo $hotel->{'name_'.Auth::user()->lang} ?></option>
							                                    @endforeach
							                                </select>
							                                <div class="help-block with-errors"></div>
														</div>
														<div class="form-group">
															
															<select name="rest_id" id="parent_category" class="form-control"  required="">
																<option value=""> @lang('lang.choose restaurant') </option>
							                                    @foreach ($restaurants as $restaurant )
							                                        <option value="{{ $restaurant->id }}"><?php echo $restaurant->{'name_'.Auth::user()->lang} ?></option>
							                                    @endforeach
							                                </select>
							                                <div class="help-block with-errors"></div>
														</div>
														
														<div class="form-group">
															<button type="submit" class="btn btn-secondary " style="pointer-events: all; cursor: pointer;">@lang('lang.Reservation')</button>

														</div>

													</form>
													<div class="margin-bottom-50"></div>
							
						                         </div>
										</div>
									</div>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="tab3">
								<div class="row">
									<div class="col-sm-12 col-md-12">
										<div class="body-tab">
											<div class="content">
												
													<div class="margin-bottom-30"></div>
													<h3 class="section-heading-2">
														 @lang('lang.Reservation Details')
													</h3>
													<br>
												
													<form action="{{ url(Auth::user()->lang.'/confirm__company_reservation/'.Request::segment(3).'/'.Request::segment(4))}}" class=" form-contact"  data-toggle="validator" novalidate="true" method="POST" enctype="multipart/form-data" >
														 {{ csrf_field() }}
											 			 <div class="form-group">
							                                <label for="name_ar" class="col-md-4 col-form-label text-md-right">@lang('lang.Name_Ar')</label>

							                                <input id="name_ar" type="text" class="form-control{{ $errors->has('name_ar') ? ' is-invalid' : '' }}" name="name_ar" required>

							                                @if ($errors->has('name_ar'))
							                                    <span class="invalid-feedback" role="alert">
							                                        <strong>{{ $errors->first('name_ar') }}</strong>
							                                    </span>
							                                @endif
							                           
							                                
							                                <div class="help-block with-errors"></div>
							                            </div>
							                             <div class="form-group">
							                                <label for="name_en" class="col-md-4 col-form-label text-md-right">@lang('lang.Name_En')</label>

							                                <input id="name_en" type="text" class="form-control{{ $errors->has('name_en') ? ' is-invalid' : '' }}" name="name_en" required>

							                                @if ($errors->has('name_en'))
							                                    <span class="invalid-feedback" role="alert">
							                                        <strong>{{ $errors->first('name_en') }}</strong>
							                                    </span>
							                                @endif
							                           
							                                
							                                <div class="help-block with-errors"></div>
							                            </div>
														 <div class="form-group">
							                                <label for="address" class="col-md-4 col-form-label text-md-right">@lang('lang.Address')</label>

							                                <input id="address" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" required>

							                                @if ($errors->has('address'))
							                                    <span class="invalid-feedback" role="alert">
							                                        <strong>{{ $errors->first('address') }}</strong>
							                                    </span>
							                                @endif
							                           
							                                
							                                <div class="help-block with-errors"></div>
							                            </div>

							                             <div class="form-group">
							                                <label for="contact_name" class="col-md-4 col-form-label text-md-right">@lang('lang.contact_name')</label>

							                                <input id="contact_name" type="text" class="form-control{{ $errors->has('contact_name') ? ' is-invalid' : '' }}" name="contact_name" required>

							                                @if ($errors->has('contact_name'))
							                                    <span class="invalid-feedback" role="alert">
							                                        <strong>{{ $errors->first('contact_name') }}</strong>
							                                    </span>
							                                @endif
							                           
							                                
							                                <div class="help-block with-errors"></div>
							                            </div>

							                             <div class="form-group">
							                                <label for="candidate_number" class="col-md-4 col-form-label text-md-right">@lang('lang.candidates number')</label>

							                                <input id="candidate_number" type="text" class="form-control{{ $errors->has('candidate_number') ? ' is-invalid' : '' }}" name="candidate_number" required>

							                                @if ($errors->has('candidate_number'))
							                                    <span class="invalid-feedback" role="alert">
							                                        <strong>{{ $errors->first('candidate_number') }}</strong>
							                                    </span>
							                                @endif
							                           
							                                
							                                <div class="help-block with-errors"></div>
							                            </div>
														
														<div class="form-group">
															<button type="submit" class="btn btn-secondary " style="pointer-events: all; cursor: pointer;">@lang('lang.Reservation')</button>

														</div>

													</form>
													<div class="margin-bottom-50"></div>
							
						                         </div>
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
					<!-- END TAB -->

				</div>
			</div>
		</div>
	</div>
	
	


@endsection