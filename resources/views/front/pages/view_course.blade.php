@extends('layout.frontlayout')
@section('content')
<?php $id=Request::segment('3');
 $subjectcategory=App\SubjectCategory::where('trash','0')->where('id',$id)->first(); ?>
<div class="section banner-page" style="background-image:url('{{asset('/assets/back/upload/stc.png')}}')">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="title-page"><?php echo $subjectcategory->{'name_'.$lang} ?></div>
					<ol class="breadcrumb">
						<li><a href="{{url('/')}}">STC</a></li>
						<li class="active"><?php echo $subjectcategory->{'name_'.$lang} ?></li>
					</ol>
				</div>
			</div>
		</div>
	</div>

<div class="section why overlap">
	<div class="container">
		<h3 class="text-center"><?php echo $subjectcategory->{'name_'.$lang} ?></h3>
		


		@foreach($subject as $course)
                <div class="col-sm-6 col-md-4" >
                    <!-- BOX 1 -->
                    <a href="{{url($lang.'/course_all/'.$course->id)}}">
                    <div class="feature-box-8">
                      <div class="media" style="height: 241px;">
                        <img src="{{asset($course->image)}}" alt="rud" class="img-responsive">
                      </div>
                      <div class="body">
                        <div class="icon-holder">
                          <span class="fa fa-flask"></span>
                        </div>
                        <a href="{{ url($lang.'/course_all/'.$course->id)}}" class="title" style="height:54px; "><?php echo $course->{'name_'.$lang} ?></a>
                        <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed</p> -->
                        <a href="{{ url($lang.'/course_all/'.$course->id)}}" class="readmore">@lang('lang.LEARN MORE')</a>
                      </div>
                    </div>
                    </a>
                </div>
            @endforeach   
	</div>
</div>

@endsection