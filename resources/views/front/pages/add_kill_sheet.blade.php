                   <div>
                    <div class="single-page">
                        
                    <div class="content">
                        
                        <div class="margin-bottom-30"></div>
                        <h3 class="section-heading-2">
                            @lang('lang.add kill sheet')
                        </h3>
                      

                        <form action="{{url(Auth::user()->lang.'/store_kill_sheet/')}}"  class="form-contact"  data-toggle="validator" novalidate="true" method="POST" >
                             {{ csrf_field() }}
                             <input type="hidden" name="course_id" value="{{$course_id}}">
                            <div class="form-group">
                                <label for="name_ar" class="col-md-4 col-form-label text-md-right">@lang('lang.Name_Ar')</label>

                                <input id="name_ar" type="text" class="form-control{{ $errors->has('name_ar') ? ' is-invalid' : '' }}" name="name_ar" required>

                                @if ($errors->has('name_ar'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name_ar') }}</strong>
                                    </span>
                                @endif
                           
                                
                                <div class="help-block with-errors"></div>
                            </div>

                             <div class="form-group ">
                                <label for="name_en" class="col-md-4 col-form-label text-md-right">@lang('lang.Name_En')</label>

                                <input id="name_en" type="text" class="form-control{{ $errors->has('name_en') ? ' is-invalid' : '' }}" name="name_en" required>

                                @if ($errors->has('name_en'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name_en') }}</strong>
                                    </span>
                                @endif
                           
                                
                                <div class="help-block with-errors"></div>
                            </div>
 

                            <div class="form-group">
                                <button type="submit" class="btn btn-secondary " style="pointer-events: all; cursor: pointer;">@lang('lang.Confirm')</button>

                            </div>

                        </form>
                        <div class="margin-bottom-50"></div>
                        
                     </div>
                        
                     </div>
                </div>