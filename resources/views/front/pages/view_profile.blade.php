@extends('layout.frontlayout')
@section('content')

<div class="section banner-page" style="background-image:url('{{asset('/assets/back/upload/stc.png')}}')">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="title-page"> {{Auth::user()->name}}</div>
					<ol class="breadcrumb">
						<li><a href="{{url('/')}}">STC</a></li>
						<li class="active">{{Auth::user()->name}}</li>
					</ol>
				</div>
			</div>
		</div>
</div>

<div class="section why overlap"  id="edit">
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-md-4 col-md-push-8">
					<div class="widget categories">
						<ul class="category-nav">
							<li class="active"><a href="{{url(Auth::user()->lang.'/view_profile')}}"> @lang('lang.Edit Profile information')</a></li>
							<li ><a href="{{url(Auth::user()->lang.'/update_password')}}">@lang('lang.Update password')</a></li>
							<li ><a href="{{url(Auth::user()->lang.'/view_course_users')}}">@lang('lang.Courses')</a></li>
						<!-- 	<li><a href="#">Power And Energy</a></li>
							<li><a href="#">Chemical Research</a></li>
							<li><a href="#">Material Engineering</a></li> -->
						</ul>
					</div> 
		

				</div>
				<div class="col-sm-6 col-md-6 col-md-pull-3">
					<div class="single-page">
						
                    <div class="content">
                        
                        <div class="margin-bottom-30"></div>
                        <h3 class="section-heading-2">
                          @lang('lang.Edit personal Info')
                        </h3>
                        <br><br><br>
                        <!--  @if(Session::has('edit_profile'))
                            <div class="alert alert-danger" style="background-color:#041e42;">
                                <div class="m-alert__icon">
                                    <i class="flaticon-exclamation-1"></i>
                                </div>
                                <div class="m-alert__text" style="color: white;">
                                    {{ Session::get('edit_profile') }}
                                </div>
                            </div>
                          @endif -->

                        <form action="{{url(Auth::user()->lang.'/edit_profile/'.Auth::user()->id)}}"  class="form-contact"  data-toggle="validator" novalidate="true" method="POST" >
                             {{ csrf_field() }}
                             {{ method_field('PATCH') }}
                           
                            <div class="form-group row">
                                 <label for="name" class="col-md-4 col-form-label text-md-right">@lang('lang.Name')</label>

                           
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value ="{{ Auth::user()->name }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                           
                                <div class="help-block with-errors"></div>
                           </div>

                         	<div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">@lang('lang.E-Mail Address')</label>

                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ Auth::user()->email}}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                
                                <div class="help-block with-errors"></div>
                         	</div>

                    

                          	<div class="form-group row">
                                <label for="Phone" class="col-md-4 col-form-label text-md-right">@lang('lang.Phone')</label>

                                <input id="phone" type="text" class="form-control{{ $errors->has('Phone') ? ' is-invalid' : '' }}" name="phone" required value="{{ Auth::user()->phone }}">

                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                           
                                
                                <div class="help-block with-errors"></div>
                         	</div>


                          	<div class="form-group row">
                                <label for="Phone" class="col-md-4 col-form-label text-md-right">@lang('lang.Language')</label>

                                <select name="lang" id="parent_category" class="form-control"  required="">
                                    <option value="">@lang('lang.Language')</option>
                                   
                                        <option value="en" @if (Auth::user()->lang=='en')selected @endif>@lang('lang.EN')</option>
					                                                
					                                          
                                        <option value="ar" @if (Auth::user()->lang=='ar')selected @endif>@lang('lang.Ar')</option>
                                   
                                </select>
                                
                                <div class="help-block with-errors"></div>
                         	</div>

                           <div class="form-group row">
                                                
                           
                                <label for="address" class="col-md-12 col-form-label text-md-right">@lang('lang.User Type')</label>
                                <div class="m-input-icon m-input-icon--right">
                                    <div class="m-form__group form-group row">
                                            
                                            <div class="col-6">
                                                <div class="m-checkbox-inline">

                                                     <label class="col-md-4 col-form-label text-md-right">
                                                        <input type="checkbox" name="candidate" value="1" @if(Auth::user()->candidate == 1) checked="checked" @endif >
                                                        Candidate
                                                        <span></span>
                                                    </label>
                                                    
                                                    <label class="col-md-4 col-form-label text-md-right">
                                                        <input type="checkbox" name="instructor" value="1"@if(Auth::user()->instructor == 1) checked="checked" @endif>
                                                        Instructor
                                                        <span></span>
                                                    </label>
                             
                                                </div>
                                                
                                            </div>
                                        </div>
                                </div>
                                                  
                                               
 
                            </div>


                        	<div class="form-group row">
	                            <label for="address" class="col-md-4 col-form-label text-md-right">@lang('lang.Address')</label>
	                            <input id="searchTextField" type="text" class="form-control" name="address"  value="{{ Auth::user()->address }}">
	                            <br>
	                            <input name="latitude" class="MapLat"  type="hidden" placeholder="Latitude" style="width: 161px;" value="{{ Auth::user()->latitude }}" >
	                            <input name="longitude" class="MapLon"  type="hidden" placeholder="Longitude" style="width: 161px;" value="{{ Auth::user()->longitude }}"> 

	                            <div id="map_canvas" style="height: 350px;width: 500px;margin: 0.6em;"></div>
	                            <div class="help-block with-errors"></div>
	                         </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-secondary " style="pointer-events: all; cursor: pointer;">@lang('lang.Confirm')</button>

                            </div>

                        </form>
                        <div class="margin-bottom-50"></div>
                        
                     </div>
						
					 </div>
				</div>

			</div>
		</div>
	</div>
@endsection