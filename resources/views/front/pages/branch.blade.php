@extends('layout.frontlayout')
@section('content')

<div class="section banner-page" style="background-image:url('{{asset('/assets/back/upload/stc.png')}}')">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="title-page">@lang('lang.Courses')</div>
					<ol class="breadcrumb">
						<li><a href="{{url('/')}}">STC</a></li>
						<li class="active">@lang('lang.Courses')</li>
					</ol>
				</div>
			</div>
		</div>
	</div>

<div class="section contact overlap">
		<div class="container">
			<div class="row">
				<div>
					<h3 class="text-center"><?php echo $branch->{'name_'.Request::segment('1')} ?></h3>
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3453.1893314661347!2d31.327839000000015!3d30.060107000000002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14583e5cdb650f4f%3A0x9da55607d18a6930!2sSwcS+Oil+%26+Gas+Training!5e0!3m2!1sar!2seg!4v1420639407103" width="100%" height="450" frameborder="0" style="border:0"></iframe>
				</div>
				
				<div class="col-sm-8 col-md-8">
				<div class="widget download">
						
					</div>
					<div class="widget contact-info-sidebar"> 
						<div class="widget-title">
							@lang('lang.Contact Data')</div>
						<ul class="list-info">
							<li>
								<div class="info-icon">
									<span class="fa fa-map-marker"></span>
		 						</div>
								<div class="info-text">  <?php echo $branch->{'address_'.Request::segment('1')} ?></div> </li>
							<li>
								<div class="info-icon">
									<span class="fa fa-phone"></span>
								</div>
								@if(Request::segment('1') == "ar")
								 <?php $western_arabic = array('0','1','2','3','4','5','6','7','8','9');
                                  $eastern_arabic = array('٠','١','٢','٣','٤','٥','٦','٧','٨','٩');
                                  $contact_phone=$branch->contact_phone;
                                  $contact_phone=str_replace($western_arabic, $eastern_arabic, $contact_phone);
                                  $contact_mobile=$branch->contact_mobile;
                                  $contact_mobile=str_replace($western_arabic, $eastern_arabic, $contact_mobile);?>

								<div class="info-text">{{$contact_phone}}</div>
								@else
								<div class="info-text">{{$branch->contact_phone}}</div>
								@endif
							</li>
							<li>
								<div class="info-icon">
									<span class="fa fa-phone"></span>
								</div>
								@if(Request::segment('1') == "ar")
								<div class="info-text">{{$contact_mobile}}</div>
								@else
								<div class="info-text">{{$branch->contact_mobile}}</div>
								@endif
							</li>
							<li>
								<div class="info-icon">
									<span class="fa fa-envelope"></span>
								</div>
								<div class="info-text">{{$branch->contact_email}}</div>
							</li>
							
						</ul>
					</div> 
				</div>

			</div>
			
		</div>
</div>
@endsection