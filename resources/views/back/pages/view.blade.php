@extends('layout.layout')
@section('content')


                            

<div class="m-content">
    <?php
        if(Session::has('msg')){
            echo App\Helpers\Globals::msg(Session::get('msg'));
        }
    ?>

    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        All Pages
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body2">
            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30 hidden">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1 ">
                        <div class="form-group m-form__group row align-items-center">
                            <div class="col-md-4">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="generalSearch">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-search"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                        <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>
                </div>
            </div>
            <!--end: Search Form -->
            <!--begin: Datatable -->
            @if(session()->has('message'))
                <div class="alert alert-danger  ">
                    {{ session()->get('message') }}
                </div>
            @endif

               @if(Session::has('trash'))
                <div class="alert alert-brand m-alert m-alert--icon m-alert--air m-alert--square m--margin-bottom-30" role="alert">
                    <div class="m-alert__icon">
                        <i class="flaticon-exclamation-1"></i>
                    </div>
                    <div class="m-alert__text">
                        {{ Session::get('trash') }}
                    </div>
                </div>
                @endif

                 @if(Session::has('delete'))
                <div class="alert alert-danger m-alert m-alert--icon m-alert--air m-alert--square m--margin-bottom-30" role="alert">
                    <div class="m-alert__icon">
                        <i class="flaticon-exclamation-1"></i>
                    </div>
                    <div class="m-alert__text">
                        {{ Session::get('delete') }}
                    </div>
                </div>
                @endif
            <a class="btn btn-brand" href="{{ route('pages.create') }}" >
                <span class="m-nav__link-text">
                    Add New Pages
                </span>
            </a>
            <br><br>
            <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" id="local_data" >
                <table class="table table-bordered m-table m-table--border-brand m-table--head-bg-brand" style="padding:10%; ">
                    <thead>
                    <tr>
                        <th>ID </th>
                        <th>Pages Name </th>
                        <th>Pages Photo </th>
                        <th>Control Pages </th>
                      
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($pages as $page)
                            <tr style="background-color: #ababab; " @if($page->trash==1) class='kashat_alert_danger' @endif id="trashed_{{ $page->id }}">
                                <td>{{ $page->id }} </td>
                                <td>{{ $page->name_en.'_'.$page->name_ar }} </td>
                                <td>
                                    @if(isset($page->photo) && $page->photo != NULL)
                                    <img src="{{asset($page->photo)}}" width="75px">
                                    @endif
                                </td>
                               
                                <td>
                                    @if($check_perimation->permission_id != 1)
                                    <a class="btn btn-brand" href="{{ route('pages.edit', ['id' => $page->id]) }}" title="Edit">
                                        <i class="m-nav__link-icon fa fa-pencil-square-o"></i>
                                    </a>
                                    @endif
                                    @if($check_perimation->permission_id == 3)
                                    <a class="btn btn-danger delete" href="javascript:void(0);"  data-id="<?php echo $page->id; ?>" data-page="pages" base_url="{{ url('/') }}"  route="pages" title="Delete">
                                        <i class="m-nav__link-icon fa fa-trash-o"></i>
                                       
                                    </a>
                                    @endif   

                                     <a class="btn m-btn m-btn--gradient-from-success m-btn--gradient-to-accent show-page" href="javascript:void(0);"  data-id="<?php echo $page->id; ?>" data-page="pages" base_url="{{ url('/') }}" title="Details">
                                        <i class="m-nav__link-icon fa fa-eye"></i>
                                       
                                    </a>
                                    @if($page->trash==0)
                                     <a class="btn btn-warning" href="{{ url('admin/pages/trash/'.$page->id.'/'.$page->trash) }}" title="Trash" >
                                        <i class="m-nav__link-icon fa fa-times" style="color: white;"></i>
                                    </a>
                                    @else
                                     <a class="btn btn-success" href="{{ url('admin/pages/trash/'.$page->id.'/'.$page->trash) }}" title="Untrash">
                                        <i class="m-nav__link-icon fa fa-check"></i>
                                    </a>
                                    @endif
                                </td>
                               
                            </tr>
                            
                            <div class="modal fade" id="m_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">
                                                Confirmation Message
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">
                                                    &times;
                                                </span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                           
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal" style="background-color:black;">
                                                Close
                                            </button>
                                            <form action="{{ route('pages.destroy', ['id' => $page->id]) }}" method="post" enctype="multipart/form-data" >
                                                         {{ csrf_field() }}
                                                         {{ method_field('DELETE') }}
                                            <input type="submit" class="btn btn-brand" value="ok">
                                                       
                                                       
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if(isset($page->sec_level) && !empty($page->sec_level))
                                @foreach($page->sec_level as $sec_level)
                                    <tr style="background-color: #d4d3d3; " @if($sec_level->trash==1) class='kashat_alert_danger' @endif id="trashed_{{ $sec_level->id }}">
                                        <td>{{ $sec_level->id }} </td>
                                        <td>{{ $sec_level->name_en.'_'.$sec_level->name_ar }} </td>
                                        <td><img src="{{asset($sec_level->photo)}}" width="75px"></td>
                                       
                                        <td>
                                            <a class="btn btn-brand" href="{{ route('pages.edit', ['id' => $sec_level->id]) }}" >
                                                <i class="m-nav__link-icon fa fa-pencil-square-o"></i>
                                            </a>
                                            
                                            <a class="btn btn-danger delete" href="javascript:void(0);"  data-id="<?php echo $sec_level->id; ?>" data-page="pages" base_url="{{ url('/') }}"  route="pages">
                                                <i class="m-nav__link-icon fa fa-trash-o"></i>
                                               
                                            </a>
                                               

                                             <a class="btn m-btn m-btn--gradient-from-success m-btn--gradient-to-accent show-page" href="javascript:void(0);"  data-id="<?php echo $sec_level->id; ?>" data-page="pages" base_url="{{ url('/') }}">
                                                <i class="m-nav__link-icon fa fa-eye"></i>
                                               
                                            </a>
                                            @if($sec_level->trash==0)
                                             <a class="btn btn-warning" href="{{ url('admin/pages/trash/'.$sec_level->id.'/'.$sec_level->trash) }}" >
                                                <i class="m-nav__link-icon fa fa-times" style="color: white;"></i>
                                            </a>
                                            @else
                                             <a class="btn btn-success" href="{{ url('admin/pages/trash/'.$sec_level->id.'/'.$sec_level->trash) }}" >
                                                <i class="m-nav__link-icon fa fa-check"></i>
                                            </a>
                                            @endif
                                        </td>
                                       
                                    </tr>
                                    
                                    <div class="modal fade" id="m_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">
                                                        Confirmation Message
                                                    </h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">
                                                            &times;
                                                        </span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                   
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="background-color:black;">
                                                        Close
                                                    </button>
                                                    <form action="{{ route('pages.destroy', ['id' => $sec_level->id]) }}" method="post" enctype="multipart/form-data" >
                                                                 {{ csrf_field() }}
                                                                 {{ method_field('DELETE') }}
                                                    <input type="submit" class="btn btn-brand" value="ok">
                                                               
                                                               
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    @if(isset($sec_level->thrd_level) && !empty($sec_level->thrd_level))
                                        @foreach($sec_level->thrd_level as $thrd_level)
                                            <tr  @if($thrd_level->trash==1) class='kashat_alert_danger' @endif id="trashed_{{ $thrd_level->id }}">
                                                <td>{{ $thrd_level->id }} </td>
                                                <td>{{ $thrd_level->name_en.'_'.$thrd_level->name_ar }} </td>
                                                <td><img src="{{asset($thrd_level->photo)}}" width="75px"></td>
                                               
                                                <td>
                                                    <a class="btn btn-brand" href="{{ route('pages.edit', ['id' => $thrd_level->id]) }}" >
                                                        <i class="m-nav__link-icon fa fa-pencil-square-o"></i>
                                                    </a>
                                                    
                                                    <a class="btn btn-danger delete" href="javascript:void(0);"  data-id="<?php echo $thrd_level->id; ?>" data-page="pages" base_url="{{ url('/') }}"  route="pages">
                                                        <i class="m-nav__link-icon fa fa-trash-o"></i>
                                                       
                                                    </a>
                                                       

                                                     <a class="btn m-btn m-btn--gradient-from-success m-btn--gradient-to-accent show-page" href="javascript:void(0);"  data-id="<?php echo $thrd_level->id; ?>" data-page="pages" base_url="{{ url('/') }}">
                                                        <i class="m-nav__link-icon fa fa-eye"></i>
                                                       
                                                    </a>
                                                    @if($thrd_level->trash==0)
                                                     <a class="btn btn-warning" href="{{ url('admin/pages/trash/'.$thrd_level->id.'/'.$thrd_level->trash) }}" >
                                                        <i class="m-nav__link-icon fa fa-times" style="color: white;"></i>
                                                    </a>
                                                    @else
                                                     <a class="btn btn-success" href="{{ url('admin/pages/trash/'.$thrd_level->id.'/'.$thrd_level->trash) }}" >
                                                        <i class="m-nav__link-icon fa fa-check"></i>
                                                    </a>
                                                    @endif
                                                </td>
                                               
                                            </tr>
                                            
                                            <div class="modal fade" id="m_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">
                                                                Confirmation Message
                                                            </h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">
                                                                    &times;
                                                                </span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                           
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal" style="background-color:black;">
                                                                Close
                                                            </button>
                                                            <form action="" id="form" method="post" enctype="multipart/form-data" >
                                                                         {{ csrf_field() }}
                                                                         {{ method_field('DELETE') }}
                                                            <input type="submit" class="btn btn-brand" value="ok">
                                                                       
                                                                       
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif
                            

                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable -->
                <br>
                {{$pages->links()}}
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="m_modal_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">
                                            More Details
                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">
                                                &times;
                                            </span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                       
                                    </div>
                                    <div class="modal-footer">
                                       <!--  <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                            Close
                                        </button> -->
                                        <button type="button" class="btn btn-brand" data-dismiss="modal">
                                            Close
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

@endsection