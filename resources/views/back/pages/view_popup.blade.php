

           
            <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" id="local_data" >
                 <table class="table table-bordered m-table m-table--border-brand m-table--head-bg-brand" style="padding:10%; ">
                    <thead>
                    <tr>
                        <th>ID </th>
                        <th> Name </th>
                        <th> Photo </th>
                        <th> Banner </th>
                        <th>Bref </th>
                        <th>Content </th>
                        <th>Order</th>
                        <th>Trash</th>

                       
                    </tr>
                    </thead>
                    <tbody>
                        

                                <tr @if($page->trash==1) class='kashat_alert_danger' @endif id="trashed_{{ $page->id }}"  >
                                    <td>{{ $page->id }} </td>
                                    <td>{{ $page->name_en.'_'.$page->name_ar }} </td>
                                    <td><img src="{{asset($page->photo)}}" width="75px"></td>
                                    <td><img src="{{asset($page->banner)}}" width="75px"></td>
                                    <td>{{ $page->bref_en.'_'.$page->bref_ar }}</td>
                                    <td>{{ $page->description_en.'_'.$page->description_ar }}</td>
                                    <td>{{ $page->order }}</td>
                                    <td>@if($page->trash==1) {{"Untrashed"}} @else {{"Trashed"}} @endif </td>
                                   
                                   
                                    
                                
                                </tr>
                       
                    </tbody>
                </table>
                <!--end: Datatable -->
                <br>
                
            </div>
        </div>
    </div>
</div>




