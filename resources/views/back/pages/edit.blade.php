@extends('layout.layout')
@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">

					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">
									Edit {{$page->name_en}} Page
								</h3>
								<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
									<li class="m-nav__item m-nav__item--home">
										<a href="#" class="m-nav__link m-nav__link--icon">
											<i class="m-nav__link-icon la la-home"></i>
										</a>
									</li>
									<li class="m-nav__separator">
										-
									</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												All_Pages
											</span>
										</a>
									</li>
									<li class="m-nav__separator">
										-
									</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												Edit Page
											</span>
										</a>
									</li>
									
									
								</ul>
							</div>
							
							<div>
								
								<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover" aria-expanded="true">
									<a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
										<i class="la la-plus m--hide"></i>
										<i class="la la-ellipsis-h"></i>
									</a>
									<div class="m-dropdown__wrapper">
										<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
										<div class="m-dropdown__inner">
											<div class="m-dropdown__body">
												<div class="m-dropdown__content">
													<ul class="m-nav">
														<li class="m-nav__section m-nav__section--first m--hide">
															<span class="m-nav__section-text">
																Quick Actions
															</span>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-share"></i>
																<span class="m-nav__link-text">
																	Activity
																</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-chat-1"></i>
																<span class="m-nav__link-text">
																	Messages
																</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-info"></i>
																<span class="m-nav__link-text">
																	FAQ
																</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-lifebuoy"></i>
																<span class="m-nav__link-text">
																	Support
																</span>
															</a>
														</li>
														<li class="m-nav__separator m-nav__separator--fit"></li>
														<li class="m-nav__item">
															<a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">
																Submit
															</a>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END: Subheader -->
					<div class="m-content">
						<div class="row">
							<div class="col-lg-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
												<h3 class="m-portlet__head-text">
													Edit new page
												</h3>
											</div>
										</div>
									</div>
									<!--begin::Form-->
									 @if(Session::has('edit-page'))
					                <div class="alert alert-brand m-alert m-alert--icon m-alert--air m-alert--square m--margin-bottom-30" role="alert">
					                    <div class="m-alert__icon">
					                        <i class="flaticon-exclamation-1"></i>
					                    </div>
					                    <div class="m-alert__text">
					                        {{ Session::get('edit-page') }}
					                    </div>
					                </div>
					          		  @endif
									<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="POST" action="{{route('pages.update', ['id' => $page->id])}}" enctype="multipart/form-data">
                       				 {{ csrf_field() }} 
                       				 {{ method_field('PATCH') }}
										<div class="m-portlet__body">
											<div class="form-group m-form__group row">
												<div class="col-lg-6">
													<label>
													  Name_EN:
													</label>
													<input type="text" class="form-control m-input" placeholder="Enter name_en" name="name_en" value="{{$page->name_en}}" required>
													<span class="m-form__help">
														Please enter your name_en
													</span>
												</div>

												<div class="col-lg-6">
													<label>
													  Name_AR:
													</label>
													<input type="text" class="form-control m-input" placeholder="Enter name_ar" name="name_ar" value="{{$page->name_ar}}" required>
													<span class="m-form__help">
														Please enter your name_ar
													</span>
												</div>

												
												<div class="col-lg-6">
													<label class="">
														Bref_EN:
													</label>
													<input type="text" class="form-control m-input" placeholder="Enter bref_en" name="bref_en" value="{{$page->bref_en}}" required>
													<span class="m-form__help">
														Please enter your bref_en
													</span>
												</div>
												<div class="col-lg-6">
													<label class="">
														Bref_AR:
													</label>
													<input type="text" class="form-control m-input" placeholder="Enter bref_ar" name="bref_ar" value="{{$page->bref_en}}" required>
													<span class="m-form__help">
														Please enter bref_ar
													</span>
												</div>
											
												<div class="col-lg-6">
													<div class="form-group m-form__group">
					                                	<label for="exampleInputEmail1">
					                                    	Describtion_EN
					                                	</label>
					                               
					                                	<textarea name="description_en" id="editor" required>{{$page->description_en}}</textarea>	
					                            	</div>
					                           	</div>
												


												<div class="col-lg-6">
													<div class="form-group m-form__group">
						                                <label for="exampleInputEmail1">
						                                    Describtion_AR
						                                </label>
						                                <textarea name="description_ar" id="editor1" required>{{$page->description_ar}}</textarea>
					                            	</div>
					                          	</div>


											
												<div class="col-lg-6">
													<label class="">
														Order:
													</label>
													<input type="number" class="form-control m-input" placeholder="Enter order" name="order" value="{{$page->order}}" required>
													<span class="m-form__help">
														Please enter order
													</span>
												</div>

												<div class="col-lg-6">
													<label class="">
														Sub_Of
													</label>
													<div class="m-input-icon m-input-icon--right">
														<select name="sub_of" id="parent_category" class="form-control m-input  m-input--square" required>
															<option value="0">Main</option>
						                                    @foreach ($sub_page as $sub )
						                                        <option value="{{ $sub->id }}" @if ($sub->id==$page->sub_of)
					                                               selected 
					                                           @endif>{{ $sub->name_en.'_'.$sub->name_ar }}</option>
						                                    @endforeach
						                                </select>
													</div>
													<span class="m-form__help">
														Please choose sub_page
													</span>
												
												</div>
												<div class="col-lg-6">
													<label class="">
														Is_main_page?
													</label>
													<div class="m-input-icon m-input-icon--right">
														<div class="m-radio-inline">
															<label class="m-radio m-radio--solid m-radio--state-brand">
																<input type="radio"  name="is_main_page" value="1" @if($page->is_main_page=="1") checked="checked" @endif>
																Yes
																<span></span>
															</label>
															<label class="m-radio m-radio--solid m-radio--state-success">
																<input type="radio" name="is_main_page" value="0" @if($page->is_main_page=="0") checked="checked" @endif>
																No
																<span></span>
															</label>
															
														</div>
														<span class="m-form__help">
															Please enter is_main_page?
														</span>	
													</div>
													
												</div>
												<div class="col-lg-6">
													<label class="">
														Nav
													</label>
													<div class="m-input-icon m-input-icon--right">
																<div class="m-radio-inline">
																	<label class="m-radio m-radio--solid m-radio--state-brand">
																		<input type="radio" name="nav" value="1" @if($page->nav=="1") checked="checked" @endif>
																		Yes
																		<span></span>
																	</label>
																	<label class="m-radio m-radio--solid m-radio--state-success">
																		<input type="radio" name="nav" value="0" @if($page->nav=="0") checked="checked" @endif>
																		No
																		<span></span>
																	</label>
																	
																</div>
																<span class="m-form__help">
																	Please enter nav?
																</span>
															
													</div>
													
												</div>

												<div class="col-lg-6">
													<label class="">
														Category
													</label>
													<div class="m-input-icon m-input-icon--right">
																<div class="m-radio-inline">
																	<label class="m-radio m-radio--solid m-radio--state-brand">
																		<input type="radio" name="category" value="1" @if($page->category=="1") checked="checked" @endif>
																		Yes
																		<span></span>
																	</label>
																	<label class="m-radio m-radio--solid m-radio--state-success">
																		<input type="radio" name="category" value="0" @if($page->category=="0") checked="checked" @endif>
																		No
																		<span></span>
																	</label>
																	
																</div>
																<span class="m-form__help">
																	Please enter category?
																</span>
															
													</div>
													
												</div>
												<div class="col-lg-6">
													<label class="">
														Delete
													</label>
													<div class="m-input-icon m-input-icon--right">
																<div class="m-radio-inline">
																	<label class="m-radio m-radio--solid m-radio--state-brand">
																		<input type="radio" name="delete" value="1" @if($page->delete=="1") checked="checked" @endif>
																		Yes
																		<span></span>
																	</label>
																	<label class="m-radio m-radio--solid m-radio--state-success">
																		<input type="radio" name="delete" value="0" @if($page->delete=="0") checked="checked" @endif>
																		No
																		<span></span>
																	</label>
																	
																</div>
																<span class="m-form__help">
																	Please enter delete?
																</span>
															
													</div>
													
												</div>
												<!-- <div class="col-lg-6">
													<label class="">
														Can_not_delete
													</label>
													<div class="m-input-icon m-input-icon--right">
																<div class="m-radio-inline">
																	<label class="m-radio m-radio--solid m-radio--state-brand">
																		<input type="radio" name="can_not_delete" value="1" @if($page->can_not_delete=="1") checked="checked" @endif>
																		Yes
																		<span></span>
																	</label>
																	<label class="m-radio m-radio--solid m-radio--state-success">
																		<input type="radio" name="can_not_delete" value="0" @if($page->can_not_delete=="0") checked="checked" @endif>
																		No
																		<span></span>
																	</label>
																	
																</div>
																<span class="m-form__help">
																	Please enter can_not_delete?
																</span>
															
													</div>
													
												</div> -->
												<div class="col-lg-6">
													<label class="">
														Accordion
													</label>
													<div class="m-input-icon m-input-icon--right">
																<div class="m-radio-inline">
																	<label class="m-radio m-radio--solid m-radio--state-brand">
																		<input type="radio" name="accordion" value="1" @if($page->accordion=="1") checked="checked" @endif>
																		Yes
																		<span></span>
																	</label>
																	<label class="m-radio m-radio--solid m-radio--state-success">
																		<input type="radio" name="accordion" value="0" @if($page->accordion=="0") checked="checked" @endif>
																		No
																		<span></span>
																	</label>
																	
																</div>
																<span class="m-form__help">
																	Please enter accordion?
																</span>
															
													</div>
													
												</div>
												<div class="col-lg-6">
													<label class="">
														Gallery
													</label>
													<div class="m-input-icon m-input-icon--right">
																<div class="m-radio-inline">
																	<label class="m-radio m-radio--solid m-radio--state-brand">
																		<input type="radio" name="gallery" value="1" @if($page->gallery=="1") checked="checked" @endif>
																		Yes
																		<span></span>
																	</label>
																	<label class="m-radio m-radio--solid m-radio--state-success">
																		<input type="radio" name="gallery" value="0" @if($page->gallery=="0") checked="checked" @endif >
																		No
																		<span></span>
																	</label>
																	
																</div>
																<span class="m-form__help">
																	Please enter gallery?
																</span>
															
													</div>
													
												</div>

												<div class="col-lg-6">
													<label class="">
														Selector
													</label>
													<div class="m-input-icon m-input-icon--right">
																<div class="m-radio-inline">
																	<label class="m-radio m-radio--solid m-radio--state-brand">
																		<input type="radio" name="selector" value="1" @if($page->selector=="1") checked="checked" @endif>
																		Yes
																		<span></span>
																	</label>
																	<label class="m-radio m-radio--solid m-radio--state-success">
																		<input type="radio" name="selector" value="0" @if($page->selector=="0") checked="checked" @endif>
																		No
																		<span></span>
																	</label>
																	
																</div>
																<span class="m-form__help">
																	Please enter selector?
																</span>
															
													</div>
													
												</div>

												<div class="col-lg-6">
													<label>
													  Photo:
													</label>
													<img src="{{asset($page->photo)}}" width="155px">
													<input type="file" class="form-control m-input" placeholder="Enter photo" name="photo" required>
													<span class="m-form__help">
														Please enter your photo
													</span>
												</div>

												<div class="col-lg-6">
													<label>
													  Banner:
													</label>
													<img src="{{asset($page->banner)}}" width="55px">
													<input type="file" class="form-control m-input" placeholder="Enter banner" name="banner" required>
													<span class="m-form__help">
														Please enter your banner
													</span>
												</div>

												


											</div>


											
											
										</div>
										<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
											<div class="m-form__actions m-form__actions--solid">
												<div class="row">
													<div class="col-lg-6">
														<button type="Submit" class="btn btn-brand">
															Save
														</button>
														<a type="button" class="btn btn-default" href="{{route('pages.index')}}" style="color: #716aca;">
															Cancel
														</a>
													</div>
													<div class="col-lg-6 m--align-right">
														<button type="reset" class="btn btn-danger">
															Delete
														</button>
													</div>
												</div>
											</div>
										</div>
									</form>

									
									<!--end::Form-->
								</div>
								@if($page->gallery==1)
								           <div class="m-portlet__body">
								           	 @if(session()->has('message'))
								                <div class="alert alert-danger  ">
								                    {{ session()->get('message') }}
								                </div>
								            @endif

								               @if(Session::has('trash1'))
								                <div class="alert alert-brand m-alert m-alert--icon m-alert--air m-alert--square m--margin-bottom-30" role="alert">
								                    <div class="m-alert__icon">
								                        <i class="flaticon-exclamation-1"></i>
								                    </div>
								                    <div class="m-alert__text">
								                        {{ Session::get('trash1') }}
								                    </div>
								                </div>
								                @endif

								                 @if(Session::has('delete1'))
								                <div class="alert alert-danger m-alert m-alert--icon m-alert--air m-alert--square m--margin-bottom-30" role="alert">
								                    <div class="m-alert__icon">
								                        <i class="flaticon-exclamation-1"></i>
								                    </div>
								                    <div class="m-alert__text">
								                        {{ Session::get('delete1') }}
								                    </div>
								                </div>
								                @endif
								            <a class="btn btn-brand" href="{{ url('admin/gallery/add/'.$page->id.'/pages') }}" >
								                <span class="m-nav__link-text">
								                    Add New Gallery to page
								                </span>
								            </a>
								            <br><br>
											<div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" id="local_data" >
							                <table class="table table-bordered m-table m-table--border-brand m-table--head-bg-brand" style="padding:10%; ">
							                    <thead>
							                    <tr>
							                        <th title="Field #1">ID </th>
							                        <th title="Field #2">Gallery Name </th>
							                        <th title="Field #2">Gallery Photo </th>
							                        <th title="Field #5">Control Gallery </th>
							                      
							                    </tr>
							                    </thead>
							                    <tbody>
							                        @foreach($gallery as $gall)
							                                <tr @if($gall->trash==1) class='kashat_alert_danger' @endif id="trashed_{{ $gall->id }}">
							                                    <td>{{ $gall->id }} </td>
							                                    <td>{{ $gall->name_en.'_'.$gall->name_ar }} </td>
							                                    <td><img src="{{asset($gall->file)}}" width="75px"></td>
							                                   
							                                    <td>
							                                        <a class="btn btn-brand" href="{{ route('gallery.edit', ['id' => $gall->id]) }}" >
							                                            <i class="m-nav__link-icon fa fa-pencil-square-o"></i>
							                                        </a>
							                                        
							                                        <a class="btn btn-danger delete" href="javascript:void(0);"  data-id="<?php echo $gall->id; ?>" data-page="gallery" base_url="{{ url('/') }}"  route="gallery">
							                                            <i class="m-nav__link-icon fa fa-trash-o"></i>
							                                           
							                                        </a>
							                                           

							                                         <a class="btn m-btn m-btn--gradient-from-success m-btn--gradient-to-accent show-page" href="javascript:void(0);"  data-id="<?php echo $gall->id; ?>" data-page="gallaries" base_url="{{ url('/') }}">
							                                            <i class="m-nav__link-icon fa fa-eye"></i>
							                                           
							                                        </a>
							                                        @if($gall->trash==0)
							                                         <a class="btn btn-warning" href="{{ url('admin/gallery/trash/'.$gall->id.'/'.$gall->trash) }}" >
							                                            <i class="m-nav__link-icon fa fa-times" style="color: white;"></i>
							                                        </a>
							                                        @else
							                                         <a class="btn btn-success" href="{{ url('admin/gallery/trash/'.$gall->id.'/'.$gall->trash) }}" >
							                                            <i class="m-nav__link-icon fa fa-check"></i>
							                                        </a>
							                                        @endif
							                                    </td>
							                                   
							                                </tr>
							                                
							                                <div class="modal fade" id="m_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							                                <div class="modal-dialog" role="document">
							                                    <div class="modal-content">
							                                        <div class="modal-header">
							                                            <h5 class="modal-title" id="exampleModalLabel">
							                                                Confirmation Message
							                                            </h5>
							                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							                                                <span aria-hidden="true">
							                                                    &times;
							                                                </span>
							                                            </button>
							                                        </div>
							                                        <div class="modal-body">
							                                           
							                                        </div>
							                                        <div class="modal-footer">
							                                            <button type="button" class="btn btn-secondary" data-dismiss="modal" style="background-color:black;">
							                                                Close
							                                            </button>
							                                            <form action="" id="form" method="post" enctype="multipart/form-data" >
							                                                         {{ csrf_field() }}
							                                                         {{ method_field('DELETE') }}
							                                            <input type="submit" class="btn btn-brand" value="ok">
							                                                       
							                                                       
							                                            </form>
							                                        </div>
							                                    </div>
							                                </div>
							                        </div>
							                        @endforeach
							                    </tbody>
							                </table>
							                <!--end: Datatable -->
							                <br>
							                {{$gallery->links()}}
          								  </div>
          								</div>
          							
									@endif

									@if($page->accordion==1)
								           <div class="m-portlet__body">
								           	 @if(session()->has('message'))
								                <div class="alert alert-danger  ">
								                    {{ session()->get('message') }}
								                </div>
								            @endif

								               @if(Session::has('trash'))
								                <div class="alert alert-brand m-alert m-alert--icon m-alert--air m-alert--square m--margin-bottom-30" role="alert">
								                    <div class="m-alert__icon">
								                        <i class="flaticon-exclamation-1"></i>
								                    </div>
								                    <div class="m-alert__text">
								                        {{ Session::get('trash') }}
								                    </div>
								                </div>
								                @endif

								                 @if(Session::has('delete'))
								                <div class="alert alert-danger m-alert m-alert--icon m-alert--air m-alert--square m--margin-bottom-30" role="alert">
								                    <div class="m-alert__icon">
								                        <i class="flaticon-exclamation-1"></i>
								                    </div>
								                    <div class="m-alert__text">
								                        {{ Session::get('delete') }}
								                    </div>
								                </div>
								                @endif
								            <a class="btn btn-brand" href="{{ url('admin/accordion/create/'.$page->id) }}" >
								                <span class="m-nav__link-text">
								                    Add New Accordion to page
								                </span>
								            </a>
								            <br><br>
											<div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" id="local_data" >
							                <table class="table table-bordered m-table m-table--border-brand m-table--head-bg-brand" style="padding:10%; ">
							                    <thead>
							                    <tr>
							                        <th title="Field #1">ID </th>
							                        <th title="Field #2">accordion Name </th>
							                        <th title="Field #2">accordion Photo </th>
							                        <th title="Field #5">Control accordion </th>
							                      
							                    </tr>
							                    </thead>
							                    <tbody>
							                        @foreach($accordion as $accord)
							                                <tr @if($accord->trash==1) class='kashat_alert_danger' @endif id="trashed_{{ $accord->id }}">
							                                    <td>{{ $accord->id }} </td>
							                                    <td>{{ $accord->name_en.'_'.$accord->name_ar }} </td>
							                                    <td><img src="{{asset($accord->photo)}}" width="75px"></td>
							                                   
							                                    <td>
							                                        <a class="btn btn-brand" href="{{ route('accordion.edit', ['id' => $accord->id]) }}" >
							                                            <i class="m-nav__link-icon fa fa-pencil-square-o"></i>
							                                        </a>
							                                        
							                                        <a class="btn btn-danger delete" href="javascript:void(0);"  data-id="<?php echo $accord->id; ?>" data-page="accordions" base_url="{{ url('/') }}"  route="accordion">
							                                            <i class="m-nav__link-icon fa fa-trash-o"></i>
							                                           
							                                        </a>
							                                           

							                                         <a class="btn m-btn m-btn--gradient-from-success m-btn--gradient-to-accent show-page" href="javascript:void(0);"  data-id="<?php echo $accord->id; ?>" data-page="accordions" base_url="{{ url('/') }}">
							                                            <i class="m-nav__link-icon fa fa-eye"></i>
							                                           
							                                        </a>
							                                        @if($accord->trash==0)
							                                         <a class="btn btn-warning" href="{{ url('admin/accordion/trash/'.$accord->id.'/'.$accord->trash) }}" >
							                                            <i class="m-nav__link-icon fa fa-times" style="color: white;"></i>
							                                        </a>
							                                        @else
							                                         <a class="btn btn-success" href="{{ url('admin/accordion/trash/'.$accord->id.'/'.$accord->trash) }}" >
							                                            <i class="m-nav__link-icon fa fa-check"></i>
							                                        </a>
							                                        @endif
							                                    </td>
							                                   
							                                </tr>
							                                
							                                <div class="modal fade" id="m_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							                                <div class="modal-dialog" role="document">
							                                    <div class="modal-content">
							                                        <div class="modal-header">
							                                            <h5 class="modal-title" id="exampleModalLabel">
							                                                Confirmation Message
							                                            </h5>
							                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							                                                <span aria-hidden="true">
							                                                    &times;
							                                                </span>
							                                            </button>
							                                        </div>
							                                        <div class="modal-body">
							                                           
							                                        </div>
							                                        <div class="modal-footer">
							                                            <button type="button" class="btn btn-secondary" data-dismiss="modal" style="background-color:black;">
							                                                Close
							                                            </button>
							                                            <form action="" id="form" method="post" enctype="multipart/form-data" >
							                                                         {{ csrf_field() }}
							                                                         {{ method_field('DELETE') }}
							                                            <input type="submit" class="btn btn-brand" value="ok">
							                                                       
							                                                       
							                                            </form>
							                                        </div>
							                                    </div>
							                                </div>
							                        </div>
							                        @endforeach
							                    </tbody>
							                </table>
							                <!--end: Datatable -->
							                <br>
							                {{$accordion->links()}}
          								  </div>
          								</div>
          							   
									@endif
								<!--end::Portlet-->
								<div class="form-group m-form__group text-center" >
										<div class="fb-comments" data-href="{{Request::url()}}" data-numposts="5"></div>
								       <div id="fb-root"></div>
								    </div>
							</div>
						</div>
					</div>
				</div>


				<div class="modal fade" id="m_modal_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">
                                            More Details
                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">
                                                &times;
                                            </span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                       
                                    </div>
                                    <div class="modal-footer">
                                       <!--  <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                            Close
                                        </button> -->
                                        <button type="button" class="btn btn-brand" data-dismiss="modal">
                                            Close
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>



@endsection