

           
            <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" id="local_data" >
                 <table class="table table-bordered m-table m-table--border-brand m-table--head-bg-brand" style="padding:10%; ">
                    <thead>
                    <tr>
                        <th>ID </th>
                        <th> Name </th>
                        <th> Email </th>
                        <th> Address </th>
                        <th>Contact Num </th>
                        <th>Contact Title </th>
                        <th>Price_per_day</th>
                        <th>Trash</th>

                       
                    </tr>
                    </thead>
                    <tbody>
                        

                                <tr @if($hotel->trash==1) class='kashat_alert_danger' @endif id="trashed_{{ $hotel->id }}"  >
                                    <td>{{ $hotel->id }} </td>
                                    <td>{{ $hotel->name_en.'_'.$hotel->name_ar }} </td>
                                    <td>{{ $hotel->email }}</td>
                                    <td>{{ $hotel->address_en.'_'.$hotel->address_ar }}</td>
                                    <td>{{ $hotel->contact_num }}</td>
                                    <td>{{ $hotel->contact_title }}</td>
                                    <td>{{ $hotel->price_per_day }}</td>
                                    <td>@if($hotel->trash==1) {{"Untrashed"}} @else {{"Trashed"}} @endif </td>
                                   
                                   
                                    
                                
                                </tr>
                       
                    </tbody>
                </table>
                <!--end: Datatable -->
                <br>
                
            </div>
        </div>
    </div>
</div>




