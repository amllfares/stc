

           
            <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" id="local_data" >
                 <table class="table table-bordered m-table m-table--border-brand m-table--head-bg-brand" style="padding:10%; ">
                    <thead>
                    <tr>
                        <th> ID </th>
                        <th> Name </th>
                        <th> Num_hours </th>
                        <th> type </th>
                        <th> course </th>
                        <th>Trash</th>

                       
                    </tr>
                    </thead>
                    <tbody>
                        

                                <tr @if($exam->trash==1) class='kashat_alert_danger' @endif id="trashed_{{ $exam->id }}"  >
                                    <td>{{ $exam->id }} </td>
                                    <td>{{ $exam->name_en.'_'.$exam->name_ar }} </td>
                                    <td>{{ $exam->num_hours }}</td>
                                    <td>{{ $exam->type }}</td>
                                    <td>{{ $exam->course->name_en.'_'.$exam->course->name_ar }}</td>
                                    <td>@if($exam->trash==1) {{"Untrashed"}} @else {{"Trashed"}} @endif </td>
                                   
                                   
                                    
                                
                                </tr>
                       
                    </tbody>
                </table>
                <!--end: Datatable -->
                <br>
                
            </div>
        </div>
    </div>
</div>




