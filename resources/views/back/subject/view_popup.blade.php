

           
            <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" id="local_data" >
                 <table class="table table-bordered m-table m-table--border-brand m-table--head-bg-brand" style="padding:10%; ">
                    <thead>
                    <tr>
                        <th>ID </th>
                        <th> Name </th>
                        <th> Image </th>
                        <th> describtion1 </th>
                        <th> describtion2 </th>
                     
                        <th>category name  </th>
                        <th>trash</th>
                       
                    </tr>
                    </thead>
                    <tbody>
                        

                                <tr @if($subject->trash==1) class='kashat_alert_danger' @endif id="trashed_{{ $subject->id }}" >
                                   
                                    <td>{{ $subject->id }} </td>
                                    <td>{{ $subject->name_en.'_'.$subject->name_ar }} </td>
                                    <td><img src="{{asset($subject->image)}}" width="75px"></td>
                                   
                                    <td>{{ $subject->descrption_en1.'_'.$subject->descrption_ar1 }}</td>
                                    <td>{{ $subject->descrption_en2.'_'.$subject->descrption_ar2 }}</td>
                                    <td>{{$subject->subCategory->name_en.'_'.$subject->subCategory->name_ar }}</td>
                                   
                                     <td>@if($subject->trash==1) {{"Untrashed"}} @else {{"Trashed"}} @endif </td>
                            
                                
                                </tr>
                       
                    </tbody>
                </table>
                <!--end: Datatable -->
                <br>
                
            </div>
        </div>
    </div>
</div>




