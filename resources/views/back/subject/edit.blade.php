@extends('layout.layout')
@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">

					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">
									Edit {{$subject->name}} Subject
								</h3>
								<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
									<li class="m-nav__item m-nav__item--home">
										<a href="#" class="m-nav__link m-nav__link--icon">
											<i class="m-nav__link-icon la la-home"></i>
										</a>
									</li>
									<li class="m-nav__separator">
										-
									</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												All_Subject
											</span>
										</a>
									</li>
									<li class="m-nav__separator">
										-
									</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												Edit Subject
											</span>
										</a>
									</li>
									
									
								</ul>
							</div>
							
							<div>
								
								<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover" aria-expanded="true">
									<a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
										<i class="la la-plus m--hide"></i>
										<i class="la la-ellipsis-h"></i>
									</a>
									<div class="m-dropdown__wrapper">
										<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
										<div class="m-dropdown__inner">
											<div class="m-dropdown__body">
												<div class="m-dropdown__content">
													<ul class="m-nav">
														<li class="m-nav__section m-nav__section--first m--hide">
															<span class="m-nav__section-text">
																Quick Actions
															</span>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-share"></i>
																<span class="m-nav__link-text">
																	Activity
																</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-chat-1"></i>
																<span class="m-nav__link-text">
																	Messages
																</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-info"></i>
																<span class="m-nav__link-text">
																	FAQ
																</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-lifebuoy"></i>
																<span class="m-nav__link-text">
																	Support
																</span>
															</a>
														</li>
														<li class="m-nav__separator m-nav__separator--fit"></li>
														<li class="m-nav__item">
															<a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">
																Submit
															</a>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END: Subheader -->
					<div class="m-content">
						<div class="row">
							<div class="col-lg-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
												<h3 class="m-portlet__head-text">
													Edit {{$subject->name}} subject
												</h3>
											</div>
										</div>
									</div>
									<!--begin::Form-->
									 @if(Session::has('edit-subject'))
					                <div class="alert alert-brand m-alert m-alert--icon m-alert--air m-alert--square m--margin-bottom-30" role="alert">
					                    <div class="m-alert__icon">
					                        <i class="flaticon-exclamation-1"></i>
					                    </div>
					                    <div class="m-alert__text">
					                        {{ Session::get('edit-subject') }}
					                    </div>
					                </div>
					          		  @endif
									<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="POST" action="{{route('subjects.update', ['id' => $subject->id])}}" enctype="multipart/form-data">
                       				 {{ csrf_field() }} 
                       				 {{ method_field('PATCH') }} 
										<div class="m-portlet__body">
											<div class="form-group m-form__group row">
												<div class="col-lg-6">
													<label>
														NameEN:
													</label>
													<input type="text" class="form-control m-input" placeholder="Enter name" name="name_en" value="{{$subject->name_en}}" required>
													<span class="m-form__help">
														Please enter name_en
													</span>
												</div>
												<div class="col-lg-6">
													<label>
														NameAR:
													</label>
													<input type="text" class="form-control m-input" placeholder="Enter name" name="name_ar" value="{{$subject->name_ar}}" required>
													<span class="m-form__help">
														Please enter name_ar
													</span>
												</div>
											


												<div class="col-lg-6">
													<label class="">
														Sub_Title_En:
													</label>
													<input type="text" class="form-control m-input" placeholder="Sub_Title_En" name="sub_title_en" value="{{ $subject->sub_title_en}}" required>
													<span class="m-form__help">
														Please enter your sub_title_en
													</span>
												</div>
												<div class="col-lg-6">
													<label class="">
														Sub_Title_Ar:
													</label>
													<input type="text" class="form-control m-input" placeholder="Sub_Title_Ar" name="sub_title_ar" value="{{ $subject->sub_title_ar}}" required>
													<span class="m-form__help">
														Please enter your sub_title_ar
													</span>
												</div>
	
												<div class="col-lg-6">
												 <div class="form-group m-form__group">
					                                <label for="exampleInputEmail1">
					                                    Describtion_EN1
					                                </label>
					                               
					                                <textarea name="description_en1" id="editor" required>{{$subject->descrption_en1}}</textarea>
					                            </div>
					                           </div>
												


												<div class="col-lg-6">
												 <div class="form-group m-form__group">
					                                <label for="exampleInputEmail1">
					                                    Describtion_AR1
					                                </label>
					                                <textarea name="description_ar1" id="editor1" required>{{$subject->descrption_ar1}}</textarea>
					                            </div>
					                           </div>

					                           <div class="col-lg-6">
												 <div class="form-group m-form__group">
					                                <label for="exampleInputEmail1">
					                                    Describtion_EN2
					                                </label>
					                               
					                                <textarea name="description_en2" id="editor2" required>{{$subject->descrption_en2}}</textarea>
					                            </div>
					                           </div>
												


												<div class="col-lg-6">
												 <div class="form-group m-form__group">
					                                <label for="exampleInputEmail1">
					                                    Describtion_AR2
					                                </label>
					                                <textarea name="description_ar2" id="editor3" required>{{$subject->descrption_ar2}}</textarea>
					                            </div>
					                           </div>
											

											
												<div class="col-lg-6">
													<label class="">
														Subject_Category
													</label>
													<div class="m-input-icon m-input-icon--right">
														<select name="cate_id" id="parent_category" class="form-control m-input  m-input--square" required>
						                                    @foreach ($sub_cate as $sub )
						                                        <option value="{{ $sub->id }}" @if($sub->id==$subject->category_id)
					                                               selected 
					                                           @endif>{{ $sub->name_en.'_'.$sub->name_ar }}</option>
						                                    @endforeach
						                                </select>
													</div>
													<span class="m-form__help">
														Please enter Subject_Category
													</span>
												</div>

												<div class="col-lg-6">
													<label>
														Image:
													</label>
													<img src="{{ URL($subject->image)}}" width="155px" >
													<input type="file" class="form-control m-input" placeholder="Enter your Image" name="image" value="{{ $subject->name}}" width="100px" required>
													<span class="m-form__help">
														Please upload your Image
													</span>
												</div>

												<div class="col-lg-6">
													<label class="">
														Media
													</label>
													<input type="text" class="form-control m-input" placeholder="media" name="media" value="{{ $subject->media }}" required>
													<span class="m-form__help" >
														Please enter your media
													</span>
													
												</div>

												


											</div>


											
											
										</div>
										<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
											<div class="m-form__actions m-form__actions--solid">
												<div class="row">
													<div class="col-lg-6">
														<button type="Submit" class="btn btn-brand">
															Save
														</button>
														<a type="button" class="btn btn-default" href="{{route('subjects.index')}}" style="color: #716aca;">
															Cancel
														</a>
													</div>
													<div class="col-lg-6 m--align-right">
														<button type="reset" class="btn btn-danger">
															Delete
														</button>
													</div>
												</div>
											</div>
										</div>
									</form>
									<!--end::Form-->
								</div>
								<!--end::Portlet-->
							</div>
						</div>
					</div>
				</div>



@endsection