

           
            <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" id="local_data" >
                 <table class="table table-bordered m-table m-table--border-brand m-table--head-bg-brand" style="padding:10%; ">
                    <thead>
                    <tr>
                        <th>ID </th>
                        <th> Name </th>
                        <th> Email </th>
                        <th> Type </th>
                        <th> Trash </th>

                       
                    </tr>
                    </thead>
                    <tbody>
                        

                                <tr @if($users->trash==1) class='kashat_alert_danger' @endif id="trashed_{{ $users->id }}"  >
                                    <td>{{ $users->id }} </td>
                                    <td>{{ $users->name }} </td>
                                    <td>{{ $users->email }}</td>
                                    <td>@if($users->admin==1) {{"admin "}}<br> @endif @if($users->instructor==1) {{"instructor"}}<br> @endif @if($users->employe==1) {{"employe"}}<br> @endif  @if($users->candidate==1) {{"candidate"}} @endif
                                    </td>
                                     <td>@if($users->trash==1) {{"Untrashed"}} @else {{"Trashed"}} @endif </td>
                            
                                
                                </tr>
                       
                    </tbody>
                </table>
                <!--end: Datatable -->
                <br>
                
            </div>
        </div>
    </div>
</div>




