

           
            <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" id="local_data" >
                 <table class="table table-bordered m-table m-table--border-brand m-table--head-bg-brand" style="padding:10%; ">
                    <thead>
                    <tr>
                        <th>ID </th>
                        <th> Name </th>
                        <th> Email </th>
                        <th> Address </th>
                        <th>Contact Num </th>
                        <th>Price_per_hour</th>
                        <th>Trash</th>

                       
                    </tr>
                    </thead>
                    <tbody>
                        

                                <tr @if($instructor->trash==1) class='kashat_alert_danger' @endif id="trashed_{{ $instructor->id }}"  >
                                    <td>{{ $instructor->id }} </td>
                                    <td>{{ $instructor->name_en.'_'.$instructor->name_ar }} </td>
                                    <td>{{ $instructor->email }}</td>
                                    <td>{{ $instructor->address_en.'_'.$instructor->address_ar }}</td>
                                    <td>{{ $instructor->contact_num }}</td>
                                    <td>{{ $instructor->price_per_hour }}</td>
                                    <td>@if($instructor->trash==1) {{"Untrashed"}} @else {{"Trashed"}} @endif </td>
                                   
                                   
                                    
                                
                                </tr>
                       
                    </tbody>
                </table>
                <!--end: Datatable -->
                <br>
                
            </div>
        </div>
    </div>
</div>




