@extends('layout.layout')
@section('content')

<div class="m-content">
    <?php
        if(Session::has('msg')){
            echo App\Helpers\Globals::msg(Session::get('msg'));
        }
    ?>

    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        All Applied For Job
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body2">
            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30 hidden">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1 ">
                        <div class="form-group m-form__group row align-items-center">
                            <div class="col-md-4">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="generalSearch">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-search"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                        <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>
                </div>
            </div>
            <!--end: Search Form -->
            <!--begin: Datatable -->
            @if(session()->has('message'))
                <div class="alert alert-danger  ">
                    {{ session()->get('message') }}
                </div>
            @endif

               @if(Session::has('trash'))
                <div class="alert alert-brand m-alert m-alert--icon m-alert--air m-alert--square m--margin-bottom-30" role="alert">
                    <div class="m-alert__icon">
                        <i class="flaticon-exclamation-1"></i>
                    </div>
                    <div class="m-alert__text">
                        {{ Session::get('trash') }}
                    </div>
                </div>
                @endif

                @if(Session::has('approve'))
                <div class="alert alert-brand m-alert m-alert--icon m-alert--air m-alert--square m--margin-bottom-30" role="alert">
                    <div class="m-alert__icon">
                        <i class="flaticon-exclamation-1"></i>
                    </div>
                    <div class="m-alert__text">
                        {{ Session::get('approve') }}
                    </div>
                </div>
                @endif
                @if(Session::has('reject'))
                <div class="alert alert-brand m-alert m-alert--icon m-alert--air m-alert--square m--margin-bottom-30" role="alert">
                    <div class="m-alert__icon">
                        <i class="flaticon-exclamation-1"></i>
                    </div>
                    <div class="m-alert__text">
                        {{ Session::get('reject') }}
                    </div>
                </div>
                @endif

                 @if(Session::has('delete'))
                <div class="alert alert-danger m-alert m-alert--icon m-alert--air m-alert--square m--margin-bottom-30" role="alert">
                    <div class="m-alert__icon">
                        <i class="flaticon-exclamation-1"></i>
                    </div>
                    <div class="m-alert__text">
                        {{ Session::get('delete') }}
                    </div>
                </div>
                @endif
           
            <br><br>
            <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" id="local_data" >
                <table class="table table-bordered m-table m-table--border-brand m-table--head-bg-brand" style="padding:10%; ">
                    <thead>
                    <tr>
                        <th title="Field #1">ID </th>
                        <th title="Field #2">Name </th>
                        <th title="Field #2">Attachment </th>
                        <th title="Field #2">Control</th>

                       
                       
                      
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($applies as $apply)
                                <tr @if($apply->trash==1) class='kashat_alert_danger' @endif id="trashed_{{ $apply->id }}">
                                    <td>{{ $apply->id }} </td>
                                    <td>{{ $apply->user->name }} </td>
                                    <td> <a class="btn btn-brand" href="{{url('admin/careers/download/'.$apply->id)}}">
                                        <i class="fa fa-download" aria-hidden="true"></i>@lang('lang.Download')
                                    </a></td>
                                    <td>
                                        @if($apply->status != 1)
                                        <a class="btn btn-success" href="{{ url('admin/careers/approve/'.$apply->id) }}" >
                                            <i class="m-nav__link-icon fa fa-check"></i> Apply
                                        </a>
                                        @endif
                                        <a class="btn btn-danger" href="{{ url('admin/careers/reject/'.$apply->id) }}" >
                                            <i class="m-nav__link-icon fa fa-times"></i> Reject
                                        </a>

                                    </td>
    
                                   
                                </tr>
                                
                               
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable -->
                <br>
                {{$applies->links()}}
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="m_modal_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    More Details
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-brand" data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
    </div>
</div>

@endsection