

           
            <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" id="local_data" >
                 <table class="table table-bordered m-table m-table--border-brand m-table--head-bg-brand" style="padding:10%; ">
                    <thead>
                    <tr>
                        <th>ID </th>
                        <th>To </th>
                        <th>From </th>
                        <th>Amount </th>
                        <th>Extra</th>
                        <th>Confirm</th>
                        <th>Month</th>
                        <th>Labels</th>
                    </tr>
                    </thead>
                    <tbody>
                        

                        <tr>
                            <td>{{  $account->id }} </td>
                            @if(isset($account->users->name))
                            <td>{{ $account->users->name }} </td>
                            @else
                            <td></td>
                            @endif
                            @if(isset($account->user->name))
                            <td>{{ $account->user->name }} </td>
                            @else
                            <td></td>
                            @endif
                            <td>{!! $account->amount; !!} </td>
                            <td>{{ $account->extra }} </td>
                            <td>@if($account->confirm==0) Not Confirmed @else Confirmed @endif</td>
                            <td>{{$account->month}}</td>
                            <td>{{$account->labels}}</td>
                            
                        </tr>
                       
                    </tbody>
                </table>
                <!--end: Datatable -->
                <br>
                
            </div>
        </div>
    </div>
</div>




