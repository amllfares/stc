@extends('layout.layout')
@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">

					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">
									Add New Reserve
								</h3>
								<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
									<li class="m-nav__item m-nav__item--home">
										<a href="#" class="m-nav__link m-nav__link--icon">
											<i class="m-nav__link-icon la la-home"></i>
										</a>
									</li>
									<li class="m-nav__separator">
										-
									</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												All_Reserves
											</span>
										</a>
									</li>
									<li class="m-nav__separator">
										-
									</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												add Reserve
											</span>
										</a>
									</li>
									
									
								</ul>
							</div>
							
							<div>
								
								<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover" aria-expanded="true">
									<a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
										<i class="la la-plus m--hide"></i>
										<i class="la la-ellipsis-h"></i>
									</a>
									<div class="m-dropdown__wrapper">
										<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
										<div class="m-dropdown__inner">
											<div class="m-dropdown__body">
												<div class="m-dropdown__content">
													<ul class="m-nav">
														<li class="m-nav__section m-nav__section--first m--hide">
															<span class="m-nav__section-text">
																Quick Actions
															</span>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-share"></i>
																<span class="m-nav__link-text">
																	Activity
																</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-chat-1"></i>
																<span class="m-nav__link-text">
																	Messages
																</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-info"></i>
																<span class="m-nav__link-text">
																	FAQ
																</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-lifebuoy"></i>
																<span class="m-nav__link-text">
																	Support
																</span>
															</a>
														</li>
														<li class="m-nav__separator m-nav__separator--fit"></li>
														<li class="m-nav__item">
															<a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">
																Submit
															</a>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END: Subheader -->
					<div class="m-content">
						<div class="row">
							<div class="col-lg-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
												<h3 class="m-portlet__head-text">
													Add new Reserve
												</h3>
											</div>
										</div>
									</div>
									<!--begin::Form-->
									 @if(Session::has('Reservation'))
					                <div class="alert alert-brand m-alert m-alert--icon m-alert--air m-alert--square m--margin-bottom-30" role="alert">
					                    <div class="m-alert__icon">
					                        <i class="flaticon-exclamation-1"></i>
					                    </div>
					                    <div class="m-alert__text">
					                        {{ Session::get('Reservation') }}
					                    </div>
					                </div>
					          		  @endif
					          	
									<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="POST" action="{{route('private_reservations.store')}}" enctype="multipart/form-data">
	                   				 {{ csrf_field() }} 


										<div class="m-portlet__body">
											
											<div class="form-group m-form__group row">

														<div class="col-lg-6">
														<label>
															Search for email
														</label>
														<div class="input-group">
															<input type="text" class="form-control" placeholder="Search for email..."  name="email" id="amollllla" required>
															<span class="input-group-btn" onclick="get_search('{{url('/')}}');">
																<button class="btn btn-brand" type="button">
																	Go!
																</button>
															</span>
														</div>
													  </div>
												
										
							          		    <div class="form-group m-form__group row" id="search">
							          		    	<div class="col-lg-6">
														<label>
															 Name:
														</label>
														<input type="text" class="form-control m-input" placeholder="Enter full name" name="name"  required>
														<span class="m-form__help">
															Please enter your name
														</span>
													</div>
													<div class="col-lg-6">
														<label>
															Email:
														</label>
														<input type="email" class="form-control m-input" placeholder="Enter your email" name="email"  required>
														<span class="m-form__help">
															Please enter your email
														</span>
													</div>
													<div class="col-lg-6">
														<label class="">
															Password:
														</label>
														<input type="number" class="form-control m-input" placeholder="Enter Password" name="password" required>
														<span class="m-form__help">
															Please enter your new Password
														</span>
													</div>
													<div class="col-lg-6">
														<label class="">
															Confirmation Password:
														</label>
														<input type="text" class="form-control m-input" placeholder="Enter Confirmation Password" name="confirmation pass"  required>
														<span class="m-form__help">
															Please enter Confirmation Password
														</span>
													</div>
													<div class="col-lg-6">
														<label class="">
															User Type
														</label>
														<div class="m-input-icon m-input-icon--right">
															<div class="m-form__group form-group row">
																	
																	<div class="col-9">
																		<div class="m-checkbox-inline">
																			<label class="m-checkbox m-checkbox--solid m-checkbox--state-brand">
																				<input type="checkbox" name="admin" value="1" required>
																				Admin
																				<span></span>
																			</label>
																			<label class="m-checkbox m-checkbox--solid m-checkbox--state-success">
																				<input type="checkbox" name="instructor" value="1" required>
																				Instructor
																				<span></span>
																			</label>
																			<label class="m-checkbox m-checkbox--solid m-checkbox--state-brand">
																				<input type="checkbox" name="employe" value="1" required>
																				Employe
																				<span></span>
																			</label>

																			<label class="m-checkbox m-checkbox--solid m-checkbox--state-success">
																				<input type="checkbox" name="candidate" value="1"  required>
																				Candidate
																				<span></span>
																			</label>
																		</div>
																		
																	</div>
																</div>
														</div>
														<span class="m-form__help">
															Please Select User Type
														</span>
													</div>
													
												</div>

												<div class="col-lg-6">
													<label>
														Hotel
													</label>
													<div class="m-input-icon m-input-icon--right">
														<select name="hotel_id" id="parent_category" class="form-control m-input  m-input--square" required>
															<option>choose hotel </option>
						                                    @foreach ($hotels as $hotel )
						                                        <option value="{{ $hotel->id }}">{{ $hotel->name_en.'_'.$hotel->name_ar}}</option>
						                                    @endforeach
						                                </select>
													</div>
													<span class="m-form__help">
														Please choose hotel
													</span>

												
												</div>
												<div class="col-lg-6">
													<label>
														Resturant:
													</label>
													<div class="m-input-icon m-input-icon--right">
														<select name="rest_id" id="parent_category" class="form-control m-input  m-input--square" required>
															<option>choose resturant </option>
						                                    @foreach ($restaurants as $restaurant )
						                                        <option value="{{ $restaurant->id }}">{{ $restaurant->name_en.'_'.$restaurant->name_ar}}</option>
						                                    @endforeach
						                                </select>
													</div>
													<span class="m-form__help">
														Please choose restaurant
													</span>

												
												</div>
												<div class="col-lg-6">
													<label>
														Course
													</label>
													<div class="m-input-icon m-input-icon--right">
														<select name="course_id" id="parent_category" class="form-control m-input  m-input--square" onchange="course('{{url('/')}}',this.options[this.selectedIndex].value)" required>
															<option>choose course </option>
						                                    @foreach ($courses as $course )
						                                        <option value="{{ $course->id }}">{{ 'Price:'.$course->price.'  Offer:'.$course->offer}}</option>
						                                    @endforeach
						                                </select>
													</div>
													<span class="m-form__help">
														Please choose course
													</span>

												
												</div>
											</div>
												<div class="form-group m-form__group row">
												<div class="col-lg-6">
													<div id="course">
													
													</div>
												</div>
											
											</div>
											
										</div>
										<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
											<div class="m-form__actions m-form__actions--solid">
												<div class="row">
													<div class="col-lg-6">
														<button type="Submit" class="btn btn-brand">
															Save
														</button>
														<a type="button" class="btn btn-default" href="{{route('course.index')}}" style="color: #716aca;">
															Cancel
														</a>
													</div>
													<div class="col-lg-6 m--align-right">
														<button type="reset" class="btn btn-danger">
															Delete
														</button>
													</div>
												</div>
											</div>
										</div>
									</form>
									<!--end::Form-->
								</div>
								<!--end::Portlet-->
							</div>
						</div>
					</div>
</div>
@endsection