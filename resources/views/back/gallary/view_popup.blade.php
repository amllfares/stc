

           
            <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" id="local_data" >
                 <table class="table table-bordered m-table m-table--border-brand m-table--head-bg-brand" style="padding:10%; ">
                    <thead>
                    <tr>
                        <th>ID </th>
                        <th> Name </th>
                        <th> Photo </th>
                        <th> Banner </th>
                        <th>Bref </th>
                        <th>Trash</th>

                    </tr>
                    </thead>
                    <tbody>
                        

                                <tr @if($gallary->trash==1) class='kashat_alert_danger' @endif id="trashed_{{ $gallary->id }}"  >
                                    <td>{{ $gallary->id }} </td>
                                    <td>{{ $gallary->name_en.'_'.$gallary->name_ar }} </td>
                                    <td><img src="{{asset($gallary->photo)}}" width="75px"></td>
                                    <td><img src="{{asset($gallary->banner)}}" width="75px"></td>
                                    <td>{{ $gallary->bref_en.'_'.$gallary->bref_ar }}</td>
                                    <td>@if($gallary->trash==1) {{"Untrashed"}} @else {{"Trashed"}} @endif </td>
                                   
                                   
                                    
                                
                                </tr>
                       
                    </tbody>
                </table>
                <!--end: Datatable -->
                <br>
                
            </div>
        </div>
    </div>
</div>




