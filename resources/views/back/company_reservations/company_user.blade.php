<div class="col-lg-6">
	<label>
		 Name:
	</label>
	<input type="text" class="form-control m-input" placeholder="Enter full name" name="name" value="{{ $user->name }}">
	<span class="m-form__help">
		Please enter your name
	</span>
</div>
<div class="col-lg-6">
	<label>
		Email:
	</label>
	<input type="email" class="form-control m-input" placeholder="Enter your email" name="email" value="{{ $user->email }}">
	<span class="m-form__help">
		Please enter your email
	</span>
</div>
<div class="col-lg-6">
	<label class="">
		Password:
	</label>
	<input type="number" class="form-control m-input" placeholder="Enter Password" name="password">
	<span class="m-form__help">
		Please enter your new Password
	</span>
</div>
<div class="col-lg-6">
	<label class="">
		Confirmation Password:
	</label>
	<input type="text" class="form-control m-input" placeholder="Enter Confirmation Password" name="confirmation pass" >
	<span class="m-form__help">
		Please enter Confirmation Password
	</span>
</div>
<div class="col-lg-6">
	<label class="">
		User Type
	</label>
	<div class="m-input-icon m-input-icon--right">
		<div class="m-form__group form-group row">
				
				<div class="col-9">
					<div class="m-checkbox-inline">
						<label class="m-checkbox m-checkbox--solid m-checkbox--state-brand">
							<input type="checkbox" name="admin" value="1" @if($user->admin == 1) checked="checked" @endif >
							Admin
							<span></span>
						</label>
						<label class="m-checkbox m-checkbox--solid m-checkbox--state-success">
							<input type="checkbox" name="instructor" value="1" @if($user->instructor == 1) checked="checked" @endif>
							Instructor
							<span></span>
						</label>
						<label class="m-checkbox m-checkbox--solid m-checkbox--state-brand">
							<input type="checkbox" name="employe" value="1" @if($user->employe == 1) checked="checked" @endif >
							Employe
							<span></span>
						</label>

						<label class="m-checkbox m-checkbox--solid m-checkbox--state-success">
							<input type="checkbox" name="candidate" value="1" @if($user->candidate == 1) checked="checked" @endif >
							Candidate
							<span></span>
						</label>
					</div>
					
				</div>
			</div>
	</div>
	<span class="m-form__help">
		Please Select User Type
	</span>
</div>

<div class="col-lg-6">
	<label>
		Company Name_EN:
	</label>
	<input type="text" class="form-control m-input" placeholder="Enter name" name="name_en" value="{{$company->name_en}}">
	<span class="m-form__help">
		Please enter name_en
	</span>
</div>
<div class="col-lg-6">
	<label>
		Company Name_AR:
	</label>
	<input type="text" class="form-control m-input" placeholder="Enter name" name="name_ar" value="{{$company->name_ar}}">
	<span class="m-form__help">
		Please enter name_ar
	</span>
</div>
<div class="col-lg-6">
	<label>
		Company Address:
	</label>
	<input type="text" class="form-control m-input" placeholder="Enter your address" name="address" value="{{$company->address}}">
	<span class="m-form__help">
		Please enter your address
	</span>
</div>

<div class="col-lg-6">
	<label class="">
	Company	Contact Name:
	</label>
	<input type="text" class="form-control m-input" placeholder="Enter fees" name="contact_name" value="{{$company->contact_name}}">
	<span class="m-form__help">
		Please enter your contact name
	</span>
</div>