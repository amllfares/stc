@extends('layout.layout')
@section('content')

<div class="m-content">
    <?php
        if(Session::has('msg')){
            echo App\Helpers\Globals::msg(Session::get('msg'));
        }
    ?>

    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        All Dashbord Activites
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body2">
            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30 hidden">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1 ">
                        <div class="form-group m-form__group row align-items-center">
                            <div class="col-md-4">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="generalSearch">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-search"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                        <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>
                </div>
            </div>
            <!--end: Search Form -->
            <!--begin: Datatable -->
          
            <br><br>
        @if($check_perimation->permission_id == 3)
         <div class="m-section__content">
         	@foreach($activites as $activity)
         	@if($activity->action=="Created")
			<div class="m-alert m-alert--outline alert alert-success alert-dismissible fade show" role="alert">
				
				<strong>
					<i class="fa fa-plus" aria-hidden="true"></i>
				</strong>
				{{$activity->text}}.
			</div>
			@elseif($activity->action=="Updated")
			<div class="m-alert m-alert--outline alert alert-info alert-dismissible fade show" role="alert">
				
				<strong>
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				</strong>
				{{$activity->text}}.
			</div>
			@elseif($activity->action=="Trashed")
			<div class="m-alert m-alert--outline alert alert-warning alert-dismissible fade show" role="alert">
				
				<strong>
					<i class="la la-warning"></i>
				</strong>
				{{$activity->text}}.
			</div>
			@elseif($activity->action=="UNTrashed")
			<div class="m-alert m-alert--outline alert alert-brand alert-dismissible fade show" role="alert">
				
				<strong>
					<i class="fa fa-check" aria-hidden="true"></i>
				</strong>
				{{$activity->text}}.
			</div>
			@else
			<div class="m-alert m-alert--outline alert alert-danger alert-dismissible fade show" role="alert">
				
				<strong>
					<i class="fa fa-times"></i>
				</strong>
				{{$activity->text}}.
			</div>
			@endif
			@endforeach
		</div>
        @endif

        </div>
    </div>
</div>



@endsection