

           
            <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" id="local_data" >
                 <table class="table table-bordered m-table m-table--border-brand m-table--head-bg-brand" style="padding:10%; ">
                    <thead>
                    <tr>
                        <th>ID </th>
                        <th>Employee Name </th>
                        <th>Employee Email </th>
                        <th>Trash</th>

                       
                    </tr>
                    </thead>
                    <tbody>
                        

                                <tr @if($employee->trash==1) class='kashat_alert_danger' @endif id="trashed_{{ $employee->id }}">
                                    <td>{{ $employee->id }} </td>
                                    <td>{{ $employee->user->name }} </td>
                                    <td>{{ $employee->salary }}</td>
                                    <td>@if($employee->trash==1) {{"Untrashed"}} @else {{"Trashed"}} @endif </td>
                                
                                   
                                </tr>
                       
                    </tbody>
                </table>
                <!--end: Datatable -->
                <br>
                
            </div>
        </div>
    </div>
</div>




