@extends('layout.layout')
@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">

					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">
									Edit  question
								</h3>
								<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
									<li class="m-nav__item m-nav__item--home">
										<a href="#" class="m-nav__link m-nav__link--icon">
											<i class="m-nav__link-icon la la-home"></i>
										</a>
									</li>
									<li class="m-nav__separator">
										-
									</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												All_question
											</span>
										</a>
									</li>
									<li class="m-nav__separator">
										-
									</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												Edit question
											</span>
										</a>
									</li>
									
									
								</ul>
							</div>
							
							<div>
								
								<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover" aria-expanded="true">
									<a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
										<i class="la la-plus m--hide"></i>
										<i class="la la-ellipsis-h"></i>
									</a>
									<div class="m-dropdown__wrapper">
										<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
										<div class="m-dropdown__inner">
											<div class="m-dropdown__body">
												<div class="m-dropdown__content">
													<ul class="m-nav">
														<li class="m-nav__section m-nav__section--first m--hide">
															<span class="m-nav__section-text">
																Quick Actions
															</span>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-share"></i>
																<span class="m-nav__link-text">
																	Activity
																</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-chat-1"></i>
																<span class="m-nav__link-text">
																	Messages
																</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-info"></i>
																<span class="m-nav__link-text">
																	FAQ
																</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-lifebuoy"></i>
																<span class="m-nav__link-text">
																	Support
																</span>
															</a>
														</li>
														<li class="m-nav__separator m-nav__separator--fit"></li>
														<li class="m-nav__item">
															<a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">
																Submit
															</a>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END: Subheader -->
					<div class="m-content">
						<div class="row">
							<div class="col-lg-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
												<h3 class="m-portlet__head-text">
													edit  question
												</h3>
											</div>
										</div>
									</div>
									<!--begin::Form-->
									 @if(Session::has('edit-question'))
					                <div class="alert alert-brand m-alert m-alert--icon m-alert--air m-alert--square m--margin-bottom-30" role="alert">
					                    <div class="m-alert__icon">
					                        <i class="flaticon-exclamation-1"></i>
					                    </div>
					                    <div class="m-alert__text">
					                        {{ Session::get('edit-question') }}
					                    </div>
					                </div>
					          		  @endif
									<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="POST" action="{{route('questions.update', ['id' => $question->ID])}}" enctype="multipart/form-data">
                       				 {{ csrf_field() }}
                       				 {{ method_field('PATCH') }} 
										<div class="m-portlet__body">
											<div class="form-group m-form__group row">

												<div class="col-lg-6">
												 <div class="form-group m-form__group">
					                                <label for="exampleInputEmail1">
					                                    Question
					                                </label>
					                               
					                                <textarea name="Question" id="editor" required>{{$question->Question}}</textarea>
					                              </div>
					                            </div>

					                           
												
												<div class="col-lg-6">
												 <div class="form-group m-form__group">
					                                <label for="exampleInputEmail1">
					                                    Text_Answer
					                                </label>
					                                <textarea name="Text_Answer" id="editor1" required>{{$question->Text_Answer}}</textarea>
					                             </div>
					                            </div>
					                             <div class="col-lg-6">
													<label class="">
														Question Type
													</label>
													<div class="m-input-icon m-input-icon--right">
														<select name="Type_ID" id="parent_category" class="form-control m-input  m-input--square" required>
															<option value="0">choose question type</option>
						                                    @foreach ($types as $type )
						                                        <option value="{{ $type->ID }}" @if ($type->ID==$question->Type_ID)
					                                               selected 
					                                           @endif>{{ $type->Type }}</option>
						                                    @endforeach
						                                </select>
													</div>
													<span class="m-form__help">
														Please choose sub_page
													</span>
												
												</div>

												<div class="col-lg-6">
													<label>
														Degree:
													</label>
													<input type="text" class="form-control m-input" placeholder="Enter Type" name="Degree" value="{{$question->Degree}}" required>
													<span class="m-form__help">
														Please enter Degree
													</span>
												</div>

												<div class="col-lg-6">
													<label class="">
														Type
													</label>
													<div class="m-input-icon m-input-icon--right">
														<select name="Type" id="parent_category" class="form-control m-input  m-input--square" required>
															<option value="">choose type</option>
															<option value="1" @if ($question->Type=='1') selected @endif>driller</option>   
															<option value="2" @if ($question->Type=='2') selected @endif>supervisor </option>
															<option value="3" @if ($question->Type=='3') selected @endif>introductory</option>
						                                    
						                                 
						                                </select>
													</div>
													<span class="m-form__help">
														Please choose Type
													</span>
												
												</div>
												

											</div>
											
										</div>
										<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
											<div class="m-form__actions m-form__actions--solid">
												<div class="row">
													<div class="col-lg-6">
														<button type="Submit" class="btn btn-brand">
															Save
														</button>
														<a type="button" class="btn btn-default" href="{{route('questions.index')}}" style="color: #716aca;">
															Cancel
														</a>
													</div>
													<div class="col-lg-6 m--align-right">
														<button type="reset" class="btn btn-danger">
															Delete
														</button>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group m-form__group text-center" >
										<div class="fb-comments" data-href="{{Request::url()}}" data-numposts="5"></div>
								       <div id="fb-root"></div>
								       </div>
									</form>
									<!--end::Form-->
								</div>
								<!--end::Portlet-->
							</div>
						</div>
					</div>
				</div>



@endsection