@extends('layout.layout')
@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">

					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">
									Edit branch
								</h3>
								<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
									<li class="m-nav__item m-nav__item--home">
										<a href="#" class="m-nav__link m-nav__link--icon">
											<i class="m-nav__link-icon la la-home"></i>
										</a>
									</li>
									<li class="m-nav__separator">
										-
									</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												All_branches
											</span>
										</a>
									</li>
									<li class="m-nav__separator">
										-
									</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												Edit branch
											</span>
										</a>
									</li>
									
									
								</ul>
							</div>
							
							<div>
								
								<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover" aria-expanded="true">
									<a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
										<i class="la la-plus m--hide"></i>
										<i class="la la-ellipsis-h"></i>
									</a>
									<div class="m-dropdown__wrapper">
										<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
										<div class="m-dropdown__inner">
											<div class="m-dropdown__body">
												<div class="m-dropdown__content">
													<ul class="m-nav">
														<li class="m-nav__section m-nav__section--first m--hide">
															<span class="m-nav__section-text">
																Quick Actions
															</span>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-share"></i>
																<span class="m-nav__link-text">
																	Activity
																</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-chat-1"></i>
																<span class="m-nav__link-text">
																	Messages
																</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-info"></i>
																<span class="m-nav__link-text">
																	FAQ
																</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-lifebuoy"></i>
																<span class="m-nav__link-text">
																	Support
																</span>
															</a>
														</li>
														<li class="m-nav__separator m-nav__separator--fit"></li>
														<li class="m-nav__item">
															<a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">
																Submit
															</a>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END: Subheader -->
					<div class="m-content">
						<div class="row">
							<div class="col-lg-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
												<h3 class="m-portlet__head-text">
													Edit branch
												</h3>
											</div>
										</div>
									</div>
									<!--begin::Form-->
									 @if(Session::has('add-branche'))
					                <div class="alert alert-brand m-alert m-alert--icon m-alert--air m-alert--square m--margin-bottom-30" role="alert">
					                    <div class="m-alert__icon">
					                        <i class="flaticon-exclamation-1"></i>
					                    </div>
					                    <div class="m-alert__text">
					                        {{ Session::get('add-branche') }}
					                    </div>
					                </div>
					          		  @endif
									<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="POST" action="{{route('branches.update', ['id' => $branch->id])}}" enctype="multipart/form-data">
                       				 {{ csrf_field() }} 
                       				 {{ method_field('PATCH') }} 
										<div class="m-portlet__body">
											<div class="form-group m-form__group row">
												<div class="col-lg-6">
													<label>
													  Name_EN:
													</label>
													<input type="text" class="form-control m-input" placeholder="Enter tilte_en" name="name_en" required="" value="{{$branch->name_en}}">
													<span class="m-form__help">
														Please enter your name_en
													</span>
												</div>

												<div class="col-lg-6">
													<label>
													  Name_AR:
													</label>
													<input type="text" class="form-control m-input" placeholder="Enter name_ar" name="name_ar" required="" value="{{$branch->name_ar}}">
													<span class="m-form__help">
														Please enter your name_ar
													</span>
												</div>

												<div class="col-lg-6">
													<label>
													  Contact_email:
													</label>
													<input type="text" class="form-control m-input" placeholder="Enter contact_email" name="contact_email" required="" value="{{$branch->contact_email}}">
													<span class="m-form__help">
														Please enter your contact_email
													</span>
												</div>

												<div class="col-lg-6">
													<label>
													  Contact_phone:
													</label>
													<input type="text" class="form-control m-input" placeholder="Enter contact_phone" name="contact_phone" required="" value="{{$branch->contact_phone}}">
													<span class="m-form__help">
														Please enter your contact_phone
													</span>
												</div>

												<div class="col-lg-6">
													<label>
													  Contact_mobile:
													</label>
													<input type="text" class="form-control m-input" placeholder="Enter contact_mobile" name="contact_mobile" required="" value="{{$branch->contact_mobile}}">
													<span class="m-form__help">
														Please enter your contact_mobile
													</span>
												</div>

										
											</div>
											<div class="form-group m-form__group row">
												<div class="col-lg-6">
													<label>
													  Address AR:
													</label>
													<input type="text" class="form-control m-input" placeholder="Enter address" name="address_ar" id="searchTextField" required="" value="{{$branch->address_ar}}">

													<span class="m-form__help">
														Please enter your address
													</span>
													 <input name="latitude" class="MapLat"  type="hidden" placeholder="Latitude" style="width: 161px;"  >
                          						    <input name="longitude" class="MapLon"  type="hidden" placeholder="Longitude" style="width: 161px;" > 
                          						      <div id="map_canvas" style="height: 350px;width: 500px;margin: 0.6em;"></div>
												</div>

												<div class="col-lg-6">
													<label>
													  Address EN:
													</label>
													<input type="text" class="form-control m-input" placeholder="Enter address" name="address_en" id="searchTextField" required="" value="{{$branch->address_en}}">

													<span class="m-form__help">
														Please enter your address
													</span>
													 
                          						      <div id="map_canvas" style="height: 350px;width: 500px;margin: 0.6em;"></div>
												</div>
											</div>

											
										</div>
										<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
											<div class="m-form__actions m-form__actions--solid">
												<div class="row">
													<div class="col-lg-6">
														<button type="Submit" class="btn btn-brand">
															Save
														</button>
														<a type="button" class="btn btn-default" href="{{route('branches.index')}}" style="color: #716aca;">
															Cancel
														</a>
													</div>
													<div class="col-lg-6 m--align-right">
														<button type="reset" class="btn btn-danger">
															Reset	
														</button>
													</div>
												</div>
											</div>
										</div>
									</form>
									<!--end::Form-->
								</div>
								<!--end::Portlet-->
							</div>
						</div>
					</div>
				</div>



@endsection