

<div class="m-grid__item m-grid__item--fluid m-wrapper">

				
					<!-- END: Subheader -->
					<div class="m-content">
						<div class="row">
							<div class="col-lg-12">
								
									<!--begin::Form-->
									 @if(Session::has('add-course'))
					                <div class="alert alert-brand m-alert m-alert--icon m-alert--air m-alert--square m--margin-bottom-30" role="alert">
					                    <div class="m-alert__icon">
					                        <i class="flaticon-exclamation-1"></i>
					                    </div>
					                    <div class="m-alert__text">
					                        {{ Session::get('add-course') }}
					                    </div>
					                </div>
					          		  @endif
									<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="POST" action="{{ url('admin/course/subscrib_all_save') }}" enctype="multipart/form-data">
                       				 {{ csrf_field() }} 
										
										<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
											<div class="m-form__actions m-form__actions--solid">
												<div class="row">
											    
													<input type="hidden" class="form-control m-input" placeholder="Enter subject of email" name="id" value="{{ $id }}"  >
													
												
												<div class="col-lg-12">
													<label>
														Subject:
													</label>
													<input type="text" class="form-control m-input" placeholder="Enter subject of email"  name="subject">
													
												</div>

												<div class="col-lg-12">
													<label>
													 Body:
													</label>
													<textarea  class="form-control m-input" placeholder="write content of email" name="body" rows="5" ></textarea>
													<span class="m-form__help">
														
													</span>
												</div> 
													<div class="col-lg-6">
														<button type="Submit" class="btn btn-brand">
															Send
														</button>
													</div>
													
												</div>
											</div>
										</div>
									</form>
									<!--end::Form-->
								</div>
								<!--end::Portlet-->
							</div>
						</div>
					</div>
				</div>



