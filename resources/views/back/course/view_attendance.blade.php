@extends('layout.layout')
@section('content')

<div class="m-content">
    <?php
        if(Session::has('msg')){
            echo App\Helpers\Globals::msg(Session::get('msg'));
        }
    ?>

    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        All Courses
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body2">
            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30 hidden">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1 ">
                        <div class="form-group m-form__group row align-items-center">
                            <div class="col-md-4">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="generalSearch">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-search"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                        <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>
                </div>
            </div>
            <!--end: Search Form -->
            <!--begin: Datatable -->
            @if(session()->has('message'))
                <div class="alert alert-danger  ">
                    {{ session()->get('message') }}
                </div>
            @endif

               @if(Session::has('trash'))
                <div class="alert alert-brand m-alert m-alert--icon m-alert--air m-alert--square m--margin-bottom-30" role="alert">
                    <div class="m-alert__icon">
                        <i class="flaticon-exclamation-1"></i>
                    </div>
                    <div class="m-alert__text">
                        {{ Session::get('trash') }}
                    </div>
                </div>
                @endif

                 @if(Session::has('delete'))
                <div class="alert alert-danger m-alert m-alert--icon m-alert--air m-alert--square m--margin-bottom-30" role="alert">
                    <div class="m-alert__icon">
                        <i class="flaticon-exclamation-1"></i>
                    </div>
                    <div class="m-alert__text">
                        {{ Session::get('delete') }}
                    </div>
                </div>
                @endif
            <a class="btn btn-brand" href="{{ url('/admin/course/schedule_view/'.$course_id) }}" >
                <span class="m-nav__link-text">
                   Back To Schedule
                </span>
            </a>
            <br><br>
            <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" id="local_data" >
                <table class="table table-bordered m-table m-table--border-brand m-table--head-bg-brand" style="padding:10%; ">
                    <thead>
                    <tr>
                        <th>Candidate </th>
                        <th>Attendances </th>
                 
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($schedules as $schedule)
                                <tr>
                                    <td>{{ $schedule->user->name }} </td>
                                    <td>
                                        @if( in_array($schedule->user->id, $cand))
                                        <div class="m-radio-inline">
                                        <label class="m-radio m-radio--solid m-radio--state-brand">
                                            <input type="radio" checked>
                                            Yes
                                            <span></span>
                                        </label>
                                        <label class="m-radio m-radio--solid m-radio--state-success">
                                            <input type="radio">
                                            No
                                            <span></span>
                                        </label>
                                        
                                        </div>
                                        @else
                                         <div class="m-radio-inline">
                                        <label class="m-radio m-radio--solid m-radio--state-brand">
                                            <input type="radio" >
                                            Yes
                                            <span></span>
                                        </label>
                                        <label class="m-radio m-radio--solid m-radio--state-success">
                                            <input type="radio" checked>
                                            No
                                            <span></span>
                                        </label>
                                        
                                        </div>
                                        @endif
                                    </td>
               
                                </tr>

                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable -->
                <br>
                 <div class="text-center">
                {{$schedules->links()}}
                </div>
            </div>
        </div>
    </div>
</div>


@endsection