@extends('layout.layout')
@section('content')


							

<div class="m-content">
    <?php
        if(Session::has('msg')){
            echo App\Helpers\Globals::msg(Session::get('msg'));
        }
    ?>

    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        All Courses
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body2">
            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30 hidden">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1 ">
                        <div class="form-group m-form__group row align-items-center">
                            <div class="col-md-4">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="generalSearch">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-search"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                        <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>
                </div>
            </div>
            <!--end: Search Form -->
            <!--begin: Datatable -->
            @if(session()->has('message'))
                <div class="alert alert-danger  ">
                    {{ session()->get('message') }}
                </div>
            @endif

               @if(Session::has('trash'))
                <div class="alert alert-brand m-alert m-alert--icon m-alert--air m-alert--square m--margin-bottom-30" role="alert">
                    <div class="m-alert__icon">
                        <i class="flaticon-exclamation-1"></i>
                    </div>
                    <div class="m-alert__text">
                        {{ Session::get('trash') }}
                    </div>
                </div>
                @endif

                 @if(Session::has('delete'))
                <div class="alert alert-danger m-alert m-alert--icon m-alert--air m-alert--square m--margin-bottom-30" role="alert">
                    <div class="m-alert__icon">
                        <i class="flaticon-exclamation-1"></i>
                    </div>
                    <div class="m-alert__text">
                        {{ Session::get('delete') }}
                    </div>
                </div>
                @endif
            <a class="btn btn-brand" href="{{ route('course.create') }}" >
                <span class="m-nav__link-text">
                    Add New Course
                </span>
            </a>
            <br><br>
            <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" id="local_data" >
                <table class="table table-bordered m-table m-table--border-brand m-table--head-bg-brand" style="padding:10%; ">
                    <thead>
                    <tr>
                        <th>ID </th>
                        <th>Course subject </th>
                       
                        <th>Control Course </th>
                     
                      
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($courses as $course)
                                <tr @if($course->trash==1) class='kashat_alert_danger' @endif id="trashed_{{ $course->id }}">
                                    <td>{{ $course->id }} </td>
                                    <td>{{ $course->subject->name_en.'_'.$course->subject->name_ar }}</td>
                                  
                                    
                                   
                                    <td>
                                        @if($check_perimation->permission_id != 1)
                                        <a class="btn btn-brand" href="{{ route('course.edit', ['id' => $course->id]) }}" title="Edit">
                                            <i class="m-nav__link-icon fa fa-pencil-square-o"></i>
                                        </a>
                                        @endif
                                        @if($check_perimation->permission_id == 3)
                                        <a class="btn btn-danger delete" href="javascript:void(0);"  data-id="<?php echo $course->id; ?>" data-page="courses" base_url="{{ url('/') }}"  route="course" title="Delete">
                                            <i class="m-nav__link-icon fa fa-trash-o"></i>
                                           
                                        </a>
                                         @endif   

                                        <a class="btn m-btn m-btn--gradient-from-success m-btn--gradient-to-accent show-page" href="javascript:void(0);"  data-id="<?php echo $course->id; ?>" data-page="courses" base_url="{{ url('/') }}" title="Details">
                                            <i class="m-nav__link-icon fa fa-eye"></i>
                                           
                                        </a>
                                        @if($course->trash==0)
                                         <a class="btn btn-warning" href="{{ url('admin/course/trash/'.$course->id.'/'.$course->trash) }}" title=" Trash">
                                            <i class="m-nav__link-icon fa fa-times" style="color: white;"></i>
                                        </a>
                                        @else
                                         <a class="btn btn-success" href="{{ url('admin/course/trash/'.$course->id.'/'.$course->trash) }}" title="Untrash">
                                            <i class="m-nav__link-icon fa fa-check"></i>
                                        </a>
                                        @endif

                                         <a class="btn btn-primary" href="{{ url('admin/course/subscribe/'.$course->id) }}" title="Subscriber" >
                                            <i class="m-nav__link-icon fa fa-envelope-o"></i>
                                        </a>
                                   
                                        <div class="dropdown" style="display: inline-block;color: white;">
                                            <a class="btn btn-brand"  id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-paper-plane" aria-hidden="true" title="More Action"></i>
                                                more action 
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

                                                
                                                 <a class="dropdown-item" href="{{ url('admin/course/schedule_view/'.$course->id) }}" >
                                                    <i class="m-nav__link-icon fa fa-eye"></i> view schedule
                                                </a>
                                              
                                                 <a class="dropdown-item" href="{{ url('admin/course/schedule_add/'.$course->id) }}" >
                                                    <i class="m-nav__link-icon fa fa-plus"></i> add schedule
                                                </a>
                                                 <a class="dropdown-item" href="{{ url('admin/subject_content/'.$course->id) }}" >
                                                    <i class="m-nav__link-icon fa fa-eye"></i> view subject content
                                                </a>

                                                <a class="dropdown-item" href="{{ url('admin/subject_content/add/'.$course->id) }}" >
                                                    <i class="m-nav__link-icon fa fa-plus"></i> add subject content
                                                </a>

                                                 <a class="dropdown-item" href="{{ url('admin/matirial/'.$course->id) }}" >
                                                    <i class="m-nav__link-icon fa fa-eye"></i> view matiral
                                                </a>

                                                <a class="dropdown-item" href="{{ url('admin/matirial/add/'.$course->id) }}" >
                                                    <i class="m-nav__link-icon fa fa-plus"></i> add matiral
                                                </a>
                                                
                                               
                                            </div>
                                        </div>
                                    
                                    </td>
                                   
                                </tr>
                                
                                
                                <div class="modal fade" id="m_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">
                                                Confirmation Message
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">
                                                    &times;
                                                </span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                           
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal" style="background-color:black;">
                                                Close
                                            </button>
                                            <form action="" id="form" method="post" enctype="multipart/form-data" >
                                                         {{ csrf_field() }}
                                                         {{ method_field('DELETE') }}
                                            <input type="submit" class="btn btn-brand" value="ok">
                                                       
                                                       
                                            </form>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable -->
                <br>
                 <div class="text-center">
                {{$courses->links()}}
            </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="m_modal_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lgg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">
                                            More Details
                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">
                                                &times;
                                            </span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                       
                                    </div>
                                    <div class="modal-footer">
                                       <!--  <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                            Close
                                        </button> -->
                                        <button type="button" class="btn btn-brand" data-dismiss="modal">
                                            Close
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

@endsection