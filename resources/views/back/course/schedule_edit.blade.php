@extends('layout.layout')
@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">

					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">
									Edit  Course
								</h3>
								<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
									<li class="m-nav__item m-nav__item--home">
										<a href="#" class="m-nav__link m-nav__link--icon">
											<i class="m-nav__link-icon la la-home"></i>
										</a>
									</li>
									<li class="m-nav__separator">
										-
									</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												All_Course
											</span>
										</a>
									</li>
									<li class="m-nav__separator">
										-
									</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												edit schedule
											</span>
										</a>
									</li>
									
									
								</ul>
							</div>
							
							<div>
								
								<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover" aria-expanded="true">
									<a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
										<i class="la la-plus m--hide"></i>
										<i class="la la-ellipsis-h"></i>
									</a>
									<div class="m-dropdown__wrapper">
										<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
										<div class="m-dropdown__inner">
											<div class="m-dropdown__body">
												<div class="m-dropdown__content">
													<ul class="m-nav">
														<li class="m-nav__section m-nav__section--first m--hide">
															<span class="m-nav__section-text">
																Quick Actions
															</span>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-share"></i>
																<span class="m-nav__link-text">
																	Activity
																</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-chat-1"></i>
																<span class="m-nav__link-text">
																	Messages
																</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-info"></i>
																<span class="m-nav__link-text">
																	FAQ
																</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-lifebuoy"></i>
																<span class="m-nav__link-text">
																	Support
																</span>
															</a>
														</li>
														<li class="m-nav__separator m-nav__separator--fit"></li>
														<li class="m-nav__item">
															<a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">
																Submit
															</a>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END: Subheader -->
					<div class="m-content">
						<div class="row">
							<div class="col-lg-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
												<h3 class="m-portlet__head-text">
													Edit new schedule 
												</h3><br>

											</div>
										</div>
									</div>
									
									<!--begin::Form-->

									 @if(Session::has('edit-course'))
					                <div class="alert alert-brand m-alert m-alert--icon m-alert--air m-alert--square m--margin-bottom-30" role="alert">
					                    <div class="m-alert__icon">
					                        <i class="flaticon-exclamation-1"></i>
					                    </div>
					                    <div class="m-alert__text">
					                        {{ Session::get('edit-course') }}
					                    </div>
					                </div>
					          		  @endif
									<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="POST" action="{{ url('admin/course/schedule_update/'.Request::segment(4)) }}" enctype="multipart/form-data">
                       				 {{ csrf_field() }} 
                       				 {{ method_field('PATCH') }} 
										<div class="m-portlet__body">
											<div class="form-group m-form__group row">
												 <div class="col-lg-6">
													<label>
														From:
													</label>
													<input type="date" class="form-control m-input" placeholder="Enter date" name="from" value="{{$schedule->from}}" required="">
													<span class="m-form__help">
														Please enter date from
													</span>
												</div>
												<div class="col-lg-6">
													<label>
														To:
													</label>
													<input type="date" class="form-control m-input" placeholder="Enter date to" name="to" value="{{$schedule->to}}" required="">
													<span class="m-form__help">
														Please enter date to
													</span>
												</div>
												
												<div class="col-lg-6">
													<label class="">
														Instructor
													</label>
													<div class="m-input-icon m-input-icon--right">
														<select name="instruct_id" id="parent_category" class="form-control m-input  m-input--square" required="">
						                                    @foreach ($instructor as $instruct )
						                                        <option value="{{ $instruct->id }}" @if ($instruct->id==$schedule->instructor_id)
					                                               selected 
					                                           @endif>{{ $instruct->name_en.'_'.$instruct->name_ar }}</option>
						                                    @endforeach
						                                </select>
													</div>
													<span class="m-form__help">
														Please choose instructor
													</span>
												
												</div>

												<div class="col-lg-6">
													<label class="">
														Branche:
													</label>
													<div class="m-input-icon m-input-icon--right">
														<select name="branch_id" id="parent_category" class="form-control m-input  m-input--square" required="">
						                                    @foreach ($branches as $branche )
						                                        <option value="{{ $branche->id }}"@if ($branche->id==$schedule->branch_id)
					                                               selected 
					                                           @endif>{{ $branche->name_en.'_'.$branche->name_ar }}</option>
						                                    @endforeach
						                                </select>
													</div>
													<span class="m-form__help">
														Please enter branch
													</span>
												</div>


												<div class="col-lg-6">
													<label class="" >
														Exam:
													</label>
													<div class="m-input-icon m-input-icon--right">
														<select name="exam_id" id="parent_category" class="form-control m-input  m-input--square" required="">
						                                    @foreach ($exams as $exam )
						                                        <option value="{{ $exam->id }}"@if ($exam->id==$schedule->branch_id)
					                                               selected 
					                                           @endif>{{ $exam->name_en.'_'.$exam->name_ar }}</option>
						                                    @endforeach
						                                </select>
													</div>
													<span class="m-form__help">
														Please enter exam
													</span>
												</div>

												<div class="col-lg-6">
													<label class="">
														Private?
													</label>
													<div class="m-input-icon m-input-icon--right">
														<div class="m-radio-inline">
															<label class="m-radio m-radio--solid m-radio--state-brand">
																<input type="radio" name="private" value="1" @if($schedule->private=="1") checked="checked" @endif>
																Yes
																<span></span>
															</label>
															<label class="m-radio m-radio--solid m-radio--state-success">
																<input type="radio" name="private" value="0" @if($schedule->private=="0") checked="checked" @endif>
																No
																<span></span>
															</label>
															
														</div>
														<span class="m-form__help">
															Please enter private?
														</span>	
													</div>
													
											</div>
						
												
										
												<div class="col-lg-6" id="all_div">
													<label class="">
														Dates: <a href="javascript:void(0);" onclick="myFunction()" id="date" class="btn btn-brand btn-sm">
															+
														</a>
													</label>
													@foreach($dates as $dat)	
													<input type="date" class="form-control m-input"  name="dates[]" class="append" value="{{$dat->date}}" >
													
													<span class="m-form__help">
														Please enter schedule dates
													</span>
													@endforeach
												</div>

												<div class="col-lg-6">
													<label class="">
														Schedule_AR:
													</label>
													<input type="text" class="form-control m-input" placeholder="Enter schedule" name="schedule_ar" value="{{$schedule->schedule_ar}}">
													<span class="m-form__help">
														Please enter schedule_ar
													</span>
												</div>

												<div class="col-lg-6">
													<label class="">
														Schedule_EN:
													</label>
													<input type="text" class="form-control m-input" placeholder="Enter schedule" name="schedule_en" value="{{$schedule->schedule_en}}">
													<span class="m-form__help">
														Please enter schedule_en
													</span>
												</div>
												
												

												 
											</div>
											<!-- onchange="select_category(this.options[this.selectedIndex].value)" -->
										</div>
										<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
											<div class="m-form__actions m-form__actions--solid">
												<div class="row">
													<div class="col-lg-6">
														<button type="Submit" class="btn btn-brand">
															Save
														</button>
														
													</div>
													<div class="col-lg-6 m--align-right">
														<button type="reset" class="btn btn-danger">
															Delete
														</button>
													</div>
												</div>
											</div>
										</div>
									</form>
									<!--end::Form-->
								</div>
								<!--end::Portlet-->
							</div>
						</div>
					</div>
				</div>



@endsection