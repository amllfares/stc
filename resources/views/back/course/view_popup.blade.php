

           
            <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" id="local_data" >
                 <table class="table table-bordered m-table m-table--border-brand m-table--head-bg-brand" style="padding:10%; ">
                    <thead>
                    <tr>
                        <th> ID </th>
                        <th> Min_candidates </th>
                        <th> Max_candidates </th>
                        <th>Price</th>
                        <th>Offer</th>
                        <th>Number Of Hours</th>
                        <th>Number Of Reservations</th>
                        <th> Subject</th>
                        <th> Dates_of </th>
                        <th>Trash</th>

                       
                    </tr>
                    </thead>
                    <tbody>
                        

                                <tr @if($course->trash==1) class='kashat_alert_danger' @endif id="trashed_{{ $course->id }}"  >
                                    <td>{{ $course->id }} </td>
                                   
                                    <td>{{ $course->min_candidates }}</td>
                                    <td>{{ $course->max_candidates }}</td>
                                    <td>{{$course->price}} EGP</td>
                                    <td>{{$course->offer}} %</td>
                                    <td>{{$course->number_hours}}</td>
                                    <td>{{$reservations}}</td>
                                    <td>{{ $course->subject->name_en.'_'.$course->subject->name_ar }}</td>
                                    <td>@foreach($dates as $date) {{ $date->date }}.<br> @endforeach</td>
                                    <td>@if($course->trash==1) {{"Untrashed"}} @else {{"Trashed"}} @endif </td>
                                   
                                   
                                    
                                
                                </tr>
                       
                    </tbody>
                </table>
                <!--end: Datatable -->
                <br>
                
            </div>
        </div>
    </div>
</div>




