@extends('layout.layout')
@section('content')
<div class="row" style="padding:30px;">
<div class="m-portlet">
							<div class="m-portlet__body  m-portlet__body--no-padding">
								<div class="row m-row--no-padding m-row--col-separator-xl">
									<div class="col-xl-4">
										<!--begin:: Widgets/Stats2-1 -->
										<div class="m-widget1">
											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div class="col">
														<h3 class="m-widget1__title">
															Users
														</h3>
														<span class="m-widget1__desc">
															users number
														</span>
													</div>
													<?php  $count_user=App\User::count();?>
													<div class="col m--align-right">
														<span class="m-widget1__number m--font-brand">
															+{{$count_user}}
														</span>
													</div>
												</div>
											</div>
											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div class="col">
														<h3 class="m-widget1__title">
															Pages
														</h3>
														<span class="m-widget1__desc">
															Pages number
														</span>
													</div>
													<?php  $count_page=App\Page::count();?>
													<div class="col m--align-right">
														<span class="m-widget1__number m--font-danger">
															+{{$count_page}}
														</span>
													</div>
												</div>
											</div>
											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div class="col">
														<h3 class="m-widget1__title">
															Subject
														</h3>
														<span class="m-widget1__desc">
															Subject number
														</span>
													</div>
													<?php  $count_sub=App\Subject::count();?>
													<div class="col m--align-right">
														<span class="m-widget1__number m--font-success">
															+{{$count_sub}}
														</span>
													</div>
												</div>
											</div>
										</div>
										<!--end:: Widgets/Stats2-1 -->
									</div>
									<div class="col-xl-4">
										<!--begin:: Widgets/Daily Sales-->
										<div class="m-widget14">
											<div class="m-widget14__header m--margin-bottom-30">
												<h3 class="m-widget14__title">
													STC Activities
												</h3>
												<span class="m-widget14__desc">
													Check out each collumn for more details
												</span>
											</div>
											<div class="m-widget14__chart" style="height:120px;">
												<canvas  id="m_chart_daily_sales"></canvas>
											</div>
										</div>
										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-4">
										<!--begin:: Widgets/Profit Share-->
										<div class="m-widget14">
											<div class="m-widget14__header">
												<h3 class="m-widget14__title">
													Dashbord statistics
												</h3>
												<span class="m-widget14__desc">
													 sections statistics
												</span>
											</div>
											<div class="row  align-items-center">
												<div class="col">
													<div id="m_chart_profit_share" class="m-widget14__chart" style="height: 160px">
														<div class="m-widget14__stat">
															100
														</div>
													</div>
												</div>
												<div class="col">
													<div class="m-widget14__legends">
														<div class="m-widget14__legend">
															<span class="m-widget14__legend-bullet m--bg-accent"></span>
															<span class="m-widget14__legend-text">
																users
															</span>
														</div>
														<div class="m-widget14__legend">
															<span class="m-widget14__legend-bullet m--bg-warning"></span>
															<span class="m-widget14__legend-text">
																pages
															</span>
														</div>
														<div class="m-widget14__legend">
															<span class="m-widget14__legend-bullet m--bg-brand"></span>
															<span class="m-widget14__legend-text">
																subject
															</span>
														</div>
													</div>
												</div>
											</div>
										</div>

										<!--end:: Widgets/Profit Share-->
									</div>
								</div>
							</div>
						</div>
						</div>


@endsection