<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="POST" action="{{url('admin/general/change_status_save')}}" enctype="multipart/form-data">
	 {{ csrf_field() }} 
	 	
				<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row">
							<input type="hidden" name="id" value="{{$company_reservation->id}}">
							<label>
							     Change Status:
							</label>
	          		    	<div class="col-lg-6">
								
								<div class="m-input-icon m-input-icon--right">
									<select name="status" id="parent_category" class="form-control m-input  m-input--square" required>
										<option>choose status </option>
									    <option value="0">Pending</option>
									    <option value="1">Lead</option>
									    <option value="2">Finical</option>
									    <option value="3">Paid</option>
									   
									</select>
									</div>
							</div>

							<div class="col-lg-6">
								<button type="Submit" class="btn btn-brand">
									Save
								</button>
								
							</div>
							
						</div>
					</div>
				</div>
	

</form>