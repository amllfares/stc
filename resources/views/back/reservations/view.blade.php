@extends('layout.layout')
@section('content')

<div class="m-content">
    <?php
        if(Session::has('msg')){
            echo App\Helpers\Globals::msg(Session::get('msg'));
        }
    ?>

    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        All Reservation
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body2">
            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30 hidden">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1 ">
                        <div class="form-group m-form__group row align-items-center">
                            <div class="col-md-4">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="generalSearch">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-search"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                        <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>
                </div>
            </div>
            <!--end: Search Form -->
            <!--begin: Datatable -->
      
           <!--  <a class="btn btn-brand" href="{{ route('reservations.create') }}" >
                <span class="m-nav__link-text">
                    Add New Reservation
                </span>
            </a> -->
            <br><br>
            <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" id="local_data" >
                <table class="table table-bordered m-table m-table--border-brand m-table--head-bg-brand" style="padding:10%; ">
                    <thead>
                    <tr>
                        <th>ID </th>
                        <th>User Name </th>
                        <th>course </th>
                        <th>Schedule </th>
                        <th>Hotel </th>
                        <th>Resturant </th>
                        <th>Status </th>
                        <th style="width:315px;">Control Reservation </th>
                      
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($reservations as $reservation)
                                <tr @if($reservation->trash==1) class='kashat_alert_danger' @endif id="trashed_{{ $reservation->id }}">
                                    <td>{{ $reservation->id }} </td>
                                    <td>{{ $reservation->user->name }} </td>
                                    <td>{{ $reservation->course_schedule->from.' To  '.$reservation->course_schedule->to }} </td>
                                     <td>{{ $reservation->course_schedule->schedule_en.'_'.$reservation->course_schedule->schedule_ar }}</td>
                                    <td>{{ $reservation->hotel->name_en }} </td>
                                    <td>{{ $reservation->resturant->name_en }} </td>
                                    <td>@if($reservation->status=='0')
                                          Pendig
                                        @elseif($reservation->status=='1')
                                         Lead
                                        @elseif($reservation->status=='2')
                                          Transfer Accounting
                                        @else
                                         Done
                                        @endif
                                    </td>
                                    <td>
                                        @if($check_perimation->permission_id != 1)
                                        <a class="btn btn-brand" href="{{ route('reservations.edit', ['id' => $reservation->id]) }}" title="Edit" >
                                            <i class="m-nav__link-icon fa fa-pencil-square-o"></i>
                                        </a>
                                        @endif
                                        @if($check_perimation->permission_id == 3)
                                        <a class="btn btn-danger delete" href="javascript:void(0);"  data-id="<?php echo $reservation->id; ?>" data-page="reservations" base_url="{{ url('/') }}"  route="reservations" title="Delete">
                                            <i class="m-nav__link-icon fa fa-trash-o"></i>
                                           
                                        </a>
                                        @endif  
                                        @if($reservation->status == '0')
                                        <a class="btn m-btn m-btn--gradient-from-success m-btn--gradient-to-accent " href="{{ url('admin/reservations/status/'.$reservation->id.'/1') }}"  data-id="<?php echo $reservation->id; ?>" base_url="{{ url('/') }}" title="Details">
                                            <i class="m-nav__link-icon fa fa-money"> Lead</i>
                                           
                                        </a>
                                        @elseif($reservation->status == '1')
                                         <a class="btn btn-warning " href="{{ url('admin/reservations/status/'.$reservation->id.'/2') }}"  data-id="<?php echo $reservation->id; ?>" base_url="{{ url('/') }}" title="Details">
                                            <i class="m-nav__link-icon fa fa-user" style="color:white;"> Transfer Accounting</i>
                                           
                                        </a>
                                        @elseif($reservation->status == '2')
                                            <a class="btn btn-success" href="{{ url('admin/reservations/status/'.$reservation->id.'/3') }}"  data-id="<?php echo $reservation->id; ?>" base_url="{{ url('/') }}" title="Details">
                                                <i class="m-nav__link-icon fa fa-check-square-o"> Done</i>
                                               
                                            </a>
                                        
                                        @endif
                                        <!-- @if($reservation->trash==0)
                                         <a class="btn btn-warning" href="{{ url('admin/reservations/trash/'.$reservation->id.'/'.$reservation->trash) }}" title="Trash" >
                                            <i class="m-nav__link-icon fa fa-times" style="color: white;"></i>
                                        </a>
                                        @else
                                         <a class="btn btn-success" href="{{ url('admin/reservations/trash/'.$reservation->id.'/'.$reservation->trash) }}" title="Untrash" >
                                            <i class="m-nav__link-icon fa fa-check"></i>
                                        </a>
                                        @endif -->
                                    </td> 
                                   
                                </tr>
                                
                                <div class="modal fade" id="m_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">
                                                Confirmation Message
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">
                                                    &times;
                                                </span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                           
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal" style="background-color:black;">
                                                Close
                                            </button>
                                            <form action="" id="form" method="post" enctype="multipart/form-data" >
                                                         {{ csrf_field() }}
                                                         {{ method_field('DELETE') }}
                                            <input type="submit" class="btn btn-brand" value="ok">
                                                       
                                                       
                                            </form>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable -->
                <br>
                {{$reservations->links()}}
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="m_modal_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">
                                            More Details
                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">
                                                &times;
                                            </span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                       
                                    </div>
                                    <div class="modal-footer">
                                       <!--  <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                            Close
                                        </button> -->
                                        <button type="button" class="btn btn-brand" data-dismiss="modal">
                                            Close
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

@endsection