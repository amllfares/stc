@extends('layout.layout')
@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">

					<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">
									edit {{$candidate_attendance->user->name}}  candidate_attendance
								</h3>
								<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
									<li class="m-nav__item m-nav__item--home">
										<a href="#" class="m-nav__link m-nav__link--icon">
											<i class="m-nav__link-icon la la-home"></i>
										</a>
									</li>
									<li class="m-nav__separator">
										-
									</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												All_candidate_attendances
											</span>
										</a>
									</li>
									<li class="m-nav__separator">
										-
									</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												edit candidate_attendance
											</span>
										</a>
									</li>
									
									
								</ul>
							</div>
							
							<div>
								
								<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover" aria-expanded="true">
									<a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
										<i class="la la-plus m--hide"></i>
										<i class="la la-ellipsis-h"></i>
									</a>
									<div class="m-dropdown__wrapper">
										<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
										<div class="m-dropdown__inner">
											<div class="m-dropdown__body">
												<div class="m-dropdown__content">
													<ul class="m-nav">
														<li class="m-nav__section m-nav__section--first m--hide">
															<span class="m-nav__section-text">
																Quick Actions
															</span>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-share"></i>
																<span class="m-nav__link-text">
																	Activity
																</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-chat-1"></i>
																<span class="m-nav__link-text">
																	Messages
																</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-info"></i>
																<span class="m-nav__link-text">
																	FAQ
																</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-lifebuoy"></i>
																<span class="m-nav__link-text">
																	Support
																</span>
															</a>
														</li>
														<li class="m-nav__separator m-nav__separator--fit"></li>
														<li class="m-nav__item">
															<a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">
																Submit
															</a>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END: Subheader -->
					<div class="m-content">
						<div class="row">
							<div class="col-lg-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
												<h3 class="m-portlet__head-text">
													edit candidate_attendance
												</h3>
											</div>
										</div>
									</div>
									<!--begin::Form-->
									 @if(Session::has('edit-candidate_attendance'))
					                <div class="alert alert-brand m-alert m-alert--icon m-alert--air m-alert--square m--margin-bottom-30" role="alert">
					                    <div class="m-alert__icon">
					                        <i class="flaticon-exclamation-1"></i>
					                    </div>
					                    <div class="m-alert__text">
					                        {{ Session::get('edit-candidate_attendance') }}
					                    </div>
					                </div>
					          		  @endif
									<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="POST" action="{{route('candidate_attendances.update', ['id' => $candidate_attendance->id])}}" enctype="multipart/form-data">
                       				 {{ csrf_field() }}
                       				 {{ method_field('PATCH') }} 
										<div class="m-portlet__body">
											<div class="form-group m-form__group row">
												
										        <div class="col-lg-6">
													<label class="">
														Candidate
													</label>
													<div class="m-input-icon m-input-icon--right">
														<select name="candidate_id" id="parent_category" class="form-control m-input  m-input--square" required="">
						                                    @foreach ($users as $user )
						                                        <option value="{{ $user->id }}" @if ($user->id==$candidate_attendance->candidate_id)
					                                               selected 
					                                           @endif>{{ $user->name}}</option>
						                                    @endforeach
						                                </select>
													</div>
													<span class="m-form__help">
														Please choose Candidate
													</span>
												
												</div>

												 <div class="col-lg-6">
													<label class="">
														Date
													</label>
													<div class="m-input-icon m-input-icon--right">
														<select name="course_date_id" id="parent_category" class="form-control m-input  m-input--square" required="">
						                                    @foreach ($course_dates as $course_date )
						                                        <option value="{{ $course_date->id }}" @if ($course_date->id==$candidate_attendance->course_date_id)
					                                               selected 
					                                           @endif>{{ $course_date->date}}</option>
						                                    @endforeach
						                                </select>
													</div>
													<span class="m-form__help">
														Please choose date
													</span>
												
												</div>

												<div class="col-lg-6">
													<label class="">
														Attendance?
													</label>
													<div class="m-input-icon m-input-icon--right">
														<div class="m-radio-inline">
															<label class="m-radio m-radio--solid m-radio--state-brand">
																<input type="radio" name="status" value="1" @if($candidate_attendance->status=="1") checked="checked" @endif>
																Yes
																<span></span>
															</label>
															<label class="m-radio m-radio--solid m-radio--state-success">
																<input type="radio" name="status" value="0" @if($candidate_attendance->status=="0") checked="checked" @endif>
																No
																<span></span>
															</label>
															
														</div>
														<span class="m-form__help">
															Please enter Attendance?
														</span>	
													</div>
													
												</div>

											
												
											</div>
											
										</div>
										<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
											<div class="m-form__actions m-form__actions--solid">
												<div class="row">
													<div class="col-lg-6">
														<button type="Submit" class="btn btn-brand">
															Save
														</button>
														<a type="button" class="btn btn-default" href="{{route('candidate_attendances.index')}}" style="color: #716aca;">
															Cancel
														</a>
													</div>
													<div class="col-lg-6 m--align-right">
														<button type="reset" class="btn btn-danger">
															Delete
														</button>
													</div>
												</div>
											</div>
										</div>
									</form>
									<!--end::Form-->
								</div>
								<!--end::Portlet-->
							</div>
						</div>
					</div>
				</div>



@endsection