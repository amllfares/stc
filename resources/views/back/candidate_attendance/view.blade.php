@extends('layout.layout')
@section('content')

<div class="m-content">
    <?php
        if(Session::has('msg')){
            echo App\Helpers\Globals::msg(Session::get('msg'));
        }
    ?>

    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        All Candidate_attendances
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body2">
            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30 hidden">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1 ">
                        <div class="form-group m-form__group row align-items-center">
                            <div class="col-md-4">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="generalSearch">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-search"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                        <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>
                </div>
            </div>
            <!--end: Search Form -->
            <!--begin: Datatable -->
            
              
            <a class="btn btn-brand" href="{{ route('candidate_attendances.create') }}" >
                <span class="m-nav__link-text">
                    Add New Candidate_attendance
                </span>
            </a>
            <br><br>
            <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" id="local_data" >
                <table class="table table-bordered m-table m-table--border-brand m-table--head-bg-brand" style="padding:10%; ">
                    <thead>
                    <tr>
                        <th>ID </th>
                        <th>Candidate </th>
                        <th>Candidate_attendance date </th>
                        <th>Control Candidate_attendance </th>
                      
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($candidate_attendances as $attendance)
                                <tr @if($attendance->trash==1) class='kashat_alert_danger' @endif id="trashed_{{ $attendance->id }}">
                                    <td>{{ $attendance->id }} </td>
                                    <td>{{ $attendance->user->name }} </td>
                                    <td>{{ $attendance->course_date->date }} </td>
                                   
                                    <td>
                                        @if($check_perimation->permission_id != 1)
                                        <a class="btn btn-brand" href="{{ route('candidate_attendances.edit', ['id' => $attendance->id]) }}" title="Edit" >
                                            <i class="m-nav__link-icon fa fa-pencil-square-o"></i>
                                        </a>
                                        @endif
                                        @if($check_perimation->permission_id == 3)
                                        <a class="btn btn-danger delete" href="javascript:void(0);"  data-id="<?php echo $attendance->id; ?>" data-attendance="candidate_attendances" base_url="{{ url('/') }}"  route="candidate_attendances" title="Delete">
                                            <i class="m-nav__link-icon fa fa-trash-o"></i>
                                           
                                        </a>
                                        @endif
                                        @if($attendance->trash==0)
                                         <a class="btn btn-warning" href="{{ url('admin/candidate_attendances/trash/'.$attendance->id.'/'.$attendance->trash) }}"  title="Trash">
                                            <i class="m-nav__link-icon fa fa-times" style="color: white;"></i>
                                        </a>
                                        @else
                                         <a class="btn btn-success" href="{{ url('admin/candidate_attendances/trash/'.$attendance->id.'/'.$attendance->trash) }}" title="Untrash" >
                                            <i class="m-nav__link-icon fa fa-check"></i>
                                        </a>
                                        @endif
                                    </td>
                                   
                                </tr>
                                
                                <div class="modal fade" id="m_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">
                                                Confirmation Message
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">
                                                    &times;
                                                </span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                           
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal" style="background-color:black;">
                                                Close
                                            </button>
                                            <form action="" id="form" method="post" enctype="multipart/form-data" >
                                                         {{ csrf_field() }}
                                                         {{ method_field('DELETE') }}
                                            <input type="submit" class="btn btn-brand" value="ok">
                                                       
                                                       
                                            </form>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable -->
                <br>
                {{$candidate_attendances->links()}}
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="m_modal_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    More Details
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-brand" data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
    </div>
</div>

@endsection