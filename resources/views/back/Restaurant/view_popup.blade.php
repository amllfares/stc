

           
            <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" id="local_data" >
                 <table class="table table-bordered m-table m-table--border-brand m-table--head-bg-brand" style="padding:10%; ">
                    <thead>
                    <tr>
                        <th>ID </th>
                        <th> Name </th>
                        <th> Logo </th>
                        <th> Address </th>
                        <th>Contact Num </th>
                        <th>Contact Title </th>
                        <th>Price_per_day</th>
                        <th>Trash</th>

                       
                    </tr>
                    </thead>
                    <tbody>
                        

                                <tr @if($restaurant->trash==1) class='kashat_alert_danger' @endif id="trashed_{{ $restaurant->id }}"  >
                                    <td>{{ $restaurant->id }} </td>
                                    <td>{{ $restaurant->name_en.'_'.$restaurant->name_ar }} </td>
                                    <td><img src="{{asset($restaurant->logo)}}" width="75px"></td>
                                    <td>{{ $restaurant->address }}</td>
                                    <td>{{ $restaurant->contact_num }}</td>
                                    <td>{{ $restaurant->contact_title }}</td>
                                    <td>{{ $restaurant->price_per_day }}</td>
                                    <td>@if($restaurant->trash==1) {{"Untrashed"}} @else {{"Trashed"}} @endif </td>
                                   
                                   
                                    
                                
                                </tr>
                       
                    </tbody>
                </table>
                <!--end: Datatable -->
                <br>
                
            </div>
        </div>
    </div>
</div>




