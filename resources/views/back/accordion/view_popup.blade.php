

           
            <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" id="local_data" >
                 <table class="table table-bordered m-table m-table--border-brand m-table--head-bg-brand" style="padding:10%; ">
                    <thead>
                    <tr>
                        <th>ID </th>
                        <th> Name </th>
                        <th> Photo </th>
                        <th>Content </th>
                        <th>Order</th>
                        <th>Trash</th>

                       
                    </tr>
                    </thead>
                    <tbody>
                        

                                <tr @if($accordion->trash==1) class='kashat_alert_danger' @endif id="trashed_{{ $accordion->id }}"  >
                                    <td>{{ $accordion->id }} </td>
                                    <td>{{ $accordion->name_en.'_'.$accordion->name_ar }} </td>
                                    <td><img src="{{asset($accordion->image)}}" width="75px"></td>
                                    <td>{{ $accordion->content_en.'_'.$accordion->content_ar }}</td>
                                    <td>{{ $accordion->order }}</td>
                                    <td>@if($accordion->trash==1) {{"Untrashed"}} @else {{"Trashed"}} @endif </td>
                                   
                                   
                                    
                                
                                </tr>
                       
                    </tbody>
                </table>
                <!--end: Datatable -->
                <br>
                
            </div>
        </div>
    </div>
</div>




