<!DOCTYPE html>

<html lang="en">
<!-- begin::Head -->
<head>
    <meta charset="utf-8"/>
    <title>
        STC Admin
    </title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>
   <script src="https://cdn.ckeditor.com/4.10.0/standard/ckeditor.js"></script>
    
    <!--end::Web font -->
    <!--begin::Base Styles -->
    <!--begin::Page Vendors -->
    <link href="{{ URL('/assets/back/vendors/custom/fullcalendar/fullcalendar.bundle.css')}}" rel="stylesheet"
          type="text/css"/>
    <!--end::Page Vendors -->
    <link href="{{ URL('/assets/back/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL('/assets/back/demo/default/base/style.bundle.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL('/assets/back/custom.css')}}" rel="stylesheet" type="text/css"/>
    <!--end::Base Styles -->
    <link rel="shortcut icon" href="{{ URL('/assets/back/demo/default/media/img/logo/favicon.ico')}}"/>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">


    
    <!-- Font -->
    <link href="https://fonts.googleapis.com/css?family=Changa" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ URL('/assets/back/toastr.min.css')}}">      

     <script src="http://maps.google.com/maps/api/js?sensor=false"  type="text/javascript"></script>
     <script src="http://maps.google.com/maps/api/js?key=AIzaSyD1pzxgf9AUfrWE2pLVQanO6Ti9a5lZDGo&libraries=places&region=uk&language=en&sensor=true"></script>

</head>
<!-- end::Head -->
    <!-- end::Body -->
    <body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
        <!-- begin:: Page -->
        <div class="m-grid m-grid--hor m-grid--root m-page">
            <!-- BEGIN: Header -->
            <header class="m-grid__item    m-header "  data-minimize-offset="200" data-minimize-mobile-offset="200" >
                <div class="m-container m-container--fluid m-container--full-height">
                    <div class="m-stack m-stack--ver m-stack--desktop">
                        <!-- BEGIN: Brand -->
                        <div class="m-stack__item m-brand  m-brand--skin-dark ">
                            <div class="m-stack m-stack--ver m-stack--general">
                                <div class="m-stack__item m-stack__item--middle m-brand__logo">
                                    <a href="{{url('/admin')}}" class="m-brand__logo-wrapper">
                                        <img alt="" src="{{ URL('/assets/back/demo/default/media/img/logo/logo_default_dark.png')}}"/>
                                    </a>
                                </div>
                                <div class="m-stack__item m-stack__item--middle m-brand__tools">
                                    <!-- BEGIN: Left Aside Minimize Toggle -->
                                    <a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block 
                     ">
                                        <span></span>
                                    </a>
                                    <!-- END -->
                            <!-- BEGIN: Responsive Aside Left Menu Toggler -->
                                    <a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
                                        <span></span>
                                    </a>
                                    <!-- END -->
                            <!-- BEGIN: Responsive Header Menu Toggler -->
                                    <a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
                                        <span></span>
                                    </a>
                                    <!-- END -->
            <!-- BEGIN: Topbar Toggler -->
                                    <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
                                        <i class="flaticon-more"></i>
                                    </a>
                                    <!-- BEGIN: Topbar Toggler -->
                                </div>
                            </div>
                        </div>
                        <!-- END: Brand -->
                        <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
                            <!-- BEGIN: Horizontal Menu -->
                            <button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn">
                                <i class="la la-close"></i>
                            </button>
                            <div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-light m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-dark m-aside-header-menu-mobile--submenu-skin-dark "  >
                                <ul class="m-menu__nav  m-menu__nav--submenu-arrow ">
                                 
                                </ul>
                            </div>
                            <!-- END: Horizontal Menu -->                               <!-- BEGIN: Topbar -->
                            <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
                                <div class="m-stack__item m-topbar__nav-wrapper">
                                    <ul class="m-topbar__nav m-nav m-nav--inline">
                                        <li class="m-nav__item m-topbar__notifications m-topbar__notifications--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-center  m-dropdown--mobile-full-width" data-dropdown-toggle="click" data-dropdown-persistent="true">
                                            <a href="#" class="m-nav__link m-dropdown__toggle" id="m_topbar_notification_icon">
                                                <span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--danger"></span>
                                                <span class="m-nav__link-icon">
                                                    <i class="flaticon-music-2"></i>
                                                </span>
                                            </a>
                                            <div class="m-dropdown__wrapper">
                                                <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                                <div class="m-dropdown__inner">
                                                    <div class="m-dropdown__header m--align-center" style="background: url({{ URL('/assets/back/notification_bg.jpg')}}); background-size: cover;">
                                                        @if(auth()->user()->unreadNotifications()->count())
                                                        <span class="m-dropdown__header-title">
                                                            {{auth()->user()->unreadNotifications()->count()}} New
                                                        </span>
                                                        @endif
                                                        <span class="m-dropdown__header-subtitle">
                                                            {{auth()->user()->name}} Notifications
                                                        </span>
                                                        
                                                    </div>

                                                    <div class="m-dropdown__body">
                                                        <div class="m-dropdown__content">
                                                            
                                                            <div class="tab-content">
                                                                <div class="tab-pane active" id="topbar_notifications_notifications" role="tabpanel">
                                                                    <div class="m-scrollable" data-scrollable="true" data-max-height="250" data-mobile-max-height="200">
                                                                        <div class="m-list-timeline m-list-timeline--skin-light">
                                                                           <i style="color: #716aca;" class="fa fa-check" aria-hidden="true"></i> <span class="m-dropdown__header-subtitle"><a href="{{url('/read')}}" style="color:#716aca;">Mark All As Read</a>                       
                                                                            </span>
                                                                            <div class="m-list-timeline__items">
                                                                                @foreach(auth()->user()->unreadNotifications as $notification)
                                                                                <div class="m-list-timeline__item">
                                                                                    <span class="m-list-timeline__badge -m-list-timeline__badge--state-success"></span>
                                                                                    <span class="m-list-timeline__text">
                                                                                     {{$notification->data['name'] .'   :  '.  $notification->data['data'] }} 
                                                                                    </span>


                                                                                    <span class="m-list-timeline__time">
                                                                                        <!-- Just now -->
                                                                                       {{$notification->created_at}}
                                                                                       
                                                                                      
                                                                                    </span>
                                                                                </div>
                                                                                @endforeach
                                                                           
                                                                                @foreach(auth()->user()->readNotifications as $notifi)
                                                                                <div class="m-list-timeline__item m-list-timeline__item--read">
                                                                                    <span class="m-list-timeline__badge"></span>
                                                                                    <span href="" class="m-list-timeline__text">
                                                                                        {{$notifi->data['name'] .'   :  '.  $notifi->data['data'] }}
                                                                                        
                                                                                    </span>
                                                                                    <span class="m-list-timeline__time">
                                                                                         {{$notifi->created_at}}
                                                                                    </span>
                                                                                </div>
                                                                                @endforeach
                                                                               

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="tab-pane" id="topbar_notifications_events" role="tabpanel">
                                                                    <div class="m-scrollable" m-scrollabledata-scrollable="true" data-max-height="250" data-mobile-max-height="200">
                                                                        <div class="m-list-timeline m-list-timeline--skin-light">
                                                                            <div class="m-list-timeline__items">
                                                                                <div class="m-list-timeline__item">
                                                                                    <span class="m-list-timeline__badge m-list-timeline__badge--state1-success"></span>
                                                                                    <a href="" class="m-list-timeline__text">
                                                                                        New order received
                                                                                    </a>
                                                                                    <span class="m-list-timeline__time">
                                                                                        Just now
                                                                                    </span>
                                                                                </div>
                                                                                <div class="m-list-timeline__item">
                                                                                    <span class="m-list-timeline__badge m-list-timeline__badge--state1-danger"></span>
                                                                                    <a href="" class="m-list-timeline__text">
                                                                                        New invoice received
                                                                                    </a>
                                                                                    <span class="m-list-timeline__time">
                                                                                        20 mins
                                                                                    </span>
                                                                                </div>
                                                                                <div class="m-list-timeline__item">
                                                                                    <span class="m-list-timeline__badge m-list-timeline__badge--state1-success"></span>
                                                                                    <a href="" class="m-list-timeline__text">
                                                                                        Production server up
                                                                                    </a>
                                                                                    <span class="m-list-timeline__time">
                                                                                        5 hrs
                                                                                    </span>
                                                                                </div>
                                                                                <div class="m-list-timeline__item">
                                                                                    <span class="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>
                                                                                    <a href="" class="m-list-timeline__text">
                                                                                        New order received
                                                                                    </a>
                                                                                    <span class="m-list-timeline__time">
                                                                                        7 hrs
                                                                                    </span>
                                                                                </div>
                                                                                <div class="m-list-timeline__item">
                                                                                    <span class="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>
                                                                                    <a href="" class="m-list-timeline__text">
                                                                                        System shutdown
                                                                                    </a>
                                                                                    <span class="m-list-timeline__time">
                                                                                        11 mins
                                                                                    </span>
                                                                                </div>
                                                                                <div class="m-list-timeline__item">
                                                                                    <span class="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>
                                                                                    <a href="" class="m-list-timeline__text">
                                                                                        Production server down
                                                                                    </a>
                                                                                    <span class="m-list-timeline__time">
                                                                                        3 hrs
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="tab-pane" id="topbar_notifications_logs" role="tabpanel">
                                                                    <div class="m-stack m-stack--ver m-stack--general" style="min-height: 180px;">
                                                                        <div class="m-stack__item m-stack__item--center m-stack__item--middle">
                                                                            <span class="">
                                                                                All caught up!
                                                                                <br>
                                                                                No new logs.
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="
                                            m-nav__item m-dropdown m-dropdown--large m-dropdown--arrow m-dropdown--align-center m-dropdown--mobile-full-width m-dropdown--skin-light    m-list-search m-list-search--skin-light" 
                                            data-dropdown-toggle="click" data-dropdown-persistent="true" id="m_quicksearch" data-search-type="dropdown">
                                            <!-- <a href="#" class="m-nav__link m-dropdown__toggle">
                                                <span class="m-nav__link-icon">
                                                    <i class="flaticon-search-1"></i>
                                                </span>
                                            </a> -->
                                            <div class="m-dropdown__wrapper">
                                                <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                                <div class="m-dropdown__inner ">
                                                    <div class="m-dropdown__header">
                                                        <form  class="m-list-search__form">
                                                            <div class="m-list-search__form-wrapper">
                                                                <span class="m-list-search__form-input-wrapper">
                                                                    <input id="m_quicksearch_input" autocomplete="off" type="text" name="q" class="m-list-search__form-input" value="" placeholder="Search...">
                                                                </span>
                                                                <span class="m-list-search__form-icon-close" id="m_quicksearch_close">
                                                                    <i class="la la-remove"></i>
                                                                </span>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div class="m-dropdown__body">
                                                        <div class="m-dropdown__scrollable m-scrollable" data-scrollable="true" data-max-height="300" data-mobile-max-height="200">
                                                            <div class="m-dropdown__content"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>

                                       
                                       
                                        <li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" data-dropdown-toggle="click">
                                            <a href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="m-nav__link m-dropdown__toggle">
                                                <span class="m-topbar__userpic">
                                                    <!-- <img src="{{ URL('/assets/back/app/media/img/users/user4.jpg')}}" class="m--img-rounded m--marginless m--img-centered" alt=""/> -->
                                                    
                                                    <i class=" flaticon-logout btn m-btn--pill    btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder" aria-hidden="true" >
                                                    
                                                    Logout
                                                
                                                    </i>
                                                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                        @csrf
                                                    </form>

                                                </span>
                                                
                                            </a>
                                          
                                        </li>
                                       
                                    </ul>
                                </div>
                            </div>
                            <!-- END: Topbar -->
                        </div>
                    </div>
                </div>
            </header>
            <!-- END: Header -->        
        <!-- begin::Body -->
            <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
                <!-- BEGIN: Left Aside -->
                <button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
                    <i class="la la-close"></i>
                </button>
                <div id="m_aside_left" class="m-grid__item  m-aside-left  m-aside-left--skin-dark ">
                    <!-- BEGIN: Aside Menu -->
    <div 
        id="m_ver_menu" 
        class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " 
        data-menu-vertical="true"
         data-menu-scrollable="false" data-menu-dropdown-timeout="500"  
        >
                        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
                            <li class="m-menu__item  m-menu__item--active" aria-haspopup="true" >
                                <a  href="{{ url('/admin') }}" class="m-menu__link ">
                                    <i class="m-menu__link-icon flaticon-line-graph"></i>
                                    <span class="m-menu__link-title">
                                        <span class="m-menu__link-wrap">
                                            <span class="m-menu__link-text">
                                               STC  Dashboard
                                            </span>
                                            <span class="m-menu__link-badge">
                                                <span class="m-badge m-badge--danger">
                                                   control
                                                </span>
                                            </span>
                                        </span>
                                    </span>
                                </a>
                            </li>
                            <li class="m-menu__section">
                                <h4 class="m-menu__section-text">
                                    Components
                                </h4>
                                <i class="m-menu__section-icon flaticon-more-v3"></i>
                            </li>
                            <?php
                            // $user_role=App\User_Role::where('user_id',auth()->user()->id)->with('role')->first();
                            // $user_sections=App\Role_Permission::orderBy('id', 'desc')->where('role_id',$user_role->role_id)->get();
                            // var_dump($user_sections);die();
                            $main_sections =App\Section::where('sub_of','0')->get();
                            $general_count=App\Reservation::where('status',0)->where('private','0')->count();
                            $company_count= App\Company_reservation::where('status',0)->count();
                            $private_count=App\Reservation::where('status',0)->where('private','1')->count();
                            $count_all=$general_count+ $company_count+$private_count ;
                            $count_unseen=App\Apply_Career::where('seen',0)->count();?>


                            @foreach($user_sections as $main_section)
                            <?php 
                            $activ=Request::segment('2');
                            $active= str_replace('_', ' ', $activ) ;
                            $actives=$active.'s'
                            // var_dump(ucfirst($active));die(); ?>
                            @if($main_section->section->name == ucwords($active) || $main_section->section->name == ucwords($actives))
                              <li class="m-menu__item  m-menu__item--submenu m-menu__item--active  m-menu__item--open" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                            @else
                             <li class="m-menu__item  m-menu__item--submenu " aria-haspopup="true"  data-menu-submenu-toggle="hover">
                            @endif
                                @if($main_section->section->id==52)
                                <a  href="#" class="m-menu__link m-menu__toggle">
                                    <i class="m-menu__link-icon {{$main_section->section->icon}}"></i>
                                    <span class="m-menu__link-text">
                                        {{$main_section->section->name}}
                                    </span>
                                     @if(isset($general_count) && !empty($general_count)&&($general_count >0))
                                     <span class="m-badge m-badge--danger" style="    margin-left: 25px;">
                                                   {{$general_count}}  
                                    </span>
                                     @endif
                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                @elseif($main_section->section->id==43)
                                 <a  href="#" class="m-menu__link m-menu__toggle">
                                    <i class="m-menu__link-icon {{$main_section->section->icon}}"></i>
                                    <span class="m-menu__link-text">
                                        {{$main_section->section->name}}
                                    </span>
                                     @if(isset($count_all) && !empty($count_all) && ($count_unseen >0))
                                     <span class="m-badge m-badge--danger" style="    margin-left: 25px;">
                                                   {{$count_unseen}} 
                                    </span>
                                     @endif
                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                                </a>

                                @else
                                 <a  href="#" class="m-menu__link m-menu__toggle">
                                    <i class="m-menu__link-icon {{$main_section->section->icon}}"></i>
                                    <span class="m-menu__link-text">
                                        {{$main_section->section->name}}
                                    </span>
                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                @endif

                                <div class="m-menu__submenu">
                                    <span class="m-menu__arrow"></span>
                                    <ul class="m-menu__subnav">
                                        
                                     @if($main_section->permission_id=='1')
                                     <?php $sub_sections =App\Section::where('sub_of',$main_section->section->id)->get(); ?>
                                    
                                     @foreach($sub_sections as $main_section)
                                     @if(!(strpos($main_section->link,'create')))
                                        <li class="m-menu__item " aria-haspopup="true" >
                                            @if($main_section->id==53)
                                            <a  href="{{ url('/admin/'.$main_section->link) }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                    {{$main_section->name}}
                                                </span>
                                                @if(isset($general_count) && !empty($general_count)&&($general_count >0))
                                                <span class="m-badge m-badge--danger" >
                                                   {{$general_count}} 
                                                </span>
                                                @endif
                                            </a>
                                            @elseif($main_section->id==55)
                                             <a  href="{{ url('/admin/'.$main_section->link) }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                    {{$main_section->name}}
                                                </span>
                                                @if(isset($private_count) && !empty($private_count)&&($private_count >0))
                                                <span class="m-badge m-badge--danger" >
                                                   {{$private_count}}
                                                </span>
                                                @endif
                                            </a>
                                            @elseif($main_section->id==57)
                                             <a  href="{{ url('/admin/'.$main_section->link) }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                    {{$main_section->name}}
                                                </span>
                                                @if(isset($company_count) && !empty($company_count) &&($company_count >0))
                                                <span class="m-badge m-badge--danger" >
                                                   {{$company_count}} 
                                                </span>
                                                @endif
                                            </a>
                                            @else
                                             <a  href="{{ url('/admin/'.$main_section->link) }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                    {{$main_section->name}}
                                                </span>
                                            </a>
                                            @endif
                                        </li>
                                    @endif
                                    @endforeach
                                    
                                     @else
                                      <?php $sub_sections =App\Section::where('sub_of',$main_section->section->id)->get(); ?>
                                     @foreach($sub_sections as $main_section)
                                        <li class="m-menu__item " aria-haspopup="true" >
                                            @if($main_section->id==53)
                                            <a  href="{{ url('/admin/'.$main_section->link) }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                    {{$main_section->name}}
                                                </span>
                                                @if(isset($general_count) && !empty($general_count)&&($general_count >0))
                                                <span class="m-badge m-badge--danger" >
                                                   {{$general_count}} 
                                                </span>
                                                @endif
                                            </a>
                                            @elseif($main_section->id==55)
                                             <a  href="{{ url('/admin/'.$main_section->link) }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                    {{$main_section->name}}
                                                </span>
                                                @if(isset($private_count) && !empty($private_count)&&($private_count >0))
                                                <span class="m-badge m-badge--danger" >
                                                   {{$private_count}}
                                                </span>
                                                @endif
                                            </a>
                                            @elseif($main_section->id==57)
                                             <a  href="{{ url('/admin/'.$main_section->link) }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                    {{$main_section->name}}
                                                </span>
                                                @if(isset($company_count) && !empty($company_count) &&($company_count >0))
                                                <span class="m-badge m-badge--danger" >
                                                   {{$company_count}} 
                                                </span>
                                                @endif
                                            </a>
                                            @else
                                             <a  href="{{ url('/admin/'.$main_section->link) }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                    {{$main_section->name}}
                                                </span>
                                            </a>
                                            @endif
                                        </li>
                                    @endforeach
                                    @endif
                                    </ul>
                                </div>
                               
                            </li>
                             @endforeach
                         
                          <!--   <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                                <a  href="#" class="m-menu__link m-menu__toggle">
                                    <i class="m-menu__link-icon fa fa-bed"></i>
                                    <span class="m-menu__link-text">
                                        Hotels
                                    </span>
                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="m-menu__submenu">
                                    <span class="m-menu__arrow"></span>
                                    <ul class="m-menu__subnav">
                                        <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
                                            <a  href="#" class="m-menu__link ">
                                                <span class="m-menu__link-text">
                                                  View Hotels
                                                </span>
                                            </a>
                                        </li>
                                       <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/hotels/') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                     View Hotels
                                                </span>
                                            </a>
                                        </li>
                                    
                                        <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/hotels/create') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                    Add Hotel
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li> -->

                       <!--   <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                                <a  href="#" class="m-menu__link m-menu__toggle">
                                    <i class="m-menu__link-icon fa fa-cutlery"></i>
                                    <span class="m-menu__link-text">
                                        Restaurants
                                    </span>
                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="m-menu__submenu">
                                    <span class="m-menu__arrow"></span>
                                    <ul class="m-menu__subnav">
                                        <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
                                            <a  href="#" class="m-menu__link ">
                                                <span class="m-menu__link-text">
                                                  View Restaurants
                                                </span>
                                            </a>
                                        </li>
                                       <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/restaurants/') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                     View Restaurants
                                                </span>
                                            </a>
                                        </li>
                                    
                                        <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/restaurants/create') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                    Add Restaurant
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li> -->

                       <!--   <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                                <a  href="#" class="m-menu__link m-menu__toggle">
                                    <i class="m-menu__link-icon fa fa-table"></i>
                                    <span class="m-menu__link-text">
                                        Subject_Categories
                                    </span>
                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="m-menu__submenu">
                                    <span class="m-menu__arrow"></span>
                                    <ul class="m-menu__subnav">
                                        <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
                                            <a  href="#" class="m-menu__link ">
                                                <span class="m-menu__link-text">
                                                  View Subject_Categories
                                                </span>
                                            </a>
                                        </li>
                                       <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/sub_cat/') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                     View Subject_Categories
                                                </span>
                                            </a>
                                        </li>
                                    
                                        <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/sub_cat/create') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                    Add Subject_Category
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li> -->

                       <!--   <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                                <a  href="#" class="m-menu__link m-menu__toggle">
                                    <i class="m-menu__link-icon fa fa-book"></i>
                                    <span class="m-menu__link-text">
                                        Subjects
                                    </span>
                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="m-menu__submenu">
                                    <span class="m-menu__arrow"></span>
                                    <ul class="m-menu__subnav">
                                        <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
                                            <a  href="#" class="m-menu__link ">
                                                <span class="m-menu__link-text">
                                                  View Subjects
                                                </span>
                                            </a>
                                        </li>
                                       <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/subject/') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                     View Subjects
                                                </span>
                                            </a>
                                        </li>
                                    
                                        <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/subject/create') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                    Add Subject
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li> -->

                     <!--  <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                                <a  href="#" class="m-menu__link m-menu__toggle">
                                    <i class="m-menu__link-icon fa fa-male"></i>
                                    <span class="m-menu__link-text">
                                        Instructors
                                    </span>
                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="m-menu__submenu">
                                    <span class="m-menu__arrow"></span>
                                    <ul class="m-menu__subnav">
                                        <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
                                            <a  href="#" class="m-menu__link ">
                                                <span class="m-menu__link-text">
                                                  View Instructors
                                                </span>
                                            </a>
                                        </li>
                                       <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/instructor/') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                     View Instructors
                                                </span>
                                            </a>
                                        </li>
                                    
                                        <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/instructor/create') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                    Add Instructor
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                        </li>

                        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                                <a  href="#" class="m-menu__link m-menu__toggle">
                                    <i class="m-menu__link-icon fa fa-book"></i>
                                    <span class="m-menu__link-text">
                                        Courses
                                    </span>
                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="m-menu__submenu">
                                    <span class="m-menu__arrow"></span>
                                    <ul class="m-menu__subnav">
                                        <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
                                            <a  href="#" class="m-menu__link ">
                                                <span class="m-menu__link-text">
                                                  View Courses
                                                </span>
                                            </a>
                                        </li>
                                       <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/course/') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                     View Courses
                                                </span>
                                            </a>
                                        </li>
                                    
                                        <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/course/create') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                    Add Course
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li> -->

                       <!--   <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                                <a  href="#" class="m-menu__link m-menu__toggle">
                                    <i class="m-menu__link-icon fa fa-file"></i>
                                    <span class="m-menu__link-text">
                                        Exams
                                    </span>
                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="m-menu__submenu">
                                    <span class="m-menu__arrow"></span>
                                    <ul class="m-menu__subnav">
                                        <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
                                            <a  href="#" class="m-menu__link ">
                                                <span class="m-menu__link-text">
                                                  View Exams
                                                </span>
                                            </a>
                                        </li>
                                       <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/exam/') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                     View Exams
                                                </span>
                                            </a>
                                        </li>
                                    
                                        <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/exam/create') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                    Add Exam
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>

                           <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                                <a  href="#" class="m-menu__link m-menu__toggle">
                                    <i class="m-menu__link-icon fa fa-money"></i>
                                    <span class="m-menu__link-text">
                                       Employee_Salary
                                    </span>
                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="m-menu__submenu">
                                    <span class="m-menu__arrow"></span>
                                    <ul class="m-menu__subnav">
                                        <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
                                            <a  href="#" class="m-menu__link ">
                                                <span class="m-menu__link-text">
                                                  View Employee_Salary
                                                </span>
                                            </a>
                                        </li>
                                       <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/employee_salary/') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                     View Employee_Salary
                                                </span>
                                            </a>
                                        </li>
                                    
                                        <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/employee_salary/create') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                    Add Employee_Salary
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li> -->

            <!--                  <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                                <a  href="#" class="m-menu__link m-menu__toggle">
                                    <i class="m-menu__link-icon fa fa-commenting"></i>
                                    <span class="m-menu__link-text">
                                        Feedbacks
                                    </span>
                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="m-menu__submenu">
                                    <span class="m-menu__arrow"></span>
                                    <ul class="m-menu__subnav">
                                        <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
                                            <a  href="#" class="m-menu__link ">
                                                <span class="m-menu__link-text">
                                                  View Feedbacks
                                                </span>
                                            </a>
                                        </li>
                                       <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/feedback/instructor') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                     View Feedbacks of instructor
                                                </span>
                                            </a>
                                        </li>

                                         <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/feedback/admin') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                     View Feedbacks of admin
                                                </span>
                                            </a>
                                        </li>

                                        <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/feedback/employe') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                     View Feedbacks of employe
                                                </span>
                                            </a>
                                        </li>
                                        <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/feedback/candidate') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                     View Feedbacks of candidate
                                                </span>
                                            </a>
                                        </li>
                                    
                                        
                                    </ul>
                                </div>
                            </li>



                             <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                                <a  href="#" class="m-menu__link m-menu__toggle">
                                    <i class="m-menu__link-icon fa fa-files-o"></i>
                                    <span class="m-menu__link-text">
                                        Pages
                                    </span>
                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="m-menu__submenu">
                                    <span class="m-menu__arrow"></span>
                                    <ul class="m-menu__subnav">
                                        <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
                                            <a  href="#" class="m-menu__link ">
                                                <span class="m-menu__link-text">
                                                  View Pages
                                                </span>
                                            </a>
                                        </li>
                                       <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/pages/') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                     View Pages
                                                </span>
                                            </a>
                                        </li>
                                    
                                        <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/pages/create') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                    Add Page
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>



                             <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                                <a  href="#" class="m-menu__link m-menu__toggle">
                                    <i class="m-menu__link-icon fa fa-newspaper-o"></i>
                                    <span class="m-menu__link-text">
                                        s
                                    </span>
                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="m-menu__submenu">
                                    <span class="m-menu__arrow"></span>
                                    <ul class="m-menu__subnav">
                                        <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
                                            <a  href="#" class="m-menu__link ">
                                                <span class="m-menu__link-text">
                                                  View News
                                                </span>
                                            </a>
                                        </li>
                                       <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/news/') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                     View News
                                                </span>
                                            </a>
                                        </li>
                                    
                                        <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/news/create') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                    Add new
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li> -->


                         <!--     <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                                <a  href="#" class="m-menu__link m-menu__toggle">
                                    <i class="m-menu__link-icon fa fa-calendar"></i>
                                    <span class="m-menu__link-text">
                                        Events
                                    </span>
                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="m-menu__submenu">
                                    <span class="m-menu__arrow"></span>
                                    <ul class="m-menu__subnav">
                                        <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
                                            <a  href="#" class="m-menu__link ">
                                                <span class="m-menu__link-text">
                                                  View Events
                                                </span>
                                            </a>
                                        </li>
                                       <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/events/') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                     View Events
                                                </span>
                                            </a>
                                        </li>
                                    
                                        <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/events/create') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                    Add Event
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>



                             <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                                <a  href="#" class="m-menu__link m-menu__toggle">
                                    <i class="m-menu__link-icon fa fa-user-md"></i>
                                    <span class="m-menu__link-text">
                                        Careers
                                    </span>
                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="m-menu__submenu">
                                    <span class="m-menu__arrow"></span>
                                    <ul class="m-menu__subnav">
                                        <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
                                            <a  href="#" class="m-menu__link ">
                                                <span class="m-menu__link-text">
                                                  View Careers
                                                </span>
                                            </a>
                                        </li>
                                       <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/careers/') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                     View Careers
                                                </span>
                                            </a>
                                        </li>
                                    
                                        <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/careers/create') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                    Add Career
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>


                             <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                                <a  href="#" class="m-menu__link m-menu__toggle">
                                    <i class="m-menu__link-icon fa fa-picture-o"></i>
                                    <span class="m-menu__link-text">
                                       Gallary
                                    </span>
                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="m-menu__submenu">
                                    <span class="m-menu__arrow"></span>
                                    <ul class="m-menu__subnav">
                                        <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
                                            <a  href="#" class="m-menu__link ">
                                                <span class="m-menu__link-text">
                                                  View Gallary
                                                </span>
                                            </a>
                                        </li>
                                       <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/gallery/') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                     View Gallary
                                                </span>
                                            </a>
                                        </li>
                                    
                                        <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/gallery/create') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                    Add Gallary
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>

                             <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                                <a  href="#" class="m-menu__link m-menu__toggle">
                                    <i class="m-menu__link-icon fa fa-building"></i>
                                    <span class="m-menu__link-text">
                                        Companies_accounts
                                    </span>
                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="m-menu__submenu">
                                    <span class="m-menu__arrow"></span>
                                    <ul class="m-menu__subnav">
                                        <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
                                            <a  href="#" class="m-menu__link ">
                                                <span class="m-menu__link-text">
                                                  View Companies_accounts
                                                </span>
                                            </a>
                                        </li>
                                       <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/company_account/') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                     View Companies_accounts
                                                </span>
                                            </a>
                                        </li>
                                    
                                        <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/company_account/create') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                    Add company_account
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>


                             <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                                <a  href="#" class="m-menu__link m-menu__toggle">
                                    <i class="m-menu__link-icon fa fa-ticket"></i>
                                    <span class="m-menu__link-text">
                                        Reservations
                                    </span>
                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="m-menu__submenu">
                                    <span class="m-menu__arrow"></span>
                                    <ul class="m-menu__subnav">
                                        <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
                                            <a  href="#" class="m-menu__link ">
                                                <span class="m-menu__link-text">
                                                  View Reservations
                                                </span>
                                            </a>
                                        </li>
                                       <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/reservations/') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                     View General Reservations
                                                </span>
                                            </a>
                                        </li>

                                      <li class="m-menu__item " aria-haspopup="true" >
                                        <a  href="{{ url('/admin/reservations/create') }}" class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                <span></span>
                                            </i>
                                            <span class="m-menu__link-text">
                                                 Add General Reservations
                                            </span>
                                        </a>
                                    </li>

                                  <li class="m-menu__item " aria-haspopup="true" >
                                        <a  href="{{ url('/admin/private_reservations/') }}" class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                <span></span>
                                            </i>
                                            <span class="m-menu__link-text">
                                                 View Private Reservations
                                            </span>
                                        </a>
                                    </li>
                                      <li class="m-menu__item " aria-haspopup="true" >
                                        <a  href="{{ url('/admin/private_reservations/create') }}" class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                <span></span>
                                            </i>
                                            <span class="m-menu__link-text">
                                                 Add Private Reservations
                                            </span>
                                        </a>
                                    </li>
                                    
                                      <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/company_reservations/') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                     View Company Reservations
                                                </span>
                                            </a>
                                        </li>

                                          <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/company_reservations/create') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                      Add Company Reservations
                                                </span>
                                            </a>
                                        </li>
                                      
                                    </ul>
                                </div>
                            </li> -->

                           <!--   <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                                <a  href="#" class="m-menu__link m-menu__toggle">
                                    <i class="m-menu__link-icon fa fa-cc-visa"></i>
                                    <span class="m-menu__link-text">
                                        Financial
                                    </span>
                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="m-menu__submenu">
                                    <span class="m-menu__arrow"></span>
                                    <ul class="m-menu__subnav">
                                        <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
                                            <a  href="#" class="m-menu__link ">
                                                <span class="m-menu__link-text">
                                                  View Financial
                                                </span>
                                            </a>
                                        </li>
                                       <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/financial/') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                     View Financial
                                                </span>
                                            </a>
                                        </li>
                                    
                                    <li class="m-menu__item " aria-haspopup="true" >
                                            <a  href="{{ url('/admin/financial/create') }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">
                                                    Add Reservation
                                                </span>
                                            </a>
                                        </li> 
                                    </ul>
                                </div>
                            </li> -->



                          
                        </ul>
                    </div>
                    <!-- END: Aside Menu -->
                </div>
                <!-- END: Left Aside -->
                <div class="m-grid__item m-grid__item--fluid m-wrapper">
                 
                    @yield('content')

                </div>
            </div>
            <!-- end:: Body -->
<!-- begin::Footer -->
            <footer class="m-grid__item     m-footer ">
                <div class="m-container m-container--fluid m-container--full-height m-page__container">
                    <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
                        <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
                            <span class="m-footer__copyright">
                                2018 &copy; STC dashboard by 
                                <a href="#" class="m-link">
                                    thetailors
                                </a>
                            </span>
                        </div>
                        <div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
                            <ul class="m-footer__nav m-nav m-nav--inline m--pull-right">
                                <li class="m-nav__item">
                                    <a href="#" class="m-nav__link">
                                        <span class="m-nav__link-text">
                                            About
                                        </span>
                                    </a>
                                </li>
                                <li class="m-nav__item">
                                    <a href="#"  class="m-nav__link">
                                        <span class="m-nav__link-text">
                                            Privacy
                                        </span>
                                    </a>
                                </li>
                                <li class="m-nav__item">
                                    <a href="#" class="m-nav__link">
                                        <span class="m-nav__link-text">
                                            T&C
                                        </span>
                                    </a>
                                </li>
                                <li class="m-nav__item">
                                    <a href="#" class="m-nav__link">
                                        <span class="m-nav__link-text">
                                            Purchase
                                        </span>
                                    </a>
                                </li>
                                <li class="m-nav__item m-nav__item">
                                    <a href="#" class="m-nav__link" data-toggle="m-tooltip" title="Support Center" data-placement="left">
                                        <i class="m-nav__link-icon flaticon-info m--icon-font-size-lg3"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- end::Footer -->
        </div>
        <!-- end:: Page -->
                    <!-- begin::Quick Sidebar -->
      
        <!-- end::Quick Sidebar -->         
        <!-- begin::Scroll Top -->
        <div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
            <i class="la la-arrow-up"></i>
        </div>
        <!-- end::Scroll Top -->            <!-- begin::Quick Nav -->
     
        <!-- begin::Quick Nav -->   
    
        <script src="{{ URL('/assets/back/global/plugins/jquery.min.js')}}" type="text/javascript"></script>
        <script src="{{ URL('/assets/back/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>

        <!--begin::Base Scripts -->
        <script src="{{ URL('/assets/back/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
        <script src="{{ URL('/assets/back/demo/default/base/scripts.bundle.js')}}" type="text/javascript"></script>
        <script src="{{ URL('/assets//back/demo/default/custom/components/forms/widgets/bootstrap-select.js')}}" type="text/javascript"></script>
        <!--end::Base Scripts -->
        <!--begin::Page Vendors -->
        <script src="{{ URL('/assets/back/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
        <!--end::Page Vendors -->
        <!--begin::Page Snippets -->
        <script src="{{ URL('/assets/back/app/js/dashboard.js')}}" type="text/javascript"></script>
        <script src="{{ URL('/assets/back/app/js/custom.js')}}" type="text/javascript"></script>

<!--end::Page Snippets -->
        <script src="{{ URL('/assets/back/app/js/dashboard.js')}}" type="text/javascript"></script>
        <!--end::Page Snippets -->
         <!--begin::Page Resources -->
        <script src="{{ URL('/assets/back/demo/default/custom/components/datatables/base/data-local.js')}}" type="text/javascript"></script>
        <script>
            function myFunction() {
                // var node = document.getElementByClass("append");
                    
                $('#all_div').append("<input type='date' class='form-control m-input' name='dates[]' class='append' > <span class='m-form__help'> Please enter course dates </span>");
            }
        </script>

        

    <script src="{{ URL('/assets/back/demo/default/custom/components/forms/widgets/summernote.js')}}" type="text/javascript"></script>
        <!--end::Page Resources -->

    <script type="text/javascript">
        
        
        var edit = function() { 
          $('.click2edit').summernote({focus: true});
          $('.note-editor').find('textarea').attr('name','mytextarea');
        };
    </script>

    <script>
        CKEDITOR.replace( 'editor' );
        CKEDITOR.replace( 'editor1' );
        CKEDITOR.replace( 'editor2' );
        CKEDITOR.replace( 'editor3' );
    </script>

     <script>
     $(function () {
         var lat = 44.88623409320778,
             lng = -87.86480712897173,
             latlng = new google.maps.LatLng(lat, lng),
             image = 'http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png';
         //zoomControl: true,
         //zoomControlOptions: google.maps.ZoomControlStyle.LARGE,
         var mapOptions = {
             center: new google.maps.LatLng(lat, lng),
             zoom: 13,
             mapTypeId: google.maps.MapTypeId.ROADMAP,
             panControl: true,
             panControlOptions: {
                 position: google.maps.ControlPosition.TOP_RIGHT
             },
             zoomControl: true,
             zoomControlOptions: {
                 style: google.maps.ZoomControlStyle.LARGE,
                 position: google.maps.ControlPosition.TOP_left
             }
         },
         map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions),
             marker = new google.maps.Marker({
                 position: latlng,
                 map: map,
                 icon: image
             });
         var input = document.getElementById('searchTextField');
         var autocomplete = new google.maps.places.Autocomplete(input, {
             types: ["geocode"]
         });
         autocomplete.bindTo('bounds', map);
         var infowindow = new google.maps.InfoWindow();
         google.maps.event.addListener(autocomplete, 'place_changed', function (event) {
             infowindow.close();
             var place = autocomplete.getPlace();
             if (place.geometry.viewport) {
                 map.fitBounds(place.geometry.viewport);
             } else {
                 map.setCenter(place.geometry.location);
                 map.setZoom(17);
             }
             moveMarker(place.name, place.geometry.location);
             $('.MapLat').val(place.geometry.location.lat());
             $('.MapLon').val(place.geometry.location.lng());
         });
         google.maps.event.addListener(map, 'click', function (event) {
             $('.MapLat').val(event.latLng.lat());
             $('.MapLon').val(event.latLng.lng());
             infowindow.close();
                     var geocoder = new google.maps.Geocoder();
                     geocoder.geocode({
                         "latLng":event.latLng
                     }, function (results, status) {
                         console.log(results, status);
                         if (status == google.maps.GeocoderStatus.OK) {
                             console.log(results);
                             var lat = results[0].geometry.location.lat(),
                                 lng = results[0].geometry.location.lng(),
                                 placeName = results[0].address_components[0].long_name,
                                 latlng = new google.maps.LatLng(lat, lng);
                             moveMarker(placeName, latlng);
                             $("#searchTextField").val(results[0].formatted_address);
                         }
                     });
         });
        
         function moveMarker(placeName, latlng) {
             marker.setIcon(image);
             marker.setPosition(latlng);
             infowindow.setContent(placeName);
             //infowindow.open(map, marker);
         }
     });
</script>


<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.1&appId=407099163154146&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script src="{{ URL('/assets/back/app/js/toastr.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
    @if (Session::has('success'))
        toastr.success('{{Session::get('success')}}')
    @endif
    @if (Session::has('info'))
        toastr.info('{{Session::get('info')}}')
    @endif
    @if (Session::has('danger'))
        toastr.error('{{Session::get('danger')}}')
    @endif

    @if (Session::has('warning'))
        toastr.warning('{{Session::get('warning')}}')
    @endif
    @if (Session::has('untrash'))
        toastr.untrash('{{Session::get('untrash')}}')
    @endif
</script>
   
    </body>
    <!-- end::Body -->
</html>
