<!DOCTYPE html>
<html lang="zxx">
  <head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>STC</title>
    @if(isset($MetaTag)&& !empty($MetaTag))
    <meta name="description" content=" {!! $MetaTag->describtion !!}">
    <meta name="keywords" content="{!! $MetaTag->keyword !!}">
    <meta name="author" content="rudhisasmito.com"> 
    @else
    <meta name="description" content="Petro - Industrial HTML Template. It is built using bootstrap 3.3.2 framework, works totally responsive, easy to customise, well commented codes and seo friendly.">
    <meta name="keywords" content="petro, industrial, oil and gas, company, manufacturing, mechanical, power and energy, engineering">
    <meta name="author" content="rudhisasmito.com"> 

    @endif
    
    <!-- ==============================================
    Favicons
    =============================================== -->
    <link rel="shortcut icon" href="{{ URL('/assets/front/images/favicon.ico')}}">
    <link rel="apple-touch-icon" href="{{ URL('/assets/front/images/apple-touch-icon.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ URL('/assets/front/images/apple-touch-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ URL('/assets/front/images/apple-touch-icon-114x114.png')}}">
   
    
   
    <!-- ==============================================
    CSS VENDOR
    =============================================== -->
    
    @if(Request::segment('1') == "ar")
    <link href="{{ URL('/assets/front/v/css/menu_rtl.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ URL('/assets/front/css/vendor/bootstrap_rtl.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ URL('/assets/front/css/vendor/font-awesome_rtl.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ URL('/assets/front/css/vendor/owl.carousel_rtl.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ URL('/assets/front/css/vendor/owl.theme.default_rtl.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ URL('/assets/front/css/vendor/magnific-popup_rtl.css')}}">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    @else
    <link href="{{ URL('/assets/front/v/css/menu.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ URL('/assets/front/css/vendor/bootstrap.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ URL('/assets/front/css/vendor/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ URL('/assets/front/css/vendor/owl.carousel.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ URL('/assets/front/css/vendor/owl.theme.default.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ URL('/assets/front/css/vendor/magnific-popup.css')}}">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

    @endif 
    <!-- ==============================================
    Custom Stylesheet
    =============================================== -->
    @if(Request::segment('1') == "ar")
     <link rel="stylesheet" type="text/css" href="{{ URL('/assets/front/css/style_rtl.css')}}" />
    @else
     <link rel="stylesheet" type="text/css" href="{{ URL('/assets/front/css/style.css')}}" />
    @endif
     <script src="http://maps.google.com/maps/api/js?sensor=false"  type="text/javascript"></script>
     <script src="http://maps.google.com/maps/api/js?key=AIzaSyD1pzxgf9AUfrWE2pLVQanO6Ti9a5lZDGo&libraries=places&region=uk&language=en&sensor=true"></script>
     
     <!-- <script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyDDYt0AfLoYzYx5yJapP8SDga4JT4jkAXQ'></script> -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    
    <script type="text/javascript" src="{{ URL('/assets/front/js/vendor/modernizr.min.js')}}"></script>


    <!-- Font -->
    <link href="https://fonts.googleapis.com/css?family=Changa" rel="stylesheet">
</head>

<body>

    <!-- Load page -->
    <div class="animationload">
        <div class="loader"></div>
    </div>

    <!-- BACK TO TOP SECTION -->
    <a href="#0" class="cd-top cd-is-visible cd-fade-out">Top</a>

    <!-- HEADER -->
    <div class="header">
        <!-- TOPBAR -->
         @if(Request::segment('1') == "ar")
         
        <div class="topbar">
            <div class="container">
                <div class="row">
                    <div class="col-sm-5 col-md-6">
                        <div class="topbar-left">
                           <div class="welcome-text " style="margin: -4px;">
                              @guest
                               <a href="{{ url('ar/login_page')}}" class="welcome-text"> <i class="fa fa-paper-plane" aria-hidden="true"></i> @lang('lang.Login')</a>
                                              

                                <a href="{{ url('ar/register_page')}}" class="welcome-text"><i class="fa fa-lock" aria-hidden="true"></i> | @lang('lang.Register') </a>
                              @else
                                  <a href="{{ url(Auth::user()->lang.'/view_profile')}}" class="welcome-text"><i class="fa fa-user" aria-hidden="true"></i>  {{ Auth::user()->name }}</a>
                                  
                                   <a class="dropdown-item welcome-text " href="{{ route('logout') }}" style="color: white;" 
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="fa fa-sign-out" aria-hidden="true"></i> |
                                        @lang('lang.Logout')
                                    </a>
                                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                               

                              @endguest
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7 col-md-6">
                        <div class="topbar-right">
                           
                            <ul class="topbar-sosmed">
                         <!--    <li><a href="{{ url('ar/home')}}"> AR</a></li> -->
                            <li><a href="{{ route('ChangeLang',['lang' => 'en']) }}"> EN </a>
                            </li>
                            <li>
                                <a target="_blank" href="https://www.facebook.com/SolemanSTC"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li>
                                <a target="_blank" href="https://www.linkedin.com/in/stc-training-78ab24107/"><i class="fa fa-linkedin"></i></a>
                            </li>

                            <li>
                              <a href="https://www.instagram.com/soleman_stc/" target="_blank"><i class="fa fa-instagram"></i></a>
                            </li>
                           
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @else

         <div class="topbar">
            <div class="container">
                <div class="row">
                    <div class="col-sm-5 col-md-6">
                        <div class="topbar-left">
                            <div class="welcome-text ">
                              @guest
                                <a href="{{ url('en/login_page')}}" class="welcome-text"> <i class="fa fa-paper-plane" aria-hidden="true"></i> Login</a>
                                               |
                                <a href="{{ url('en/register_page')}}" class="welcome-text"><i class="fa fa-lock" aria-hidden="true"></i> Register</a>
                              @else
                                  <a href="{{ url(Auth::user()->lang.'/view_profile')}}" class="welcome-text"><i class="fa fa-user" aria-hidden="true"></i>  {{ Auth::user()->name }}</a>
                                  |
                                   <a class="dropdown-item " href="{{ route('logout') }}" style="color: white;" 
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="fa fa-sign-out" aria-hidden="true"></i>
                                        {{ __('Logout') }}
                                    </a>
                                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                               

                              @endguest
                            </div>

                        </div>
                    </div>
                    <div class="col-sm-7 col-md-6">
                        <div class="topbar-right">
                            
                            <ul class="topbar-sosmed">
                              @if(Request::segment(1)==null)
                              <li><a href="{{ url('ar/home') }}"> AR</a></li>
                              @else
                               <li><a href="{{ route('ChangeLang',['lang' => 'ar']) }}"> AR</a></li>
                              @endif
                            <li>
                                <a target="_blank" href="https://www.facebook.com/SolemanSTC"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li>
                                <a target="_blank" href="https://www.linkedin.com/in/stc-training-78ab24107/"><i class="fa fa-linkedin"></i></a>
                            </li>
                            <li>
                              <a href="https://www.instagram.com/soleman_stc/" target="_blank"><i class="fa fa-instagram"></i></a>
                            </li>
                           
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif

        <!-- TOPBAR LOGO SECTION -->
        <div class="topbar-logo">
            <div class="container">
                

                <div class="contact-info">
                    <!-- INFO 1 -->
                    <div class="box-icon-1">
                        <div class="icon">
                            <div class="fa fa-envelope-o"></div>
                        </div>
                        <div class="body-content">
                            <div class="heading"> @lang('lang.Email Support')</div>
                             
                            <a href="mailto:info@stc-eg.com" target="_blank">{{setting('email')}}</a>
                        </div>
                    </div>
                    <!-- INFO 2 -->
                    <div class="box-icon-1">
                        <div class="icon">
                            <div class="fa fa-phone"></div>
                        </div>
                        <div class="body-content">
                            <div class="heading">@lang('lang.Call Support')</div>
                           @if(Request::segment('1')=='ar')
                                 <?php $western_arabic = array('0','1','2','3','4','5','6','7','8','9');
                                  $eastern_arabic = array('٠','١','٢','٣','٤','٥','٦','٧','٨','٩');
                                  $str=setting('phone_ar');
                                  $s=str_replace(' (+2) ','', setting('phone_en'));
                                 
                                  $str=str_replace($western_arabic, $eastern_arabic, $str);
                                 ?>

                             <a href="https://api.whatsapp.com/send?phone=002{{$s}}" target="_blank">{{$str}}</a>
                           @else
                             <?php $s=str_replace(' (+2) ','', setting('phone_en')); ?>
                            <a href="https://api.whatsapp.com/send?phone=002{{$s}}" target="_blank">{{setting('phone_en')}}</a>
                           @endif
                        </div>
                    </div>
                    <!-- INFO 3 -->
                    <a href="{{ url(Request::segment('1').'/contact-us')}}" title="" class="btn btn-cta pull-right">@lang('lang.Contact us')</a>

                </div>
            </div>
        </div>

        <!-- NAVBAR SECTION -->
        <div class="navbar navbar-main">
        
            <div class="container container-nav">
                <div class="rowe">
                        
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        
                    </div>
                    @if(Request::segment('1') == "ar")
                    <a class="navbar-brand" href="{{url('/ar/home')}}">
                        <img src="{{ URL('/assets/front/images/logo.png')}}" alt="" />
                    </a>
                    @else
                    <a class="navbar-brand" href="{{url('/')}}">
                        <img src="{{ URL('/assets/front/images/logo.png')}}" alt="" />
                    </a>
                    @endif
                    
                    @if(Request::segment('1') == "ar")
                    <nav class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-left">
                          <?php $urlactive=Request::segment('2');
                            $ur=ucfirst($urlactive);
                            $activeurl= str_replace('-', ' ', $ur) ; ?>
                            @foreach($pages as $page)
                            <?php  $count =App\Page::orderBy('order', 'ASC')->where('sub_of',$page->id)->where('nav','1')->count(); ?>
                            <li class="dropdown">
                              @if($page->name_en == $activeurl)
                              <a href="{{ url('ar/'.$page->slug_en)}}" class="dropdown-toggle activelink"  data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{$page->name_ar}} @if($count != 0 || $page->id==5 || $page->id==12) <span class="caret"></span>@endif </a>
                              @else
                               <a href="{{ url('ar/'.$page->slug_en)}}" class="dropdown-toggle "  data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{$page->name_ar}} @if($count != 0 || $page->id==5 || $page->id==12) <span class="caret"></span>@endif </a>
                              @endif
                              <ul class="dropdown-menu">
                                <?php $sub_page =App\Page::orderBy('order', 'ASC')->where('sub_of',$page->id)->where('nav','1')->get(); ?>
                                 @foreach($sub_page as $sub)
                                 <li><a href="{{ url('ar/'.$sub->slug_en)}}">{{$sub->name_ar}} </a></li>
                                 @endforeach
                                 @if($page->id==5)
                                 <ul class="dropdown-menu bold">
                                   @foreach($types as $type)
                                  
                                   <li class="dropdown"><a href="{{ url('ar/type/'.$type->id)}}">{{$type->name_ar}}<i class="icon-angle-left"></i></a>
                                   <ul style="display: none;" class="dropdown-menu sub-menu-level1 bold">
                                 
                                     @foreach($type->categories as $course)
                                     <?php ?>
                                    <li><a href="{{ url('ar/course/'.$course->id)}}">{{$course->name_ar}}<i class="icon-angle-left"></i></a>

                                      <ul style="display: none;" class="dropdown-menu sub-menu-level2 bold">
                                       @foreach($course->subjects as $sub_sub_course)
                                      <li><a href="{{ url('ar/course_all/'.$sub_sub_course->id)}}">{{$sub_sub_course->name_ar}}</a></li>
                                      @endforeach
                                      </ul>

                                    </li>
                                   @endforeach
                                  </ul>
                                  </li>
                                 @endforeach
                              </ul>
                              @endif
                               @if($page->id==12)
                                <ul class="dropdown-menu">
                                   @foreach($branches as $branch)
                                   <li class="dropdown"><a href="{{ url('ar/branch/'.$branch->id)}}">{{$branch->name_ar}}</a>
                                  
                                  </li>
                                 @endforeach
                              </ul>
                              @endif
                              </ul>

                            </li>
                            @endforeach


                        </ul>

                    </nav>
                    @else
                      <nav class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-left">
                            @foreach($pages as $page)
                            <?php  $count =App\Page::orderBy('order', 'ASC')->where('sub_of',$page->id)->where('nav','1')->count();
                            $urlactive=Request::segment('2');
                            $ur=ucfirst($urlactive);
                            $activeurl= str_replace('-', ' ', $ur) ;?>
                            <li class="dropdown">
                              @if($page->name_en == $activeurl)
                              <a href="{{ url('en/'.$page->slug_en)}}" class="activelink"  data-hover="dropdown"  aria-haspopup="true" aria-expanded="false">{{$page->name_en}} @if($count != 0 || $page->id==5 || $page->id==12) <span class="caret"></span>@endif </a>
                              @else
                               <a href="{{ url('en/'.$page->slug_en)}}"   data-hover="dropdown"  aria-haspopup="true" aria-expanded="false">{{$page->name_en}} @if($count != 0 || $page->id==5 || $page->id==12) <span class="caret"></span>@endif </a>
                              @endif
                              <ul class="dropdown-menu">
                                 <?php $sub_page =App\Page::orderBy('order', 'ASC')->where('sub_of',$page->id)->where('nav','1')->get(); ?>
                                 @foreach($sub_page as $sub)
                                 <li><a href="{{ url('en/'.$sub->slug_en)}}">{{$sub->name_en}}</a></li>
                                 @endforeach
                              </ul>
                              @if($page->id==5)
                                <ul class="dropdown-menu bold">
                                   @foreach($types as $type)
                                  
                                   <li class="dropdown"><a href="{{ url('en/type/'.$type->id)}}">{{$type->name_en}}<i class="icon-angle-right"></i></a>
                                   <ul style="display: none;" class="dropdown-menu sub-menu-level1 bold">
                                 
                                     @foreach($type->categories as $course)
                                     <?php ?>
                                    <li><a href="{{ url('en/course/'.$course->id)}}">{{$course->name_en}}<i class="icon-angle-right"></i></a>

                                      <ul style="display: none;" class="dropdown-menu sub-menu-level2 bold">
                                       @foreach($course->subjects as $sub_sub_course)
                                      <li><a href="{{ url('en/course_all/'.$sub_sub_course->id)}}">{{$sub_sub_course->name_en}}</a></li>
                                      @endforeach
                                      </ul>

                                    </li>
                                   @endforeach
                                  </ul>
                                  </li>
                                 @endforeach
                              </ul>
                              @endif

                               @if($page->id==12)
                                <ul class="dropdown-menu">
                                   @foreach($branches as $branch)
                                   <li class="dropdown"><a href="{{ url('en/branch/'.$branch->id)}}">{{$branch->name_en}}</i></a>
                                  
                                  </li>
                                 @endforeach
                              </ul>
                              @endif

                            </li>
                            @endforeach

                            <!--  <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Language <span class="caret"></span></a>
                              <ul class="dropdown-menu">
                                
                                 <li><a href="{{ url('ar/home')}}"> ar</a></li>
                                 <li><a href="{{ url('en/home')}}"> en </a></li>
                               <li><a href="index-2.html">HOMEPAGE 2</a></li> 
                              </ul>
                            </li> -->
                     

                        </ul>

                      <!--   <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-search"></i></a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <form class="navbar-form navbar-left" role="search">
                                          <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Type and hit enter">
                                          </div>
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        </ul> -->

                    </nav>
                    @endif
                        
                </div>
            </div>
        </div>

    </div>


          
    @yield('content')


      <!-- INFO BOX -->
    <div class="section info overlap-bottom">
        <div class="container">
            <div class="row">
                
                <div class="col-sm-4 col-md-4">
                    <!-- BOX 1 -->
                    <div class="box-icon-4">
                        <div class="icon"><i class="fa fa-phone"></i></div>
                        <div class="body-content">
                            <div class="heading">@lang('lang.CALL US NOW')</div>
                            @if(Request::segment('1') == "ar")
                            <?php $western_arabic = array('0','1','2','3','4','5','6','7','8','9');
                                  $eastern_arabic = array('٠','١','٢','٣','٤','٥','٦','٧','٨','٩');
                                  $str=setting('phone_ar');
                                  $s=str_replace(' (+2) ','', setting('phone_en'));
                                  $s2=str_replace(' (+2) ','', setting('phone2_en'));
                                  $str=str_replace($western_arabic, $eastern_arabic, $str);
                                  $str2=setting('phone2_ar');
                                  $str2=str_replace($western_arabic, $eastern_arabic, $str2);?>
                           <a href="https://api.whatsapp.com/send?phone=002{{$s}}" target="_blank">{{$str}}</a> <br> 
                            <a href="https://api.whatsapp.com/send?phone=002{{$s2}}" target="_blank">{{$str2}}</a>
                            @else
                             <?php  $s=str_replace(' (+2) ','', setting('phone_en'));
                              $s2=str_replace(' (+2) ','', setting('phone2_en')); ?>
                             <a href="https://api.whatsapp.com/send?phone=002{{$s}}" target="_blank">{{setting('phone_en')}}</a> <br>
                             <a href="https://api.whatsapp.com/send?phone=002{{$s2}}" target="_blank">{{setting('phone2_en')}}</a>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4">
                    <!-- BOX 2 -->
                    <div class="box-icon-4">
                        <div class="icon"><i class="fa fa-map-marker"></i></div>
                        <div class="body-content">
                            <div class="heading">@lang('lang.COME VISIT US')</div>
                           @if(Request::segment('1') == "ar")
                           {{setting('address_ar')}}
                           @else
                           {{setting('address_en')}}
                           @endif
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4">
                    <!-- BOX 3 -->
                    <div class="box-icon-4">
                        <div class="icon"><i class="fa fa-envelope"></i></div>
                        <div class="body-content">
                            <div class="heading">@lang('lang.SEND US A MESSAGE')</div>
                           {{setting('email')}}
                        </div>
                    </div>
                </div>
                
            </div>

        </div>
    </div>
     
    <!-- FOOTER SECTION -->
    <div class="footer">
        
        <div class="container">
            
            <div class="row">
                <div class="col-sm-4 col-md-4">
                    <div class="footer-item">
                        <img src="{{ URL('/assets/front/images/logo-light.png')}}" alt="logo bottom" class="logo-bottom">
                        <p>@lang('lang.Desc').</p>
                        <div class="footer-sosmed">
                            <a target="_blank" href="https://www.facebook.com/SolemanSTC" title="">
                                <div class="item">
                                    <i class="fa fa-facebook"></i>
                                </div>
                            </a>
                            <a target="_blank" href="https://www.linkedin.com/in/solemantraining" title="">
                                <div class="item">
                                    <i class="fa fa-linkedin"></i>
                                </div>
                            </a>
                            <!-- <a href="#" title="">
                                <div class="item">
                                    <i class="fa fa-instagram"></i>
                                </div>
                            </a>
                            <a href="#" title="">
                                <div class="item">
                                    <i class="fa fa-pinterest"></i>
                                </div>
                            </a>  -->
                        </div>
                    </div>
                </div>
                <!-- <div class="col-sm-3 col-md-3">
                    <div class="footer-item">
                        <div class="footer-title">
                            Recent Post
                        </div>
                        <ul class="recent-post">
                            <li><a href="#" title="">The Best in dolor sit amet consectetur adipisicing elit sed</a>
                            <span class="date"><i class="fa fa-clock-o"></i> June 16, 2017</span></li><li><a href="#" title="">The Best in dolor sit amet consectetur adipisicing elit sed</a>
                            <span class="date"><i class="fa fa-clock-o"></i> June 16, 2017</span></li>
                        </ul>
                    </div>
                </div> -->
                @if(Request::segment('1') == "ar")
                <div class="col-sm-4 col-md-4">
                    <div class="footer-item">
                        <div class="footer-title">
                           @lang('lang.recent_courses')
                        </div>
                       
                        <ul class="list">
                          @foreach($last_courses as $course )
                            <li><a href="{{ url($lang.'/course'.$course->id)}}" target="_blank" title="">{{$course->name_ar}}</a></li>
                            @endforeach
                           
                        </ul>
                    </div>
                </div>
                @else
                
                <div class="col-sm-4 col-md-4">
                    <div class="footer-item">
                        <div class="footer-title">
                           Recent Courses
                        </div>
                        <ul class="list">
                            @foreach($last_courses as $course )
                            <li><a href="{{ url($lang.'/course/'.$course->id)}}" target="_blank" title="">{{$course->name_en}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                @endif
                @if(Request::segment('1') == "ar")
                <div class="col-sm-4 col-md-4">
                    <div class="footer-item">
                        <div class="footer-title">
                            @lang('lang.Subscribe')
                        </div>
                        <p>@lang('lang.subscribe now')</p>
                          @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form action="{{url('ar/subscribe')}}" class="footer-subscribe" method="post">
                           {{ csrf_field() }}
                          <input type="email" name="email" class="form-control" placeholder="@lang('lang.enter email')">
                         <label for="p_submit"><!-- <i></i> -->
                            <button type="submit" value="send" class="fa fa-envelope subscribe" ></button>
                          </label>
                          <p>@lang('lang.get').</p>
                        </form>
                    </div>
                </div>
                @else
                <div class="col-sm-4 col-md-4">
                    <div class="footer-item">
                        <div class="footer-title">
                            @lang('lang.Subscribe')
                        </div>

                        <p>@lang('lang.subscribe now')</p>
                          @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="{{url('en/subscribe')}}" class="footer-subscribe" method="post">
                           {{ csrf_field() }}
                          <input type="email" name="email" class="form-control" placeholder="@lang('lang.enter email')">
                          
                             
                          <label for="p_submit"><!-- <i></i> -->
                            <button type="submit" value="send" class="fa fa-envelope subscribe" ></button>
                          </label>
                        
                          <p>@lang('lang.get').</p>
                        </form>
                    </div>
                </div>
                @endif
            </div>
        </div>
        
        <div class="fcopy">
            <div class="container">
                <div class="row">
                  @if(Request::segment('1')=='ar')
                    <div class="col-sm-12 col-md-12">
                        <p class="ftex">STC &copy;  2018  مدعوم من  <a href="http://thetailors.net/"  target="_blank" class="link">Thetailors</a>

                         - جميع الحقوق محفوظة </p> 
                    </div>
                    @else
                      <div class="col-sm-12 col-md-12">
                        <p class="ftex">&copy; 2018 STC Powered by <a href="http://thetailors.net/"  target="_blank" class="link">Thetailors</a>

                         - All Rights Reserved</p> 
                      </div>
                    @endif
                </div>
            </div>
        </div>
   
        
    </div>
    
    <!-- JS VENDOR -->
    <script src="//platform-api.sharethis.com/js/sharethis.js#property=5ba535221b076c0011d54112&product=inline-share-buttons"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="{{ asset('js/share.js') }}"></script>
    <script src="{{ URL('/assets/front/v/js/jquery.js')}}"></script>
    <script src="{{ URL('/assets/front/v/js/custom.js')}}"></script>
    <script src="{{ URL('/assets/front/js/custom.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ URL('/assets/front/js/vendor/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL('/assets/front/js/vendor/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL('/assets/front/js/vendor/jquery.superslides.js')}}"></script>
    <script type="text/javascript" src="{{ URL('/assets/front/js/vendor/owl.carousel.js')}}"></script>
    <script type="text/javascript" src="{{ URL('/assets/front/js/vendor/bootstrap-hover-dropdown.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL('/assets/front/js/vendor/jquery.magnific-popup.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL('/assets/front/js/vendor/easings.js')}}"></script>
    <script type="text/javascript" src="{{ URL('/assets/front/js/vendor/isotope.pkgd.min.js')}}"></script>

    <!-- sendmail -->
    <script type="text/javascript" src="{{ URL('/assets/front/js/vendor/validator.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL('/assets/front/js/vendor/form-scripts.js')}}"></script>
    
    <!-- <script type='text/javascript' src='https://maps.google.com/maps/api/js?sensor=false&#038;ver=4.1.5'></script> -->

    <script type="text/javascript" src="{{ URL('/assets/front/js/script.js')}}"></script>

    <script>
     $(function () {
         var lat = 44.88623409320778,
             lng = -87.86480712897173,
             latlng = new google.maps.LatLng(lat, lng),
             image = 'http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png';
         //zoomControl: true,
         //zoomControlOptions: google.maps.ZoomControlStyle.LARGE,
         var mapOptions = {
             center: new google.maps.LatLng(lat, lng),
             zoom: 13,
             mapTypeId: google.maps.MapTypeId.ROADMAP,
             panControl: true,
             panControlOptions: {
                 position: google.maps.ControlPosition.TOP_RIGHT
             },
             zoomControl: true,
             zoomControlOptions: {
                 style: google.maps.ZoomControlStyle.LARGE,
                 position: google.maps.ControlPosition.TOP_left
             }
         },
         map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions),
             marker = new google.maps.Marker({
                 position: latlng,
                 map: map,
                 icon: image
             });
         var input = document.getElementById('searchTextField');
         var autocomplete = new google.maps.places.Autocomplete(input, {
             types: ["geocode"]
         });
         autocomplete.bindTo('bounds', map);
         var infowindow = new google.maps.InfoWindow();
         google.maps.event.addListener(autocomplete, 'place_changed', function (event) {
             infowindow.close();
             var place = autocomplete.getPlace();
             if (place.geometry.viewport) {
                 map.fitBounds(place.geometry.viewport);
             } else {
                 map.setCenter(place.geometry.location);
                 map.setZoom(17);
             }
             moveMarker(place.name, place.geometry.location);
             $('.MapLat').val(place.geometry.location.lat());
             $('.MapLon').val(place.geometry.location.lng());
         });
         google.maps.event.addListener(map, 'click', function (event) {
             $('.MapLat').val(event.latLng.lat());
             $('.MapLon').val(event.latLng.lng());
             infowindow.close();
                     var geocoder = new google.maps.Geocoder();
                     geocoder.geocode({
                         "latLng":event.latLng
                     }, function (results, status) {
                         console.log(results, status);
                         if (status == google.maps.GeocoderStatus.OK) {
                             console.log(results);
                             var lat = results[0].geometry.location.lat(),
                                 lng = results[0].geometry.location.lng(),
                                 placeName = results[0].address_components[0].long_name,
                                 latlng = new google.maps.LatLng(lat, lng);
                             moveMarker(placeName, latlng);
                             $("#searchTextField").val(results[0].formatted_address);
                         }
                     });
         });
        
         function moveMarker(placeName, latlng) {
             marker.setIcon(image);
             marker.setPosition(latlng);
             infowindow.setContent(placeName);
             //infowindow.open(map, marker);
         }
     });
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script type="text/javascript">
    @if (Session::has('success'))
        toastr.success('{{Session::get('success')}}')
    @endif
    @if (Session::has('info'))
        toastr.info('{{Session::get('info')}}')
    @endif
    @if (Session::has('danger'))
        toastr.error('{{Session::get('danger')}}')
    @endif

    @if (Session::has('Reservation'))
        toastr.success('{{Session::get('Reservation')}}')
    @endif
</script>
    

        
</body>
</html>