<?php
// use Analytics;
use Spatie\Analytics\Period;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
| 
*/
Route::get('/', function () {
    return view('home');
    })->middleware('SharedVariables');
Route::get('/welcome','HomeController@welcome_admin');
Route::get('/welcome_error','HomeController@welcome_error');



Auth::routes();


if ((Request::segment('1') != 'admin') && (Request::segment('1') == 'en')||(Request::segment('1') == 'ar')) {
	Route::group(['prefix'=>'{lang?}','middleware'=>['lang','SharedVariables']],function()
   {
		Route::get('home', 'HomeController@index');
		Route::get('course/{id}', 'FrontController@course');
		Route::get('type/{id}', 'FrontController@type');
		Route::get('branch/{id}', 'FrontController@branch');
		Route::get('news/{id}', 'FrontController@news');
		Route::get('events/{id}', 'FrontController@events');
		Route::get('course_schedule/{id}', 'FrontController@course_schedule');
		Route::get('course_all/{id}', 'FrontController@course_all');
		Route::get('course_reservation/{id}/{course_id}', 'FrontController@course_reservation')->middleware('auth');
		Route::post('confirm_reservation/{id}/{course_id}', 'FrontController@confirm_reservation')->middleware('auth');

		Route::post('confirm__private_reservation/{id}/{course_id}', 'FrontController@confirm__private_reservation')->middleware('auth');
		Route::post('confirm__company_reservation/{id}/{course_id}', 'FrontController@confirm__company_reservation')->middleware('auth');
	 
		Route::get('register_page', 'Auth\RegisterController@register');
	    Route::post('register_success', 'Auth\RegisterController@create_new');
		Route::get('login_page', 'Auth\LoginController@login_page');
		Route::post('login_success', 'Auth\LoginController@check_login');
		Route::get('view_profile', 'UserController@view_profile')->middleware('auth');
		Route::patch('edit_profile/{id}', 'UserController@edit_profile')->middleware('auth');
		Route::get('update_password', 'UserController@update_password')->middleware('auth');
		Route::patch('edit_password/{id}', 'UserController@edit_password')->middleware('auth');
		Route::get('view_course_users', 'UserController@view_user_course')->middleware('auth');
		Route::get('course_dates','UserController@course_dates');
	    Route::get('course_matirial','UserController@course_matirial');
	    Route::get('course_feedback','UserController@course_feedback');
	    Route::post('feedback_course_store', 'UserController@feedback_course_store');
		Route::get('download/{id}', 'UserController@getDownload');
		Route::get('view_kill_sheet', 'UserController@view_kill_sheet');
		Route::get('add_kill_sheet', 'UserController@add_kill_sheet');
		Route::get('applay', 'UserController@applay');
		Route::post('applay_store', 'UserController@applay_store');
		Route::post('store_kill_sheet', 'UserController@store_kill_sheet');
		Route::get('del/{id}', 'UserController@del');
		Route::get('edit_kill_sheet', 'UserController@edit_kill_sheet');
		Route::get('date/{id}/{schedule}', 'UserController@date');
		Route::get('kill_can/', 'UserController@kill_can');
		Route::post('kill_can_store/', 'UserController@kill_can_store');
		Route::get('view_kill_can', 'UserController@view_kill_can');
		Route::get('user_rate', 'UserController@user_rate');
		Route::post('contact_us', 'FrontController@contact');
		Route::get('content', 'FrontController@content');
		Route::get('more', 'FrontController@more');
		Route::post('subscribe', 'FrontController@subscribe');
		Route::get('subscribe_course', 'FrontController@subscribe_course');
		Route::post('subscribe_store', 'FrontController@subscribe_store');
		Route::get('course_branch', 'FrontController@get_branch');
		Route::get('not_apply', 'FrontController@not_apply');
		Route::get('instructor_candidate_attendances', 'UserController@instructor_candidate_attendances'); 
		Route::post('store_instructor_candidate_attendances', 'UserController@store_instructor_candidate_attendances');
		Route::get('check_right/', 'UserController@check_right');
		Route::get('check_false/', 'UserController@check_false');
		Route::get('{page}', 'FrontController@page');


   });
	
}
elseif(Request::segment('1') == 'admin')
{
	Route::get('admin/Home/view_page_popup','ViewpopupController@Viewpop')->middleware('AuthAdmin')->middleware('IsAdmin');
	Route::get('admin/user_category','AccountingController@user_category')->middleware('AuthAdmin')->middleware('IsAdmin');
	Route::group(['prefix'=>'admin','middleware'=>['AuthAdmin','IsAdmin','Permiation']],function()
    {
		Route::get('/','HomeController@admin');
		
		Route::get('/hotels/delete','HotelController@delete');
		Route::resource('/hotels','HotelController');
		Route::get('hotels/trash/{id}/{trash}','HotelController@trash');
		Route::get('/restaurants/delete','RestaurantController@delete');
		Route::resource('/restaurants','RestaurantController');
		Route::get('restaurants/trash/{id}/{trash}','RestaurantController@trash');
		Route::get('/users/delete','UserController@delete');
		Route::resource('/users','UserController');
		Route::get('users/trash/{id}/{trash}','UserController@trash');
		Route::get('users/exams/{id}','UserController@user_exams');

		Route::get('/subject_categories/delete','SubjectCategoryController@delete');
		Route::resource('/subject_categories','SubjectCategoryController');
		Route::get('subject_categories/trash/{id}/{trash}','SubjectCategoryController@trash');

		Route::get('/subjects/delete','SubjectController@delete');
		Route::resource('/subjects','SubjectController');
		Route::get('subjects/trash/{id}/{trash}','SubjectController@trash');

		Route::get('/instructors/delete','InstructorController@delete');
		Route::resource('/instructors','InstructorController');
		Route::get('instructors/trash/{id}/{trash}','InstructorController@trash');
		Route::get('instructors/courses/{id}/{type}','InstructorController@instructor_course');
		Route::get('course/subscribe_course','CoursesController@subscribe_course');
		Route::post('/course/subscrib_course_save','CoursesController@subscrib_course_save');

		Route::get('course/subscribe_all','CoursesController@subscribe_all');
		Route::post('/course/subscrib_all_save','CoursesController@subscrib_all_save');
		Route::get('subscribe_all_web','SubscriberController@subscribe_all_web');
		Route::post('/subscrib_all_save','SubscriberController@subscrib_all_save');
		
		Route::get('/course/delete','CoursesController@delete');
		Route::resource('/course','CoursesController');
		Route::get('course/trash/{id}/{trash}','CoursesController@trash');
		Route::get('/course/schedule_add/{id}','CoursesController@schedule_add');
		Route::post('/course/schedule_save/{id}','CoursesController@schedule_save');
		Route::get('/course/schedule_view/{id}','CoursesController@schedule_view');
		Route::get('/course/schedule_edit/{id}','CoursesController@schedule_edit');
	    Route::PATCH('/course/schedule_update/{id}','CoursesController@schedule_update');
	    Route::get('course/subscribe/{id}','CoursesController@subscribe');
	    Route::get('/course/date_attendance/{id}/{date}','CoursesController@date_attendance');


		Route::get('/exam/delete','ExamController@delete');
		Route::resource('/exam','ExamController');
		Route::get('exam/trash/{id}/{trash}','ExamController@trash');
		Route::get('/feedback/{type}','FeedbackController@index');
		Route::get('/pages/delete','PagesController@delete');
		Route::resource('/pages','PagesController');
		Route::get('pages/trash/{id}/{trash}','PagesController@trash');
		Route::resource('/news','NewsController');
		Route::resource('/events','EventsController');

		Route::get('/gallery/delete','GalleryController@delete');
		Route::resource('/gallery','GalleryController');
		Route::get('gallery/trash/{id}/{trash}','GalleryController@trash');
		Route::get('gallery/add/{id}/{table}','GalleryController@add');
		Route::post('gallery/add_store/{id}/{table}','GalleryController@add_store');

		Route::get('/accordion/delete','AccordionController@delete');
		Route::resource('/accordion','AccordionController');
		Route::get('accordion/trash/{id}/{trash}','AccordionController@trash');
		Route::get('accordion/create/{id}','AccordionController@create');
		Route::post('accordion/store/{id}','AccordionController@store');

		Route::get('/company_account/delete','CompanyAccountController@delete');
		Route::resource('/company_account','CompanyAccountController');
		Route::get('company_account/trash/{id}/{trash}','CompanyAccountController@trash');

		Route::get('/reservations/delete','ReservationController@delete');
		Route::resource('/reservations','ReservationController');
		Route::get('reservations/trash/{id}/{trash}','ReservationController@trash');
		Route::get('reservations/status/{id}/{trash}','ReservationController@status');
		
		


	    Route::get('private_reservations/search','PrivateController@search');
	    Route::get('private_reservations/course','PrivateController@course');
		Route::get('/private_reservations/delete','PrivateController@delete');
		Route::resource('/private_reservations','PrivateController');
		Route::get('private_reservations/trash/{id}/{trash}','PrivateController@trash');
		

		Route::get('company_reservations/company_search','CompanyreservationController@company_search');
		Route::get('/company_reservations/delete','CompanyreservationController@delete');
		Route::resource('/company_reservations','CompanyreservationController');
		Route::get('company_reservations/trash/{id}/{trash}','CompanyreservationController@trash');
		Route::get('company_reservations/status/{id}/{trash}','CompanyreservationController@status');
		Route::get('company_reservations/users/{id}','CompanyreservationController@users');
		Route::get('company_reservations/users_add/{id}','CompanyreservationController@user_add');
		Route::post('company_reservations/users_save/{id}','CompanyreservationController@user_save');
		Route::get('company_reservations/schedule_add/{id}/{course_id}','CompanyreservationController@schedule_add');
		Route::post('company_reservations/schedule_save/{id}/{course_id}','CompanyreservationController@schedule_save');
		

		Route::get('/employee_salary/delete','EmployeeController@delete');
		Route::resource('/employee_salary','EmployeeController');
		Route::get('employee_salary/trash/{id}/{trash}','EmployeeController@trash');

		Route::get('/financial/delete','FinancialController@delete');
		Route::resource('/financial','FinancialController');
		Route::get('financial/trash/{id}/{trash}','FinancialController@trash');
		Route::get('financial/status/{id}/{status}','FinancialController@status');

		Route::get('/subject_content/delete','Subject_contentController@delete');
		Route::get('/subject_content/{id}','Subject_contentController@index');
		Route::get('/subject_content/add/{id}','Subject_contentController@create');
		Route::post('/subject_content/store/{id}','Subject_contentController@store');
		Route::resource('/subject_content','Subject_contentController');
		Route::get('subject_content/trash/{id}/{trash}','Subject_contentController@trash');

		Route::get('/matirial/delete','MatirialController@delete');
		Route::get('/matirial/{id}','MatirialController@index');
		Route::get('/matirial/add/{id}','MatirialController@create');
		Route::post('/matirial/store/{id}','MatirialController@store');
		Route::resource('/matirial','MatirialController');
		Route::get('matirial/trash/{id}/{trash}','MatirialController@trash');


		Route::get('/careers/delete','CareerController@delete');
		Route::resource('/careers','CareerController');
		Route::get('careers/trash/{id}/{trash}','CareerController@trash');
		Route::get('careers/apply/{id}','CareerController@apply');
		Route::get('careers/download/{id}', 'CareerController@getDownload'); 
		Route::get('careers/approve/{id}', 'CareerController@approve'); 
		Route::get('careers/reject/{id}', 'CareerController@reject'); 

		Route::get('/branches/delete','BranchController@delete');
		Route::resource('/branches','BranchController');
		Route::get('branches/trash/{id}/{trash}','BranchController@trash');

		Route::get('/sections/delete','SectionController@delete');
		Route::resource('/sections','SectionController');
		Route::get('sections/trash/{id}/{trash}','SectionController@trash');

		Route::get('/candidate_attendances/delete','Candidate_attendanceController@delete');
		Route::resource('/candidate_attendances','Candidate_attendanceController');
		Route::get('candidate_attendances/trash/{id}/{trash}','Candidate_attendanceController@trash');

		Route::get('/permissions_role/delete','PermissionRoleController@delete');
		Route::get('/roles/confirm','PermissionRoleController@confirm');
		Route::resource('/permissions_role','PermissionRoleController');
		Route::get('permissions_role/trash/{id}/{trash}','PermissionRoleController@trash');

		Route::get('/roles/delete','RoleController@delete');
		Route::resource('/roles','RoleController');
		Route::get('roles/trash/{id}/{trash}','RoleController@trash');

		Route::get('/permissions/delete','PermissionController@delete');
		Route::resource('/permissions','PermissionController');
		Route::get('permissions/trash/{id}/{trash}','PermissionController@trash');


		Route::get('/meta_tags/delete','MetaTagController@delete');
		Route::resource('/meta_tags','MetaTagController');
		Route::get('meta_tags/trash/{id}/{trash}','MetaTagController@trash');

		Route::resource('subscribes','SubscriberController');

		Route::get('/question_type/delete','QuestionTypeController@delete');
		Route::resource('/question_type','QuestionTypeController');
		Route::get('/questions/delete','QuestionController@delete');
		Route::resource('/questions','QuestionController');

		Route::get('/answers/delete','AnswerController@delete');
		Route::resource('/answers','AnswerController');
		Route::get('/answers/view/{id}','AnswerController@index');
		Route::get('/answers/create/{id}','AnswerController@create');
		Route::post('/answers/save/{id}','AnswerController@store');

		Route::get('/types/delete','TypeCategoryController@delete');
		Route::resource('/types','TypeCategoryController');
		Route::get('types/trash/{id}/{trash}','TypeCategoryController@trash');

		Route::get('/accounting/delete','AccountingController@delete');
		Route::resource('/accounting','AccountingController');
		Route::get('accounting/trash/{id}/{trash}','AccountingController@trash');

		Route::get('/category_accounting/delete','AccountingCategoryController@delete');
		Route::resource('/category_accounting','AccountingCategoryController');
		Route::get('category_accounting/trash/{id}/{trash}','AccountingCategoryController@trash');

		Route::get('/repeatly_accounting/delete','RepeatlyAccountingController@delete');
		Route::resource('/repeatly_accounting','RepeatlyAccountingController');
		Route::get('repeatly_accounting/trash/{id}/{trash}','RepeatlyAccountingController@trash');

		Route::get('/lables/delete','LabelController@delete');
		Route::resource('/lables','LabelController');
		Route::get('lables/trash/{id}/{trash}','LabelController@trash');

		Route::get('/logo/delete','LogoController@delete');
		Route::resource('/logo','LogoController');
		Route::get('logo/trash/{id}/{trash}','LogoController@trash');

		Route::resource('/activites','ActivitesController');


	

    });
}







Route::get('/data', function ()
{
	$analyticsData = Analytics::fetchVisitorsAndPageViews(Period::days(7));
	dd($analyticsData);
   
 });

Route::get('/shar', function ()
{
	Share::currentPage()
	->facebook()
	->twitter()
	->googlePlus()
	->linkedin('Extra linkedin summary can be passed here');
   
 });

Route::get('/read', function (){
	auth()->user()->unreadNotifications->markAsRead();
	return redirect()->back();
    });


 Route::get('switchTo={lang}', [
    'as'   => 'ChangeLang',
    'uses' => 'UserController@switchLang',
]);


 Route::get('/test', function () {
    return view('test');
    })->middleware('SharedVariables');


